#ifndef __TESTALLOC_H
#define __TESTALLOC_H

#ifdef __cplusplus
extern "C" {
#endif
    
extern const char *expected;
ITest *get_cppobj();
void free_cppobj(ITest *t);

#if C_OBJ_IMPLEMENTED_YET
ITest *get_cobj();
void free_cobj(ITest *t);
#endif
    
#ifdef __cplusplus
};
#endif

#endif // __TESTALLOC_H
