#include "masterdispatch.h"
#include "IObject.h"
#include "IMulti.hpp"

static AnyType wrap_getInt(AnyType &self, AnyType parms[])
{
    IInt *obj = self;
    assert(obj);
    
    printf("wrap_getInt(%p)\n", obj);
    return AnyType(obj->getInt());
}


static AnyType wrap_setInt(AnyType &self, AnyType parms[])
{
    IInt *obj = self;
    assert(obj);
    int val = parms[0].str.num();
    
    printf("wrap_setInt(%p, %d)\n", obj, val);
    obj->setInt(val);
    return AnyType();
}


CallEntry mycalls[] = {
    {"getInt", wrap_getInt, 0, NULL},
    {"setInt", wrap_setInt, 1, &UUID_null},
};


IfcInfo intinfo("IInt", IInt_IID,
		mycalls, sizeof(mycalls)/sizeof(mycalls[0]));


