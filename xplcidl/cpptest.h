#ifndef __CPPTEST_H
#define __CPPTEST_H

#include <xplc/uuid.h>

#ifndef UNSTABLE
/**
 * Used to mark an interface as unstable.  Add an UNSTABLE_INTERFACE
 * declaration to your object if you don't want people to use your
 * interface without knowing the interface might change from
 * underneath them.  They will then have to \#define UNSTABLE if they
 * want their program to compile.
 */
#define UNSTABLE_INTERFACE static bool UNSTABLE_INTERFACE = true;
#else
#define UNSTABLE_INTERFACE
#endif

class IWeakRef;
class IObject;
#include "test.hpp"
#include "testalloc.h"

#endif // __CPPTEST_H
