#include "uniscript.h"
#include "masterdispatch.h"
#include "wvmoniker.h"
#include "wvtclstring.h"
#include "wvstringlist.h" 
#include "anytype.h"
#include "wvhashtable.h"


DeclareWvDict(AnyType, WvString, str);
static AnyTypeDict floaters(100);


static void register_obj(AnyType &ret)
{
    if (ret.raw)
    {
	// save return value for later access, if it's an object
	ret.str = WvString("obj:%s", (int)ret.raw);
	if (!floaters[ret.str])
	    floaters.add(new AnyType(ret), true);
    }
}


const char *unicreate(const char *moniker)
{
    static WvString empty;
    
    IObject *obj = wvcreate<IObject>(moniker, NULL, NULL);
    if (obj) 
    {
	AnyType ret(obj, IObject_IID, obj);
	register_obj(ret);
	return ret.str;
    }
    else
	return empty;
}


const char *unicall(const char *selfid, const char *parms)
{
    AnyType *self = floaters[selfid];
    assert(self);
    
    WvStringList list;
    wvtcl_decode(list, parms);
    WvString fname = list.popstr();
    
    AnyType aparms[list.count()];
    int count = 0;
    WvStringList::Iter i(list);
    for (i.rewind(), count = 0; i.next(); count++)
    {
	AnyType *x = floaters[*i];
	if (x)
	    aparms[count] = *x;
	else
	    aparms[count] = AnyType(*i);
    }
    
    assert(master);
    assert(self);
    assert(self->raw);
    assert(fname);
    assert(fname[0]);
    AnyType ret = master->call(*self, fname, list.count(), aparms);
    register_obj(ret);
    return ret.str;
}


