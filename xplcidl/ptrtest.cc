#include "xplc/ptr.h"
#include "xplc/utils.h"
#include <stdio.h>
#include <assert.h>

static int nobjs;

class MyObject : public IObject
{
    IMPLEMENT_IOBJECT(MyObject);
public:
    MyObject() { nobjs++; }
    virtual ~MyObject() { nobjs--; }
};


UUID_MAP_BEGIN(MyObject)
    UUID_MAP_ENTRY(IObject)
UUID_MAP_END;


int main()
{
    {
	IObject *a = new MyObject;
	assert(nobjs == 1);
	a->release();
	assert(!nobjs);
    }
    
    {
	xplc_ptr<IObject> a(new MyObject);
	assert(nobjs == 1);
    }
    assert(!nobjs);
    
    {
	xplc_ptr<IObject> a(new MyObject);
	assert(nobjs == 1);
	a = 0;
	assert(nobjs == 0);
    }
    
    {
	xplc_ptr<IObject> a(new MyObject);
	IObject *stupid = a->getInterface(IObject_IID);
	a = 0;
	assert(nobjs == 1);
	stupid->release();
	assert(!nobjs);
    }
    
#if 1
    {
	xplc_ptr<IObject> a(new MyObject);
	IObject *stupid = a->getInterface(IObject_IID);
	a = 0;
	assert(nobjs == 1);
	a = stupid;
	assert(nobjs == 1);
    }
    assert(!nobjs);          // assertion fails
#endif

#if 1
    {
	xplc_ptr<IObject> a(new MyObject);
	xplc_ptr<IObject> b;
	b = a;               // doesn't compile: "operator= is private"
	assert(nobjs == 1);
    } 
    assert(!nobjs);
#endif
    
#if 1
    {
	xplc_ptr<IObject> a(new MyObject);
	xplc_ptr<IObject> b(a);
	assert(nobjs == 1);
    }                        // crashes
    assert(!nobjs);
#endif
    
    printf("all tests passed.\n");
    return 0;
}
