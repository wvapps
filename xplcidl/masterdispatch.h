#ifndef __MASTERDISPATCH_H
#define __MASTERDISPATCH_H

#include "anytype.h"
#include "wvhashtable.h"

typedef AnyType (GenFunc)(AnyType &self, AnyType parms[]);

struct CallEntry {
    const char *name;
    GenFunc *func;
    int argc;
    const UUID *argtypes;
};


class IfcInfo
{
public:
    IfcInfo(WvStringParm _iname, const UUID &_iid,
	    CallEntry *_calls, int _ncalls);
    ~IfcInfo();
    
    WvString iname;
    const UUID &iid;
    
    CallEntry *calls;
    int ncalls;
};


static inline int WvHash(const UUID &uuid)
{
    // it's random!  any four bytes are as good as any others.
    return WvHash(*(int *)&uuid); 
}

DeclareWvDict(IfcInfo, UUID, iid);

class MasterDispatch
{
public:
    IfcInfoDict ifcs;
    
    MasterDispatch()
	: ifcs(10)
	{ }
    ~MasterDispatch()
        { /* nothing special */ }
    
    void add(IfcInfo *info)
        { ifcs.add(info, false); }
    void remove(IfcInfo *info)
        { ifcs.remove(info); }
    
    CallEntry *find(AnyType &self, WvStringParm fname, int argc,
		    AnyType parms[])
    {
	// FIXME: this is stupid.  We have a hash table of interfaces that
	// we iterate through, but we should have a hash table of function
	// names with a list of CallEntry for each name.  That would be
	// much faster, but this is easier, so it'll do for now.
	IfcInfoDict::Iter ifc(ifcs);
	for (ifc.rewind(); ifc.next(); )
	{
	    //printf("dispatch: checking interface '%s'\n", ifc->iname.cstr());
	    bool ret = self.switchto(ifc->iid);
	    //printf("   switch: %p -> %d\n", (IObject *)self.obj, ret);
	    if (!ret) continue; // incompatible object
	    
	    for (int i = 0; i < ifc->ncalls; i++)
	    {
		CallEntry &ent = ifc->calls[i];
		//printf("  dispatch: checking func '%s'\n", ent.name);
		
		if (fname != ent.name) continue;
		if (ent.argc != argc) continue;
		assert(!argc || ent.argtypes);
		
		bool ok = true;
		for (int arg = 0; arg < argc; arg++)
		{
		    if (!parms[arg].switchto(ent.argtypes[arg]))
		    {
			ok = false;
			break;
		    }
		}
		if (!ok) continue;
		
		// if we get here, we found a match!
		// printf("found entry '%s' (%p)\n", ent.name, &ent);
		return &ent;
	    }
	}
	
	// whoops, no match!
	return NULL;
    }
    
    AnyType call(AnyType &self, WvStringParm fname, int argc, AnyType parms[])
    {
	CallEntry *ent = find(self, fname, argc, parms);
	assert(ent);
	return ent->func(self, parms);
    }
};


extern MasterDispatch *master;


#endif // __MASTERDISPATCH_H
