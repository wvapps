#ifndef __FAKEUUID_H
#define __FAKEUUID_H

// XPLC's uuid.h doesn't work in standard C, so we need this.
 
typedef struct _GUID {
  unsigned long Data1;
  unsigned short Data2;
  unsigned short Data3;
  unsigned char Data4[8];
} GUID;

typedef GUID UUID;

#define DEFINE_IID(iface, u1, u2, u3, u4, u5, u6, u7, u8, u9, u10, u11) \
static const UUID iface##_IID = u1, u2, u3, u4, u5, u6, u7, u8, u9, u10, u11

#endif // __FAKEUUID_H
