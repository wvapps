#ifndef __CTEST_H
#define __CTEST_H

#include "fakeuuid.h"

typedef struct IWeakRef IWeakRef;
typedef struct IObject IObject;

#define bool int

#include "test.h"
#include "testalloc.h"

#endif // __CTEST_H
