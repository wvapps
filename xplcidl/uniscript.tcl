#!/usr/bin/env tclsh
load ./uniscript_tcl.so
set x [unicreate "mymulti:hello world!"]
puts "create result: $x"

puts [unicall $x getString]
puts [unicall $x getBool]
puts [unicall $x "setInt 555"]
puts [unicall $x getInt]
