#include "masterdispatch.h"
#include <stdio.h>

MasterDispatch *master;


IfcInfo::IfcInfo(WvStringParm _iname, const UUID &_iid,
		 CallEntry *_calls, int _ncalls)
    : iname(_iname), iid(_iid)
{ 
    calls = _calls;
    ncalls = _ncalls;
    
    printf("Creating master...\n");
    if (!master) master = new MasterDispatch;
    master->add(this);
}


IfcInfo::~IfcInfo()
{
    assert(master);
    master->remove(this);
}


