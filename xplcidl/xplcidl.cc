#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <libIDL/IDL.h>
#include "wvstream.h"
#include "wvcont.h"
#include "wvpipe.h"
#include "wvhashtable.h"
#include <xplc/uuidops.h>

class MyTfd;
class MyParser;
class MyInterface;
class MyFunc;
class MyParm;
class MyType;


class MyTfd
{
public:
    IDL_ns ns;
    IDL_tree_func_data *tfd;
    
    MyTfd(IDL_ns _ns, IDL_tree_func_data *_tfd)
    {
	assert(_tfd);
	assert(WvCont::isok());
	ns = _ns;
	tfd = _tfd;
    }
    
    operator IDL_tree_func_data* () const
        { return tfd; }
    
    IDL_tree tree() const
        { return tfd->tree; }
    
    IDL_tree_func_data *cur()
        { return tfd; }
    
    IDL_tree_func_data *next(bool recurse)
    {
	if (WvCont::isok())
	{
	    tfd = (IDL_tree_func_data *)WvCont::yield((void *)recurse);
	    if (tfd)
	    {
		wverr->print(WvString("%%%ss%%s", depth()*4),
			     "", IDL_NODE_TYPE_NAME(tfd->tree));
		WvString s(ident());
		if (!!s)
		    wverr->print(" (%s)\n", s);
		else
		    wverr->print("\n");
 	    }
	    return tfd;
	}
	else
	    return NULL;
    }
    
    int type() const
    {
	return IDL_NODE_TYPE(tfd->tree);
    }
    
    int depth() const
    {
	if (!tfd) return 0;
	
	int count = -1;
	for (IDL_tree cur = tfd->tree; cur; cur = IDL_NODE_UP(cur))
	    count++;
	return count;
    }
    
    WvString ident() const
    {
	if (!tfd)
	    return WvString();
	else switch (type())
	{
	case IDLN_IDENT:
	    return IDL_IDENT(tfd->tree).str;
	case IDLN_INTERFACE:
	    return IDL_IDENT(IDL_INTERFACE(tfd->tree).ident).str;
	case IDLN_OP_DCL:
	    return IDL_IDENT(IDL_OP_DCL(tfd->tree).ident).str;
	default:
	    return WvString();
	}
    }
    
    bool inhibited() const
    {
	return (IDL_NODE_DECLSPEC(tree()) & IDLF_DECLSPEC_INHIBIT);
    }
};


class MyType
{
public:
    int type;
    WvString name;
    
    MyType() 
        { type = IDLN_NONE; }
    
    MyType(IDL_tree node)
    {
	type = IDL_NODE_TYPE(node);
	if (type == IDLN_IDENT)
	    name = IDL_IDENT(node).str;
	else
	    name = IDL_NODE_TYPE_NAME(node);
    }
    
    WvString cpp_typemap() const
    {
	switch (type)
	{
	case IDLN_TYPE_BOOLEAN:
	    return "bool ";
	case IDLN_TYPE_INTEGER:
	    return "int ";
	case IDLN_TYPE_STRING:
	    // FIXME: (const char *) is a lousy string type
	    return "const char *";
	default:
	    return WvString("%s *", name);
	}
    }
    
    WvString _c_style(bool isout, bool isret, char _sep) const
    {
	char sep[2] = {_sep, 0};
	
	if (!name)
	    return "void ";
	else if (type == IDLN_IDENT)
	{
	    if (isout)
		return WvString("%s *%s", name, sep);
	    else if (isret)
		return WvString("%s *", name);
	    else
		return WvString("const %s %s", name, sep);
	}
	else
	{
	    if (isout)
		return WvString("%s%s", cpp_typemap(), sep);
	    else
		return cpp_typemap();
	}
    }
};


class MyParm
{
public:
    WvString name;
    MyType type;
    int inout;
    
    MyParm()
	{ inout = 0; }
    
    WvString _c_style(char sep)
    {
	bool isout = (inout != IDL_PARAM_IN);
	return WvString("%s%s",
			type._c_style(isout, false, sep),
			name);
    }
    
    WvString cpp_style()
    {
	return _c_style('&');
    }
    
    WvString c_style()
    {
	return _c_style('*');
    }
    
    WvString dispatch_style_iid()
    {
	if (type.type == IDLN_IDENT)
	    return WvString("%s_IID", type.name);
	else
	    return "UUID_null";
    }
    
    WvString dispatch_style_call(WvStringParm argname)
    {
	switch (type.type)
	{
	case IDLN_TYPE_STRING:
	    return WvString("%s.str", argname);
	case IDLN_TYPE_INTEGER:
	case IDLN_TYPE_BOOLEAN:
	    return WvString("%s.str.num()", argname);
	case IDLN_IDENT:
	default:
	    return WvString("*%s.get<%s>()", argname, type.name);
	}
    }
};
DeclareWvList(MyParm);


class MyFunc
{
public:
    WvString name;
    MyType ret_type;
    MyParmList parms;
    
    MyFunc(WvStringParm _name)
	: name(_name)
        { }
    
    WvString cpp_style()
    {
	WvStringList list;
	MyParmList::Iter parm(parms);
	for (parm.rewind(); parm.next(); )
	    list.append(WvString(parm->cpp_style()));
	
	return WvString("virtual %s%s(%s) = 0",
			ret_type._c_style(false, true, '&'),
			name, list.join(", "));
    }
    
    WvString c_style(WvStringParm ifcname)
    {
	WvStringList list;
	list.append(WvString("%s *self", ifcname));
	
	MyParmList::Iter parm(parms);
	for (parm.rewind(); parm.next(); )
	    list.append(WvString(parm->c_style()));
	
	return WvString("%s(*%s)(%s)",
			ret_type._c_style(false, true, '*'),
			name, list.join(", "));
    }
    
    WvString dispatch_style_args()
    {
	WvStringList list;
	MyParmList::Iter parm(parms);
	for (parm.rewind(); parm.next(); )
	    list.append(WvString(parm->dispatch_style_iid()));
	if (list.isempty())
	    return "";
	else
	    return WvString("\n    %s\n", list.join(", "));
    }
    
    WvString dispatch_style_func(WvStringParm iname, WvStringParm fname)
    {
	WvString retcall, retgrab;
	
	WvStringList list;
	MyParmList::Iter parm(parms);
	int count;
	for (count = 0, parm.rewind(); parm.next(); count++)
	    list.append(parm->dispatch_style_call(
				  WvString("parms[%s]", count)));
	
	WvString funcall("self->%s(%s)", name, list.join(", "));
	
	if (ret_type.type == IDLN_NONE)
	{
	    retgrab = "";
	    retcall = "AnyType()";
	}
	else if (ret_type.type == IDLN_IDENT)
	{
	    retgrab = WvString("%sx = ", ret_type._c_style(false, true, '&'));
	    retcall = WvString("AnyType(x, %s_IID, x)", ret_type.name);
	}
	else // a basic type
	{
	    retgrab = WvString("%sx = ", ret_type._c_style(false, true, '&'));
	    retcall = "AnyType(x)";
	}
	
	return WvString("static AnyType %s(AnyType &_self, AnyType parms[])\n"
			"{\n"
			"    printf(\"called %s(%%p, ...)\\n\", _self.raw);\n"
			"    %s *self = _self;\n"
			"    assert(self);\n"
			"    %s%s;\n"
			"    return %s;\n"
			"}\n",
			fname, fname, 
			iname,
			retgrab, funcall,
			retcall);
    }
};
DeclareWvList(MyFunc);


class MyInterface
{
public:
    bool inhibited;
    MyParser &parser;
    WvString name;
    WvStringList inherits;
    UUID iid;
    bool unstable;
    MyFuncList funcs;
    
    MyInterface(bool _inhibited,
		MyParser *_parser, WvStringParm _name, const UUID &_iid)
	: parser(*_parser), name(_name), iid(_iid)
        { inhibited = _inhibited; unstable = false; }
    
    WvString cpp_style_funcs()
    {
	WvStringList list;
	MyFuncList::Iter func(funcs);
	for (func.rewind(); func.next(); )
	    list.append(WvString("    %s;\n", func->cpp_style()));
	
	return list.join("");
    }
    
    WvString cpp_style()
    {
	WvString uns(unstable ? "    UNSTABLE_INTERFACE\n" : "");
	WvString inh("");
	if (!inherits.isempty())
	    inh = WvString(" : public %s", inherits.join(", public "));
	
	char _s[128], *s = _s;
	UuidToString(iid, _s);
	if (s[0] == '{')
	    s++;
	
	WvString iiddecl("DEFINE_IID(%s, {0x%.8s, 0x%.4s, 0x%.4s,\n"
			 "    {0x%.2s, 0x%.2s, 0x%.2s, 0x%.2s, "
			 "0x%.2s, 0x%.2s, 0x%.2s, 0x%.2s}});",
			 name,
			 s+0, s+9, s+14,
			 s+19, s+21, s+24, s+26,
			 s+28, s+30, s+32, s+34);
	
	return WvString("#ifndef _Defined_%s_\n"
			"#define _Defined_%s_\n"
			"class %s%s\n"
			"{\n"
			"%s"
			"public:\n"
			"%s"
			"};\n"
			"%s\n"
			"#endif // _Defined_%s_\n",
			name, name,
			name, inh,
			uns,
			cpp_style_funcs(),
			iiddecl,
			name);
    }
    
    WvString c_style_funcs(WvStringParm ifcname);
    
    WvString c_wrappers(WvStringParm ifcname)
    {
	WvStringList list;
	MyFuncList::Iter func(funcs);
	for (func.rewind(); func.next(); )
	{
	    WvStringList ilist, olist;
	    ilist.append(WvString("%s *_self", ifcname));
	    olist.append(WvString("_self"));
	    
	    MyParmList::Iter parm(func->parms);
	    for (parm.rewind(); parm.next(); )
	    {
		ilist.append(WvString(parm->c_style()));
		olist.append(&parm->name, false);
	    }
	    
	    list.append(WvString("static %s%s_%s(%s)\n"
				 "    { %s(*_self)->%s(%s); }",
				 func->ret_type._c_style(false, true, '*'),
				 ifcname, func->name,
				 ilist.join(", "), 
				 !!func->ret_type.name ? "return " : "",
				 func->name, olist.join(", ")));
	}
	
	return list.join("\n");
    }

    WvString c_style(WvStringParm ifcname)
    {
	WvString uns(unstable ? "    UNSTABLE_INTERFACE\n" : "");
	WvString inh("");
	if (!inherits.isempty())
	    inh = WvString(" : public %s", inherits.join(", public "));
	
	char _s[128], *s = _s;
	UuidToString(iid, _s);
	if (s[0] == '{')
	    s++;
	
	WvString iiddecl("DEFINE_IID(%s, {0x%.8s, 0x%.4s, 0x%.4s,\n"
			 "    {0x%.2s, 0x%.2s, 0x%.2s, 0x%.2s, "
			 "0x%.2s, 0x%.2s, 0x%.2s, 0x%.2s}});",
			 name,
			 s+0, s+9, s+14,
			 s+19, s+21, s+24, s+26,
			 s+28, s+30, s+32, s+34);
	
	return WvString("typedef struct _vtbl_%s *%s;\n"
			"struct _vtbl_%s\n"
			"{\n"
			"// extra vtable bits\n"
			"    void *__extra[2];\n"
			"%s"
			"};\n\n"
			"%s\n\n"
			"%s\n",
			name, name, name, c_style_funcs(ifcname),
			iiddecl,
			c_wrappers(ifcname));
    }
    
    
    WvString dispatch_style()
    {
	MyFuncList::Iter func(funcs);
	
	WvStringList list1;
	for (func.rewind(); func.next(); )
	{
	    list1.append(WvString
			  (func->dispatch_style_func(name,
				     WvString("_wrap_%s_%s",
					      name, func->name))));
	}

	WvStringList list2;
	for (func.rewind(); func.next(); )
	{
	    list2.append(WvString("static UUID _args_%s_%s[] = { "
				  "%s"
				  "};\n", name, func->name,
				  func->dispatch_style_args()));
	}
	
	WvStringList list3;
	for (func.rewind(); func.next(); )
	{
	    list3.append(WvString
			  ("    {\"%s\", _wrap_%s_%s, %s, _args_%s_%s},\n",
			   func->name, name, func->name,
			   func->parms.count(),
			   name, func->name));
	}
	
	return WvString
	    ("%s\n"
	     "%s"
	     "static CallEntry _calls_%s[] = {\n"
	     "%s"
	     "};\n"
	     "static IfcInfo _callinfo_%s(\"%s\", %s_IID,\n"
	     "    _calls_%s, sizeof(_calls_%s)/sizeof(_calls_%s[0]));\n",
	     list1.join("\n"), list2.join(""),
	     name, list3.join(""),
	     name, name, name,
	     name, name, name);
    }
};
DeclareWvList(MyInterface);
DeclareWvDict(MyInterface, WvString, name);


class MyParser
{
public:
    WvError err;
    MyInterfaceList ifcs;
    MyInterfaceDict ifcdict;
    
private:
    WvCont cont;
    IDL_tree top;
    IDL_ns ns;
    void *callback(void *userdata);
    void parse(MyTfd &mtfd);
    
public:
    MyParser(WvStringParm filename);
    ~MyParser();
    
    WvString cpp_style();
    WvString c_style();
    WvString dispatch_style();
};


MyParser::MyParser(WvStringParm filename)
    : ifcdict(10), cont(WvCallback<void*, void*>(this, &MyParser::callback))
{
    memset(&top, 0, sizeof(top));
    memset(&ns, 0, sizeof(ns));
    
    int ret = IDL_parse_filename(filename, "-I.", NULL, &top, &ns,
				 IDLF_PROPERTIES
				 | IDLF_SHOW_CPP_ERRORS
				 | IDLF_INHIBIT_TAG_ONLY
				 | IDLF_INHIBIT_INCLUDES,
				 IDL_WARNINGMAX);
    
    if (ret == IDL_ERROR || ret < 0)
    {
	if (ret < 0)
	    err.set(errno);
	else
	    err.set("IDL parse error");
	return;
    }
    
    IDL_tree_walk_in_order(top,
			   &WvCont::c_bouncer<gboolean, IDL_tree_func_data *>,
			   &cont);
}


MyParser::~MyParser()
{
    if (ns)
	IDL_ns_free(ns);
    if (top)
	IDL_tree_free(top);
}


static UUID random_uuid()
{
    WvString s;
    
    const char * const argv[] = { "uuidgen", NULL };
    WvPipe p(argv[0], argv, false, true, false);
    s = p.getline(-1);
    
    return UuidFromString(s);
}


void MyParser::parse(MyTfd &mtfd)
{
    MyInterface *ifc = NULL;
    MyFunc *func = NULL;
    MyParm *parm = NULL;
    
    // skip the initial boring toplevel "list" item
    mtfd.next(true);
    
    while (mtfd.cur())
    {
	switch (mtfd.type())
	{
	case IDLN_INTERFACE: // new interface
	    {
		bool unstable
		    = IDL_tree_property_get(IDL_INTERFACE(mtfd.tree()).ident,
					    "unstable") ? true : false;
		WvString x
		    = IDL_tree_property_get(IDL_INTERFACE(mtfd.tree()).ident,
					    "uuid");
		ifc = new MyInterface(mtfd.inhibited(), this, mtfd.ident(),
				      (!unstable && !!x)
				         ? UuidFromString(x) : random_uuid());
		ifc->unstable = unstable;
	    }
	    
	    assert(ifc->iid != UUID_null);
	    ifcs.append(ifc, true);
	    ifcdict.add(ifc, false);
	    
	    func = NULL; // the current func is obviously closed
	    parm = NULL; // the current parm is obviously closed
		
	    // skip the interface ident, since we already have it
	    mtfd.next(true);
	    assert(mtfd.type() == IDLN_IDENT);
	    break;
	    
	case IDLN_OP_DCL: // new function in this interface
	    assert(ifc);
	    func = new MyFunc(mtfd.ident());
	    parm = NULL;
	    ifc->funcs.append(func, true);
	    
	    mtfd.next(true);
	    assert(mtfd.type() == IDLN_IDENT || IDL_NODE_IS_TYPE(mtfd.tree()));
	    if (mtfd.ident() != func->name)
		func->ret_type = MyType(mtfd.tree());
	    break;
	    
	case IDLN_PARAM_DCL:
	    assert(func);
	    parm = new MyParm;
	    func->parms.append(parm, true);
	    
	    parm->inout = IDL_PARAM_DCL(mtfd.tree()).attr;
	    
	    mtfd.next(true);
	    assert(mtfd.type() == IDLN_IDENT || IDL_NODE_IS_TYPE(mtfd.tree()));
	    parm->type = MyType(mtfd.tree());
	    break;
	    
	case IDLN_IDENT:
	    if (parm) // the parameter name
		parm->name = mtfd.ident();
	    else if (func)
		; // undefined
	    else if (ifc) // interface inheritence
	    {
		if (!ifc->inherits.isempty())
		    IDL_tree_warning(mtfd.tree(), IDL_WARNING1 ,
			     "Interface '%s': multiple inheritence "
			     "is not allowed in interfaces!\n",
			     ifc->name.cstr());
		ifc->inherits.append(WvString(mtfd.ident()));
	    }
	    break;
	}
	
	mtfd.next(true);
    }
}


void *MyParser::callback(void *userdata)
{
    IDL_tree_func_data *tfd = (IDL_tree_func_data *)userdata;
    MyTfd mtfd(ns, tfd);
    assert(mtfd.type() == IDLN_LIST);
    
    parse(mtfd);
    return NULL;
}


WvString MyParser::cpp_style()
{
    WvStringList list;
    MyInterfaceList::Iter ifc(ifcs);
    for (ifc.rewind(); ifc.next(); )
	if (!ifc->inhibited)
	    list.append(WvString(ifc->cpp_style()));
    return list.join("\n");
}


WvString MyParser::c_style()
{
    WvStringList list;
    MyInterfaceList::Iter ifc(ifcs);
    for (ifc.rewind(); ifc.next(); )
	if (!ifc->inhibited)
	    list.append(WvString(ifc->c_style(ifc->name)));
    return list.join("\n");
}


WvString MyParser::dispatch_style()
{
    WvStringList list;
    MyInterfaceList::Iter ifc(ifcs);
    for (ifc.rewind(); ifc.next(); )
	if (!ifc->inhibited)
	    list.append(ifc->dispatch_style());
    return WvString("#include \"masterdispatch.h\"\n"
		    "#include \"IObject.h\"\n"
		    "#include \"IMulti.hpp\"\n"
		    "#include <stdio.h>\n"
		    "\n"
		    "%s", list.join("\n\n"));
}


// have to define this out-of-line, since it references MyParser
WvString MyInterface::c_style_funcs(WvStringParm ifcname)
{
    WvString total;
    
    WvStringList::Iter inh(inherits);
    for (inh.rewind(); inh.next(); )
    {
	MyInterface *ifc = parser.ifcdict[*inh];
	assert(ifc);
	total.append(ifc->c_style_funcs(ifcname));
    }
    
    WvStringList list;
    MyFuncList::Iter func(funcs);
    for (func.rewind(); func.next(); )
	list.append(WvString("    %s;\n", func->c_style(ifcname)));
    
    total.append("// Interface %s:\n"
		 "%s", name, list.join(""));
    
    return total;
}


int main(int argc, char *argv[])
{
    if (argc < 3)
    {
	fprintf(stderr, "Usage: %s <c/c++/dispatch> <filename>\n", argv[0]);
	return 1;
    }
    WvString type = argv[1];
    WvString filename = argv[2];

    MyParser parser(filename);
    if (parser.err.get())
    {
	wverr->print("%s: %s\n", filename, parser.err.str());
	return 2;
    }

    if (type == "c++")
	wvcon->print(parser.cpp_style());
    else if (type == "c")
	wvcon->print(parser.c_style());
    else if (type == "dispatch")
	wvcon->print(parser.dispatch_style());
    else
    {
	wverr->print("Unsupported output type '%s'.\n", type);
	return 3;
    }

    return 0;
}
