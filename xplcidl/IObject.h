#ifndef _Defined_IObject_
#define _Defined_IObject_

#include <xplc/IObject.h>

// this is a bit weird, but UUIDs, being objects, need their own IID.
DEFINE_IID(UUID, {0x3673b273, 0x19f6, 0x47b4,
    {0xba, 0xf8, 0xac, 0xa6, 0x9b, 0x7c, 0x7d, 0x3a}});

#endif
