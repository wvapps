#include "wvmoniker.h"
#include "mymulti.h"

UUID_MAP_BEGIN(MyMulti)
    UUID_MAP_ENTRY_2(IObject, IString)
    UUID_MAP_ENTRY_2(IObject, IInt)
    UUID_MAP_ENTRY_2(IObject, IBool)
    UUID_MAP_ENTRY(IString)
    UUID_MAP_ENTRY(IInt)
    UUID_MAP_ENTRY(IBool)
    UUID_MAP_ENTRY(IMulti)
UUID_MAP_END;


static IObject *multifactory(WvStringParm s, IObject *obj, void *userdata)
{
    return (IString *)new MyMulti(s);
}

static WvMoniker<IObject> reg("mymulti", multifactory);

