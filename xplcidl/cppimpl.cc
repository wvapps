#include "cpptest.h"
#include "testalloc.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>

static bool is_expecting(const char *s)
{
    return s && expected && !strcmp(s, expected);
}

#define EXPECT_ME()  do { \
    printf("  expected '%s', got '%s'\n", expected, __func__); \
    assert(this == myself); \
    assert(is_expecting(__func__)); \
 } while (0)
#define CHECK(a) assert((int)(a) == 1);

class Test : public ITest
{
public:
    Test *myself;
    
    Test()
    {
	myself = this;
    }
    
    virtual void voidfunc()
    {
	EXPECT_ME();
    }
    
    virtual IObject *objret()
    {
	EXPECT_ME();
	return (IObject *)1;
    }
    
    virtual int intret()
    {
	EXPECT_ME();
	return 1;
    }
    
    virtual bool boolret()
    {
	EXPECT_ME();
	return 1;
    }
    
    virtual const char *strret()
    {
	EXPECT_ME();
	return (const char *)1;
    }
    
    virtual void objparm(const IObject &arg, IObject *&arg2, IObject *&arg3)
    {
	EXPECT_ME();
	CHECK(&arg);
	CHECK(arg2);
	CHECK(arg3);
    }
    
    virtual void intparm(int arg, int &arg2, int &arg3)
    {
	EXPECT_ME();
	CHECK(arg);
	CHECK(arg2);
	CHECK(arg3);
    }
    
    virtual void intparm_simple(int arg)
    {
	EXPECT_ME();
	CHECK(arg);
    }
    
    virtual void boolparm(bool arg, bool &arg2, bool &arg3)
    {
	EXPECT_ME();
	CHECK(arg);
	CHECK(arg2);
	CHECK(arg3);
    }
    
    virtual void strparm(const char *arg, const char *&arg2, const char *&arg3)
    {
	EXPECT_ME();
	CHECK(arg);
	CHECK(arg2);
	CHECK(arg3);
    }
};


ITest *get_cppobj()
{
    return new Test;
}


void free_cppobj(ITest *t)
{
    delete (Test *)t;
}
