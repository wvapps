#!/usr/bin/env tclsh
load ./ctest_tcl.so
set x [get_cppobj]

set expected voidfunc
ITest_voidfunc $x

set expected intparm_simple
ITest_intparm_simple $x 1

set expected intret
set y [ITest_intret $x]
puts "y is $y (should be 1)"
set result [expr $y != 1]
puts "result is $result"
exit $result
