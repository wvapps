#ifndef __UNISCRIPT_H
#define __UNISCRIPT_H

#ifdef __cplusplus
extern "C" {
#endif

const char *unicreate(const char *moniker);
const char *unicall(const char *selfid, const char *parms);
    
#ifdef __cplusplus
}
#endif

#endif // __UNISCRIPT_H
