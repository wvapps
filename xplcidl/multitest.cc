#include "mymulti.h"
#include <xplc/ptr.h>
#include <stdio.h>

int main()
{
    MyMulti *x1 = new MyMulti("Hello"), *x2 = new MyMulti("2 World");
    xplc_ptr<IString> s1(mutate<IString>(*x1)), s2(mutate<IString>(*x2));
    xplc_ptr<IInt> i1(get<IInt>(s1)), i2(get<IInt>(s2));
    xplc_ptr<IBool> b1(get<IBool>(s1)), b2(get<IBool>(s2));
    
    printf("'%s' '%s'\n", s1->getString(), s2->getString());
    printf("%d %d\n", i1->getInt(), i2->getInt());
    printf("%d %d\n", b1->getBool(), b2->getBool());
    
    return 0;
}
