#ifndef __ANYTYPE_H
#define __ANYTYPE_H

#include <xplc/delete.h>
#include <xplc/uuid.h>
#include <xplc/uuidops.h>
#include <xplc/ptr.h>
#include "wvstring.h"

class AnyType
{
public:
    /// a pointer to any interface of this object
    void *raw;
    
    /// the iid of the above interface
    UUID iid;
    
    /// a pointer to this object's IObject interface, if any
    IObject *obj;
    
    /// a string representing this item (only if all above objects are NULL)
    WvString str;
    
    AnyType(void *_raw, const UUID &_iid, IObject *_obj = NULL)
	: iid(_iid), obj(_obj)
        { raw = _raw; if (_obj) _obj->addRef(); }

    AnyType(WvStringParm _str)
	: iid(UUID_null), obj(0), str(_str)
	{ raw = 0; }
    
    AnyType()
	: iid(UUID_null), obj(0)
	{ raw = 0; }
    
    ~AnyType()
        { /* automatic via xplc_ptr */ }
    
    bool switchto(const UUID &niid)
    {
	if (iid == niid) return true; // already done!
	if (iid == UUID_null) return true; // anybody can do that...
	if (!obj) return false;
	
	IObject *n = obj->getInterface(niid);
	if (n)
	{
	    obj->release();
	    raw = obj = n;
	    return true;
	}
	return false;
    }
    
    template <typename T>
    T *get()
    {
	if (switchto(XPLC_IID<T>::get()))
	    return (T *)raw;
	else
	    return NULL;
    }
    
    template <typename T>
    operator T* ()
	{ return get<T>(); }
};


#endif // __ANYTYPE_H
