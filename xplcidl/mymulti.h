#ifndef __MYMULTI_H
#define __MYMULTI_H

#include "IObject.h"
#include "IMulti.hpp"
#include <xplc/utils.h>
#include "wvstring.h"

class MyMulti : public IMulti
{
    IMPLEMENT_IOBJECT(MyMulti);
public:
    WvString s;
    
    MyMulti(WvStringParm _s)
	: s(_s)
	{ printf("creating '%s'\n", s.cstr()); }
    
    virtual ~MyMulti()
        { printf("deleting '%s'\n", s.cstr()); }
    
    virtual const char *getString() { return s.edit(); }
    virtual void setString(const char *_s) { s = _s; }
    
    virtual int getInt() { return s.num(); }
    virtual void setInt(int i) { s = i; }
    
    virtual bool getBool() { return !!s; }
    virtual void setBool(bool b) { s = b; }
    
    // FIXME: this is a hack to make things slightly less ugly when
    // typecasting, because C++ can't deal with lots of IObject-based parents.
    operator IObject*() 
        { return (IString *)this; }
};

#endif // __MYMULTI_H
