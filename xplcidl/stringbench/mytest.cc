#include <stdio.h>
#include <string.h>
#include <string>
#include <ext/rope>
#include <qstring.h>
#include <glib/gstring.h>
#include <glibmm/ustring.h>
#include "wvtimeutils.h"

#define protected public
#define private public
#include "wvstring.h"
#include "wvbuf.h"

#define NITER 100000

typedef Glib::ustring MMString;

class TimeMe
{
    static const char *last_was;
public:
    WvTime start;
    
    TimeMe(const char *id)
    {
	bool same_as_last = !strcmp(last_was, id);
	if (!same_as_last)
	    printf(" %s:", id);
	else
	    printf("/");
	last_was = id;
	start = wvtime();
    }
    
    ~TimeMe()
    {
	WvTime end = wvtime();
	printf("%ld", (long)msecdiff(end, start));
    }
};

const char *TimeMe::last_was = "";


// this is a totally plain string that just copies char* pointers around,
// just like a traditional C programmer would do.  It cheats at some of the
// benchmarks because it doesn't know whether it needs to make a copy first,
// but it's a good baseline to see how your fancy C++ string does compared
// to "hand-optimized" (and error-prone) C.
class Plain
{
public:
    char *s;
    
    Plain(const char *_s) { s = (char *)_s; }
    Plain(const Plain &_s) { s = _s.s; }
    ~Plain() { }
    
    void permanent() { /* always permanent */ }
    void prealloc(int n) { /* not necessary */ }
    void append(const char *a) { /* strcat(s, a); */ /* unknown space... */ }
    const char *tochar() { return s; }
    size_t len() { return strlen(s); }
};


class Cpp
{
public:
    std::string s;
    
    Cpp(const char *_s) : s(_s) { }
    Cpp(const Cpp &_s) : s(_s.s) { }
    Cpp(std::string &_s) : s(_s) { }
    ~Cpp() { }
    
    void permanent() { /* always permanent */ }
    void prealloc(int n) { s.reserve(n); }
    void append(const char *a) { s.append(a); }
    const char *tochar() { return s.c_str(); }
    size_t len() { return s.length(); }
};


class CppRope
{
public:
    typedef __gnu_cxx::crope crope;
    crope s;
    
    CppRope(const char *_s) : s(_s) { }
    CppRope(const CppRope &_s) : s(_s.s) { }
    CppRope(crope &_s) : s(_s) { }
    ~CppRope() { }
    
    void permanent() { /* always permanent */ }
    void prealloc(int n) { /* doesn't make sense in a rope */ }
    void append(const char *a) { s.append(a); }
    const char *tochar() { return s.c_str(); }
    size_t len() { return s.length(); }
};


class WvF
{
public:
    WvFastString s;
    
    WvF(const char *_s) : s(_s) { }
    WvF(const WvF &_s) : s(_s.s) { }
    ~WvF() { }
    
    void permanent() { s = WvString(s); }
    void prealloc(int n) 
        { WvString x; x.setsize(n); strcpy(x.edit(), s); s = x; }
    void append(const char *a) { WvString x(s); x.append(a); s = x; }
    const char *tochar() { return s; }
    size_t len() { return s.len(); }
};


// not as well-optimized as WvF
class Wv
{
public:
    WvString s;
    
    Wv(const char *_s) : s(_s) { }
    Wv(const Wv &_s) : s(_s.s) { }
    ~Wv() { }
    
    void permanent() { /* always permanent */ }
    void prealloc(int n) 
        { WvString x(s); s.setsize(n); strcpy(s.edit(), x); }
    void append(const char *a) { s.append(a); }
    const char *tochar() { return s; }
    size_t len() { return s.len(); }
};


class WvDyn
{
    static const char *peekall(const WvDynBuf &_b)
        { WvDynBuf &b = *(WvDynBuf *)&_b; 
	  return (const char *)b.peek(0, b.used()); }
public:
    WvDynBuf s;
    
    WvDyn(const char *_s) { s.putstr(_s); s.putch(0); }
    WvDyn(const WvDyn &_s) { s.putstr(peekall(_s.s)); }
    ~WvDyn() { }
    
    void permanent() { /* always permanent */ }
    void prealloc(int n) 
        { /* doesn't make sense */ }
    void append(const char *a) { s.unalloc(1); s.putstr(a); s.putch(0); }
    const char *tochar() { return peekall(s); }
    size_t len() { return s.used()-1; }
};


class Qt
{
public:
    QString s;
    
    Qt(const char *_s) : s(_s) { }
    Qt(const Qt &_s) : s(_s.s) { }
    Qt(QString &_s) : s(_s) { }
    ~Qt() { }
    
    void permanent() { /* always permanent */ }
    void prealloc(int n) { s.reserve(n); }
    void append(const char *a) { s.append(a); }
    const char *tochar() { return s.latin1(); }
    size_t len() { return s.length(); }
};


class QtC
{
public:
    QString s;
    
    QtC(const char *_s) : s(_s) { }
    QtC(const QtC &_s) : s(_s.s) { }
    QtC(QString &_s) : s(_s) { }
    ~QtC() { }
    
    void permanent() { /* always permanent */ }
    void prealloc(int n) { s.reserve(n); }
    void append(const char *a) { s.append(a); }
    const char *tochar() { return s; }
    size_t len() { return s.length(); }
};


class GLib
{
public:
    GString *s;
    
    GLib(const char *_s) { s = g_string_new(_s); }
    GLib(const GLib &_s) { s = g_string_new(_s.s->str); }
    ~GLib() { g_string_free(s, true); }
    
    void permanent() { /* always permanent */ }
    void prealloc(int n) { g_string_set_size(s, n); }
    void append(const char *a) { g_string_append(s, a); }
    const char *tochar() { return s->str; }
    size_t len() { return s->len; }
};


class GlibMM
{
public:
    MMString s;
    
    GlibMM(const char *_s) : s(_s) { }
    GlibMM(const GlibMM &_s) : s(_s.s) { }
    GlibMM(MMString &_s) : s(_s) { }
    ~GlibMM() { }
    
    void permanent() { /* always permanent */ }
    void prealloc(int n) { s.reserve(n); }
    void append(const char *a) { s.append(a); }
    const char *tochar() { return s.c_str(); }
    size_t len() { return s.length(); }
};


void nothing(void *p)
{
    // just prevent the optimizer from getting too fancy
}


template<class T>
void pass_string(const T &s)
{
    // just accept the parameter and return
}


template<class T>
void test1(const char *data)
{
    TimeMe t("call");
    for (int n = 0; n < NITER; n++)
	pass_string<T>(data);
}


template<class T>
void test2(const char *data)
{
    TimeMe t("copy");
    T s(data);
    for (int n = 0; n < NITER; n++)
    {
	T s2(s);
	nothing(&s2);
    }
}


template<class T>
void test3(const char *data)
{
    TimeMe t("len");
    T s(data);
    for (int n = 0; n < NITER; n++)
	nothing((void *)s.len());
}


template<class T>
void test4(const char *data)
{
    TimeMe t("append");
    size_t len = strlen(data);

    for (int n = 0; n < NITER/10; n++)
    {
	T s("");
	s.prealloc(len * 10 + 10);
	for (int c = 0; c < 10; c++)
	    s.append(data);
    }
}


template<class T>
void all_tests(char *id)
{
    WvString small("hello");
    WvString big;
    big.setsize(2000+1);
    memset(big.edit(), 'A', 2000);
    big.edit()[2000] = 0;
    
    printf("%s:", id);
    test1<T>(small);
    test1<T>(big);
    test2<T>(small);
    test2<T>(big);
    test3<T>(small);
    test3<T>(big);
    test4<T>(small);
    test4<T>(big);
    printf("\n");
}


int main()
{
    setbuf(stdout, NULL);
    all_tests<Plain>("char*");
    all_tests<Cpp>("std_string");
    all_tests<CppRope>("std_rope");
    all_tests<WvF>("WvFastString");
    all_tests<Wv>("WvString");
    all_tests<WvDyn>("WvDynBuf");
    all_tests<Qt>("QString");
    all_tests<QtC>("QCString");
    all_tests<GLib>("g_string");
    all_tests<GlibMM>("glibmm_ustring");
    return 0;
}
