#include "cpptest.h"
#include "testalloc.h"
#include "IObject.hpp" // just to make sure it compiles
#include <assert.h>
#include <stdio.h>

#define RUN(s, stuff) do { \
    expected = #s; \
    obj->s stuff; \
 } while (0)

#define RUNCHECK(s, stuff) do { \
    expected = #s; \
    assert((int)(obj->s stuff) == 1); \
 } while (0)


int main()
{
    printf("C++ object in C++:\n");
    {
	ITest *obj = get_cppobj();
	RUN(voidfunc, ());
	
	RUNCHECK(objret, ());
	RUNCHECK(intret, ());
	RUNCHECK(boolret, ());
	RUNCHECK(strret, ());
	
	IObject *o = (IObject *)1;
	int i = 1;
	bool b = 1;
	const char *str = (const char *)1;
	
	RUN(objparm, (*o, o, o));
	RUN(intparm, (i, i, i));
	RUN(intparm_simple, (i));
	RUN(boolparm, (b, b, b));
	RUN(strparm, (str, str, str));
	
	free_cppobj(obj);
    }
    
    return 0;
}
