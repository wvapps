#include <assert.h>
#include <stdio.h>
#include "ctest.h"
#include "testalloc.h"

#define RUN(s, stuff) do { \
    expected = #s; \
    (*obj)->s stuff; \
 } while (0)

#define RUNCHECK(s, stuff) do { \
    expected = #s; \
    assert((int)(*obj)->s stuff == 1); \
 } while (0)


int main()
{
    printf("C++ object in C:\n");
    {
	IObject *o = (IObject *)1;
	int i = 1;
	bool b = 1;
	const char *str = (const char *)1;
	
	ITest *obj = get_cppobj();
	
	RUN(voidfunc, (obj));
	
	RUNCHECK(objret, (obj));
	RUNCHECK(intret, (obj));
	RUNCHECK(boolret, (obj));
	RUNCHECK(strret, (obj));
	
	RUN(objparm, (obj, o, &o, &o));
	RUN(intparm, (obj, i, &i, &i));
	RUN(intparm_simple, (obj, i));
	RUN(boolparm, (obj, b, &b, &b));
	RUN(strparm, (obj, str, &str, &str));
	
	free_cppobj(obj);
    }
    
    return 0;
}
