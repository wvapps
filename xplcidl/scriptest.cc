#include "masterdispatch.h"
#include "wvmoniker.h"
#include "IObject.h"
#include "IMulti.hpp"
#include "uniscript.h"
#include <stdio.h>
#include <assert.h>

int main()
{
    {
	IObject *obj = wvcreate<IObject>("mymulti:5 hello world!");
	assert(obj);
	
	AnyType any2(obj, IObject_IID, obj);
	AnyType any2b(any2);
	printf("switch2: %p -> %d\n",
	       (IObject *)any2.obj, any2.switchto(IString_IID));
	printf("switch2b: %p -> %d\n",
	       (IObject *)any2b.obj, any2b.switchto(IString_IID));
	
	AnyType x[] = { AnyType(98) };
	master->call(any2, "setInt", 1, x);
	AnyType ret = master->call(any2, "getInt", 0, NULL);
	printf("return: %s\n", ret.str.cstr());
	assert(ret.str.num() == 98);
	
	AnyType x2[] = { AnyType("42 silly")};
	master->call(any2, "setString", 1, x2);
	ret = master->call(any2, "getInt", 0, NULL);
	printf("return: %s\n", ret.str.cstr());
	assert(ret.str.num() == 42);
	
	obj->release();
    }
    
    {
	WvString o = unicreate("mymulti:7 hello world!");
	int x = atoi(unicall(o, "getInt"));
	printf("x = %d\n", x);
	unicall(o, "setString {blah blah}");
	printf("x2 = '%s'\n", unicall(o, "getString"));
    } 
    
    return 0;
}
