/** Some generally useful Qt utils. */
#ifndef __QTUTILS_H
#define __QTUTILS_H

#include <qobject.h>
#include <qwidget.h>

/**
 * Watches for FocusIn and FocusOut events on a target widget.
 * To use this class, create an instance (may be shared),
 * connect to the signals, and attach the object to the widgets
 * to be observed using:
 *    widgetInstance->installEventFilter(focusObserverInstance);
 */
class FocusObserver : public QObject
{
    Q_OBJECT
    
public:
    FocusObserver() { }
    virtual ~FocusObserver() { }

signals:
    // called when target widget receives focus
    void focusIn(QWidget *widget);
    
    // called when target widget loses focus
    void focusOut(QWidget *widget);

protected:
    virtual bool eventFilter(QObject *obj, QEvent *event);
};

#endif // __QTUTILS_H
