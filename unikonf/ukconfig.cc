/*
* ** Copyright (C) 2002 Net Integration Technologies
* **
* ** This program is free software; you can redistribute it and/or modify
* ** it under the terms of the GNU Lesser General Public License as published by
* ** the Free Software Foundation; either version 2 of the License, or
* ** (at your option) any later version.
* **
* ** This program is distributed in the hope that it will be useful,
* ** but WITHOUT ANY WARRANTY; without even the implied warranty of
* ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* ** GNU General Public License for more details.
* **
* ** You should have received a copy of the GNU Lesser General Public License
* ** along with this program; if not, write to the Free Software
* ** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
* */

#include "ukconfig.h"
#include "ukconfig.moc"

#include "kapplication.h"

// little helper borrowed from the KMail Sources: - thanks guys ;)
static inline QPixmap loadIcon( const char * name ) 
{
  return KGlobal::instance()->iconLoader()->loadIcon( QString::fromLatin1(name), KIcon::NoGroup, KIcon::SizeMedium);
}

/**************************************************************************/
/*									  */
/*		          UniKonfAddMount Starts Here			  */

UniKonfAddMount::UniKonfAddMount(QWidget *parent, const char *name, KSimpleConfig *_config)
	: KDialog(parent, name, true),
	  config(_config)//, mp_modified(false)
{
	aslayout = new QGridLayout(this,5,4,3,-1);
    	CHECK_PTR(aslayout);

    	ok = new KPushButton(i18n("Ok"),this,"prefs_ok");
    	CHECK_PTR(ok);

	ok->setDefault(TRUE);

    	close = new KPushButton(i18n("Close"),this,"prefs_close");
    	CHECK_PTR(close);

	help  = new KPushButton(i18n("Help"),this,"prefs_help");
    	CHECK_PTR(help);

        monikerl = new QLabel(NULL, i18n("Moniker:"), this);
        moniker = new KLineEdit(this);
        mountpointl = new QLabel(NULL, i18n("Mount Point:"), this);
        mountpoint = new KLineEdit(this);
        mountpoint->setText("/");

        aslayout->addWidget(monikerl, 0, 0);
        aslayout->addMultiCellWidget(moniker, 0, 0, 1, 3);
        aslayout->addWidget(mountpointl, 2, 0);
        aslayout->addMultiCellWidget(mountpoint, 2, 2, 1, 3);
	aslayout->addWidget(ok,5,1);
	aslayout->addWidget(close,5,2);
	aslayout->addWidget(help,5,3);

	connect( ok, SIGNAL(clicked()), SLOT(createnew()));
	connect( close, SIGNAL(clicked()), SLOT(reject()));

	adjustSize();
}

UniKonfAddMount::UniKonfAddMount(QString *_mountpoint, QWidget *parent, 
			     const char *name, KSimpleConfig *_config)
	: KDialog(parent,name,TRUE),
	  config(_config),
          old_mount(*_mountpoint)
{
	config->setGroup(_mountpoint->latin1());
	aslayout = new QGridLayout(this,5,4,3,-1);
    	CHECK_PTR(aslayout);

    	ok = new KPushButton(i18n("Ok"),this,"prefs_ok");
    	CHECK_PTR(ok);

	ok->setDefault(TRUE);

    	close = new KPushButton(i18n("Close"),this,"prefs_close");
    	CHECK_PTR(close);

	help  = new KPushButton(i18n("Help"),this,"prefs_help");
    	CHECK_PTR(help);

        monikerl = new QLabel(NULL, i18n("Moniker:"), this);
        moniker = new KLineEdit(this);
        moniker->setText(config->readEntry("moniker"));

        mountpointl = new QLabel(NULL, i18n("Mount Point:"), this);
        mountpoint = new KLineEdit(this);
        mountpoint->setText(_mountpoint->latin1());
        
        aslayout->addWidget(monikerl, 0, 0);
        aslayout->addMultiCellWidget(moniker, 0, 0, 1, 3);
        aslayout->addWidget(mountpointl, 2, 0);
        aslayout->addMultiCellWidget(mountpoint, 2, 2, 1, 3);

	aslayout->addWidget(ok,4,1);
	aslayout->addWidget(close,4,2);
	aslayout->addWidget(help,4,3);

	connect( ok, SIGNAL(clicked()), SLOT(modify()));
	connect( close, SIGNAL(clicked()), SLOT(reject()));
}


UniKonfAddMount::~UniKonfAddMount()
{

}

void UniKonfAddMount::createnew()
{
    config->setGroup(mountpoint->text());
    config->writeEntry("moniker", moniker->text());
    config->sync();
    QWidget::close();
}

void UniKonfAddMount::modify()
{
    config->setGroup(mountpoint->text());
    config->deleteGroup(old_mount, true);
    config->writeEntry("moniker", moniker->text());
    config->sync();
    QWidget::close();
}

/**************************************************************************/
/*									  */
/*		          UniKonfCfgConn Starts Here			  */
/*  - Configurationof mountpoints                                         */

UniKonfCfgConn::UniKonfCfgConn(QWidget *parent, const char *name, KSimpleConfig *_config)
	: UniKonfCfgPage(parent,name),
	  config(_config)
{
    servername = new QString(QString::null);
    CHECK_PTR(servername);

    connectlayout = new QGridLayout(this,5,3,3,-1);
    CHECK_PTR(connectlayout);
    connectwindow = new KListBox(this,"connectwindow");
    CHECK_PTR(connectwindow);
    connect_add = new KPushButton(i18n("Add"),this,"connect_add");
    CHECK_PTR(connect_add);
    connect_mod = new KPushButton(i18n("Modify"),this,"connect_mod");
    CHECK_PTR(connect_mod);
    connect_del = new KPushButton(i18n("Delete"),this,"connect_del");
    CHECK_PTR(connect_del);

    connectlayout->addMultiCellWidget(connectwindow,0,4,0,1);
    connectlayout->addWidget(connect_add,0,2);
    connectlayout->addWidget(connect_mod,1,2);
    connectlayout->addWidget(connect_del,2,2);
    connectlayout->addRowSpacing(3,50);
    connectlayout->addRowSpacing(4,50);

    connect(connect_add,SIGNAL(clicked()),SLOT(addMountPoint()));
    connect(connect_mod,SIGNAL(clicked()),SLOT(modMountPoint()));
    connect(connect_del,SIGNAL(clicked()),SLOT(delMountPoint()));
    connect(connectwindow,SIGNAL(highlighted(const QString &)), SLOT(setMountPoint(const QString &)));
    fillwindow();
}

UniKonfCfgConn::~UniKonfCfgConn()
{

}

QString UniKonfCfgConn::helpAnchor() {
  return QString::fromLatin1("configure-connections");
}

void UniKonfCfgConn::fillwindow()
{
	connectwindow->clear();
	QStringList sectlist(config->groupList());
	int count = 0;
	for ( QStringList::Iterator it = sectlist.begin(); it != sectlist.end(); ++it ) 
	{
		if (((*it).contains("KFile") == 0) && 
		    ((*it).contains("Global") == 0 ) &&
		    ((*it).contains("Cert") == 0 ) &&
		    ((*it).contains("<default>") == 0) ) 
		   connectwindow->insertItem((*it).latin1(), count++);
        }
}

void UniKonfCfgConn::addMountPoint()
{
        UniKonfAddMount dlg(this,"kdiradm",config);
        dlg.setCaption("Add Server");
        dlg.exec();
	fillwindow();
}

void UniKonfCfgConn::modMountPoint()
{
	if (!servername->isNull())
	{
        	UniKonfAddMount dlg(servername, this,"kdiradm",config);
        	dlg.setCaption(i18n("Modify Server"));
        	dlg.exec();
                fillwindow();
	} else {
		KMessageBox::sorry(this,i18n("You Must Select a Server from the list!"));
	}
}

void UniKonfCfgConn::delMountPoint()
{
        if (!servername->isNull())
        {
		config->deleteGroup(servername->latin1());
		config->sync(); 
        } else {
                KMessageBox::sorry(this,i18n("You Must Select a Server from the list!"));
        }
	fillwindow();
}

void UniKonfCfgConn::setMountPoint(const QString &_selected)
{
	servername = new QString(_selected);
}

void UniKonfCfgConn::apply()
{
// For this page, apply doesn't really make sense, so this is just an empty
// function
}

/**************************************************************************/
/*									  */
/*		             UniKonfPrefs Starts Here			  */

UniKonfCfg::UniKonfCfg(QWidget *parent, const char *name, KSimpleConfig *_config)
	: KDialogBase(IconList, i18n("Configure"), Help|Apply|Ok|Cancel, Ok, parent, name, true, true )
{
   setIconListAllVisible( true );

   QWidget *page;
   QVBoxLayout *vlay;

   // Connections to UniKonf mountpoints.
   // -> Client, IniFile, etc.
   page = addPage( i18n("Mountpoints"), i18n("Mountpoints"), loadIcon("network"));
   CHECK_PTR(page);

   vlay = new QVBoxLayout( page, 0, spacingHint() );
   CHECK_PTR(vlay);

   connwidget = new UniKonfCfgConn( page, "connections", _config);
   CHECK_PTR(connwidget);

   vlay->addWidget(connwidget);

   connwidget->setPageIndex( pageIndex( page ) );
}

UniKonfCfg::~UniKonfCfg()
{

}

void UniKonfCfg::slotHelp() 
{
  int activePage = activePageIndex();

  if ( activePage == connwidget->pageIndex() )    
    kapp->invokeHelp( connwidget->helpAnchor() ); 
}

void UniKonfCfg::slotApply()
{
	apply(false);
}

void UniKonfCfg::slotOk()
{
	apply(true);
	accept();
}

void UniKonfCfg::apply(bool AboutToClose)
{
  int activePage = activePageIndex();

  if ( AboutToClose || activePage == connwidget->pageIndex() )
    connwidget->apply();
}
