#ifndef UKMAINWIN_H 
#define UKMAINWIN_H 

#include <kmessagebox.h>
#include <klistview.h>
#include <kstddirs.h>
#include <kseparator.h>
#include <klocale.h>
#include <kstatusbar.h>
#include <qlist.h>
#include <qlayout.h>
#include <qscrollview.h>
#include <qvbox.h>
#include <qsplitter.h>
#include <qlineedit.h>
#include <qlabel.h>
#include <qpopupmenu.h>
#include "wvlog.h"
#include "uniwatch.h"

class UniKonf;
class UniKonfMainWin;

class UkMainWin : public QVBox
{
    Q_OBJECT
	
    WvLog log;
    UniConf cfg;
    UniWatch watch;
    
    KListView *listview;
    
    struct Undo
    {
	UniConf h;
	WvString oldval;
	WvString newval;
    };
    
    DeclareWvList(Undo);
    UndoList undolist, redolist;
    
public:
    UkMainWin(QWidget *parent, const UniConf &_cfg, const char *name = 0);
    virtual ~UkMainWin();
    
    void uniconf_notify(const UniConf &changed, const UniConfKey &relpath);

    void delete_tree();
    void fill_tree();

    void do_undo();
    void do_redo();
    
public slots:
    void node_changed(QListViewItem *item);
    void closenode(QListViewItem *item);
    void delSubtree();
    void addChild();
    void rightButtonPressed(QListViewItem *lvi, const QPoint &p, int);
   
};

#endif // UKMAINWIN_H 
