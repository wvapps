#ifndef UNIKONF_H 
#define UNIKONF_H 

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif 

#include <kmainwindow.h>
#include <ksimpleconfig.h>

#include "uniconfroot.h"
#include "ukmainwin.h"
#include "ukconfig.h"
#include "wvlog.h"

#define VERSION " v0.1"

class UniKonf;
class KComboBox;
class KPopupMenu;
class WvIStreamList;


class UniKonf : public KMainWindow
{
    Q_OBJECT
    
    UniConfRoot root;
    UniConf cfg;

public:
    UniKonf(QWidget *parent = 0, const char *name = 0);
    virtual ~UniKonf();
    void populate();

    KSimpleConfig *config;

    // Public so that others can change it.
    KStatusBar *status;

public slots:

protected slots:
    void settings();
    void startConn();
    void stopConn();
    void notdone();
    void saveConfig();
    void showToolB();
    void showStatB();
    void undo();
    void redo();
    void exportslot();

private:
    WvLog log;
    UkMainWin *mw;
    KToolBar *toolbar;
    KPopupMenu *file_menu, *edit_menu, *settings_menu;
};

#endif // UNIKONF_H 
