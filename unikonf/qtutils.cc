/** Some generally useful Qt utils. */
#include "qtutils.moc"

/***** FocusObserver *****/

bool FocusObserver::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::FocusIn)
        focusIn(static_cast<QWidget*>(obj));
    else if (event->type() == QEvent::FocusOut)
        focusOut(static_cast<QWidget*>(obj));
    return QObject::eventFilter(obj, event);
}
