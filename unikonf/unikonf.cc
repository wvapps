/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2002 Net Integration Technologies, Inc.
 */
#include "unikonf.h"
#include "unikonf.moc"
#include "uniconfroot.h"
#include "uniconfgen.h"
#include "wvistreamlist.h"

#include <klocale.h>
#include <kaboutdialog.h>
#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <khelpmenu.h>
#include <kmenubar.h>
#include <kpopupmenu.h>
#include <kstatusbar.h>
#include <ktoolbar.h>
#include <qfiledialog.h>


/** 
 * This is the application's main window.
 */
UniKonf::UniKonf(QWidget *parent, const char *name)
    : KMainWindow(parent, name), cfg(root), log("UniKonf", WvLog::Debug1)
{
    config = new KSimpleConfig("unikonfrc");
    
    QPixmap configureIcon(locate("icon",
				 "hicolor/16x16/actions/configure.png"));
    QPixmap searchIcon(locate("icon", "hicolor/16x16/actions/find.png"));
    QPixmap exitIcon(locate("icon", "hicolor/16x16/actions/exit.png"));
    QPixmap saveIcon(locate("icon", "hicolor/16x16/actions/filesave.png"));
    QPixmap undoIcon(locate("icon", "hicolor/16x16/actions/undo.png"));
    QPixmap redoIcon(locate("icon", "hicolor/16x16/actions/redo.png"));
    QPixmap exportIcon(locate("icon", "hicolor/16x16/actions/fileexport.png"));

    file_menu = new KPopupMenu(this);

    file_menu->insertItem(saveIcon, i18n("&Save"), this, 
			  SLOT(saveConfig()), CTRL+Key_S, 0, 2);
    file_menu->setItemEnabled(2, false);
    file_menu->insertItem(exportIcon, i18n("&Export"), this,
			  SLOT(exportslot()), CTRL+Key_X);
    file_menu->insertSeparator();
    file_menu->insertItem(exitIcon, i18n("&Quit"), qApp, 
			  SLOT(quit()), CTRL+Key_Q);
    
    edit_menu = new KPopupMenu(this);

    edit_menu->insertItem(undoIcon, i18n("Undo"), this,
			  SLOT(undo()), CTRL+Key_Z);
    edit_menu->setItemEnabled(2, true);
    edit_menu->insertItem(redoIcon, i18n("Redo"), this,
			  SLOT(redo()), CTRL+Key_R);
    edit_menu->setItemEnabled(3, true);

    settings_menu = new KPopupMenu(this);
    settings_menu->setCheckable(true);
    settings_menu->insertItem(i18n("Show Toolbar"), this,
			      SLOT(showToolB()), 0, 1);
    settings_menu->insertItem(i18n("Show StatusBar"), this,
			      SLOT(showStatB()), 0, 2);
    settings_menu->insertSeparator();
    settings_menu->insertItem(configureIcon, i18n("Configure UniKonf..."),
			      this, SLOT(settings()));
    settings_menu->setItemChecked(1, true);
    settings_menu->setItemChecked(2, true);
    
    KPopupMenu *help_menu = helpMenu();
    
    KMenuBar *_menuBar = menuBar();
    _menuBar->insertItem(i18n("&File"), file_menu);
    _menuBar->insertItem(i18n("&Edit"), edit_menu);
    _menuBar->insertItem(i18n("&Settings"), settings_menu);
    _menuBar->insertItem(i18n("&Help"), help_menu);

    toolbar = toolBar();
    toolbar->insertButton("exit", 0, SIGNAL(clicked()), qApp, SLOT(quit()),
			       true, i18n("Exit Application"));
    toolbar->insertButton("configure", 1, SIGNAL(clicked()), this,
			       SLOT(settings()), true,
			       i18n("Manage Connections to UniConf Servers"));
    toolbar->insertButton(saveIcon, 2, SIGNAL(clicked()), this,
                               SLOT(saveConfig()), true,
                               i18n("Save all changes."));
    toolbar->insertButton(undoIcon, 3, SIGNAL(clicked()), this,
                               SLOT(undo()), true,
                               i18n("Undo last change."));
    toolbar->insertButton(redoIcon, 4, SIGNAL(clicked()), this,
                               SLOT(redo()), true,
                               i18n("Redo last change."));
    
    status = statusBar();
    status->message(i18n("Ready"), 0);

    mw = new UkMainWin(this, cfg, "UkMainWin");
    setCentralWidget(mw);

    startConn();
}


UniKonf::~UniKonf()
{
    // delete this first, because it has references to some of our other
    // members (particularly UniConf).
    delete mw;
    mw = NULL;
}


void UniKonf::startConn()
{
    stopConn();

    int mountedsubtrees = 0;
    QStringList mountlist(config->groupList());
    if (mountlist.isEmpty())
    {
	KMessageBox::detailedSorry(this, i18n("No Mountpoints Specified"),
	   i18n("UniKonf currently has no subtrees mounted.\n"
		"Please select Settings -> Configure to add a mountpoint.\n"));
    } 
    else 
    {
        QStringList mounts(config->groupList());
        WvString failedlist("Failed to connect to data source(s):  ");

        // Connect to every server in our server list.
        bool failure = false;       // we failed to connect to at least one server
        setCursor(waitCursor);  

	for (QStringList::Iterator i = mounts.begin(); i != mounts.end(); i++)
        {
	    if (((*i).contains("KFile")     == 0) && 
	        ((*i).contains("Global")    == 0) &&
	        ((*i).contains("Cert")      == 0) &&
                ((*i).contains("<default>") == 0)   )
            {
                QString mountpoint = *i;
                config->setGroup(mountpoint);
                QString moniker = config->readEntry("moniker");

		if (moniker.isNull())
                    continue;
                UniConf temp = cfg[WvString(mountpoint)];
                IUniConfGen *gen = temp.mount(WvString(moniker));
                
                if (!gen || !gen->isok())
                {
                    temp.unmount(gen, false);
                    failedlist.append("%s, ", moniker);
                    failure = true;

                    // FIXME:  We should remove <temp> from the list here.
                    continue;
                } 
                else
                    mountedsubtrees++;
            }
        }
        if (failure)
        {
	    KMessageBox::detailedSorry(this, i18n("Failed to Connect"),
		   i18n(failedlist.cstr()));
            status->message(i18n("Ready"), 0);
        }
         setCursor(arrowCursor);
        
    }
    file_menu->setItemEnabled(2, true);

    WvString str("Mounted %s %s.", mountedsubtrees,
		 (mountedsubtrees==1 ? "subtree" : "subtrees"));
    status->message(i18n(str), 0);
}


void UniKonf::stopConn()
{
    // disable the watch
    cfg.del_callback(this);
    
    // Disable the Import/Export Menu Items
    file_menu->setItemEnabled(2, false);
    file_menu->setItemEnabled(3, false);
    
    // Disable the Search/Find Menu Items
    edit_menu->setItemEnabled(2, false);
    edit_menu->setItemEnabled(3, false);
    
    // Clear out the current configuration
    // FIXME: unmount everything, not just the top!
    if (root.whichmount())
	root.unmount(root.whichmount(), true);

    // Reset the Status Bar to the default state.
    status->message(i18n("Ready"), 0);
}


void UniKonf::settings()
{
    UniKonfCfg dlg(this, "unikonf", config);
    dlg.setInitialSize(QSize(556, 345));
    dlg.exec();
    if (dlg.result() == QDialog::Accepted)
    {
        stopConn();
        startConn();
    }
}


void UniKonf::notdone()
{
    KMessageBox::sorry(this, i18n("Not implemented yet!"));
}


void UniKonf::saveConfig()
{
    cfg.commit();
}


void UniKonf::showToolB()
{
    if (settings_menu->isItemChecked(1))
    {
	settings_menu->setItemChecked(1, false);
	toolbar->hide();
    }
    else
    {
	settings_menu->setItemChecked(1, true);
	toolbar->show();
    }
}


void UniKonf::showStatB()
{
    if (settings_menu->isItemChecked(2))
    {
	settings_menu->setItemChecked(2, false);
	status->hide();
    }
    else
    {
	settings_menu->setItemChecked(2, true);
	status->show();
    }
}


void UniKonf::undo()
{
    mw->do_undo();
}


void UniKonf::redo()
{
    mw->do_redo();
}


void UniKonf::exportslot()
{
    QString filename = QFileDialog::getSaveFileName(QString::null,
	    "INI Files (*.ini)", this, "Export dialog", 
            "Choose file to export to");
    UniConfRoot newroot(WvString("ini:%s", filename));
    UniConf::RecursiveIter i(newroot);
    for (i.rewind(); i.next(); )
	newroot.xset(i->fullkey(), i->getme());
    newroot.commit();
}
