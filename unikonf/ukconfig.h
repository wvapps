/*
* ** Copyright (C) 2002 Net Integration Technologies Inc.
* **
* ** This program is free software; you can redistribute it and/or modify
* ** it under the terms of the GNU Lesser General Public License as published by
* ** the Free Software Foundation; either version 2 of the License, or
* ** (at your option) any later version.
* **
* ** This program is distributed in the hope that it will be useful,
* ** but WITHOUT ANY WARRANTY; without even the implied warranty of
* ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* ** GNU General Public License for more details.
* **
* ** You should have received a copy of the GNU Lesser General Public License
* ** along with this program; if not, write to the Free Software
* ** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
* */

#ifndef UNIKONFCFG_H 
#define UNIKONFCFG_H 

#include <kmessagebox.h>
#include <klocale.h>
#include <ktabctl.h>
#include <kstddirs.h>
#include <kdialogbase.h>
#include <kiconloader.h>
#include <ksimpleconfig.h>

#include <klistbox.h>
#include <klineedit.h>
#include <qbuttongroup.h>
#include <qlayout.h>
#include <kpushbutton.h>
#include <qradiobutton.h>
#include <qcheckbox.h>
#include <qgroupbox.h>
#include <qlabel.h>

class UniKonfCfgPage : public QWidget
{
        Q_OBJECT
public:
        UniKonfCfgPage( QWidget * parent=0, const char * name=0 )
                 : QWidget( parent, name ) {}                      
        ~UniKonfCfgPage() {};  
                                     
        void setPageIndex( int aPageIndex ) { mPageIndex = aPageIndex; }
        int pageIndex() const { return mPageIndex; }

protected:
        int mPageIndex;
};


class UniKonfAddMount : public KDialog
{
	Q_OBJECT
public:
	UniKonfAddMount(QWidget *parent = 0, const char *name = 0, KSimpleConfig *_config = 0);
	UniKonfAddMount(QString *_mountpoint, QWidget *parent = 0, const char *name = 0, KSimpleConfig *_config = 0);
	virtual ~UniKonfAddMount();

	KSimpleConfig	*config;

public slots:
	void		createnew();
	void		modify();

private:
        QGridLayout     *aslayout;
        QLabel          *monikerl;
        KLineEdit       *moniker;
        QLabel          *mountpointl;
        KLineEdit       *mountpoint;
        KPushButton     *ok;
        KPushButton     *close;
        KPushButton     *help;
        QString         old_mount;
};

class UniKonfCfgConn : public UniKonfCfgPage
{
	Q_OBJECT
public:
	UniKonfCfgConn(QWidget *parent = 0, const char *name = 0, KSimpleConfig *_config = 0);
	virtual ~UniKonfCfgConn();

	QString		*servername;
	KSimpleConfig	*config;

	QString		helpAnchor();
	void		apply();

public slots:

protected slots:
	void		fillwindow();
	void		addMountPoint();
	void		modMountPoint();
	void		delMountPoint();
	void		setMountPoint(const QString &);
	
private:
	QWidget		*connecttab;
	QGridLayout	*connectlayout;
	KListBox        *connectwindow;
	KPushButton	*connect_add;
	KPushButton	*connect_mod;
	KPushButton	*connect_del;
};

class UniKonfCfg : public KDialogBase
{
	Q_OBJECT
public:
	UniKonfCfg(QWidget *parent = 0, const char *name = 0, KSimpleConfig *_config = 0);
	virtual ~UniKonfCfg();

public slots:
	virtual void slotHelp();
	virtual void slotApply();
	virtual void slotOk();

protected:
	void apply(bool);

private:
	UniKonfCfgConn	*connwidget;
	QGridLayout	*prefslayout;
};

#endif // UNIKONFCFG_H 
