#include "unikonf.h"
#include "ukmainwin.h"
#include "ukmainwin.moc"

#include "uniconf.h"
#include "uniconfgen.h"

#include "wvlog.h"
#include "wvscatterhash.h"

DeclareWvScatterTable2(WvStringHash, WvString);


class UkViewItem : public KListViewItem
{
public:
    UniConf h;
private:
    WvLog &log;
    UniWatch watcher;
    WvStringHash keymap;
    QString last_settext;
    
public:
    /** Constructor for when we have a KListView as a parent */
    UkViewItem(KListView *_parent, const UniConf &_h, WvLog &_log);
    
    /** Constructor for when we have a KListViewItem as a parent */
    UkViewItem(KListViewItem *_parent, const UniConf &_h, WvLog &_log);
    
    virtual ~UkViewItem();

    void update(const UniConf &changed, const UniConfKey &relpath);
    
    virtual void setOpen(bool isopen);
    virtual void setText(int column, const QString &text);
    
    virtual void insertItem(QListViewItem *item);
    virtual void takeItem(QListViewItem *item);
};


UkViewItem::UkViewItem(KListView *_parent, const UniConf &_h, WvLog &_log)
    : KListViewItem(_parent, WvString("/%s", _h.key())), h(_h), log(_log),
      watcher(h, UniConfCallback(this, &UkViewItem::update))
{
    _parent->insertItem(this);
    update(h, "");
}
	      

UkViewItem::UkViewItem(KListViewItem *_parent, const UniConf &_h, WvLog &_log)
    : KListViewItem(_parent, _h.key().printable().cstr()), h(_h), log(_log),
      watcher(h, UniConfCallback(this, &UkViewItem::update))
{
    _parent->insertItem(this);
    update(h, "");
}


UkViewItem::~UkViewItem()
{
    // nothing special
}


void UkViewItem::update(const UniConf &changed, const UniConfKey &relpath)
{
    fprintf(stderr, "ukview notification for '%s'-'%s'/%d/%p\n",
	    changed.key().printable().cstr(), relpath.printable().cstr(),
	    changed->isnull(),
	    keymap[changed.key()]);
    
    setExpandable(h.haschildren());

    if (relpath.numsegments() == 0)
    {
	IUniConfGen *gen = h.whichmount();
	if (gen && gen->isok())
	    setPixmap(0, locate("icon",
			"hicolor/16x16/filesystems/folder_green_open.png"));
	else if (h.ismountpoint())
	    setPixmap(0, locate("icon",
			"hicolor/16x16/filesystems/file_important.png"));
	else if (childCount() > 0) // O(1) according to Qt3.0 QListView header
	    setPixmap(0, locate("icon",
			"hicolor/16x16/filesystems/folder_grey_open.png"));
	
	WvString s(h.getme());
    
	if (s.isnull() && h.key() != "/")
	    delete this; // WHEE!
	else
	    setText(1, s);
    }
    else if (relpath.numsegments() == 1)
    {
	// one of our children immediate changed - add it if it's new
	if (isOpen() && !keymap[relpath] && !h[relpath]->isnull())
	{
	    fprintf(stderr, "Must add '%s' to '%s'\n",
		  relpath.printable().cstr(), h.fullkey().printable().cstr());
	    new UkViewItem(this, h[relpath], log);
	}
    }
}


void UkViewItem::setOpen(bool isopen)
{
    if (isopen == isOpen())
	return;
    
    if (isopen)
    {
	fprintf(stderr, "expanding %s\n", h.fullkey().printable().cstr());
	UniConf::Iter i(h);
	for (i.rewind(); i.next(); )
	{
	    new UkViewItem(this, *i, log);
	    fprintf(stderr, " ... %s\n", i->fullkey().printable().cstr());
	}
    }
    else
    {
	while (firstChild())
	    delete firstChild();
    }
    
    KListViewItem::setOpen(isopen);
}


void UkViewItem::setText(int column, const QString &newtext)
{
    if (column == 0) // key name
    {
	if (newtext != text(0))
	{
	    // this causes recursion!  Be careful.
	    h.move(h.parent()[WvString(newtext)]);
	}
    }
    else if (column == 1) // value
    {
	KListViewItem::setText(column, h.getme());
	if (last_settext != newtext)
	{
	    last_settext = newtext;
	    if (h.getme() != WvString(newtext))
		h.setme(newtext); // causes 1-level recursion if uniconf refuses!
	}
    }
}


void UkViewItem::insertItem(QListViewItem *_item)
{
    KListViewItem::insertItem(_item);

    // make sure the UkViewItem constructor is running (QListViewItem will
    // try to call us before UkViewItem is actually ready!)
    if (!_item->text(0))
	return;
    
    UkViewItem *item = (UkViewItem *)_item;
    
    WvString key(item->h.key());
    fprintf(stderr, "inserting %s.\n", key.cstr());
    keymap.add(new WvString(key), true);

    KListViewItem::takeItem(_item);
    KListViewItem::insertItem(_item);
}


void UkViewItem::takeItem(QListViewItem *item)
{
    WvString *s = keymap[item->text(0)];
    if (s)
	keymap.remove(s);
    
    KListViewItem::takeItem(item);
}


UkMainWin::UkMainWin(QWidget *_parent, const UniConf &_cfg, const char *name)
    : QVBox(_parent, name), log("UkMainWin", WvLog::Debug1), cfg(_cfg),
      watch(cfg, UniConfCallback(this, &UkMainWin::uniconf_notify))
{
    setSizePolicy(QSizePolicy(QSizePolicy::Expanding,
			      QSizePolicy::Expanding, true));

    listview = new KListView(this);

    listview->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,
					QSizePolicy::Expanding, false));
    listview->setItemsRenameable(true);
    listview->setRootIsDecorated(true);
    //listview->setTabOrderedRenaming(true);
    
    listview->addColumn(i18n("Key"));
    listview->setColumnWidth(0, 333);
    listview->setRenameable(0, true);

    listview->addColumn(i18n("Value"));
    listview->setRenameable(1, true);

    listview->clear();
    fill_tree();
    
    connect(listview, SIGNAL(currentChanged(QListViewItem *)),
	    this, SLOT(node_changed(QListViewItem *)));

    connect(listview, SIGNAL(doubleClicked(QListViewItem *)),
	    this, SLOT(closenode(QListViewItem *)));
    
    connect(listview, 
	    SIGNAL(rightButtonPressed(QListViewItem*, const QPoint &, int)),
	    SLOT(rightButtonPressed(QListViewItem*, const QPoint &, int)));

    show();
}
	      

UkMainWin::~UkMainWin()
{
}


void UkMainWin::uniconf_notify(const UniConf &changed,
			       const UniConfKey &relpath)
{
    // FIXME
}
	      

void UkMainWin::delete_tree()
{
    listview->clear();
}


void UkMainWin::fill_tree()
{
    delete_tree();

    // create the base of the tree.  He'll manage the rest when he needs to.
    UkViewItem *root = (UkViewItem *)listview->firstChild();

    // We only want to do this if our tree is empty!
    if (!root)
        root = new UkViewItem(listview, cfg, log);
 
    // root->setOpen(true);
}


void UkMainWin::do_undo()
{
#if 0
    if (undolevels->isempty() || undo_finger && !undo_finger->next)
        return;
    
    if (!undo_finger)
        undo_finger = &undolevels->head;
        
    undo_finger = undo_finger->next;

    cfg.hold_delta();

    //FIXME: why is this double-dereferenced?  Make it &todo, at least!
    undostruct *todo = (undostruct *)undo_finger->data;
    WvString redovalue = cfg.xget((*todo)->full_key);
    cfg.xset((*todo)->full_key, (*todo)->old_value);
    todo->old_value = new WvString(redovalue.unique());
    
    populateNode(*todo->full_key);

    cfg.unhold_delta();
#endif
}

	      
void UkMainWin::do_redo()
{
#if 0
    if (!undo_finger || undo_finger == &undolevels->head)
        return;

    undostruct *todo = (undostruct *)undo_finger->data;

    cfg.hold_delta();
    
    WvString oldvalue = cfg.xget((*todo)->full_key);
    cfg.xset((*todo)->full_key, (*todo)->old_value);
    todo->old_value = new WvString(oldvalue.unique());
    
    populateNode(*todo->full_key);
    
    WvLink *finger = &undolevels->head;
    while (finger->next != undo_finger)
    {
        finger = finger->next;
    }

    undo_finger = finger;

    cfg.unhold_delta();
#endif
}

	      
void UkMainWin::node_changed(QListViewItem *item)
{
    // FIXME
}

	      
void UkMainWin::closenode(QListViewItem *item)
{
    if (!item)
        return;
    if (item->isOpen())
	item->setOpen(false);
    else
	item->setOpen(true);
}

	      
void UkMainWin::delSubtree()
{
    UkViewItem *item = (UkViewItem *)listview->currentItem();

    QString text(WvString("Delete the key \"/%s\" and all its sub-keys?",
			  item->h.fullkey()).cstr());
    if (KMessageBox::warningContinueCancel(this, text, i18n("Warning"),
					   i18n("Delete"))
	== KMessageBox::Continue)
    {
        item->h.remove();
    }
}


void UkMainWin::addChild()
{
    UkViewItem *parent = (UkViewItem *)listview->currentItem();
    if (!parent)
	return;
    
#if 0
    UniKonfAddChild dialog(this, parentitem->h.printable().cstr(), parent);
    dialog.exec();
#endif
    
    parent->setOpen(true);
    
    UniConf newkey(parent->h["<name>"]);
    if (newkey->isnull()) // doesn't already exist
    {
	newkey.setme("<value>");
	if (!newkey->isnull()) // still doesn't exist??
	{
	    UkViewItem *item = new UkViewItem(parent, newkey, log);
	    listview->setCurrentItem(item);
	}
    }
}


void UkMainWin::rightButtonPressed(QListViewItem *item, const QPoint &p, int)
{
    if (!item)
        return;

    QPopupMenu *entrymenu = new QPopupMenu;

    entrymenu->insertItem("Delete this entry...",
			  this, SLOT(delSubtree()), 0, 1);
    entrymenu->insertItem("Insert child...", this, SLOT(addChild()), 0, 2);

    entrymenu->exec(p,0);
    delete entrymenu;
}
