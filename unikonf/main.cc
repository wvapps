/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2002 Net Integration Technologies, Inc.
 */
#include "unikonf.h"
#include "wvistreamlist.h"
#include "wvqtstreamclone.h"
#include <kcmdlineargs.h>
#include <kaboutdata.h>
#include <kapplication.h>

int main(int argc, char *argv[])
{
    KAboutData about("unikonf", I18N_NOOP("UniKonf"), VERSION,
		     I18N_NOOP("UniConf Browser for KDE"),
		     KAboutData::License_LGPL,
		     "(c) 2002 Net Integration Technologies Inc.", 0,
		     "http://open.nit.ca/wiki/?UniConf");
    about.addAuthor("Technical Support", 0, "support@net-itech.com");

    KCmdLineArgs::init(argc, argv, &about);
    KApplication app;
    
    WvIStreamList *l = new WvIStreamList;
    WvQtStreamClone qtclone(l, 1000);
    
    UniKonf *top_window = new UniKonf;
    app.setMainWidget(top_window);
    top_window->resize(640,430);
    top_window->show();

    return app.exec();
}
