/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * See wvsyncobj.h for details.
 *
 */

// Uncomment this to debug...
//#define RSYNC_TRACE 1

#include <string.h>
#include "wvsyncobj.h"

// FIXME: This "C" wrapper must be here until the librsync people fix their
// header file.
extern "C" {
    #include <librsync.h>
}


#define CIRCULAR_SIZE   16384       // pretty arbitrary
#define CIRCULAR_THRESH 8192        // should be half of CIRCULAR_SIZE
#define OUTBUF_MAX      1048576     // 1 meg


// given the size of the object we're generating a signature for, produce
// an optimal blocksize.  Currently we use the square root of the
// approximate size of the object, as suggested by the librsync authors.
// This is bounded by 1024 bytes and 65536 bytes, for the sake of sanity.
static size_t optimal_blocksize(off_t approxsize)
{
    if (approxsize < 0)
        return RS_DEFAULT_BLOCK_LEN;

    // do the calculations in divided-by-1024 land, since it's easier.
    // dealing with a blocksize that isn't a multiple of 1 kb seems a
    // little silly anyway.
    approxsize /= 1048576;      // squary squary

    size_t k;

    if (approxsize <= 1)
        k=1;
    else if (approxsize >= 4096)
        k=64;
    else
    {
        k=0;
        size_t thisbit=32;
        size_t ksq;
        size_t ksqplus;
        do {
            size_t test = k | thisbit;
            if (test*test < approxsize)
                k = test;
            ksq = k*k;
            ksqplus = (k+1)*(k+1);
            if (thisbit == 0)
                break;
            thisbit /= 2;
        } while (ksq > approxsize || ksqplus <= approxsize);
    }
    
    return k * 1024;
}


WvSyncObj::WvSyncObj(WvStringParm _name)
: name(_name), log("WvSyncObj", WvLog::Debug5), err(log.split(WvLog::Error))
{
}


/** jobiter() is used internally.  It's a wrapper for rs_job_iter() to run
 * a job with handy debugging output on the buffering, and to print an
 * error message when something goes wrong.
 */
static rs_result jobiter(WvStringParm id, rs_job_t *job,
                         rs_buffers_t *bufs, WvLog &log)
{
    rs_result res;

#if RSYNC_TRACE
    log("=========== %s\n", id);
    log("avail_in=%s eof_in=%s avail_out=%s\n",
        bufs->avail_in, bufs->eof_in, bufs->avail_out);
#endif

    res = rs_job_iter(job, bufs);

#if RSYNC_TRACE
    log("avail_in=%s eof_in=%s avail_out=%s res=%s\n",
        bufs->avail_in, bufs->eof_in, bufs->avail_out, res);
    log("===========\n\n");
#endif
    if (res > 2)                            // see librsync.h
        log(WvLog::Error, "Uh oh: %s\n", rs_strerror(res));

    return res;
}


/** _jobloop() is used internally, and is a big loop.  If no "inbuf" or
 * "infile" is given, it goes through the entire contents of the object
 * (using getdata()), runs a given job (using jobiter()) and a
 * WvCircularBuf to buffer the input, placing the entire set of results in
 * "out".
 *
 * If an "inbuf" is given, that is used as the input for the job, and
 * getdata() is *not* called.
 * 
 * If an "infile" is given, that file is used as the input for the job, and
 * getdata() is *not* called.  This is mainly used for input deltas, which
 * can be quite large, and might be stored in a file in that case.
 */ 
static rs_result _jobloop(WvStringParm id, rs_job_t *job,
                          WvBuf *inbuf, WvFile *infile, WvBuf &out,
                          WvSyncObj &obj, WvLog &log, WvSyncCallback cb)
{
    rs_buffers_t bufs;
    memset(&bufs, '\0', sizeof(rs_buffers_t));

    // use a new circular buffer if no "inbuf" was given, and use
    // getdata() to fill it as we go (as in getsig and makedelta.)
    // If an "inbuf" was given, just use that, and don't call getdata()
    // (as in applydelta).
    // If an "infile" was given, just use that, and don't call getdata()
    // (as in applydelta).
    WvCircularBuf circ(CIRCULAR_SIZE);
    WvBuf &myinbuf = inbuf ? *inbuf : circ;

    rs_result res = RS_DONE;

    // loop through the data, calling getdata() if appropriate shoving the
    // results into out.
    bool done = false;
    off_t ofs = 0;

    // if we're not calling getdata() or reading from infile, we're already
    // done.
    if (inbuf)
        done = true;

    while (myinbuf.used() || !done || res != RS_DONE)
    {
        size_t avail_in = myinbuf.used();

        // get more data?
        if (!inbuf && !infile && !done && (avail_in < CIRCULAR_THRESH))
        {
            done = obj.getdata(myinbuf, ofs, CIRCULAR_THRESH);
            ofs += (myinbuf.used() - avail_in);
            avail_in = myinbuf.used();
        }
        else if (infile && !done && (avail_in < CIRCULAR_THRESH))
        {
            done = !infile->read(myinbuf, CIRCULAR_THRESH);
            avail_in = myinbuf.used();
        }

        // process some data?
        bufs.next_in = (char *) myinbuf.get(avail_in);  // might be NULL
        bufs.avail_in = avail_in;                       // might be 0
        bufs.eof_in = (done && !myinbuf.used());
        size_t try_out = CIRCULAR_SIZE;     // FIXME?
        bufs.next_out = (char *) out.alloc(try_out);
        bufs.avail_out = try_out;

        res = jobiter(id, job, &bufs, log);

        // we probably didn't use all that buffer space
        out.unalloc(bufs.avail_out);
        myinbuf.unget(bufs.avail_in);

        // is the out buffer getting sorta full?
        if (cb && out.used() > OUTBUF_MAX)
            cb(obj, out);
    }

    return res;
}


/** assorted stubs for _jobloop(), for convenience */
static rs_result jobloop(WvStringParm id, rs_job_t *job, WvBuf &out,
                         WvSyncObj &obj, WvLog &log, WvSyncCallback cb=0)
{
    return _jobloop(id, job, NULL, NULL, out, obj, log, cb);
}


static rs_result jobloop(WvStringParm id, rs_job_t *job,
                         WvBuf &inbuf, WvBuf &out,
                         WvSyncObj &obj, WvLog &log, WvSyncCallback cb=0)
{
    return _jobloop(id, job, &inbuf, NULL, out, obj, log, cb);
}


static rs_result jobloop(WvStringParm id, rs_job_t *job,
                         WvStringParm infile, WvBuf &out,
                         WvSyncObj &obj, WvLog &log, WvSyncCallback cb=0)
{
    WvFile f(infile, O_RDONLY);
    if (f.isok())
    {
        rs_result res = _jobloop(id, job, NULL, &f, out, obj, log, cb);
        f.close();
        return res;
    }
    return RS_IO_ERROR;
}


int WvSyncObj::getsig(WvBuf &out)
{
    size_t blocksize = optimal_blocksize(approxsize());
    rs_job_t *job;
    rs_result res;

#if RSYNC_TRACE
    log("Using blocksize %s\n", blocksize);
#endif

    job = rs_sig_begin(blocksize, RS_DEFAULT_STRONG_LEN);
    res = jobloop("sig", job, out, *this, log);
    rs_job_free(job);

    return res;
}


int WvSyncObj::makedelta(WvBuf &in, WvBuf &out, WvSyncCallback cb)
{
    if (in.used() <= 0)
    {
        err("makedata() called with no signature\n");
        return RS_CORRUPT;  // or something
    }

    rs_job_t *job;
    rs_signature_t *somesig;
    rs_buffers_t bufs;
    rs_result res = RS_DONE;

    // must first pull the (remote) signature out of "in", load it into
    // "somesig", and build the librsync hash table.
    job = rs_loadsig_begin(&somesig);
    memset(&bufs, '\0', sizeof(rs_buffers_t));

    // it's reasonably easy to load the signature, because we already have
    // the entire thing.
    size_t sigsize = in.used();
    bufs.next_in = (char *) in.get(sigsize);
    bufs.avail_in = sigsize;
    bufs.eof_in = true;
    bufs.next_out = NULL;   // there's no output for loadsig.
    bufs.avail_out = 0;
    do
    {
        res = jobiter("loadsig", job, &bufs, log);
    }
    while( res != RS_DONE );

    // clean up that job
    rs_job_free(job);

    // build the hash table from the signature we just loaded
    res = rs_build_hash_table(somesig);
    if (res > 2)                            // see librsync.h
        err("Error building hash table: %s\n", rs_strerror(res));

    // generate the delta and stick it in the "out" buffer.
    // we'll do the same circular buffer trick as in getsig(), using
    // getdata() the same way.
    job = rs_delta_begin(somesig);
    res = jobloop("delta", job, out, *this, log, cb);
    rs_job_free(job);
    
    return res;
}


// librsync uses this function signature for its callback in the patch job.
// we'll just wrap getdata() inside it.
static rs_result getdata_wrapper(void *arg, rs_long_t pos,
                                 size_t *len, void **buf)
{
#if RSYNC_TRACE
    printf( "--> getdata_wrapper() called\n" );
#endif

    WvSyncObj *obj = (WvSyncObj *)arg;

    assert(obj && len && buf && *buf);

    WvInPlaceBuf wvbuf(*buf, 0, *len);
    bool done = obj->getdata(wvbuf, pos, *len);

    if (done)
        return RS_INPUT_ENDED;
    else
    {
        *len = wvbuf.used();
        return RS_DONE;
    }
}


int WvSyncObj::applydelta(WvBuf &in, WvBuf &out, WvSyncCallback cb)
{
    rs_job_t *job;
    rs_result res;

    job = rs_patch_begin(getdata_wrapper, this);
    res = jobloop("patch", job, in, out, *this, log, cb);
    rs_job_free(job);

    return res;
}


int WvSyncObj::applydelta(WvStringParm infile, WvBuf &out, WvSyncCallback cb)
{
    rs_job_t *job;
    rs_result res;

    job = rs_patch_begin(getdata_wrapper, this);
    res = jobloop("patch", job, infile, out, *this, log, cb);
    rs_job_free(job);

    return res;
}


void WvSyncObj::forget(UniConf &cfg)
{
    // wipe any "old" information, when it becomes worthless
    cfg["old mtime"].setme(WvString::null);
    cfg["old meta"].setme(WvString::null);
    cfg["old md5sig"].setme(WvString::null);
}


void WvSyncObj::revert(UniConf &cfg)
{
    time_t mtime = cfg["old mtime"].getmeint(-1);
    WvString meta = cfg["old meta"].getme(WvString::null);
    WvString md5sig = cfg["old md5sig"].getme(WvString::null);

    if (mtime >= 0)
        cfg["mtime"].setmeint(mtime);
    if (!!meta)
        cfg["meta"].setme(meta);
    if (!!md5sig)
        cfg["md5sig"].setme(md5sig);
}


bool WvSyncObj::revertible(UniConf &cfg)
{
    time_t mtime = cfg["old mtime"].getmeint(-1);
    WvString meta = cfg["old meta"].getme(WvString::null);
    WvString md5sig = cfg["old md5sig"].getme(WvString::null);

    return mtime >= 0 || !!meta || !!md5sig;
}
