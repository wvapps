/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * See wvsyncstring.h for details.
 *
 */

#include "wvsyncstring.h"
#include <time.h>

WvSyncString::WvSyncString(WvStringParm _name, WvStringParm _str, time_t mtime)
: WvSyncObj(_name), str(_str)
{
    if (mtime == 1)
        faketime = time(NULL);
    else
        faketime = mtime;
    setlastmodtime();
}


bool WvSyncString::getdata(WvBuf &out, off_t ofs, size_t size)
{
    bool done = false;

    const char *data = str+ofs;
    if (size >= strlen(data))
    {
        size = strlen(data);
        done = true;
    }

    out.put(data, size);

    return done;
}


off_t WvSyncString::approxsize() const
{
    return str.len();
}


time_t WvSyncString::findlastmodtime() const
{
    return faketime;
}


bool WvSyncString::installnew(WvStringParm fname, WvStringParm newmeta,
                              time_t newmtime)
{
    WvFile f(fname, O_RDONLY);
    WvDynBuf buf;
    if (!f.isok())
        return false;

    while (f.isok())
    {
        if (f.select(-1, true, false))
            f.read(buf, 1024);
    }

    str = (const char *) buf.get(buf.used());
    
    applymeta(newmeta, newmtime);

    return true;
}

bool WvSyncString::isok() const
{
    return (str != "(null)");
}

WvString WvSyncString::string() const
{
    return str;
}

