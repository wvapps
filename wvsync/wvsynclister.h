/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * WvSyncLister is the base-class interface for iterating through a set of
 * named objects and feeding them to the WvSyncProtocol.  You will want to
 * derive from this for each type of object you synchronize, as I'm sure
 * you will iterate through them differently.
 *
 * For example, WvSyncFileLister traverses a filesystem and synchronizes
 * WvSyncFile objects.
 *
 */

#ifndef __WVSYNCLISTER_H
#define __WVSYNCLISTER_H

#include <wvstring.h>
#include <wvstringtable.h>
#include <wvlog.h>

class WvSyncProtocol;
class WvSyncObj;
class UniConf;

class WvSyncLister
{
public:
    WvSyncLister(WvSyncProtocol &_prot);
    virtual ~WvSyncLister() {}

    /** run() makes everything happen.  It iterates through the collection
     * of named objects, and tells the protocol to synchronize each one by
     * calling prot.do_ihave().
     */
    void run();

    /** next() is called from run(), and returns the next WvSyncObj to be
     * processed.  It is next()'s job to call prot.do_chdir() and
     * prot.do_yourturn() if and when necessary.
     * 
     * makeobj() gives you a WvSyncObj with the given name.  Used by the
     * WvSyncProtocol when receiving information about an object from the
     * remote side.
     *
     * You need to override both of these to return the correct kind of
     * object.  For example, WvSyncFileLister::next() and makeobj() should
     * give you a WvSyncFile *.
     */

    virtual WvSyncObj *next() = 0;
    virtual WvSyncObj *makeobj(WvStringParm name) = 0;

    /** newcwd() is called by the WvSyncProtocol when it receives a CHDIR
     * command from the remote side.  It is our job to set up the
     * whatever needs setting up (control files, uniconf trees) in this
     * directory.
     *
     * Returns true on success.
     *
     * Must override this, since I don't know how your storage space is
     * laid out.  See WvSyncFileLister::newcwd() for an example.
     */
    virtual bool newcwd(WvStringParm dir) = 0;

    /** markdone() marks the object with the given name as done, so we
     * don't need to sync it again.
     *
     * isdone() returns true if the object with the given name has been
     * marked as done.
     *
     * cleardone() clears the list of objects marked done.  Called on a
     * chdir, for example.
     */
    void markdone(WvStringParm name);
    bool isdone(WvStringParm name);
    void cleardone();

protected:
    void do_chdir(WvStringParm dir);
    
    WvSyncProtocol &prot;

private:
    WvLog log, err;

    WvStringTable donetbl;

    WvSyncObj *curobj;
};

#endif // __WVSYNCLISTER_H
