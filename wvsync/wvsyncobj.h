/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * WvSyncObj is the base-class interface for a named object that can be
 * replicated using WvSync and its protocol.  You can't actually make a
 * WvSyncObj, nor would it be enough; you have to derive a subclass for
 * each type of object you plan on synchronizing, and instantiate those.
 *
 */

#ifndef __WVSYNCOBJ_H
#define __WVSYNCOBJ_H

#include <wvbuf.h>
#include <wvstring.h>
#include <wvlog.h>
#include <wvfile.h>
#include <uniconf.h>

class WvSyncObj;
typedef WvCallback<void, WvSyncObj&, WvBuf&> WvSyncCallback;

class WvSyncObj
{
public:
    WvSyncObj(WvStringParm _name);
    virtual ~WvSyncObj() {}

    WvString name;      // every replicable object has a name

    /** getsig() generates a librsync signature for the object.  It
     * currently doesn't block; it'll only return when the entire signature
     * has been calculated.
     *
     * Returns 0 on success, else error.
     *
     * The signature is placed in the "out" buffer.  Please make sure your
     * buffer is big enough!  Using WvDynBuf might be best.
     */
    int getsig(WvBuf &out);

    /** makedelta() generates a librsync delta, based on a signature
     * received from the remote side (in the "in" buffer).
     *
     * The delta, placed into the "out" buffer, when passed back to the
     * remote side, can transform its copy of the object into a duplicate
     * of the copy we have on this side.
     *
     * If cb is given, it will be called when the output buffer is getting
     * pretty full and you might want to do something with it.
     *
     * Returns 0 on success, else error.
     */
    int makedelta(WvBuf &in, WvBuf &out, WvSyncCallback cb = 0);

    /** applydelta() takes a librsync delta received from the remote side
     * in the "in" buffer, applies it to the local version of the data, and
     * places the updated data in the "out" buffer.
     *
     * If cb is given, it will be called when the output buffer is getting
     * pretty full and you might want to do something with it.
     *
     * Returns 0 on success, else error.
     *
     * The second variant of applydelta() takes a file instead of an input
     * buffer.  If your makedelta callback decided the delta was huge and
     * wrote it to a file, you can apply it directly with this.
     */
    int applydelta(WvBuf &in, WvBuf &out, WvSyncCallback cb = 0);
    int applydelta(WvStringParm in, WvBuf &out, WvSyncCallback cb = 0);

    virtual bool isok() const
        { return true; }

    /** getdata() is used by WvSyncObj as a means of requesting a
     * specific block of data from a subclass that implements this.
     * librsync in particular uses this when reconstructing updated data
     * from a delta, but we might as well also use it to get data when
     * generating a signature or a delta.
     *
     * Returns a bool indicating whether there's any data left after this
     * block.
     *
     * Takes a buffer to put the data in, the offset we want, and the size
     * we want.
     *
     * Subclasses must implement this.
     */
    virtual bool getdata(WvBuf &out, off_t ofs, size_t size) = 0;

    /** findlastmodtime() determines the last-modified time of the object.
     * This must be implemented in a subclass, so that WvSync knows how to
     * synchronize things.  Called by setlastmodtime() and
     * hasbeenmodified().
     *
     * hasbeenmodified() determines the last-modified time of the object
     * again, and returns true if it has changed.  This is used to decide
     * whether the object was stable during the sync, so that we can
     * overwrite it.
     */
    virtual time_t findlastmodtime() const = 0;
    void setlastmodtime()
        { lastmodtime = findlastmodtime(); }
    time_t getlastmodtime() const
        { return lastmodtime; }
    bool hasbeenmodified() const
        { return findlastmodtime() != getlastmodtime(); }

    /** findmeta() fetches, serializes, and returns any metadata that we
     * want to send to the other side along with the delta.  For files,
     * this would be the mode, uid, etc.  For other things, this could be
     * anything.  The default is just empty.
     */
    virtual WvString findmeta()
        { return ""; }
    void setmeta()
        { meta = findmeta(); }
    WvString getmeta() const
        { return meta; }

    /** applymeta() is called from arbremote() and friends when new meta
     * information is received from the remote end, and we had nothing else
     * to do.  Apply the meta information to the data.
     */
    virtual void applymeta(WvStringParm newmeta, time_t newmtime)
        {}

    /** isunchanged() checks the given mtime and meta information with the
     * stuff stored in the object now, and returns true if they are the
     * same.  This function can be overridden to accommodate special cases,
     * such as symlinks, where you can't change the mtime, and directories,
     * where the time doesn't matter.
     */
    virtual bool isunchanged(time_t sometime, WvStringParm somemeta) const
        { return sometime == getlastmodtime() && somemeta == getmeta(); }

    /** issyncable() returns true if this is an object that can be
     * synchronized.  If you don't override this, it will always be true.
     *
     * This is mainly useful for WvSyncFile, so that we can make sure it's
     * a regular file or symlink.
     */
    virtual bool issyncable() const
        { return true; }

    /** makecopy() makes a copy of the current object under a new name.
     * Used when there is a conflict, so the entire thing doesn't
     * necessarily have to be transmitted twice.
     *
     * You must override this.
     */
    virtual void makecopy(WvStringParm newname) const = 0;

    /** conflictname() invents a new name for an object in the event of a
     * conflict.  You can override this to accommodate whatever namespace
     * you live in.
     */
    virtual WvString conflictname(time_t sometime) const
        { return WvString("%s.%s", name, sometime); }

    /** isdir() is true if the object is a directory that contains other
     * objects, instead of having its own contents to synchronize.
     * Directories have their own metadata, but that's all.  They have
     * special properties like winning all conflicts.
     *
     * This is sort of an ugly hack for synchronizing a filesystem.  For
     * most other applications, you can leave this set to always return
     * false, and never think about it.
     *
     * The second variant analyzes metadata received from a remote object,
     * and determines whether IT is a directory.
     */
    virtual bool isdir() const
        { return false; }
    virtual bool isdir(WvStringParm somemeta) const
        { return false; }

    /** forget, revert, and revertible are used by the protocol and the
     * scanner if they decide that the current synchronization should be
     * aborted, and the old information should be restored.
     *
     * forget() erases the "old %s" entries for a given UniConf handle
     * corresponding to an object.  This should only be called when we're
     * sure we don't need the information anymore, obviously.
     *
     * revert() takes the "old %s" entries and overwrites their newer
     * counterparts.  This effectively puts back the state that was present
     * before we started synchronizing.
     *
     * revertible() is true if there are any "old %s" entries.
     */
    static void forget(UniConf &cfg);
    static void revert(UniConf &cfg);
    static bool revertible(UniConf &cfg);

    /** installnew() is called when the protocol receives a delta and
     * produces a patched version of the object.  It's your job to do the
     * right thing with the data, the metadata, and the mtime.
     *
     * FIXME: It would be nice if there was one of these that didn't use a
     * temporary file.
     *
     * Returns true on success.
     */
    virtual bool installnew(WvStringParm fname, WvStringParm newmeta,
                            time_t newmtime) = 0;

private:
    time_t lastmodtime;
    WvString meta;

    /** approxsize() is called when generating a signature, to determine
     * the optimal blocksize to use.  If you don't override this, the
     * default librsync blocksize (currently 2 kb) will be used.  That's
     * likely OK, but less than optimal for big files, of course.  It is
     * recommended that you override this and at least guess.
     *
     * A smaller blocksize gives bigger signatures, but smaller deltas.
     * A bigger blocksize gives smaller signatures, but bigger deltas.
     */
    virtual off_t approxsize() const
        { return -1; }

protected:
    WvLog log, err;
};

#endif // __WVSYNCOBJ_H
