/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 */

#ifndef __WVSYNCVER_H
#define __WVSYNCVER_H

#define WVSYNC_VER          0x00000000
#define WVSYNC_VER_STRING   "0.00 (Run!)"

#endif // __WVSYNCVER_H
