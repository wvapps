/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * See wvsynclister.h for details.
 *
 */

#include "wvsynclister.h"
#include "wvsyncobj.h"
#include "wvsyncprotocol.h"

WvSyncLister::WvSyncLister(WvSyncProtocol &_prot)
: prot(_prot), log("Lister", WvLog::Debug1), err(log.split(WvLog::Error))
{
    donetbl.zap();
}


void WvSyncLister::run()
{
    WvSyncObj *newobj = NULL;

    while (prot.isok())
    {
        if (prot.want_more() && prot.num_in_flight() < WVSYNC_MAX_IN_FLIGHT)
        {
            // sync the thing we found before we sent the yourturn
            WvSyncObj *oldobj = NULL;
            if (newobj)
                oldobj = newobj;

            newobj = next();

            if (oldobj && isdone(oldobj->name) == false)
                prot.do_ihave(oldobj);

            if (newobj)
            {
                // might not be my turn anymore!
                if (prot.want_more())
                {
                    if (isdone(newobj->name) == false)
                    {
                        prot.do_ihave(newobj);
                        newobj = NULL;
                    }
                }
            }
            else
            {
                if (prot.want_more())
                    prot.do_yourturn();

                prot.do_done();
            }
        }

        if (prot.isreadable())
            prot.callback();
    }
}


void WvSyncLister::do_chdir(WvStringParm dir)
{
    prot.do_chdir(dir);
    cleardone();
}

void WvSyncLister::markdone(WvStringParm name)
{
    donetbl.add(new WvString(name), true);
}

bool WvSyncLister::isdone(WvStringParm name)
{
    return donetbl[name];
}

void WvSyncLister::cleardone()
{
    donetbl.zap();
}
