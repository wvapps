/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * WvSyncProtocol extends the WvProtoStream interface to provide a protocol
 * for WvSync.  It connects up, and accepts hints from external objects on
 * what it ought to be synchronizing.  Give it an object, and it'll do its
 * magic.
 *
 */

#ifndef __WVSYNCPROTOCOL_H
#define __WVSYNCPROTOCOL_H

#include <wvprotostream.h>
#include <wvscatterhash.h>
#include <uniconf.h>
#include <wvlog.h>
#include <wvfile.h>

#include "wvsyncobj.h"

#define WVSYNC_MAX_IN_FLIGHT    (10)

class WvSyncArbiter;
class WvSyncLister;

class WvSyncProtocol : public WvProtoStream
{
public:
    WvSyncProtocol(WvStream *_cloned, WvLog *_debuglog, WvSyncArbiter &_arb,
                   UniConf &_cfgroot, bool _server);

    void set_lister(WvSyncLister *);

    /** The state machine.  These are overloaded from WvProtoStream.  In
     * particular we need to overload execute() because we want to be able
     * to receive binary data of a known size without having the
     * protostream attempt to parse it.
     */
    virtual void do_state(Token &t1);
    virtual void switch_state(int newstate);
    virtual void execute();
    virtual WvString getcwd() const
        { return cwd; }

    /** Called by WvSync*Lister, do_ihave() informs the protocol that it
     * should process the given object.  Sends an IHave command, etc.
     *
     * Similarly, do_chdir() informs the protocol that the lister has moved
     * to a new directory, and it should tell the other side.  Sends a
     * CHDIR command, etc.
     */
    void do_ihave(WvSyncObj *obj);
    void do_chdir(WvStringParm _cwd);
    void do_yourturn();
    void do_done();

    int num_in_flight() const
        { return in_flight.count(); }

    /** want_more() tells the WvSyncLister whether it should do_ihave()
     * another file.
     */
    bool want_more()
        { return myturn; }

    enum States
    {
        // shared
        None = 0,
        Ready,
        SentIHave,
        ExpectSig,
        ReceiveSig,
        ExpectDelta,
        ReceiveDelta,
        ExpectDone,

        // client
        Greeting
    };

    enum Commands
    {
        cHello = 0, cOK, cError, cTrivia, cHelp, cChdir, cIHave, cSig, cDelta,
        cDonewith, cAbort, cYourturn, cQuit, cHaveRep, cDone
    };
    static char *cmdstr[];

private:
    WvSyncArbiter &arb;
    UniConf cfgroot;
    WvSyncLister *lister;
    WvString cwd;
    int uid;
    bool myturn;
    bool myturn_soon;
    bool server;
    bool greeted;
    bool locdone, remdone;

    WvLog log, err;

public:
    struct ProtoObj
    {
        ~ProtoObj() { delete obj; }
        WvString id;
        WvString name;
        WvSyncObj *obj;
        UniConf cfg;
        WvDynBuf sigbuf;
        WvDynBuf mysigbuf;
        off_t signeed;
        WvDynBuf deltabuf;
        WvDynBuf mydeltabuf;
        off_t deltaneed;
        WvFile tmpfile;
    };
private:
    //ProtoObj curobj;
    DeclareWvScatterDict(ProtoObj, WvString, id);
    ProtoObjDict in_flight;
    ProtoObj *binaryobj;

    // states
    void do_ready(int cmd);
    void do_sentihave(int cmd);
    void do_expectsomething(int cmd);

    // helpers
    WvString gen_id();
    void cleanup();
    bool newcwd(WvStringParm dir);
    void makedelta(ProtoObj &);
    void applydelta(ProtoObj &);
    void send_abort(ProtoObj *, WvStringParm);
    void send_greeting();
    void send_sig(ProtoObj &);
    void remove_obj(ProtoObj *);

    void exec_ihave(WvSyncObj &obj);
    void exec_chdir(WvStringParm _cwd, UniConf root);
    void exec_yourturn();

    // command
    void do_cabort();
    void do_cquit();
    void do_cchdir();
    void do_cihave();
    void do_cbinary(Commands);
    void do_cdonewith();
    void do_cyourturn();
    void do_chaverep();
    void do_cdone();
};

#endif // __WVSYNCPROTOCOL_H
