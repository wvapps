/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * WvSyncArbiter makes decisions based on input WvSyncObj's.  This is the
 * brains of WvSync.
 *
 */

#ifndef __WVSYNCARBITER_H
#define __WVSYNCARBITER_H

#include <wvlog.h>

class UniConf;
class WvSyncObj;

class WvSyncArbiter
{
public:
    WvSyncArbiter()
        : log("Sync Arbiter", WvLog::Debug), err(log.split(WvLog::Error))
        {}

    /** arblocal() takes a UniConf handle and a WvSyncObj representing a
     * local object.  If the object was already mentioned in the UniConf
     * tree, then we know this isn't a *new* object.  If it is new, we add
     * it to the tree.  We get the librsync signature for the object, and
     * store it and a few other tidbits in the tree as well.  arbremote()
     * uses this info.
     *
     * The librsync signature is shoved into sigbuf.
     *
     * returns true if all is well, and false if getsig failed, implying
     * the data couldn't be read.
     */
    bool arblocal(UniConf &root, WvSyncObj &obj, WvBuf &sigbuf);
    
    /** arbremote() takes a UniConf handle and a WvSyncObj representing a
     * local object.  It also takes various information from the remote
     * WvSync node about the same object.  The goal of this function is to
     * decide what needs to happen in the *exact* same way that this
     * function decides on the remote node.  This is harder than it sounds.
     *
     * returns true if we need to request a delta for this object from the
     * remote side, and false if we don't.  (if false, the other side may
     * request one from us!)
     */
    bool arbremote(UniConf &root, WvSyncObj &obj,
                   WvStringParm remact, WvStringParm remmeta, time_t remmtime,
                   WvStringParm remmd5sig);

private:
    WvLog log, err;

    /** these are helpers for arbremote()
     */
    void arbr_win(UniConf &root, WvSyncObj &obj,
                  WvStringParm remact, WvStringParm remmeta, time_t remmtime,
                  WvStringParm remmd5sig);
    void arbr_lose(UniConf &root, WvSyncObj &obj,
                   WvStringParm remact, WvStringParm remmeta, time_t remmtime,
                   WvStringParm remmd5sig);
    bool arbr_conflict(UniConf &root, WvSyncObj &obj,
                       WvStringParm remact, WvStringParm remmeta,
                       time_t remmtime, WvStringParm remmd5sig);
    bool arbr_ok(UniConf &root, WvSyncObj &obj,
                 WvStringParm remact, WvStringParm remmeta, time_t remmtime,
                 WvStringParm remmd5sig);
    bool arbr_mod(UniConf &root, WvSyncObj &obj,
                  WvStringParm remact, WvStringParm remmeta, time_t remmtime,
                  WvStringParm remmd5sig);
    bool arbr_meta(UniConf &root, WvSyncObj &obj,
                   WvStringParm remact, WvStringParm remmeta, time_t remmtime,
                   WvStringParm remmd5sig);
    bool arbr_del(UniConf &root, WvSyncObj &obj,
                  WvStringParm remact, WvStringParm remmeta, time_t remmtime,
                  WvStringParm remmd5sig);
};

#endif // __WVSYNCARBITER_H

