/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * See wvsyncarbiter.h for details.
 *
 */

#include "wvsyncarbiter.h"
#include "wvsyncobj.h"
#include "uniconf.h"

#include <wvhex.h>
#include <wvdigest.h>

bool WvSyncArbiter::arblocal(UniConf &root, WvSyncObj &obj, WvBuf &sigbuf)
{
    // make a local handle to the file in the given tree
    UniConf cfg(root[obj.name]);
    cfg["act"].setme(WvString::null);
    
    time_t oldmtime = cfg.xgetint("mtime", -1);
    WvString oldmeta = cfg["meta"].getme();

    // remember a few old things for revertability, should a
    // transfer fail or be unacceptable.
    if (oldmtime > 0)
        cfg["old mtime"].setmeint(oldmtime);
    cfg["old meta"].setme(oldmeta);
    cfg["old md5sig"].setme(cfg["md5sig"].getme());

    if (oldmtime < 0)
    {
        // if it didn't exist, obviously we're adding.
        sigbuf.zap();
        int sig_ret = obj.getsig(sigbuf);
        log(WvLog::Debug5, "getsig: %s\n", sig_ret);
        int siglen = sigbuf.used();

        if (obj.isok())
        {
            log(WvLog::Debug2, "      NEW: %s\n", obj.name);
            WvDynBuf tmpbuf;
            WvString md5sig;
            
            // special case: if isdir(), that's different from
            // something that DOES have contents, but 0 bytes in it!
            if (!obj.isdir())
            {
                // used to unget() here, but sometimes siglen was larger
                // than maxungettable.  Make a copy instead... (sigh)
                WvDynBuf tmpbuf2;
                tmpbuf2.put(sigbuf.peek(0, siglen), siglen);
                WvMD5Digest().flush(tmpbuf2, tmpbuf, true);
                md5sig = WvHexEncoder().strflushbuf(tmpbuf, true);
            }
            else
                md5sig = "none";

            cfg["md5sig"].setme(md5sig);
            cfg["meta"].setme(obj.getmeta());
            cfg["act"].setme("new");
            cfg["mtime"].setmeint(obj.getlastmodtime());
        }
        else
            return false;   // couldn't getsig(), so it's broken.  skip it.
    }
    else
    {
        // if it did exist in cfg, we're (potentially) changing.
        // check the mtime and signature.
        if (obj.isunchanged(oldmtime, oldmeta))
        {
            // if mtimes and meta match, nothing has changed.
            log(WvLog::Debug5, "UNCHANGED: %s\n", obj.name);
            cfg["act"].setme("ok");

            //jdeboer: Lets compute a sig, maybe good things will happen.
            //dcoombs: FIXME: why is this necessary?
            sigbuf.zap();
            obj.getsig(sigbuf);
        }
        else
        {
            // mtime mismatch, so we have to think harder.
            // if the signature has changed, the contents obviously have
            // too.  if the signatures match, but the metadata has changed,
            // then we can send just the metadata and save time.

            sigbuf.zap();
            obj.getsig(sigbuf);
            int siglen = sigbuf.used();
            WvString meta = obj.getmeta();

            if (!obj.isok())
            {
                // couldn't getsig(), I guess it was deleted.
                // should still return true!
                log(WvLog::Debug2, "     GONE: %s\n", obj.name);
                cfg["act"].setme("del");
                WvSyncObj::forget(cfg);
            }
            else
            {
                WvDynBuf tmpbuf;
                WvString md5sig;

                // special case: if isdir(), that's different from
                // something that DOES have contents, but 0 bytes in it!
                if (!obj.isdir())
                {
                    // used to unget() here, but sometimes siglen was larger
                    // than maxungettable.  Make a copy instead... (sigh)
                    WvDynBuf tmpbuf2;
                    tmpbuf2.put(sigbuf.peek(0, siglen), siglen);
                    WvMD5Digest().flush(tmpbuf2, tmpbuf, true);
                    md5sig = WvHexEncoder().strflushbuf(tmpbuf, true);
                }
                else
                    md5sig = "none";

                WvString oldmd5sig = cfg["md5sig"].getme();
                if (strcmp(md5sig, oldmd5sig))
                {
                    // signatures differ
                    log(WvLog::Debug2, "  CHANGED: %s\n", obj.name);
                    cfg["act"].setme("mod");
                    cfg["md5sig"].setme(md5sig);
                }
                else
                {
                    // signatures don't differ.  just send metadata.
                    log(WvLog::Debug2, "     META: %s\n", obj.name);
                    cfg["act"].setme("meta");
                }
                cfg["meta"].setme(meta);
                cfg["mtime"].setmeint(obj.getlastmodtime());
            }
        }
    }

    cfg.commit();
    return true;
}


bool WvSyncArbiter::arbremote(UniConf &root, WvSyncObj &obj,
                              WvStringParm remact, WvStringParm remmeta,
                              time_t remmtime, WvStringParm remmd5sig)
{
    // make a local handle to the file in the given tree
    UniConf cfg(root[obj.name]);

    // First question: Did the remote end know about this object?  If not,
    // remmtime will be -1.
    if (remmtime < 0)
    {
        // If the other side didn't know about it, we obviously can't
        // revert our own, even if we thought we could.  And if it's
        // locally anything other than delete, we might as well call it
        // new, since the other side will.
        WvSyncObj::forget(cfg);

        if (cfg["act"].getme() != "del")
            cfg["act"].setme("new");

        cfg.commit();
        return false;   // won't request
    }

    // So the remote side knew about it.

    // Next question: Did WE know about it?  If not, obj.isok() will be
    // false because we will have failed to calculate the signature.  In
    // that case, we should consider it new from remote, and request it,
    // unless the remote side is deleting it.
    if (!obj.isok())
    {
        if (remact != "del")
        {
            log(WvLog::Debug1, "NEW FROM REMOTE: %s\n", obj.name);
            cfg["md5sig"].setme(remmd5sig);
            cfg["mtime"].setme(remmtime);
            cfg["meta"].setme(remmeta);
            cfg["act"].setme("new");
            cfg.commit();
            return true;    // will request
        }
        cfg.commit();
        return false;       // do nothing
    }

    // OK, both sides knew about the object.  Think harder.
    WvString act(cfg["act"].getme("ok"));
    bool ret;
    if (act == "new" || act == "mod")
        ret = arbr_mod(root, obj, remact, remmeta, remmtime, remmd5sig);
    else if (act == "meta")
        ret = arbr_meta(root, obj, remact, remmeta, remmtime, remmd5sig);
    else if (act == "del")
        ret = arbr_del(root, obj, remact, remmeta, remmtime, remmd5sig);
    else
        ret = arbr_ok(root, obj, remact, remmeta, remmtime, remmd5sig);

    cfg.commit();
    return ret;
}


void WvSyncArbiter::arbr_win(UniConf &root, WvSyncObj &obj,
                             WvStringParm remact, WvStringParm remmeta,
                             time_t remmtime, WvStringParm remmd5sig)
{
    // make a local handle to the file in the given tree
    UniConf cfg(root[obj.name]);

    if (cfg["act"].getme() != "new" || remact != "new")
        cfg["act"].setme("mod");

    // if we win, the remote side will request from us.
    // but we can't revert, because the remote side has renamed its copy.
    WvSyncObj::forget(cfg);
    
    // find out the name of the renamed version we have to download.
    WvString newname = obj.conflictname(remmtime);
    log(WvLog::Warning, "CONFLICT: remote '%s' will be called '%s'\n",
                        obj.name, newname);
    wvcon->print("FIXME: tell protocol it has to download %s\n", newname);

    // we copy the information (mtime, md5sig, meta) from our own winning
    // object, since we're in fact copying the winning object to give us
    // something to rsync.  when we request the renamed version later,
    // we'll figure stuff out.
    obj.makecopy(newname);
    UniConf newcfg(root[newname]);
    cfg.copy(newcfg, false);
    newcfg["act"].setme("ok");
    WvSyncObj::forget(newcfg);
}


void WvSyncArbiter::arbr_lose(UniConf &root, WvSyncObj &obj,
                              WvStringParm remact, WvStringParm remmeta,
                              time_t remmtime, WvStringParm remmd5sig)
{
    // make a local handle to the file in the given tree
    UniConf cfg(root[obj.name]);

    if (cfg["act"].getme() != "new" || remact != "new")
        cfg["act"].setme("mod");

    // if we lose, rename the object on this end, and mark it new.
    WvString newname = obj.conflictname(obj.getlastmodtime());
    log(WvLog::Warning, "CONFLICT:  local '%s' will be renamed to '%s'\n",
                        obj.name, newname);
    
    obj.makecopy(newname);
    UniConf newcfg(root[newname]);
    cfg.copy(newcfg, false);
    newcfg["act"].setme("new");     // redundant, since arblocal will
                                    // decide the same thing, but it helps
                                    // me think straight.
    WvSyncObj::forget(newcfg);

    // we still want to receive the winning object!
    WvSyncObj::forget(cfg);
    cfg["mtime"].setmeint(remmtime);
    cfg["md5sig"].setme(remmd5sig);
    cfg["meta"].setme(remmeta);
}


bool WvSyncArbiter::arbr_conflict(UniConf &root, WvSyncObj &obj,
                                  WvStringParm remact, WvStringParm remmeta,
                                  time_t remmtime, WvStringParm remmd5sig)
{
    // make a local handle to the file in the given tree
    UniConf cfg(root[obj.name]);

    // the most recent change wins.  If they were at the same time, pick
    // one randomly using strcmp.  Ha ha ha ha ha FIXME.

    if (obj.getlastmodtime() > remmtime)
    {
        arbr_win(root, obj, remact, remmeta, remmtime, remmd5sig);
        return false;   // we won, don't request
    }
    else if (obj.getlastmodtime() < remmtime)
    {
        arbr_lose(root, obj, remact, remmeta, remmtime, remmd5sig);
        return true;    // we lost, must probably request download
    }
    else if (strcmp(cfg["md5sig"].getme(), remmd5sig) < 0)
    {
        arbr_win(root, obj, remact, remmeta, remmtime, remmd5sig);
        return false;   // we won, don't request
    }
    else
    {
        arbr_lose(root, obj, remact, remmeta, remmtime, remmd5sig);
        return true;    // we lost, must probably request donwload
    }
}


bool WvSyncArbiter::arbr_ok(UniConf &root, WvSyncObj &obj,
                            WvStringParm remact, WvStringParm remmeta,
                            time_t remmtime, WvStringParm remmd5sig)
{
    // make a local handle to the file in the given tree
    UniConf cfg(root[obj.name]);

    // no change on our end, so:
    //  1) no change on remote end, we're fine
    //  2) remote end new/mod, we surrender
    //  3) remote end meta, we accept the new meta info
    //  4) remote end del, delete here too
    
    if (remact == "new" || remact == "mod")
    {
        if (remact == "new")
            WvSyncObj::forget(cfg);

        cfg["mtime"].setmeint(remmtime);
        cfg["md5sig"].setme(remmd5sig);
        cfg["meta"].setme(remmeta);
        cfg["act"].setme(remact);

        // if the object has changed in nature so that it no longer has
        // contents, consider that a conflict.  The one with no contents
        // wins.  This is a little random, but it's probably only used for
        // synchronizing files, where something with no contents is a
        // directory, and when directories turn into files bad things can
        // happen.
        if (!obj.isdir() && obj.isdir(remmeta))
        {
            arbr_lose(root, obj, remact, remmeta, remmtime, remmd5sig);
            return true;    // we lost, must request download
        }
        else if (obj.isdir() && !obj.isdir(remmeta))
        {
            arbr_win(root, obj, remact, remmeta, remmtime, remmd5sig);
            return false;   // we won, don't request
        }

        // no "conflict"
        return true;    // will request
    }

    if (remact == "del")
    {
        cfg["act"].setme(remact);
        return false;   // nothing to request
    }

    if (remact == "meta")
    {
        cfg["meta"].setme(remmeta);
        cfg["mtime"].setmeint(remmtime);
        obj.applymeta(remmeta, remmtime);
        return false;   // don't need to request the actual data
    }

    return false;       // don't need to request anything
}


bool WvSyncArbiter::arbr_mod(UniConf &root, WvSyncObj &obj,
                             WvStringParm remact, WvStringParm remmeta,
                             time_t remmtime, WvStringParm remmd5sig)
{
    // make a local handle to the file in the given tree
    UniConf cfg(root[obj.name]);

    // changed on our end, so
    //  1) no change on remote end, it'll surrender
    //  2) remote end new/mod, check for conflict...
    //  3) remote end meta, conflict
    //  4) remote end del, it'll forget it

    // can't revert if it's new
    if (cfg["act"].getme() == "new")
        WvSyncObj::forget(cfg);

    if (remact == "meta")
    {
        // if it was changed locally, and only the metadata changed on the
        // remote end, I guarantee you have a conflict.
        return arbr_conflict(root, obj, remact, remmeta, remmtime, remmd5sig);
    }

    if (remact == "new" || remact == "mod")
    {
        // if the object has changed in nature so that it no longer has
        // contents, consider that a conflict.  The one with no contents
        // wins.  This is a little random, but it's probably only used for
        // synchronizing files, where something with no contents is a
        // directory, and when directories turn into files bad things can
        // happen.
        if (!obj.isdir() && obj.isdir(remmeta))
        {
            arbr_lose(root, obj, remact, remmeta, remmtime, remmd5sig);
            return true;    // we lost, must request download
        }
        else if (obj.isdir() && !obj.isdir(remmeta))
        {
            arbr_win(root, obj, remact, remmeta, remmtime, remmd5sig);
            return false;   // we won, don't request
        }

        // no conflict, at least not due to type change

        // if the md5sigs and metadata match, the same change was made on
        // both ends!  just update the mtime in that case.
        if (cfg["md5sig"].getme() == remmd5sig
            && obj.getmeta() == remmeta)
        {
            // most recent mtime and meta information wins
            if (obj.getlastmodtime() < remmtime)
            {
                cfg["mtime"].setmeint(remmtime);
                obj.applymeta(remmeta, remmtime);   // remmeta was the same
            }

            return false;
        }
        else
        {
            // if there was an md5sig or metadata conflict...
            return arbr_conflict(root, obj, remact, remmeta,
                                 remmtime, remmd5sig);
        }
    }

    if (remact == "del")
    {
        // no point being revertible if other side isn't
        WvSyncObj::forget(cfg);
    }

    return false;       // other side is requesting, not us
}


bool WvSyncArbiter::arbr_meta(UniConf &root, WvSyncObj &obj,
                              WvStringParm remact, WvStringParm remmeta,
                              time_t remmtime, WvStringParm remmd5sig)
{
    // make a local handle to the file in the given tree
    UniConf cfg(root[obj.name]);

    // metadata changed on our end, so
    //  1) no change on remote end, it'll surrender
    //  2) remote end new/mod, conflict
    //  3) remote end meta, check for conflict...
    //  4) remote end del, it'll forget it

    if (remact == "new" || remact == "mod")
    {
        // if it was changed remotely, and only the metadata changed on
        // this end, I guarantee you have a conflict.
        return arbr_conflict(root, obj, remact, remmeta, remmtime, remmd5sig);
    }

    if (remact == "meta")
    {
        // if the metadata matches, the same change was made on both ends!
        // just update the mtime in that case.
        if (obj.getmeta() == remmeta)
        {
            // most recent mtime and meta information wins
            if (obj.getlastmodtime() < remmtime)
            {
                cfg["mtime"].setmeint(remmtime);
                obj.applymeta(remmeta, remmtime);   // remmeta was the same
            }

            return false;
        }
        else
        {
            // if there was a metadata conflict...
            return arbr_conflict(root, obj, remact, remmeta,
                                 remmtime, remmd5sig);
        }
    }

    if (remact == "del")
    {
        // no point being revertible if other side isn't
        WvSyncObj::forget(cfg);
    }

    return false;
}


bool WvSyncArbiter::arbr_del(UniConf &root, WvSyncObj &obj,
                             WvStringParm remact, WvStringParm remmeta,
                             time_t remmtime, WvStringParm remmd5sig)
{
    // make a local handle to the file in the given tree
    UniConf cfg(root[obj.name]);

    // deleted on our end, so
    //  1) no change on remote end, it'll delete
    //  2) remote end new/mod, forget the delete, surrender
    //  3) remote end meta, be safe, forget the delete, surrender
    //  4) remote end del, we're fine

    WvSyncObj::forget(cfg);

    if (remact == "new" || remact == "mod" || remact == "meta")
    {
        cfg["mtime"].setmeint(remmtime);
        cfg["md5sig"].setme(remmd5sig);
        cfg["meta"].setme(remmeta);
        cfg["act"].setme(remact);

        return true;    // will request
    }

    return false;
}
