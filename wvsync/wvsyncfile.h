/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * WvSyncFile provides the WvSyncObj implementation necessarily to
 * conduct librsync operations on a standard file.
 *
 * Note that I use the regular ::open() and ::read() calls, rather than
 * using WvFile, because I don't need or particularly want the buffer, and
 * I need to be able to lseek().
 */

#ifndef __WVSYNCFILE_H
#define __WVSYNCFILE_H

#include "wvsyncobj.h"

class WvSyncFile : public WvSyncObj
{
public:
    WvSyncFile(WvStringParm _name, WvStringParm _rootdir);

    virtual bool isok() const
        { return ok; }

    virtual time_t findlastmodtime() const;

    /** metadata for WvSyncFile looks like this:
     *      mode uidname uid gidname gid
     *  eg:
     *      49152:dcoombs:1000:weaver:4242
     */
    virtual WvString findmeta();
    virtual void applymeta(WvStringParm newmeta, time_t newmtime);

    /** override isunchanged() to accommodate symlinks, where you can't
     * change the mtime.
     */
    virtual bool isunchanged(time_t sometime, WvStringParm somemeta) const;

    /** issyncable() should return true for a regular file, a symlink, or a
     * directory, and false for everything else.  If the file doesn't
     * exist, we can still sync it down, so check for that too.
     */
    virtual bool issyncable() const
        { return S_ISREG(mode) || S_ISLNK(mode) || S_ISDIR(mode) || mode<0; }

    virtual void makecopy(WvStringParm newname) const;

    virtual bool isdir() const;
    virtual bool isdir(WvStringParm somemeta) const;

    virtual bool installnew(WvStringParm fname, WvStringParm newmeta,
                            time_t newmtime);

private:
    WvString rootdir;
    int fd;
    off_t myofs;
    bool ok;

    time_t mtime;
    mode_t mode;
    off_t  size;
    uid_t  uid;
    gid_t  gid;

    virtual bool getdata(WvBuf &out, off_t ofs, size_t size);

    virtual off_t approxsize() const;

    WvString realname() const
        { return WvString("%s/%s", rootdir, name); }
};

#endif // __WVSYNCFILE_H
