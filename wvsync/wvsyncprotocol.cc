/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * See wvsyncprotocol.h for details.
 *
 */

#include "wvsyncprotocol.h"
#include "wvsyncobj.h"
#include "wvsyncarbiter.h"
#include "wvsyncver.h"
#include "wvsynclister.h"
#include "wvsyncstring.h"

#include <wvcallback.h>

char *WvSyncProtocol::cmdstr[] = {
    "HELLO", "OK", "ERROR", "!!", "help", "chdir", "ihave", "sig", "delta",
    "donewith", "abort", "yourturn", "quit", "haverep", "done",
    NULL
};


WvSyncProtocol::WvSyncProtocol(WvStream *_cloned, WvLog *_debuglog,
                               WvSyncArbiter &_arb, UniConf &_cfgroot,
                               bool _server)
: WvProtoStream(_cloned, _debuglog), arb(_arb), cfgroot(_cfgroot),
    log("Net", WvLog::Debug1), err(log.split(WvLog::Error))
{
    server = _server;
    myturn = !server;
    myturn_soon = false;
    cwd = "";
    greeted = false;
    lister = 0;
    binaryobj = 0;
    uid = 0;
    locdone = remdone = false;

    if (server)
        send_greeting();
}


void WvSyncProtocol::set_lister(WvSyncLister *_lister)
{
    lister = _lister;
}


void WvSyncProtocol::do_state(Token &t1)
{
    int cmd = tokanal(t1, cmdstr);

    if (cmd < 0)
    {
        print("ERROR Unknown command \"%s\"\n", t1.data);
        return;
    }

    // some commands always work, regardless of state
    if (cmd == cTrivia)
        return; // always ignore trivia
    else if (cmd == cHelp)
    {
        print("OK Known commands: chdir, ihave, sig, delta, donewith, "
              "abort, yourturn, quit\n");
        return;
    }
    else if (cmd == cAbort)
        do_cabort();
    
    else if (cmd == cQuit)
        do_cquit();
    
    else if (cmd ==  cChdir)
        do_cchdir();
    
    else if (cmd == cIHave)
        do_cihave();

    else if (cmd == cHaveRep)
        do_chaverep();
    
    else if (cmd == cSig || cmd == cDelta)
        do_cbinary((Commands)cmd);
    
    else if (cmd == cDonewith)
        do_cdonewith();
    
    else if (cmd == cYourturn)
        do_cyourturn();
    
    else if (cmd == cHello)
        greeted = true;

    else if (cmd == cDone)
        do_cdone();
    
    if (!greeted)
    {
        err("Server didn't say hello.  Dying...\n");
        close();
    }
}


void WvSyncProtocol::switch_state(int newstate)
{
    state = newstate;
}


void WvSyncProtocol::execute()
{
    log(WvLog::Debug5, "Started execute state=%s\n", state);

    if (state == ReceiveSig || state == ReceiveDelta)
    {
        assert(binaryobj);
        log(WvLog::Debug, "Loading into %s\n", binaryobj->name);
        WvDynBuf &buf  = (state==ReceiveSig) ? 
            binaryobj->sigbuf  : binaryobj->deltabuf;
        off_t &need = (state==ReceiveSig) ? 
            binaryobj->signeed : binaryobj->deltaneed;
        log(WvLog::Debug5, "Waiting on %s bytes\n", need);
        int n = read(buf, need);
        log(WvLog::Debug5, "AA Buf now has %s bytes\n", buf.used());
        
        if (n == 0) {    // closed? NO!
            return;
        }

        need -= n;

        // write the delta to a file... although it'd be great if we didn't
        // have to.  But it might be big.  So I guess we do.
        if (state == ReceiveDelta)
        {
            log(WvLog::Debug5, "It's delta time\n");
            if (binaryobj->tmpfile.isok() == false)
                binaryobj->tmpfile.open(WvString("/tmp/delta-%s",
                                                 binaryobj->id),      // FIXME
                                    O_WRONLY | O_CREAT | O_TRUNC);
            binaryobj->tmpfile.write(buf, buf.used());
        }

        if (need != 0)  // still need more?
        {
            WvStreamClone::execute();
            return;
        }

        // done.
        print("!! Done receiving\n");
        log(WvLog::Debug5, "Buf now has %s bytes\n", buf.used());
        if (state == ReceiveSig)
            makedelta(*binaryobj);
        else
        {
            binaryobj->tmpfile.close();
            applydelta(*binaryobj);
        }
    }

    if (locdone && remdone && in_flight.isempty())
    {
        log("Exiting because everybody's done.\n");
        cleanup();
        close();
    }

    WvProtoStream::execute();
    log(WvLog::Debug5, "Ending execute\n");
}


void WvSyncProtocol::do_chaverep()
{
    WvString s;
    int remarbremote;
    WvString remact, remmd5sig, remmeta, remname, id;
    time_t remmtime;
    bool ret;

    ProtoObj *curobj;
    id = next_token_str();
    curobj = in_flight[id];

    remarbremote = next_token_str().num();
    remact = next_token_str();
    remmd5sig = next_token_str();
    remmtime = next_token_str().num();
    remmeta = next_token_str();
    tokbuf.get(1);
    remname = tokbuf.getstr();

    // if the other side doesn't have the file, mtime will be -1.
    // Disregard everything else.
    if (remmtime < 0)
        remact = remmd5sig = remmeta = "";

    if (remname != curobj->name)
    {
        err("Weird!  Remote end replied about the wrong file. (%s) != (%s)\n",
                remname, curobj->name);
        send_abort(curobj, "Weird! Remote end replied about the wrong file.");
        return;
    }

    // decide what to do and send local status to other end
    ret = arb.arbremote(cfgroot, *curobj->obj, remact, remmeta,
                        remmtime, remmd5sig);
    log(WvLog::Debug5, "arbremote returned %s\n", ret);

    // both sides better not be requesting...
    if (remarbremote && ret)
    {
        err("Weird!  Both sides want to request the same file.\n");
        send_abort(curobj, "Both sides want to request the same file");
        return;
    }

    // both sides are allowed to *not* request...
    if (!remarbremote && !ret)
    {
        log(WvLog::Debug5, "Neither side wants to request.\n");
        print("DONEWITH %s (%s)\n", id, curobj->name);
        remove_obj(curobj);
    }

    if (ret)
    {
        log(WvLog::Debug5, "Sending the sig\n");
        send_sig(*curobj);
    }
}


void WvSyncProtocol::send_sig(ProtoObj &curobj)
{
    print("SIG %s %s %s\n", 
            curobj.id, curobj.mysigbuf.used(), curobj.name);
    cloned->write(curobj.mysigbuf, curobj.mysigbuf.used());
}


bool WvSyncProtocol::newcwd(WvStringParm dir)
{
    bool ret = lister->newcwd(dir);
    lister->cleardone();

    log(WvLog::Debug5, "lister returned: %s\n", ret);
    if (ret)
    {
        cwd = dir;
    }

    return ret;
}


void deltacb(WvSyncProtocol::ProtoObj &curobj, WvSyncObj &obj, WvBuf &out)
{
    assert(curobj.tmpfile.isok());  // FIXME?

    curobj.tmpfile.write(out, out.used());
}


// FIXME: I would prefer it if this didn't use a temporary file.  It'd be
// great to have deltacb send the delta across in chunks as it is produced.
// But this is quick and hacky, just for now...
void WvSyncProtocol::makedelta(ProtoObj &curobj)
{
    WvString fname("/tmp/mmdelta-%s", curobj.id);   // FIXME
    curobj.tmpfile.open(fname, O_WRONLY | O_TRUNC | O_CREAT);

    if (curobj.tmpfile.isok())
    {
        int ret = curobj.obj->makedelta(curobj.sigbuf, curobj.deltabuf,
                WvBoundCallback<WvSyncCallback, ProtoObj &>
                               (&deltacb, curobj));
        deltacb(curobj, *curobj.obj, curobj.deltabuf);   // write the last bit
        curobj.tmpfile.close();

        if (ret == 0)
        {
            log(WvLog::Debug5, "Starting to dump file.\n");
            struct stat st;
            stat(fname, &st);
            print("DELTA %s %s %s\n", curobj.id, st.st_size, curobj.name);
            WvFile f(fname, O_RDONLY);   // FIXME
            f.autoforward(*(WvStream *) this->cloned);
            while (f.isok())
            {
                if (f.select(-1, true, false))
                f.callback();
            }
            f.close();
            log(WvLog::Debug5, "Finished dumping file.\n");
            switch_state(Ready);
            ::unlink(fname);
            return;
        }
    }
    send_abort(&curobj, "couldn't make delta, let's skip");
}


void WvSyncProtocol::applydelta(ProtoObj &curobj)
{
    WvString infname("/tmp/delta-%s", curobj.id);     // FIXME
    WvString outfname("/tmp/patched-%s", curobj.id);  // FIXME
    curobj.tmpfile.open(outfname, O_WRONLY | O_TRUNC | O_CREAT);

    if (curobj.tmpfile.isok())
    {
        WvDynBuf buf;

        int ret = curobj.obj->applydelta(infname, buf,
                WvBoundCallback<WvSyncCallback, ProtoObj &>
                               (&deltacb, curobj));
        deltacb(curobj, *curobj.obj, buf);      // write the last bit
        curobj.tmpfile.close();
        ::unlink(infname);

        if (ret == 0)
        {
            WvString meta = curobj.cfg["meta"].getme();
            time_t mtime = curobj.cfg["mtime"].getmeint();
            if (curobj.obj->installnew(outfname, meta, mtime))
            {
                print("DONEWITH %s (%s)\n", curobj.id, curobj.name);
                remove_obj(&curobj);
                switch_state(Ready);
                ::unlink(outfname);
                return;
            }
            else
                log(WvLog::Debug5, "installnew just didn't work\n");
        }
        else
            log(WvLog::Debug5, "librsync ret'ed badly\n");
    }
    else
        log(WvLog::Debug5, "tmpfile was not ok\n");
    send_abort(&curobj, "couldn't apply delta, let's skip");
}


void WvSyncProtocol::send_abort(ProtoObj *curobj, WvStringParm msg)
{
    curobj->obj->revert(curobj->cfg);
    print("ABORT %s (%s)\n", curobj->id, msg);
    remove_obj(curobj);
}


void WvSyncProtocol::do_ihave(WvSyncObj *obj)
{
    ProtoObj *curobj = new ProtoObj;
    curobj->id = gen_id();
    curobj->obj = obj;
    curobj->name = obj->name;
    curobj->cfg = cfgroot[obj->name];

    log(WvLog::Debug5, "Running do_ihave on %s\n", obj->name);
    int ret = arb.arblocal(cfgroot, *curobj->obj, curobj->mysigbuf);
    log(WvLog::Debug5, "arblocal returned %s\n", ret);

    WvString act = curobj->cfg["act"].getme();
    WvString md5sig = curobj->cfg["md5sig"].getme();
    time_t mtime = curobj->cfg["mtime"].getmeint();
    WvString meta = curobj->cfg["meta"].getme();

    print("IHAVE %s %s %s %s %s %s\n", 
            curobj->id, act, md5sig, mtime, meta, curobj->name);
    in_flight.add(curobj, true);
}


void WvSyncProtocol::do_chdir(WvStringParm _cwd)
{
    // FIXME: should only set this when we get an OK to the CHDIR command...
    cwd = _cwd;
    if (!cwd)
        cwd = ".";

    print("CHDIR %s\n", cwd);
}


void WvSyncProtocol::do_yourturn()
{
    print("YOURTURN\n");
    myturn = false;
}


void WvSyncProtocol::do_done()
{
    print("DONE\n");
    locdone = true;
}


void WvSyncProtocol::do_cchdir()
{
    WvString s = next_token_str();
    if (!!s)
    {
        if (newcwd(s))
            print("OK Setting CWD\n");
        else
        {
            print("ERROR Couldn't set CWD\n");
            err("Couldn't change into %s\n.  Dying.\n");
            close();
        }
    }
    else
        print("ERROR Specify CWD\n");
    return;
}


void WvSyncProtocol::do_cihave()
{
    WvString id, remact, remmd5sig, remmeta, remname;
    WvString act, md5sig, mtime, meta;
    int lret, ret, remmtime;
    
    id = next_token_str();
    if (!cwd)
    {
        print("ABORT %s (cwd first)\n", id);
        return;
    }
    remact = next_token_str();
    remmd5sig = next_token_str();
    remmtime = next_token_str().num();
    remmeta = next_token_str();
    tokbuf.get(1); // drop the space
    remname = tokbuf.getstr();

    // if the other side doesn't have the file but is requesting it
    // anyway (such as in a conflict) mtime will be -1.  Disregard
    // everything else.
    if (remmtime < 0)
        remact = remmd5sig = remmeta = "";

    if (!remname)
    {
        print("ABORT %s Use ihave id action md5sig mtime meta objname\n", id);
        return;
    }

    ProtoObj *curobj = new ProtoObj;
   
    curobj->id = id;
    curobj->name = remname;
    log(WvLog::Debug5, "remname: %s\n", remname);
    curobj->cfg = cfgroot[remname];
    curobj->obj = lister->makeobj(remname);
    if (!curobj->obj)
    {
        print("ABORT %s skip\n", id);
        return;
    }
    lret = arb.arblocal(cfgroot, *curobj->obj, curobj->mysigbuf);
    log(WvLog::Debug5, "arblocal returned %s\n", lret);

    // decide what to do and send local status to other end
    act = curobj->cfg["act"].getme();
    md5sig = curobj->cfg["md5sig"].getme();
    mtime = curobj->cfg["mtime"].getmeint();
    meta = curobj->cfg["meta"].getme();
    ret = arb.arbremote(cfgroot, *curobj->obj, remact, remmeta,
                                           remmtime, remmd5sig);
    log(WvLog::Debug5, "arbremote returned %s\n", ret);
    print("HAVEREP %s %s %s %s %s %s %s\n", id, ret,
                                    act, md5sig, mtime, meta, curobj->name);
    in_flight.add(curobj, true);

    if (ret)
        send_sig(*curobj);
}


void WvSyncProtocol::do_cbinary(Commands c)
{
    assert(!in_flight.isempty());
    WvString id = next_token_str();
    off_t binarybytes = next_token_str().num();
    ProtoObj *curobj = in_flight[id];
    if (!curobj)
    {
        print("ABORT %s (Could not find object with proper id)\n", id);
        return;
    }
    binaryobj = curobj;
    log(WvLog::Debug5, "binaryobj.name %s\n", binaryobj->name);
    if (c == cSig)
    {
        log(WvLog::Debug5, "Switching state to ReceiveSig\n");
        curobj->signeed = binarybytes;
        switch_state(ReceiveSig);
    }
    else
    {
        log(WvLog::Debug5, "Switching state to ReceiveDelta\n");
        curobj->deltaneed = binarybytes;
        switch_state(ReceiveDelta);
    }
}


void WvSyncProtocol::do_cdonewith()
{
    WvString id = next_token_str();
    ProtoObj *obj = in_flight[id];
    remove_obj(obj);
    return;
}


void WvSyncProtocol::do_cyourturn()
{
    if (in_flight.isempty())
        myturn = true;
    else
        myturn_soon = true;
}


void WvSyncProtocol::do_cabort()
{
    WvString id = next_token_str();
    ProtoObj *obj = in_flight[id];
    obj->obj->revert(obj->cfg);
    remove_obj(obj);
}


void WvSyncProtocol::do_cquit()
{
    cleanup();
    close();
}


void WvSyncProtocol::do_cdone()
{
    remdone = true;
}


void WvSyncProtocol::send_greeting()
{
    print("HELLO Welcome to WvSync (" WVSYNC_VER_STRING ")\n");
    greeted = true;
}


void WvSyncProtocol::cleanup()
{
    ProtoObjDict::Iter i(in_flight);

    i.rewind();
    while (i.next())
    {
        ProtoObj *obj = i.ptr();
        obj->obj->revert(obj->cfg);
        remove_obj(obj);
        i.rewind();
    }
    binaryobj = 0;
}


WvString WvSyncProtocol::gen_id()
{
    return WvString("a%s", uid++);
}


void WvSyncProtocol::remove_obj(ProtoObj *obj)
{
    lister->markdone(obj->name);

    in_flight.remove(obj);
    if (myturn_soon && in_flight.isempty())
        myturn = true;
}
