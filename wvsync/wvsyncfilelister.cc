/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * See wvsyncfilelister.h for details.
 *
 */

#include "wvsyncfilelister.h"
#include "wvsyncprotocol.h"

WvSyncFileLister::WvSyncFileLister(WvSyncProtocol &_prot, 
            WvStringParm _rootdir, UniConf _root)
    : WvSyncLister(_prot), 
    log("FileLister", WvLog::Debug1), 
    err(log.split(WvLog::Error)),
    rootdir(_rootdir), 
    root(_root),
    i(_rootdir),
    curdir("(nil)")
{
    log(WvLog::Debug3, "FileLister created on %s\n", rootdir);
    i.rewind();
    chdir_coming = false;
}


WvSyncObj *WvSyncFileLister::next()
{
    if (chdir_coming)
    {
        if (curdir != prot.getcwd())
            do_chdir(curdir);
        chdir_coming = false;
    }

    if (!i.next())
        return NULL;

    // Get the current reldir
    WvString reldir = i->relname;
    reldir.edit()[strlen(i->relname)-strlen(i->name)] = '\0';
    int n = strlen(reldir);
    while (reldir[--n] == '/')
        reldir.edit()[n] = '\0';

    if (!reldir)
        reldir = ".";
    if (prot.getcwd() != reldir)
    {
        curdir = reldir;
        prot.do_yourturn();
        chdir_coming = true;
    }

    // FIXME: do we leak this?
    return new WvSyncFile(i->relname, rootdir);
}


WvSyncObj *WvSyncFileLister::makeobj(WvStringParm name)
{
    // FIXME: do we leak this?
    return new WvSyncFile(name, rootdir);
}


bool WvSyncFileLister::newcwd(WvStringParm dir)
{
    curdir = dir;
    log(WvLog::Debug3, "WvSyncFileLister::newcwd got dir:%s\n", dir);
    return true;
}

