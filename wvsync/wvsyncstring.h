/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * WvSyncString provides the WvSyncObj implementation necessarily to
 * conduct librsync operations on a WvString.  This is mainly useful for
 * testing.
 *
 */

#ifndef __WVSYNCSTRING_H
#define __WVSYNCSTRING_H

#include "wvsyncobj.h"
#include <time.h>

class WvSyncString : public WvSyncObj
{
public:
    WvSyncString(WvStringParm _name, WvStringParm _str, time_t mtime = 1);
    virtual time_t findlastmodtime() const;

    // no real way to implement this... but in real life, you'd have to.
    virtual void makecopy(WvStringParm newname) const
        {}

    virtual bool installnew(WvStringParm fname, WvStringParm newmeta,
                            time_t newmtime);

    virtual bool isok() const;

    WvString string() const;

private:
    WvString str;
    time_t faketime;

    virtual bool getdata(WvBuf &out, off_t ofs, size_t size);
    virtual off_t approxsize() const;
};

#endif // __WVSYNCSTRING_H
