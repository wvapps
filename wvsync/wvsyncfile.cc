/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * See wvsyncfile.h for details.
 *
 */

#include <fcntl.h>
#include <errno.h>
#include <pwd.h>
#include <grp.h>
#include <utime.h>

#include <wvstringlist.h>
#include <wvfileutils.h>

#include "wvsyncfile.h"

WvSyncFile::WvSyncFile(WvStringParm name, WvStringParm _rootdir)
: WvSyncObj(name), rootdir(_rootdir)
{
    fd = -1;
    myofs = 0;
    ok = true;
    setlastmodtime();
    setmeta();
}


bool WvSyncFile::getdata(WvBuf &out, off_t ofs, size_t _size)
{
    if (isdir())
    {
        // it's a directory.  no contents, so we should obviously return
        // true... but we also have to check if it exists.
        struct stat st;
        if (lstat(realname(), &st) != 0)
            ok = false;
        return true;
    }
        
    // if the file is actually a symlink we need to treat that separately.
    if (S_ISLNK(mode))
    {
        assert(!ofs);
        char *buf = (char *) out.alloc(size);
        if (readlink(realname(), buf, size) < 0)
        {
            out.unalloc(size);
            err("%s: %s\n", name, strerror(errno));
            ok = false;
            return true;
        }
        return true;
    }

    // do we need to open the file?
    if (fd < 0)
    {
        fd = open(realname(), O_RDONLY);
        if (fd < 0)
        {
            err("%s: %s\n", name, strerror(errno));
            ok = false;
            return true;
        }
        myofs = 0;
    }

    // do we need to seek?
    if (ofs != myofs)
    {
        lseek(fd, ofs, SEEK_SET);
        myofs = ofs;
    }

    // grab the data.
    char *b = (char *) out.alloc(_size);
    assert(b);      // come on, now.
    size_t bytes = read(fd, b, _size);
    myofs += bytes;

    // too much data?
    if (bytes < _size)
        out.unalloc(_size-bytes);

    // ONLY done if end of file!
    return (bytes == 0);
}


off_t WvSyncFile::approxsize() const
{
    return size;
}


time_t WvSyncFile::findlastmodtime() const
{
    struct stat st;

    if (lstat(realname(), &st) < 0)
        return -1;
    
    return st.st_mtime;
}


WvString WvSyncFile::findmeta()
{
    struct stat st;
    mode = size = -1;
    if (lstat(realname(), &st) == 0)
    {
        mode = st.st_mode;
        size = st.st_size;
        uid  = st.st_uid;
        gid  = st.st_gid;
    }
    else
    {
        ok = false;
        return "";
    }

    WvString uidname(uid), gidname(gid);

    struct passwd *pw = getpwuid(uid);
    if (pw)
    {
        uidname = pw->pw_name;
        uidname.unique();
    }
    struct group *gr = getgrgid(gid);
    if (gr)
    {
        gidname = gr->gr_name;
        gidname.unique();
    }

    return WvString("%s:%s:%s:%s:%s", mode, uidname, uid, gidname, gid);
}


void WvSyncFile::applymeta(WvStringParm newmeta, time_t newmtime)
{
    // parse the new meta information
    WvStringList l;
    l.split(newmeta, ":");
    WvStringList::Iter i(l);
    i.rewind();
    if (i.next())
    {
        // we have a mode
        mode_t newmode = i->num();
        if (!S_ISLNK(mode) && !S_ISLNK(newmode))
        {
            chmod(realname(), newmode);
            mode = newmode;
        }
    }
    else
        goto applymeta_error;
    
    if (i.next())
    {
        // we have a uidname
        WvString uidname = *i;

        if (i.next())   // we have a uid
            uid = i->num();

        // if we can get a uid from the uidname given, that's better than
        // just using the number.
        struct passwd *pw = getpwnam(uidname);
        if (pw)
            uid = pw->pw_uid;

        if (uid >= 0)
            lchown(realname(), uid, gid);
    }
    else
        goto applymeta_error;
    
    if (i.next())
    {
        // we have a gidname
        WvString gidname = *i;

        if (i.next())   // we have a gid
            gid = i->num();
        
        // if we can get a gid from the gidname given, that's better than
        // just using the number.
        struct group *gr = getgrnam(gidname);
        if (gr)
            gid = gr->gr_gid;

        if (gid >= 0)
            lchown(realname(), uid, gid);

        setmeta();
    }
    else
        goto applymeta_error;

    // apply the new mtime
    if (S_ISREG(mode))
    {
        struct utimbuf utim;
        utim.actime = newmtime;
        utim.modtime = newmtime;
        utime(realname(), &utim);
        setlastmodtime();
    }

    return;
applymeta_error:
        err("Incomplete metadata received for %s\n", name);
}


bool WvSyncFile::isunchanged(time_t sometime, WvStringParm somemeta) const
{
    if (S_ISLNK(mode) || S_ISDIR(mode))
        return somemeta == getmeta();
    else
        return somemeta == getmeta() && sometime == getlastmodtime();
}


void WvSyncFile::makecopy(WvStringParm newname) const
{
    fcopy(realname(), WvString("%s/%s", rootdir, newname));
}


bool WvSyncFile::isdir() const
{
    return S_ISDIR(mode);
}


bool WvSyncFile::isdir(WvStringParm somemeta) const
{
    WvStringList l;
    l.split(somemeta, ":");
    WvStringList::Iter i(l);
    i.rewind();
    if (i.next())
    {
        mode_t somemode = i->num();
        return S_ISDIR(somemode);
    }

    return WvSyncObj::isdir(somemeta);
}


bool WvSyncFile::installnew(WvStringParm fname, WvStringParm newmeta,
                            time_t newmtime)
{
    // make sure it's safe
    if (hasbeenmodified())
        return false;

    // out with the old...
    ::unlink(realname());

    // ...and in with the new.
    // we need the new mode to do that.  It's the thing before the first :
    mode_t newmode = atoi(newmeta);

    if (S_ISDIR(newmode))
    {
        mkdir(realname(), 0700);
    }
    else if (S_ISLNK(newmode))
    {
        WvFile f(fname, O_RDONLY);
        WvDynBuf buf;
        while (f.isok())
        {
            if (f.select(-1, true, false))
                f.read(buf, 1024);
        }

        if (buf.used() == 0)
            return false;

        if (symlink((const char *) buf.get(buf.used()), realname()))
            return false;
    }
    else
    {
        log(WvLog::Debug4, "Copying from %s to %s\n", fname, name);
        fcopy(fname, realname());
    }

    applymeta(newmeta, newmtime);

    return true;
}
