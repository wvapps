/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * Lets you play with WvSyncArbiter::arbremote.  I should really write an
 * automatic test for this some day.
 *
 * Assumes there will be two subdirectories, 'arbrem1' and 'arbrem2'.
 * Takes a filename as an argument, checks the status of each one, in each
 * of those directories, and updates the lists appropriately.
 *
 * You sort of have to know what you're expecting when you run this.
 * Bleah.
 */

#include "wvsyncarbiter.h"
#include "wvsyncfile.h"
#include <uniconfroot.h>

int main(int argc, char *argv[])
{
    free(malloc(1));

    if (argc < 2)
    {
        wvcon->print("Usage: %s filename\n", argv[0]);
        return -1;
    }

    WvSyncFile file1(WvString("arbrem1/%s", argv[1]), "");
    WvSyncFile file2(WvString("arbrem2/%s", argv[1]), "");

    UniConfRoot root1("ini:arbrem1/foo.ini");
    UniConfRoot root2("ini:arbrem2/foo.ini");
    UniConf cfg1(root1[WvString("arbrem1/%s", argv[1])]);
    UniConf cfg2(root2[WvString("arbrem2/%s", argv[1])]);

    WvSyncArbiter arb1;
    WvSyncArbiter arb2;

    WvDynBuf sigbuf;
    bool ret;

    // run arblocal on each end
    ret = arb1.arblocal(root1, file1, sigbuf);
    wvcon->print("arblocal 1 returned %s\n", ret);

    ret = arb2.arblocal(root2, file2, sigbuf);
    wvcon->print("arblocal 2 returned %s\n", ret);

    // run arbremote on each end
    WvString remact1 = cfg2["act"].getme();
    WvString remmeta1 = cfg2["meta"].getme();
    time_t remmtime1 = cfg2["mtime"].getmeint();
    WvString remmd5sig1 = cfg2["md5sig"].getme();

    WvString remact2 = cfg1["act"].getme();
    WvString remmeta2 = cfg1["meta"].getme();
    time_t remmtime2 = cfg1["mtime"].getmeint();
    WvString remmd5sig2 = cfg1["md5sig"].getme();

    ret = arb1.arbremote(root1, file1, remact1, remmeta1,
                                       remmtime1, remmd5sig1);
    wvcon->print("arbremote 1 returned %s\n", ret);
    ret = arb2.arbremote(root2, file2, remact2, remmeta2,
                                       remmtime2, remmd5sig2);
    wvcon->print("arbremote 2 returned %s\n", ret);

    // all done
    wvcon->print("check both foo.ini files\n");

    return 0;
}
