/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * Generates a librsync delta that can turn one string into another string.
 *
 */

#include <strutils.h>

#include "wvsyncstring.h"

int main()
{
    char *line;
    WvDynBuf sigbuf;
    WvDynBuf deltabuf;

    wvcon->print("Type the source string.\n");

    if ((line = wvcon->blocking_getline(-1)))
    {
        WvSyncString rstr("foo", line);

        sigbuf.zap();
        if (rstr.getsig(sigbuf))
            return -1;
        
        size_t size = sigbuf.used();
        wvcon->print("Signature is %s bytes.\n", size);
        wvcon->print(hexdump_buffer(sigbuf.peek(0, size), size));
    }
    else
        return 0;

    wvcon->print("Now type the modified string.\n");

    if ((line = wvcon->blocking_getline(-1)))
    {
        WvSyncString rstr("foo", line);

        deltabuf.zap();
        if (rstr.makedelta(sigbuf, deltabuf))
            return -1;

        size_t size = deltabuf.used();
        wvcon->print("Delta is %s bytes.\n", size);
        wvcon->print(hexdump_buffer(deltabuf.get(size), size));
    }
    else
        return 0;

    return 0;
}
