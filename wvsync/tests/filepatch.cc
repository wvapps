/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * Generates a librsync delta and then turns one file into another.
 *
 */

#include <strutils.h>
#include <wvfile.h>

#include "wvsyncfile.h"

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        wvcon->print("Usage: %s srcfile newfile [outfile]\n", argv[0]);
        return -1;
    }

    // grab the signature
    WvSyncFile srcfile(argv[1], "");

    WvDynBuf sigbuf;
    srcfile.getsig(sigbuf);

    if (!srcfile.isok())
        return -1;
    
    size_t size = sigbuf.used();
    wvcon->print("Signature is %s bytes.\n", size);
    wvcon->print(hexdump_buffer(sigbuf.peek(0, size), size));

    // make the delta
    WvSyncFile newfile(argv[2], "");

    WvDynBuf deltabuf;
    newfile.makedelta(sigbuf, deltabuf);

    if (!newfile.isok())
        return -1;

    size = deltabuf.used();
    wvcon->print("Delta is %s bytes.\n", size);
    wvcon->print(hexdump_buffer(deltabuf.peek(0, size), size));

    // apply the patch
    WvDynBuf patchbuf;
    srcfile.applydelta(deltabuf, patchbuf);

    size = patchbuf.used();
    wvcon->print("Patched data is %s bytes.\n", size);
    wvcon->print(hexdump_buffer(patchbuf.peek(0, size), size));

    // write to outfile, if given
    if (argc > 3)
    {
        WvFile outfile(argv[3], O_WRONLY | O_CREAT | O_TRUNC);
        outfile.write(patchbuf);
        outfile.close();
    }

    return 0;
}
