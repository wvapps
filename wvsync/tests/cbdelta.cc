/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * Generates a librsync delta that can turn one file into another.
 * Uses a callback to handle a large delta so it doesn't need to be all in
 * memory at once.
 *
 */

#include <strutils.h>

#include "wvsyncfile.h"

static off_t totalsize = 0;

void deltacb(WvSyncObj &, WvBuf &out)
{
    totalsize += out.used();
    off_t size = out.used();
    wvcon->print("--> deltacb() called (%s)\n", size);
    wvcon->print(hexdump_buffer(out.get(size), size));
}


int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        wvcon->print("Usage: %s srcfile newfile\n", argv[0]);
        return -1;
    }

    // grab the signature
    WvSyncFile srcfile(argv[1], "");

    WvDynBuf sigbuf;
    srcfile.getsig(sigbuf);

    if (!srcfile.isok())
        return -1;
    
    size_t size = sigbuf.used();
    wvcon->print("Signature is %s bytes.\n", size);
    wvcon->print(hexdump_buffer(sigbuf.peek(0, size), size));

    // make the delta
    WvSyncFile newfile(argv[2], "");

    WvDynBuf deltabuf;
    newfile.makedelta(sigbuf, deltabuf, deltacb);

    if (!newfile.isok())
        return -1;

    size = deltabuf.used();
    totalsize += size;
    wvcon->print("Here's the last bit.\n");
    wvcon->print(hexdump_buffer(deltabuf.get(size), size));
    wvcon->print("Delta was %s bytes.\n", totalsize);

    return 0;
}
