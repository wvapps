/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * Lets you play with WvSyncArbiter::arblocal.  I should really write an
 * automatic test for this some day.
 *
 */

#include "wvsyncarbiter.h"
#include "wvsyncfile.h"
#include <uniconfroot.h>

int main(int argc, char *argv[])
{
    free(malloc(1));

    if (argc < 2)
    {
        wvcon->print("Usage: %s filename\n", argv[0]);
        return -1;
    }

    WvSyncFile file(argv[1], "");
    UniConfRoot root("ini:foo.ini");
    WvSyncArbiter arb;

    WvDynBuf sigbuf;
    bool ret = arb.arblocal(root, file, sigbuf);

    wvcon->print("returned %s\n", ret);
    wvcon->print("check foo.ini\n");

    return 0;
}
