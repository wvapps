/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * Generates a librsync delta that can turn one file into another.
 *
 */

#include <strutils.h>

#include "wvsyncfile.h"

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        wvcon->print("Usage: %s srcfile newfile\n", argv[0]);
        return -1;
    }

    // grab the signature
    WvSyncFile srcfile(argv[1], "");

    WvDynBuf sigbuf;
    srcfile.getsig(sigbuf);

    if (!srcfile.isok())
        return -1;
    
    size_t size = sigbuf.used();
    wvcon->print("Signature is %s bytes.\n", size);
    wvcon->print(hexdump_buffer(sigbuf.peek(0, size), size));

    // make the delta
    WvSyncFile newfile(argv[2], "");

    WvDynBuf deltabuf;
    newfile.makedelta(sigbuf, deltabuf);

    if (!newfile.isok())
        return -1;

    size = deltabuf.used();
    wvcon->print("Delta is %s bytes.\n", size);
    wvcon->print(hexdump_buffer(deltabuf.get(size), size));

    return 0;
}
