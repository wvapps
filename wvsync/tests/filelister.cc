#include "wvsyncfilelister.h"
#include "wvsyncprotocol.h"
#include "wvtcp.h"
#include "wvsyncarbiter.h"
#include "uniconfroot.h"
#include "wvlog.h"

void gogo(WvTCPConn *tcp, WvStringParm cfgpath, 
        WvStringParm dirpath, bool server)
{
    WvLog log("FileListerTests", WvLog::Info);
    UniConfRoot cfg(cfgpath);

    WvSyncArbiter wsa;

    WvSyncProtocol proto(tcp, &log, wsa, cfg, server);
    WvSyncFileLister fl(proto, dirpath, cfg);
    proto.set_lister(&fl);

    fl.run();
}

int main(int argc, char *argv[])
{
    if (argv[1] == WvString("server"))
    {
        WvTCPListener l(4322);
        while (true)
        {   
            gogo(l.accept(), argv[2], argv[3], true);
            return 0;
        }

        wvcon->print("Good day to you, my dear friend\n");
        return 0;
    }
    else
    {
        WvTCPConn *tcp = new WvTCPConn((WvIPPortAddr)"127.0.0.1:4322");
        gogo(tcp, argv[2], argv[3], false);
    }
    return 0;
}
        
