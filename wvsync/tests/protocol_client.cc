#include "wvstream.h"
#include "wvistreamlist.h"
#include "wvsyncstring.h"
#include "wvsyncarbiter.h"
#include "wvsyncprotocol.h"
#include "uniconfroot.h"
#include "wvtcp.h"

int main()
{
    WvLog log("Test Client", WvLog::Info);
    UniConfRoot root("ini:/tmp/wvsync_client.ini");

    WvTCPConn tcp((WvIPPortAddr)"127.0.0.1:4321");
    WvSyncArbiter wsa;
    WvSyncProtocol proto(&tcp, &log, wsa, root, false);

    WvSyncString *obj = new
        WvSyncString("ashley", "I have but a fleeting memory of thee", 10);
//    while (!proto.want_more())
//    {
//        log(WvLog::Info, "More, more, give me more\n");
//        proto.runonce();
//    }
    proto.do_chdir("foo");
//    while (!proto.want_more())
//        proto.runonce();
    proto.do_ihave(obj);

    WvIStreamList::globallist.append(&proto, false);
    
    while (!WvIStreamList::globallist.isempty())
        WvIStreamList::globallist.runonce();
    /*
    while (proto.isok() && tcp.isok())
    {
        log(WvLog::Debug4, "select\n");
        proto.runonce();
    }*/
    
#if 0
    log("CHDIR\n");
    wvcon->print("CHDIR foo\n");
    
    log("IHAVE\n");
    wvcon->print("IHAVE new none 1192347202 i_am_some_meta ashley\n");

    log("SIG\n");
    WvSyncString ash("ashley", "She is out there somewhere," 
            " but I missed my chance.\n");
    WvDynBuf buf;
    buf.zap();
    ash.getsig(buf);
    size_t size = buf.used();
    wvcon->print("OK\n");

    WvDynBuf sig;
   // log("Read %s bytes\n", wvcon->read(sig, 24));
//    wvcon->print("SIG %s ashley\n", size);
//    wvcon->flush(5000);
    sleep(5);
//    log("Wrote %s bytes\n", wvcon->write(buf, size));
  
    wvcon->autoforward(*wvcon);
    while (wvcon->isok())
        wvcon->runonce();
#endif
        
    return 0;
}
