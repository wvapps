/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * Generates a librsync delta and then turns one file into another.
 * Uses a callback to handle a large delta so it doesn't need to be all in
 * memory at once.  You could think about this as testing input *and*
 * output callbacks for applydelta().
 *
 */

#include <strutils.h>

#include "wvsyncfile.h"

#define DELTA "/tmp/cbpatch.tmp"

static WvString filename = "";

void deltacb(WvSyncObj &, WvBuf &out)
{
    off_t size = out.used();
    wvcon->print("--> deltacb() called (%s)\n", size);

    WvFile f(DELTA, O_WRONLY | O_CREAT | O_APPEND);
    f.write(out, size);
    f.close();
}


void patchcb(WvSyncObj &, WvBuf &out)
{
    off_t size = out.used();
    wvcon->print("--> patchcb() called (%s)\n", size);

    WvFile f(filename, O_WRONLY | O_CREAT | O_APPEND);
    f.write(out, size);
    f.close();
}


int main(int argc, char *argv[])
{
    if (argc < 4)
    {
        wvcon->print("Usage: %s srcfile newfile outfile\n", argv[0]);
        return -1;
    }

    // grab the signature
    WvSyncFile srcfile(argv[1], "");

    WvDynBuf sigbuf;
    srcfile.getsig(sigbuf);

    if (!srcfile.isok())
        return -1;
    
    size_t size = sigbuf.used();
    wvcon->print("Signature is %s bytes.\n", size);

    // make the delta
    WvSyncFile newfile(argv[2], "");
    unlink(DELTA);

    WvDynBuf deltabuf;
    newfile.makedelta(sigbuf, deltabuf, deltacb);
    if (!newfile.isok())
        return -1;
    deltacb(newfile, deltabuf);   // write the last part out

    // apply the patch
    filename = argv[3];
    unlink(filename);
    WvDynBuf patchbuf;
    srcfile.applydelta(DELTA, patchbuf, patchcb);
    patchcb(newfile, patchbuf);   // write the last part out

    return 0;
}
