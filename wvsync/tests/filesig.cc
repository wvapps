/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * Generates a librsync signature for a file named on the command line
 * and hexdumps it.
 *
 */

#include <strutils.h>

#include "wvsyncfile.h"

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        wvcon->print("Usage: %s filename\n", argv[0]);
        return -1;
    }

    WvSyncFile rfile(argv[1], "");

    WvDynBuf buf;
    rfile.getsig(buf);

    if (!rfile.isok())
        return -1;
    
    size_t size = buf.used();
    wvcon->print("Signature is %s bytes.\n", size);
    wvcon->print(hexdump_buffer(buf.get(size), size));

    return 0;
}
