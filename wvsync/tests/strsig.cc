/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * Generates librsync signatures for strings given on the command line and
 * hexdumps them.
 *
 */

#include <strutils.h>

#include "wvsyncstring.h"

int main()
{
    char *line;
    WvDynBuf buf;

    wvcon->print("Type something.  Ctrl-D to quit.\n");

    while ((line = wvcon->blocking_getline(-1)))
    {
        WvSyncString rstr("foo", line);

        buf.zap();
        if (rstr.getsig(buf))
            break;
        
        size_t size = buf.used();
        wvcon->print("Signature is %s bytes.\n", size);
        wvcon->print(hexdump_buffer(buf.get(size), size));
    }

    return 0;
}
