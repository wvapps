/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * Generates a librsync delta and then turns one string into another string.
 *
 */

#include <strutils.h>

#include "wvsyncstring.h"

int main()
{
    char *line;
    WvDynBuf sigbuf;
    WvDynBuf deltabuf;
    WvDynBuf patchbuf;

    // get the source string and its signature
    wvcon->print("Type the source string.\n");
    if (!(line = wvcon->blocking_getline(-1)))
        return 0;

    WvSyncString srcstr("foo", line);

    sigbuf.zap();
    if (srcstr.getsig(sigbuf))
        return -1;
    
    size_t size = sigbuf.used();
    wvcon->print("Signature is %s bytes.\n", size);
    wvcon->print(hexdump_buffer(sigbuf.peek(0, size), size));

    // get the modified string and create a delta
    wvcon->print("Now type the modified string.\n");
    if (!(line = wvcon->blocking_getline(-1)))
        return 0;

    WvSyncString rstr("foo", line);

    deltabuf.zap();
    if (rstr.makedelta(sigbuf, deltabuf))
        return -1;

    size = deltabuf.used();
    wvcon->print("Delta is %s bytes.\n", size);
    wvcon->print(hexdump_buffer(deltabuf.peek(0, size), size));

    // now apply the delta to the original string
    patchbuf.zap();
    if (srcstr.applydelta(deltabuf, patchbuf))
        return -1;

    size = patchbuf.used();
    wvcon->print("Patched data is %s bytes.\n", size);
    wvcon->print(hexdump_buffer(patchbuf.get(size), size));

    return 0;
}
