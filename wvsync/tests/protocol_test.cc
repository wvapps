#include "wvsyncprotocol.h"
#include "wvlog.h"
#include "wvsyncarbiter.h"
#include "uniconfroot.h"
#include "wvsyncstring.h"
#include "wvsynclister.h"
#include "wvtcp.h"


DeclareWvList(WvSyncString);

class WvSyncTestLister : public WvSyncLister
{
    WvSyncStringList *string_list;
    WvLog log;
    UniConf root;
    
public:
    WvSyncTestLister(WvSyncProtocol &_prot, WvLog &_log, UniConf &_root)
        : WvSyncLister(_prot), log(_log), root(_root)
    { 
        string_list = new WvSyncStringList();
    }
    ~WvSyncTestLister()
    {
        delete string_list;
    }

    WvSyncObj *next()
    {
        log(WvLog::Error, "Next is not implemented\n");
        abort();
        return NULL;
    }

    WvSyncObj *makeobj(WvStringParm n)
    {
        string_list->add(new WvSyncString(n, "(null)", -1), true);
        return string_list->last();
    }

    bool newcwd(WvStringParm dir)
    {
        // We don't support changing cwds, but don't tell anyone.
        return true;
    }
};
        
int main()
{
    wvcon->print("Welcome to the WvSync test server.\n");
    wvcon->print("Entering insanity..");

    WvTCPListener l(4321);
    while (true)
    {
        WvTCPConn *tcp = l.accept();

        WvLog log("WvSyncTest", WvLog::Info);
        UniConfRoot cfg("ini:/tmp/wvsync_test.ini");

        WvSyncArbiter wsa;
    
        WvSyncProtocol proto(tcp, &log, wsa, cfg, true);
        WvSyncTestLister tl(proto, log, cfg);
        proto.set_lister(&tl);

        while (proto.isok())
        {
            proto.runonce();
        }
        return 0;
    }
    
    wvcon->print("Good day to you too, sir.\n");
    return 0;
}


    
