/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2004 Net Integration Technologies, Inc.
 *
 * WvSyncFileLister provides the WvSyncLister implementation necessary to
 * traverse a directory tree and synchronize its contents.
 */

#ifndef __WVSYNCFILELISTER_H
#define __WVSYNCFILELISTER_H

#include "wvsynclister.h"
#include "uniconf.h"
#include "wvdiriter.h"
#include "wvsyncfile.h"

class WvSyncFileLister : public WvSyncLister
{
public:
    WvSyncFileLister(WvSyncProtocol &, WvStringParm _rootdir, UniConf);
    ~WvSyncFileLister() {}

    virtual WvSyncObj *next();
    virtual WvSyncObj *makeobj(WvStringParm name);

    virtual bool newcwd(WvStringParm dir);

private:
    WvLog log;
    WvLog err;
    WvString rootdir;
    UniConf root;

    WvDirIter i;
    WvString curdir;

    bool chdir_coming;
};

#endif
