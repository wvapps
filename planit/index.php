<!--
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2005 Net Integration Technologies, Inc.
 *
 * Dave's cheesy PlaNit thing
 *
 */
-->

<?php
include( 'config.inc.php' );

require_once( 'rss_fetch.inc' );

function parse_atom_date( $s )
{
    list( $tmp, $junk ) = split( 'Z', $s );
    if( !$tmp )
        return '';
    list( $tmp2, $junk ) = split( '\+', $tmp );
    if( !$tmp2 )
        return '';
    list( $year, $month, $rest, $junk ) = split( '-', $tmp2 );
    $tmp2 = "$year-$month-$rest";

    list( $d, $t, $junk ) = split( 'T', $tmp2 );
    $tmp = "$d $t GMT";
    return $tmp;
}

function grab_feeds()
{
    global $feeds;
    global $allrss;
    global $allitems;
    global $cutoffs;
    global $articles_per_person;
    global $maximum_days;
    global $hack;

    // grab all the feeds, and stick them in an array, indexed by URL.
    // also make a new array containing the URL, timestamp, and item key,
    // for each item in all the feeds.
    foreach( $feeds as $userid => $feed ) {
        if( $feed['hack'] && !$hack )
            continue;

        foreach( $feed['urls'] as $url ) {
            $allrss[$url] = fetch_rss( $url );
            if( $allrss[$url] === false ) {
                $feeds[$userid]['broken'] = true;
                continue;
            }

            $cutoffs[$url] = $articles_per_person;
            foreach( $allrss[$url]->items as $itemkey => $item ) {
                // is this item too old?
                $tmp = $item['pubdate'];
                if( $tmp == '' )
                    $tmp = parse_atom_date( $item['created'] );
                if( $tmp == '' )
                    $tmp = parse_atom_date( $item['modified'] );

                $timestamp = strtotime( $tmp );
                if( $timestamp === false || $timestamp == '' )
                    continue;
                if( time()-$timestamp > 3600*24*$maximum_days )
                    continue;

                $allitems[count( $allitems )] =
                        array( url => $url,
                               timestamp => $timestamp,
                               itemkey => $itemkey,
                               userid => $userid );
            }
        }
    }

    // sort the allitems list in reverse chronological order
    function itemcmp( $a, $b )
    {
        return $b['timestamp'] - $a['timestamp'];
    }
    usort( $allitems, 'itemcmp' );
    reset( $allitems );
}

function escape( $string )
{
    return( ereg_replace( '<', '&lt;',
            ereg_replace( '>', '&gt;',
            ereg_replace( '"', '&quot;',
            ereg_replace( '&', '&amp;',
                          $string ) ) ) ) );
}

function do_entry( $iteminfo, $lastperson, $entryformatting, $fd, $rssfd )
{
    global $allrss;
    global $allitems;
    global $cutoffs;
    global $feeds;
    global $mugshotdir;

    $rss = $allrss[$iteminfo['url']];
    $timestamp = $iteminfo['timestamp'];
    $item = $rss->items[$iteminfo['itemkey']];
    $userid = $iteminfo['userid'];

    if( $cutoffs[$iteminfo['url']] ) {
        $cutoffs[$iteminfo['url']]--;

        // no longer sanitize() here, that's done in magpie before caching
        $content = $item['atom_content'];
        if( $content == '' )
            $content = $item['description'];

        while( substr( $content, 0, 4 ) == '<br>' )
            $content = trim( substr( $content, 4 ) );

        do {
            $len = strlen( $content );
            $br_end = substr( $content, $len-4 );
            if( $br_end == '<br>' )
                $content = trim( substr( $content, 0, $len-4 ) );
        } while( $br_end == '<br>' );

        if( $item['title'] )
            $title = $item['title'];
        else
            $title = gmdate( 'F j, Y', $timestamp );

        unset( $mugshot );
        if( file_exists( "$mugshotdir/$userid.jpg" ) )
            $mugshot = "$mugshotdir/$userid.jpg";
        else if( file_exists( "$mugshotdir/$userid.png" ) )
            $mugshot = "$mugshotdir/$userid.png";
        else if( file_exists( "$mugshotdir/$userid.gif" ) )
            $mugshot = "$mugshotdir/$userid.gif";

        $channellink = $rss->channel['link'];
        $channelname = $feeds[$userid]['name'];
        $entrytime = gmdate( 'F j, Y H:i', $timestamp );

        $link = $item['link'];
        if( $link == '' )
            $link = $item['link_'];
        if( $link == '' )
            $link = $channellink;

        $mode = 'none';

        foreach( $entryformatting as $line ) {
            // check for special inline constants
            $tmp = $line;
            $line = preg_replace( '/(?im)<## channellink ##>/', $channellink,
                    preg_replace( '/(?im)<## channelname ##>/', $channelname,
                    preg_replace( '/(?im)<## entrylink ##>/', $link,
                    preg_replace( '/(?im)<## entrytitle ##>/', $title,
                    preg_replace( '/(?im)<## mugshot ##>/', $mugshot,
                    preg_replace( '/(?im)<## feeduserid ##>/', $userid,
                    preg_replace( '/(?im)<## entrytime ##>/', $entrytime,
                    $tmp ) ) ) ) ) ) );

            // check for flow control commands
            if( substr( $line, 0, 4 ) === '### ' ) {
                $tmp = trim( substr( $line, 4 ) );
                if( $mode != 'silent' ) {
                    if( $tmp == 'ifnewperson' ) {
                        if( $userid == $lastperson )
                            $mode = 'silent';
                    }
                    else if( $tmp == 'ifmugshot' ) {
                        if( !$mugshot )
                            $mode = 'silent';
                    }
                    else if( $tmp == 'entrycontent' )
                        fwrite( $fd, $content );
                }
                if( $tmp == 'endif' )
                    $mode = 'none';

            } else
                if( $mode != 'silent' )
                    fwrite( $fd, $line );
        }

        // RSS formatting doesn't need to be quite that flexible.
        fwrite( $rssfd, "    <item>\n" .
                        "      <title>$channelname: $title</title>\n" .
                        "      <pubDate>" . gmdate( 'r', $timestamp )
                                          . "</pubDate>\n" .
                        "      <link>$link</link>\n" .
                        "      <description>\n" .
                        escape( $content ) . "\n" .
                        "      </description>\n" .
                        "    </item>\n\n" );
    }
}

function do_feeditem( $userid, $feed, $feedformatting, $fd )
{
    global $allrss;

    if( $feed['broken'] )
        return;

    foreach( $feed['urls'] as $url ) {
        $page = $allrss[$url]->channel['link'];

        foreach( $feedformatting as $line ) {
            // check for special inline constants
            $tmp = $line;
            $line = preg_replace( '/(?im)<## feedpage ##>/', $page,
                    preg_replace( '/(?im)<## feeduserid ##>/', $userid,
                    preg_replace( '/(?im)<## feedxml ##>/', $url,
                    $tmp ) ) );

            // there are no interesting flow control commands here
            fwrite( $fd, $line );
        }
    }
}

function read_layout()
{
    global $layout;
    global $email;
    global $admin;
    global $name;
    global $feeds;
    global $allitems;
    global $tmppage;
    global $tmprss;
    global $cssoverride;

    $mode = 'none';

    $fd = @fopen( $layout, 'r' );
    if( $fd === false ) {
        print( "Couldn't find $layout.\n" );
        return;
    } else
        fclose( $fd );

    $fd = @fopen( $tmppage, 'w' );
    $rssfd = @fopen( $tmprss, 'w' );
    if( $fd === false || $rssfd === false ) {
        print( "PlaNit is broken right now.\n" );
        return;
    }

    // parse the layout
    $file = file( $layout );
    foreach( $file as $line ) {
        
        // nothing special happening
        if( $mode == 'none' || $mode == 'silent' ) {

            // check for special inline constants
            $tmp = $line;
            $line = preg_replace( '/(?im)<## email ##>/', $email,
                    preg_replace( '/(?im)<## admin ##>/', $admin,
                    preg_replace( '/(?im)<## name ##>/', $name,
                    preg_replace( '/(?im)<## cssoverride ##>/', $cssoverride,
                    $tmp ) ) ) );

            // check for flow control commands
            if( substr( $line, 0, 4 ) == '### ' ) {
                $tmp = trim( substr( $line, 4 ) );
                if( $mode != 'silent' ) {
                    if( $tmp == 'loopfeeds' ) {
                        $mode = 'feedloop';
                        unset( $feedformatting );
                    }
                    else if( $tmp == 'loopentries' ) {
                        $mode = 'entryloop';
                        unset( $entryformatting );
                    }
                    else if( $tmp == 'ifcssoverride' ) {
                        if( !$cssoverride )
                            $mode = 'silent';
                    }
                }
                if( $tmp == 'endif' )
                    $mode = 'none';
            }
            else
                if( $mode != 'silent' )
                    fwrite( $fd, $line );

        // in both kinds of loop, make a list of the formatting lines so we
        // can loop through them once we've parsed the contents
        } else if( $mode == 'feedloop' || $mode == 'entryloop' ) {

            if( substr( $line, 0, 4 ) == '### ' ) {
                $tmp = trim( substr( $line, 4 ) );
                if( $tmp == 'endloopfeeds' ) {
                    foreach( $feeds as $userid => $feed ) {
                        do_feeditem( $userid, $feed, $feedformatting, $fd );
                    }
                    $mode = 'none';
                    continue;
                
                } else if( $tmp == 'endloopentries' ) {
                    unset( $lastperson );
                    foreach( $allitems as $iteminfo ) {
                        do_entry( $iteminfo, $lastperson,
                                  $entryformatting, $fd, $rssfd );
                        $lastperson = $iteminfo['userid'];
                    }
                    $mode = 'none';
                    continue;
                }
            }
            if( $mode == 'feedloop' )
                $feedformatting[ count( $feedformatting ) ] = $line;
            else if( $mode == 'entryloop' )
                $entryformatting[ count( $entryformatting ) ] = $line;
        }
    }

    fclose( $fd );
    fclose( $rssfd );
}





if( $adminpass == $pass ) {
    // $tmppage should have a sensible default if it wasn't set.
    if( !$tmppage )
        $tmppage = $staticpage . '.tmp';
    if( !$tmprss )
        $tmprss = $staticrss . '.tmp';

    // set up the urls array for each userid that only had one url
    // defined.
    foreach( $feeds as $userid => $feed ) {
        if( $feeds[$userid]['url'] ) {
            unset( $feeds[$userid]['urls'] );
            $i = count( $feeds[$userid]['urls'] );
            $feeds[$userid]['urls'][$i] = $feeds[$userid]['url'];
        }
    }

    if( $bereallyslow == 1 ) {
        system( "rm -f cache/*" );
        $beslow = 1;
    }

    if( $force ) {
        foreach( $feeds[$force]['urls'] as $url ) {
            system( 'rm -f cache/' . md5( $url ) );
        }

        $beslow = 1;
    }

    if( $beslow == 1 || $bereallyslow == 1 ) {
        grab_feeds();
        read_layout();

        copy( $tmppage, $staticpage );
        copy( $tmprss, $staticrss );
    }
}

readfile( $staticpage );

if( !$beslow && $logfile ) {
    $fd = @fopen( $logfile, 'a' );
    if( $fd === false ) {
        print( "Email Dave and tell him to fix his program!\n" );
    } else {
        fwrite( $fd, strftime( '%Y%m%d %H:%M' ) . " -- pla -- " .
                     "$REMOTE_ADDR -- $HTTP_REFERER\n" );
        fclose( $fd );
    }
}
?>
