<?php

$name = 'Example PlaNit';
$link = 'http://planit.example.com/';
$admin = 'System Administrator';
$email = 'somebody@somewhere.org';
//$logfile = '/home/dcoombs/Files/nitloglog';
$staticpage = '/tmp/planitstatic';
$mugshotdir = 'mugshots';
$adminpass = 'changeme';

$layout = 'simple.inc.html';
//$layout = 'fancy.inc.html';

$articles_per_person = 5;
$maximum_days = 14;

$feeds = array(
    'dcoombs' => array(
        'name' => 'Dave Coombs',
        'url'  => 'http://people.nit.ca/~dcoombs/nitlog/rss.php' ),

    'mich' => array(
        'name' => 'Michel &Eacute;mond',
        'url'  => 'http://people.nit.ca/~mich/niti/rss.xml' )
    );
?>
