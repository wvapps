<?php
Header( 'Content-type: application/rss+xml' );

print "<?xml version=\"1.0\"?>\n"
?>

<!--
 * Worldvisions Weaver Software:
 *   Copyright (C) 1997-2005 Net Integration Technologies, Inc.
 *
 * Dave's cheesy PlaNit thing's RSS spewer
 *
 */
-->

<?php
include( 'config.inc.php' );
?>

<rss version="0.91">
  <channel>
    <title><?php print $name; ?></title>
    <description><?php print "$name - $link"; ?></description>
    <link><?php print $link; ?></link>
<?php
readfile( $staticrss );
?>
  </channel>
</rss>
