
#ifndef __WVGLIBSTREAMCLONE_H
#define __WVGLIBSTREAMCLONE_H

#include "wvstreamclone.h"
#include <glib/gmain.h>

/**
 * The mere existence of this object causes the cloned stream to be select()ed
 * on by the glib g_main_loop.  Thus, there is probably no point in selecting
 * on this particular stream yourself, although that wouldn't hurt anything.
 */
class WvGlibStreamClone : public WvStreamClone
{
    static WvGlibStreamClone *singleton;
    static GPollFunc oldpoll;
    static gint mypoll(GPollFD *ufds, guint nfds, gint timeout);
    
    GMainContext *context;
    
public:
    WvGlibStreamClone(GMainContext *_context, IWvStream *_clone);
    virtual ~WvGlibStreamClone();
};

#endif // __WVGLIBSTREAMCLONE_H
