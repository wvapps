 /* Be clean and pure (yeah, right!) */
#define GTK_DISABLE_DEPRECATED
#define G_DISABLE_DEPRECATED

#include "wvglibstreamclone.h"
#include "wvtcp.h"
#include "wvistreamlist.h"
#include <gconf/gconf-client.h>


static void gconf_notify(GConfClient *client,
			 guint        cnxn_id,
			 GConfEntry  *entry,
			 gpointer     user_data)
{
    if (gconf_entry_get_value(entry) == NULL)
	fprintf(stderr, "value is NULL\n");
    else if (gconf_entry_get_value(entry)->type == GCONF_VALUE_STRING)
    {
	fprintf(stderr, "value is a string.\n");
	fprintf(stderr, " ... '%s'\n", gconf_value_get_string(
			gconf_entry_get_value(entry)));
    }
    else
	fprintf(stderr, "value is something else!\n");
}


int main(int argc, char **argv)
{
    guint notify_id;
    
    WvIStreamList *list = new WvIStreamList;
    WvTCPConn tcp("127.0.0.1", 4111);
    list->append(&tcp, false);
    list->append(wvcon, false);
    tcp.autoforward(*wvcon);
    wvcon->autoforward(tcp);
    
    WvGlibStreamClone glibstream(NULL, list); // glibstream now owns list
    
    g_type_init();
    GConfClient *client = gconf_client_get_default();

    gconf_client_add_dir(client, "/test",
			 GCONF_CLIENT_PRELOAD_NONE, NULL);
    
    fprintf(stderr, "initial a: '%s'\n",
	    gconf_client_get_string(client, "/test/a", NULL));

    notify_id = gconf_client_notify_add(client, "/test/a", gconf_notify,
					0, NULL, NULL);
    notify_id = gconf_client_notify_add(client, "/test/b", gconf_notify,
					0, NULL, NULL);
    
    GMainLoop *loop = g_main_loop_new(NULL, 0);
    g_main_loop_run(loop);
    
    g_object_unref(G_OBJECT(client));
    return 0;
}
