 /* Be clean and pure */
#define GTK_DISABLE_DEPRECATED
#define G_DISABLE_DEPRECATED

#include <gconf/gconf-client.h>
#include <stdio.h>
#include <unistd.h>

static void gconf_notify(GConfClient *client,
			 guint        cnxn_id,
			 GConfEntry  *entry,
			 gpointer     user_data)
{
    if (gconf_entry_get_value(entry) == NULL)
	fprintf(stderr, "value is NULL\n");
    else if (gconf_entry_get_value(entry)->type == GCONF_VALUE_STRING)
    {
	fprintf(stderr, "value is a string.\n");
	fprintf(stderr, " ... '%s'\n", gconf_value_get_string(
			gconf_entry_get_value(entry)));
    }
    else
	fprintf(stderr, "value is something else!\n");
}


static GPollFunc oldpoll;

static gint mypoll(GPollFD *ufds, guint nfds, gint timeout)
{
    fprintf(stderr, "doing stuff (%d)\n", timeout);
    return oldpoll(ufds, nfds, timeout);
}


int main(int argc, char **argv)
{
    int i;
    guint notify_id;
    GMainLoop *loop;
    GConfClient *client;
    
    g_type_init();
    client = gconf_client_get_default();

    gconf_client_add_dir(client, "/test",
			 GCONF_CLIENT_PRELOAD_NONE, NULL);
    
    fprintf(stderr, "initial a: '%s'\n",
	    gconf_client_get_string(client, "/test/a", NULL));

    notify_id = gconf_client_notify_add(client, "/test/a", gconf_notify,
					(void *)56, NULL, NULL);
    notify_id = gconf_client_notify_add(client, "/test/b", gconf_notify,
					(void *)57, NULL, NULL);
    
    oldpoll = g_main_context_get_poll_func(NULL);
    g_main_context_set_poll_func(NULL, mypoll);
    
    loop = g_main_loop_new(NULL, 0);
    g_main_loop_run(loop);
    g_main_context_iteration(NULL, 1);
    
    for (i = 0; i < 30; i++)
    {
	fprintf(stderr, "---\n");
	g_main_context_iteration(NULL, 0);
	sleep(1);
	gconf_client_suggest_sync(client, NULL);
	fprintf(stderr, "current a: '%s'\n",
		gconf_client_get_string(client, "/test/a", NULL));

    }
    
    g_object_unref(G_OBJECT(client));
    return 0;
}
