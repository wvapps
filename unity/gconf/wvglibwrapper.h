
#ifndef __WVGLIBWRAPPER_H
#define __WVGLIBWRAPPER_H

#include "wvstream.h"
#include <glib/gmain.h>

/**
 * A wrapper class to make your glib (Gnome Library) main loop run from inside
 * a WvStreams-style main loop instead of a glib-style main loop.  Just add
 * one of these to a WvStreamList somewhere and all your glib stuff will
 * run as it should.
 */
class WvGlibWrapper : public WvStream
{
    static GPollFunc oldpoll;
    static gint mypoll(GPollFD *ufds, guint nfds, gint timeout);
    
    static WvGlibWrapper *singleton;
    GMainContext *context;

    // data saved by mypoll()
    GPollFD *ufds;
    guint nfds;
    gint timeout;
    bool sure_thing;
    
public:
    WvGlibWrapper(GMainContext *_context);
    virtual ~WvGlibWrapper();
    
protected:
    virtual bool pre_select(SelectInfo &si);
    virtual bool post_select(SelectInfo &si);
};


#endif // __WVGLIBWRAPPER_H
