#include "uniconfroot.h"
#include "wvlog.h"

extern "C" {
#include <gconf/gconf-backend.h>
#include <gconf/gconf.h>
}


// FIXME: no locking!


class UniSource : public GConfSource
{
public:
    UniConfRoot cfg;
    WvLog log;
    
    UniSource(WvStringParm moniker);
    ~UniSource();
};


UniSource::UniSource(WvStringParm moniker)
    : cfg(moniker), log(WvString("Uni %s", moniker), WvLog::Debug1)
{
    // clear out our "base class", which is really just a struct
    memset(this, 0, sizeof(GConfSource));
    
    log("Initializing.\n");
}


UniSource::~UniSource()
{
    log("Shutting down.\n");
    cfg.commit();
}


static void vtable_uni_shutdown(GError **err)
{
    // nothing special
}


static GConfSource *vtable_uni_resolve_address(const gchar *address,
					       GError **err)
{
    assert(!strncasecmp(address, "uni:", 4));
    address += 4;
    fprintf(stderr, "UniConf backend: creating source for '%s'\n",
	    address);
    return new UniSource(address);
}


static void vtable_uni_destroy_source(GConfSource *source)
{
    UniSource *s = (UniSource *)source;
    delete s;
}


static void vtable_uni_lock(GConfSource *source, GError **err)
{
    // FIXME
}


static void vtable_uni_unlock(GConfSource *source, GError **err)
{
    // FIXME
}


static gboolean vtable_uni_readable(GConfSource *source,
				    const gchar *_key, GError **err)
{
    // access controls are for weenies
    return true;
}


static gboolean vtable_uni_writeable(GConfSource *source,
				     const gchar *_key, GError **err)
{
    // access controls are for weenies
    return true;
}


static GConfValue *vtable_uni_query_value(GConfSource *source,
					  const gchar *key,
					  const gchar **locales,
					  gchar **schema_name,
					  GError **err)
{
    UniSource *s = (UniSource *)source;
    s->log("get('%s')\n", key);
    
    // ignore this for now
    if (schema_name)
	*schema_name = NULL;

    WvString val = s->cfg.xget(key);
    if (val.isnull())
	return NULL;
    else
    {
	GConfValue *value = gconf_value_new_from_string(GCONF_VALUE_STRING,
							val, NULL);
	return value;
    }
    
    return NULL;
}


static GConfMetaInfo *vtable_uni_query_metainfo(GConfSource *source,
						const gchar *key,
						GError **err)
{
    return NULL;
}


static void vtable_uni_set_value(GConfSource *source,
				 const gchar *key,
				 const GConfValue *value, GError **err)
{
    UniSource *s = (UniSource *)source;
    s->log("set '%s' = '%s'\n", key, gconf_value_to_string(value));
    s->cfg.xset(key, gconf_value_to_string(value));
}


static GSList *vtable_uni_all_entries(GConfSource *source,
				      const gchar *dir,
				      const gchar **locales,
				      GError **err)
{
    UniSource *s = (UniSource *)source;
    s->log("all_entries('%s')\n", dir);
    
    GSList *list = NULL;
    
    UniConf::Iter i(s->cfg[dir]);
    for (i.rewind(); i.next(); )
    {
	WvString key("/%s", i->key());
	s->log(" ... key: '%s'\n", key);
	GConfValue *val = gconf_value_new_from_string(GCONF_VALUE_STRING,
						      i->getme(), NULL);
	list = g_slist_prepend(list, gconf_entry_new(key, val));
    }
    
    return list;
}


static GSList *vtable_uni_all_subdirs(GConfSource *source,
				      const gchar *dir, GError **err)
{
    UniSource *s = (UniSource *)source;
    s->log("all_subdirs('%s')\n", dir);
    
    GSList *list = NULL;
    
    UniConf::Iter i(s->cfg[dir]);
    for (i.rewind(); i.next(); )
    {
	if (!i->haschildren())
	    continue;
	
	WvString key(i->key());
	s->log(" ... key: '%s'\n", key);
	
	// FIXME: gconf is actually broken for this call (in 2.2.0 at least)
	// since half of it assumes we're returning a list of entries, and
	// the other half assumes it's a list of strings.  So this won't
	// work in "local mode" of gconf unless you patched gconf.
	list = g_slist_prepend(list, g_strdup(key));
    }

    return list;
}


static void vtable_uni_unset_value(GConfSource *source,
				   const gchar *key,
				   const gchar *locale, GError **err)
{
    UniSource *s = (UniSource *)source;
    s->log("unset '%s'\n", key);
    s->cfg[key].remove();
}


static gboolean vtable_uni_dir_exists(GConfSource *source,
				      const gchar *dir, GError **err)
{
    UniSource *s = (UniSource *)source;
    s->log("exists? '%s'\n", dir);
    return s->cfg[dir].exists();
}


static void vtable_uni_remove_dir(GConfSource *source,
				  const gchar *dir, GError **err)
{
    UniSource *s = (UniSource *)source;
    s->log("rmdir '%s'\n", dir);
    s->cfg[dir].remove();
}


static void vtable_uni_set_schema(GConfSource *source,
				  const gchar *key,
				  const gchar *schema_key,
				  GError **err)
{
    // schemas are also for weenies
}


static gboolean vtable_uni_sync_all(GConfSource *source, GError **err)
{
    UniSource *s = (UniSource *)source;
    s->log("sync_all\n");
    s->cfg.commit();
    s->cfg.refresh();
    return true;
}


static void vtable_uni_clear_cache(GConfSource *source)
{
    // not really relevant...
}


static GConfBackendVTable uni_vtable = {
  vtable_uni_shutdown,
  vtable_uni_resolve_address,
  vtable_uni_lock,
  vtable_uni_unlock,
  vtable_uni_readable,
  vtable_uni_writeable,
  vtable_uni_query_value,
  vtable_uni_query_metainfo,
  vtable_uni_set_value,
  vtable_uni_all_entries,
  vtable_uni_all_subdirs,
  vtable_uni_unset_value,
  vtable_uni_dir_exists,
  vtable_uni_remove_dir,
  vtable_uni_set_schema,
  vtable_uni_sync_all,
  vtable_uni_destroy_source,
  vtable_uni_clear_cache
};




extern "C" G_MODULE_EXPORT const gchar *g_module_check_init (GModule *module)
{
    fprintf(stderr, "Initializing UniConf backend.\n");
    return NULL;
}

extern "C" G_MODULE_EXPORT GConfBackendVTable *gconf_backend_get_vtable(void)
{
    fprintf(stderr, "Getting UniConf vtable.\n");
    return &uni_vtable;
}
