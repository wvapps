#include "wvglibstreamclone.h"
#include <sys/poll.h>

WvGlibStreamClone *WvGlibStreamClone::singleton;
GPollFunc WvGlibStreamClone::oldpoll;


WvGlibStreamClone::WvGlibStreamClone(GMainContext *_context, IWvStream *_clone)
     : WvStreamClone(_clone)
{ 
    context = _context;
    assert(!singleton);
    singleton = this;
    oldpoll = g_main_context_get_poll_func(context);
    g_main_context_set_poll_func(context, mypoll);
}


WvGlibStreamClone::~WvGlibStreamClone()
{
    g_main_context_set_poll_func(context, oldpoll);

    assert(singleton == this);
    singleton = NULL;
}


gint WvGlibStreamClone::mypoll(GPollFD *ufds, guint nfds, gint timeout)
{
    SelectInfo si;
    bool sure = singleton->_build_selectinfo(si, timeout,
					     false, false, false, true);
    if (sure)
	si.msec_timeout = 0;
    
    GPollFD *newfds = new GPollFD[nfds + si.max_fd + 1], *fp;
    int newnfds = nfds;
    memcpy(newfds, ufds, nfds * sizeof(GPollFD));
    fp = newfds + nfds;
    
    // add any wvstream-related file descriptors
    for (int fd = 0; fd <= si.max_fd; fd++)
    {
	bool readme   = FD_ISSET(fd, &si.read);
	bool writeme  = FD_ISSET(fd, &si.write);
	bool exceptme = FD_ISSET(fd, &si.except);
	
	if (readme || writeme || exceptme)
	{
	    fp->fd = fd;
	    fp->events = readme*(POLLIN|POLLPRI) 
		           | writeme*POLLOUT | exceptme*POLLERR;
	    fp->revents = 0;
	    
	    fp++;
	    newnfds++;
	}
    }

    int retval = oldpoll(newfds, newnfds, si.msec_timeout);
    
    FD_ZERO(&si.read);
    FD_ZERO(&si.write);
    FD_ZERO(&si.except);
    
    if (retval > 0)
    {
	fp = newfds + nfds;
	for (int i = nfds; i < newnfds; i++)
	{
	    fp = newfds + i;
	    if (fp->revents)
	    {
		if (fp->revents & (POLLIN|POLLPRI))
		    FD_SET(fp->fd, &si.read);
		if (fp->revents & POLLOUT)
		    FD_SET(fp->fd, &si.write);
		if (fp->revents & POLLERR)
		    FD_SET(fp->fd, &si.except);
		retval--;
	    }
	}
    }
    
    memcpy(ufds, newfds, nfds * sizeof(GPollFD));
    deletev newfds;

    sure = sure || singleton->_process_selectinfo(si, true);
    if (sure)
	singleton->callback();
    
    return retval;
}


