#include "wvglibwrapper.h"
#include <sys/poll.h>

GPollFunc WvGlibWrapper::oldpoll;
WvGlibWrapper *WvGlibWrapper::singleton;


WvGlibWrapper::WvGlibWrapper(GMainContext *_context)
{
    context = _context ? _context : g_main_context_default();

    assert(!singleton);
    singleton = this;
    
    ufds = NULL;
    nfds = 0;
    timeout = 0;
    sure_thing = false;
    
    oldpoll = g_main_context_get_poll_func(context);
    g_main_context_set_poll_func(context, mypoll);
}


WvGlibWrapper::~WvGlibWrapper()
{
    g_main_context_set_poll_func(context, oldpoll);

    assert(singleton == this);
    singleton = NULL;
    
    if (ufds)
	deletev ufds;
    ufds = NULL;
}


gint WvGlibWrapper::mypoll(GPollFD *_ufds, guint _nfds, gint _timeout)
{
    //fprintf(stderr, "in mypoll(%d)...\n", _timeout);
    
    if (singleton->ufds)
	deletev singleton->ufds;
    singleton->ufds = new GPollFD[_nfds];
    memcpy(singleton->ufds, _ufds, _nfds * sizeof(_ufds[0]));
    singleton->nfds = _nfds;
    singleton->timeout = _timeout;
    
    int retval = oldpoll(_ufds, _nfds, 0); // never wait!  WvStreams will.
    singleton->sure_thing = (retval > 0);
    return retval;
}


bool WvGlibWrapper::pre_select(SelectInfo &si)
{
    if (si.inherit_request)
	return false; // we will never *actually* be readable/writable/etc.
    
    // run mypoll() and get the list of fds glib is interested in
    g_main_context_iteration(context, true);
    
    // sure_thing is true if oldpoll() was > 0.  That means mypoll() returned
    // > 0 and glib already read/wrote its fds, so we don't want to wait on
    // them now.  Instead, go around the loop one more time.
    if (sure_thing)
	return true;
    
    // If sure_thing is false, we basically did a select(0) on glib's fds
    // and they were all not ready.  In that case, it should be okay to
    // select on them ourselves and wake up when they *are* ready.
    assert(ufds);
    for (guint i = 0; i < nfds; i++)
    {
	GPollFD *fp = ufds + i;
	if (fp->events & (POLLIN|POLLPRI))
	    FD_SET(fp->fd, &si.read);
	if (fp->events & POLLOUT)
	    FD_SET(fp->fd, &si.write);
	if (fp->events & POLLERR)
	    FD_SET(fp->fd, &si.except);
	if (fp->events && si.max_fd < fp->fd)
	    si.max_fd = fp->fd;
    }
    
    // never wait longer than glib wanted to wait
    if (timeout >= 0 && (si.msec_timeout < 0 || si.msec_timeout > timeout))
	si.msec_timeout = timeout;
    
    return false;
}


bool WvGlibWrapper::post_select(SelectInfo &si)
{
    // nothing to actually do here - the real work mostly happens in
    // pre_select(), since it calls g_main_context_iteration().
    return false;
}


