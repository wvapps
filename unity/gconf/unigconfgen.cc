/*  
 * Worldvisions Weaver Software:
 *   Copyright (C) 2003 Net Integration Technologies, Inc.
 * 
 * A generator to make a UniConf object out of a GConfClient.
 */
#include "uniconfgen.h"
#include "wvmoniker.h"
#include <gconf/gconf-client.h>
#include "wvstream.h"
#include "wvtclstring.h"
#include "wvistreamlist.h"
#include "wvglibwrapper.h"
#include "wvstringlist.h"

class UniGConfGen : public UniConfGen
{
protected:
    WvGlibWrapper wrapper;
    GConfClient *client;
    guint notify_id;

    class GConfIter;

public:
    UniGConfGen();
    virtual ~UniGConfGen();

    /***** Overridden members *****/
    virtual WvString get(const UniConfKey &key);
    virtual void set(const UniConfKey &key, WvStringParm value);
    virtual void setv(const UniConfPairList &pairs);
    virtual bool haschildren(const UniConfKey &key);
    virtual Iter *iterator(const UniConfKey &key);
    virtual void flush_buffers() { }
};


/**
 * A wrapper class for the gconf iters to provide a UniConfGen-style iter.
 */
class UniGConfGen::GConfIter : public UniConfGen::Iter
{
protected:
    GConfClient *client;
    GSList *dirs, *entries, *cur;
    bool dirmode;
    IUniConfGen *gen;

public:
    GConfIter(GConfClient *client, const UniConfKey &parent,
	      IUniConfGen *_gen);
    ~GConfIter();

    /***** Overridden members *****/
    virtual void rewind();
    virtual bool next();
    virtual UniConfKey key() const;
    virtual WvString value() const;
};


static IUniConfGen *creator(WvStringParm s, IObject *, void *)
{
    return new UniGConfGen;
}

static WvMoniker<IUniConfGen> reg("gconf", creator);


static void gconf_notify(GConfClient *client, guint cnxn_id,
			 GConfEntry *entry, gpointer userdata)
{
    UniGConfGen *gen = (UniGConfGen *)userdata;
    WvString key(gconf_entry_get_key(entry));
    gen->delta(key, gen->get(key));
}


UniGConfGen::UniGConfGen() : wrapper(NULL)
{
    WvIStreamList::globallist.append(&wrapper, false);
    g_type_init();
    client = gconf_client_get_default();
    
    notify_id = gconf_client_notify_add(client, "/", gconf_notify,
					this, NULL, NULL);

    // we need to do this in order to get notifications...
    gconf_client_add_dir(client, "/",
			 GCONF_CLIENT_PRELOAD_NONE, NULL);
}


UniGConfGen::~UniGConfGen()
{
    gconf_client_notify_remove(client, notify_id);
    WvIStreamList::globallist.unlink(&wrapper);
    g_object_unref(G_OBJECT(client));
}


WvString UniGConfGen::get(const UniConfKey &key)
{
    GConfValue *val = gconf_client_get(client, WvString("/%s", key), NULL);
    if (!val)
	return haschildren(key) ? WvString("") : WvString::null;

    switch (val->type)
    {
    case GCONF_VALUE_LIST:
        {
	    GSList *list = gconf_value_get_list(val);
	    WvStringList l;
	    
	    for (; list; list = g_slist_next(list))
		l.append(new WvString(gconf_value_to_string(
				    (GConfValue *)list->data)), true);

	    return wvtcl_encode(l);
	}
    default:
	return gconf_value_to_string(val);
    }
}


void UniGConfGen::set(const UniConfKey &_key, WvStringParm value)
{
    hold_delta();
    
    WvString key("/%s", _key);
    if (value.isnull())
    {
	gconf_client_unset(client, key, NULL);
	delta(_key, WvString::null);
	unhold_delta();
	return;
    }
    
    GConfValueType type, type2;
    GConfValue *val = gconf_client_get(client, key, NULL);
    if (!val)
	type = GCONF_VALUE_STRING;
    else
    {
	type = val->type;
	if (type == GCONF_VALUE_LIST)
	    type2 = gconf_value_get_list_type(val);
    }
    fprintf(stderr, "Changing '%s': val was %p, type was %d.\n",
	    key.cstr(), val, type);
    
    switch (type)
    {
    case GCONF_VALUE_INT:
	gconf_client_set_int(client, key, value.num(), NULL);
	break;
    case GCONF_VALUE_FLOAT:
	// FIXME uses an int instead
	gconf_client_set_float(client, key, value.num(), NULL);
	break;
    case GCONF_VALUE_BOOL:
	gconf_client_set_bool(client, key, value.num(), NULL);
	break;
    case GCONF_VALUE_LIST:
	{
	    WvStringList l;
	    wvtcl_decode(l, value);
	    GSList *list = g_slist_alloc();
	    
	    WvStringList::Iter i(l);
	    for (i.rewind(); i.next(); )
	    {
		list = g_slist_prepend(list,
			       gconf_value_new_from_string(type2, *i, NULL));
	    }
	    
	    gconf_value_set_list_nocopy(val, g_slist_reverse(list));
	    gconf_client_set(client, key, val, NULL);
	}
	break;
    case GCONF_VALUE_STRING:
    default:
	gconf_client_set_string(client, key, value, NULL);
	break;
    }
    
    delta(_key, get(_key));
    unhold_delta();
}


void UniGConfGen::setv(const UniConfPairList &pairs)
{
    setv_naive(pairs);
}


bool UniGConfGen::haschildren(const UniConfKey &_key)
{
    WvString key("/%s", _key);
    return gconf_client_all_dirs(client, key, NULL)
	|| gconf_client_all_entries(client, key, NULL);
}


UniGConfGen::Iter *UniGConfGen::iterator(const UniConfKey &key)
{
    return new GConfIter(client, key, this);
}



/***** UniGConfGen::GConfIter *****/

UniGConfGen::GConfIter::GConfIter(GConfClient *_client,
				  const UniConfKey &parent,
				  IUniConfGen *_gen)
{
    client = _client;
    WvString key("/%s", parent);
    dirs    = gconf_client_all_dirs   (client, key, NULL);
    entries = gconf_client_all_entries(client, key, NULL);
    cur = NULL;
    dirmode = true;
    gen = _gen;
}


UniGConfGen::GConfIter::~GConfIter()
{
    g_slist_free(entries);
}


void UniGConfGen::GConfIter::rewind()
{
    cur = NULL;
    dirmode = true;
}


bool UniGConfGen::GConfIter::next()
{
    if (!cur)
    {
	if (dirs)
	{
	    cur = dirs;
	    dirmode = true;
	}
	else
	{
	    cur = entries;
	    dirmode = false;
	}
    }
    else
    {
	cur = g_slist_next(cur);
	if (dirmode && !cur)
	{
	    cur = entries;
	    dirmode = false;
	}
    }
    if (cur && dirmode
      && gconf_client_get(client, (const gchar *)cur->data, NULL))
	return next(); // skip dirs that have values - they're in entries too!
    return cur;
}


UniConfKey UniGConfGen::GConfIter::key() const
{
    if (dirmode)
    {
	const gchar *dir = (const gchar *)cur->data;
	return UniConfKey(dir).last();
    }
    else
    {
	GConfEntry *ent = (GConfEntry *)cur->data;
	return UniConfKey(gconf_entry_get_key(ent)).last();
    }
}


WvString UniGConfGen::GConfIter::value() const
{
    return gen->get(key());
}
