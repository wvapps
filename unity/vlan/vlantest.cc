#include "uniconfroot.h"

int main()
{
    UniConfRoot cfg("vlan: ini:/tmp/vlan.out tcp:10.0.0.201:23 admin {}",
		    false);
    
#if 0    
    cfg.xset("101/ports/24/tagging", "802.1q");
    cfg.xset("101/ports/24/tagging", "none");
    cfg.xset("101/ports/24/tagging", "");
    cfg.xset("101/ports/24/tagging", "none");
#endif
    cfg.commit();
    
    return 0;
}
