#include "wvistreamlist.h"
#include "wvstreamclone.h"
#include "wvlog.h"
#include "wvstringlist.h"
#include "unifiltergen.h"
#include "wvstrutils.h"
#include "wvtclstring.h"
#include "wvmoniker.h"
#include <ctype.h>

class WvSuperStackStream : public WvStreamClone
{
public:
    WvDynBuf in;
    WvLog log, inlog;
    
    WvSuperStackStream(IWvStream *s)
	: WvStreamClone(s),
            log("SuperStack  >> ", WvLog::Debug2),
          inlog("SuperStack <<  ", WvLog::Debug2)
    {
	const char setupstr[] = {0xff, 0xfb, 3,  0};
	print("%s", setupstr); // telnet initiation sequence
	print("\r\n", setupstr);
	waitprompt();
    }
    
    void waitprompt()
    {
	log("Waiting...\n");
	
	time_t timeout = 10000;
	while (isok() && select(timeout, true, false))
	{
	    char buf[128];
	    size_t len;
	    len = read(buf, sizeof(buf));
	    in.put(buf, len);
	    
	    inlog.write(buf, len);
	    
	    timeout = 10000;
	    size_t used = in.used();
	    if (used >= 2)
	    {
		const unsigned char *cptr = in.get(used);
		if (!memcmp(cptr+used-2, ": ", 2))
		    timeout = 200; // a prompt!
		in.unget(used);
	    }
	}
    }
    
    WvString _cmd(WvStringParm c)
    {
	log("%s\n", c);
	print("%s\r\n", c);
	waitprompt();
	return in.getstr();
    }
    
    WvString cmd(WvStringParm c)
    {
	print("\r\n");
	waitprompt();
	in.zap();
	
	return _cmd(c);
    }
    
    WvString cmd(WVSTRING_FORMAT_DECL)
        { return cmd(WvString(WVSTRING_FORMAT_CALL)); }
    
    void login(WvStringParm user, WvStringParm pass)
    {
	cmd(user);
	_cmd(pass);
    }
};


class SuperStackGen : public UniFilterGen
{
public:
    WvLog log;
    WvSuperStackStream s;
    bool refreshed_once;
    
    SuperStackGen(IUniConfGen *gen, IWvStream *conn,
		  WvStringParm user, WvStringParm pass)
	: UniFilterGen(gen), log("SuperStackGen", WvLog::Info),
          s(conn)
    {
	refreshed_once = false;
	WvIStreamList::globallist.append(&s, false);
	s.login(user, pass);
	
	// refresh the inner whether *we* get refreshed or not
	gen->refresh();
    }
    
    ~SuperStackGen()
    {
	s.cmd("logout");
	WvIStreamList::globallist.unlink(&s);
    }
    
    virtual void set(const UniConfKey &key, WvStringParm value);
    virtual bool refresh();
};


static IUniConfGen *creator(WvStringParm s, IObject *, void *)
{
    WvConstInPlaceBuf buf(s, s.len());
    IUniConfGen *gen = wvcreate<IUniConfGen>(wvtcl_getword(buf));
    IWvStream *conn = wvcreate<IWvStream>(wvtcl_getword(buf));
    WvString user = wvtcl_getword(buf), pass = wvtcl_getword(buf);

    return new SuperStackGen(gen, conn, user, pass);
}

static WvMoniker<IUniConfGen> reg("vlan", creator);



void SuperStackGen::set(const UniConfKey &_key, WvStringParm value)
{
    UniConfKey key(_key); // non-const
    WvString vlan(key.pop());
    
    if (key.pop() == "ports")
    {
	WvString port = key.pop();
	if (key.pop() == "tagging")
	{
	    WvString oldvalue = inner()->get(_key);
	    WvString type;
	    
	    if (!value && !!oldvalue)
		s.cmd("bridge vlan removePort %s %s", vlan, port);
	    else if (!oldvalue || !!strcasecmp(value, oldvalue))
	    {
		s.cmd("bridge vlan removePort %s %s", vlan, port);
		if (!strcasecmp(value, "802.1q"))
		    type = "802.1Q";
		else if (!strcasecmp(value, "none"))
		    type = "none";
		if (!!type)
		    s.cmd("bridge vlan addPort %s %s %s", vlan, port, type);
	    }
	    else
		type = oldvalue;
	    
	    inner()->set(_key, type);
	}
    }
}


bool SuperStackGen::refresh()
{
    // refreshing is super slow, so ignore any requests to do it more than
    // once.  (Sigh...)
    if (refreshed_once)
	return false;
    refreshed_once = true;
    
    inner()->set("/", WvString::null);
    
    WvStringList vlans;
    vlans.split(s.cmd("bridge vlan summary all"), "\n");
    WvStringList::Iter vlan(vlans);
    for (vlan.rewind(); vlan.next(); )
    {
	int num, local;
	char name[81];
	if (sscanf(*vlan, " %d %d %80s", &num, &local, name) == 3)
	{
	    log("Found vlan: vlan=%s, local=%s, name='%s'\n",
		num, local, name);
	    inner()->set(WvString("%s/name", num), name);
	}
    }
    
    for (int port = 1; port <= 24; port++)
    {
	int id, local;
	char name[81], tagging[81];
	WvString tagstr;
	
	WvStringList tags;
	tags.split(s.cmd("bridge port detail %s", port), "\r\n");
	WvStringList::Iter tag(tags);
	for (tag.rewind(); tag.next(); )
	{
	    if (sscanf(*tag, "%d %d %80s %80s ",
		       &id, &local, name, tagging) == 4)
	    {
		const char *cptr = strrchr(*tag, ' ');
		if (cptr)
		{
		    tagstr = cptr+1;
		    tagstr = trim_string(tagstr.edit());
		}
		else
		    tagstr = tagging;
		log("Port#%s is on vlan #%s (%s) using '%s' tagging\n",
		    port, id, name, tagstr);
		inner()->set(WvString("%s/ports/%s/tagging", id, port),
			     tagstr);
	    }
	}
    }
    
    return true;
}
