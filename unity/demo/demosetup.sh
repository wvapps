export TOPDIR=$HOME/src/niti
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$TOPDIR/src/wvstreams
unset LD_PRELOAD

killall    gconf-editor unikonf uniconfd dbus-daemon-1 gconfd-2 >&/dev/null
killall -9 gconf-editor unikonf uniconfd dbus-daemon-1 gconfd-2 >&/dev/null
rm -rf ~/.gconf
echo 'xml:readwrite:$(HOME)/.gconf' >$HOME/.gconf.path

uksetup()
(
	echo "[/]"
	echo "moniker=$1"
	echo
) >$HOME/.kde/share/config/unikonfrc

uksetup unix:/tmp/uniconf/uniconfsocket
