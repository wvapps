ifeq ($(TOPDIR),)
 TOPDIR=$(TOPSRCDIR)

 prefix=/usr/local
 WVSTREAMS_INC=
 WVSTREAMS_LIB=
 WVSTREAMS_BIN=$(prefix)/bin
 WVSTREAMS_SRC=$(TOPSRCDIR)

 PC_CFLAGS=$(shell pkg-config --cflags libwvstreams)
 ifeq ($(PC_CFLAGS),)
  $(error WvStreams does not appear to be installed)
 endif
 CPPFLAGS+=$(PC_CFLAGS)

 PC_LIBS=$(shell pkg-config --libs libwvstreams)
 ifeq ($(PC_LIBS),)
  $(error WvStreams does not appear to be installed)
 endif
 LIBS+=$(PC_LIBS)
else
 XPATH=..
 LIBS+=$(LIBUNICONF)
endif

SAVED_PACKAGE_VERSION := $(PACKAGE_VERSION)
SAVED_SO_VERSION := $(SO_VERSION)
include $(TOPDIR)/wvrules.mk
SAVED_PACKAGE_VERSION := $(SAVE_PACKAGE_VERSION)
SO_VERSION := $(SAVED_SO_VERSION)

VPATH=$(TOPSRCDIR)

ifneq ($(PC_CFLAGS),)
 CPPFLAGS+=$(PC_CFLAGS)
endif
