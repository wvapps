#ifndef __UNIMAILWRAP_H
#define __UNIMAILWRAP_H

#include "unitempgen.h"
#include "wvscatterhash.h"
#include "wvlog.h"


class UniMailWrapGen : public UniTempGen
{
    bool dirty;
    IUniConfGen *inner;
    WvLog log;
    
    struct KeyMsg {
	WvString key, msg;
	
	KeyMsg(WvStringParm _key, WvStringParm _msg)
	    : key(_key), msg(_msg)
	    { }
    };
    DeclareWvScatterDict(KeyMsg, WvString, key);
    KeyMsgDict msgname;

    /**
     * Called by inner generator when a key changes.
     * The default implementation calls delta(key).
     */
    virtual void gencallback(const UniConfKey &key, WvStringParm value,
                             void *userdata);
    
public:
    UniMailWrapGen(IUniConfGen *_inner);
    virtual ~UniMailWrapGen();
    
    /**** Overridden members ****/
    virtual void set(const UniConfKey &key, WvStringParm value);
    virtual void setv(const UniConfPairList &pairs);
    virtual void commit() { inner->commit(); }
    virtual bool refresh();
};


#endif // __UNIMAILWRAP_H
