#ifndef __WVEMAIL_H
#define __WVEMAIL_H

#include "wvlog.h"
#include "strutils.h"

class UniMailEmail
{
public:
    WvString filename;
    WvLog log;
    bool getbody;
    
    struct Header {
	WvString name;
	WvString value;
	
	Header(WvStringParm _name, WvStringParm _value)
	    : name(_name), value(_value)
	    { }
    };
    WvList<Header> headers;
    WvDynBuf body;
    
    UniMailEmail(WvStringParm _filename, bool _getbody);
    ~UniMailEmail();
    
    void save();
    
    void addheader(WvStringParm name, WvStringParm value)
        { headers.append(new Header(name, value), true); }
    
    Header *getheader(WvStringParm name)
    {
	WvList<Header>::Iter i(headers);
	for (i.rewind(); i.next(); )
	{
	    if (!strcasecmp(i->name, name))
		return i.ptr();
	}
	return NULL;
    }
    
    void setheader(WvStringParm name, WvStringParm value)
    {
	Header *h = getheader(name);
	if (h)
	    h->value = value;
	else
	    addheader(name, value);
    }
    
    WvString bodystr() const
    {
	WvBufCursor buf((WvBuf &)body, 0, body.used());
	return trim_string(buf.getstr().edit());
    }
    
    WvString getval(WvStringParm name)
    {
	Header *h = getheader(name);
	return h ? h->value : WvString();
    }
    
    void delheader(WvStringParm name)
    {
	WvList<Header>::Iter i(headers);
	for (i.rewind(); i.next(); )
	{
	    if (!strcasecmp(i->name, name))
		i.xunlink();
	}
    }
};


#endif // __WVEMAIL_H
