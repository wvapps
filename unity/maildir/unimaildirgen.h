
#ifndef __UNIMAILDIR_H
#define __UNIMAILDIR_H

#include <map>

#include "unimailemail.h"
#include "uniconfgen.h"
#include "wvdiriter.h"
#include "wvscatterhash.h"

class UniMaildirGen : public UniConfGen
{
protected:
    WvString dir;
    WvLog log;
    
    struct DirCache
    {
	WvString filename;
	time_t mtime;
	
	DirCache(WvStringParm _name, time_t _mtime)
	    : filename(_name), mtime(_mtime) 
	    { }
    };

    /* FIXME: This doesn't work. It might need to be a multimap
     * instead, depending on how it's actually used. */
    typedef std::map<WvString,DirCache *> DirCacheMap;
    
    DirCacheMap dircache;
    time_t dir_mtime;
    
    class ItemIter;
    friend class ItemIter;
    class HeaderIter;
    
    WvString filename(const UniConfKey &key);
    void update_cache();
    virtual void flush_buffers() { }
    
public:
    UniMaildirGen(WvStringParm _dir);
    virtual ~UniMaildirGen();
    
    /***** Overridden members *****/
    virtual WvString get(const UniConfKey &key);
    virtual void set(const UniConfKey &key, WvStringParm value);
    virtual void setv(const UniConfPairList &pairs);
    virtual bool haschildren(const UniConfKey &key);
    virtual bool refresh();
    virtual Iter *iterator(const UniConfKey &key);
};


/**
 * Iterate through the items in your maildir.
 */
class UniMaildirGen::ItemIter : public UniConfGen::Iter
{
protected:
    UniMaildirGen &dir;
    WvStringList list;
    WvStringList::Iter i;

public:
    ItemIter(UniMaildirGen &dir);
    ~ItemIter();

    /***** Overridden members *****/
    virtual void rewind();
    virtual bool next();
    virtual UniConfKey key() const;
    virtual WvString value() const;
};


/**
 * Iterate through the items in your maildir.
 */
class UniMaildirGen::HeaderIter : public UniConfGen::Iter
{
protected:
    UniMaildirGen &dir;
    WvString filename;
    UniMailEmail email;
    WvList<UniMailEmail::Header>::Iter i;
    bool rewound;

public:
    HeaderIter(UniMaildirGen &dir, WvStringParm _filename);
    ~HeaderIter();

    /***** Overridden members *****/
    virtual void rewind();
    virtual bool next();
    virtual UniConfKey key() const;
    virtual WvString value() const;
};


#endif // __UNIMAILDIR_H
