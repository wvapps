#include "unimailemail.h"
#include "wvfile.h"
#include "strutils.h"
#include <ctype.h>


UniMailEmail::UniMailEmail(WvStringParm _filename, bool _getbody)
    : filename(!!_filename ? _filename : WvString("")),
      log(filename, WvLog::Debug5)
{
    getbody = _getbody;
    log("Stuff: %s\n", filename);
    
    WvString name, value;
    WvFile file(filename, O_RDONLY);
    
    char *line;
    while ((line = file.getline(-1)) != NULL)
    {
	log("line: %s\n", line);
	
	if (!trim_string(line)[0] || !isspace(line[0]))
	{
	    log("End of header '%s'/'%s'\n", name, value);
	    if (!!name && !!value)
		headers.append(new Header(name, value), true);
	    name = WvString();
	}
	
	if (!trim_string(line)[0]) // end of headers
	    break;
	
	if (isspace(line[0])) // header continuation line
	    value.append(" %s", trim_string(line));
	else // new header line
	{
	    char *cptr = strchr(line, ':');
	    if (cptr)
	    {
		*cptr = 0;
		cptr++;
		if (*cptr)
		    cptr++;
		name = line;
		value = cptr;
	    }
	}
    }
    
    log("Got %s headers.\n", headers.count());

    if (getbody)
    {
	char buf[1024];
	size_t len;
	while ((len = file.read(buf, sizeof(buf))) > 0)
	    body.put(buf, len);
	log("body length is %s\n", body.used());
    }
}


UniMailEmail::~UniMailEmail()
{
    // you might want to call save()!
}


void UniMailEmail::save()
{
    assert(getbody);
    
    ::unlink(filename);
    WvFile file(filename, O_WRONLY|O_CREAT|O_TRUNC);
    
    WvList<Header>::Iter i(headers);
    for (i.rewind(); i.next(); )
	file.print("%s: %s\n", i->name, i->value);
    file.print("\n");
    file.write(bodystr());
}

