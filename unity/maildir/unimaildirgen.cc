/*  
 * Worldvisions Weaver Software:
 *   Copyright (C) 2003 Net Integration Technologies, Inc.
 * 
 * A generator to make a UniConf object out of a Unix Maildir, because I'm
 * evil.
 */
#include "unimaildirgen.h"
#include "wvmoniker.h"
#include "wvlog.h"


static IUniConfGen *creator(WvStringParm s, IObject *obj, void *)
{
    return new UniMaildirGen(s);
}

static WvMoniker<IUniConfGen> reg("maildir", creator);



UniMaildirGen::UniMaildirGen(WvStringParm _dir) 
    : dir(_dir), log(WvString("UniMaildirGen %s", dir), WvLog::Info),
	dircache(100)
{
    dir_mtime = 0;
    log("Starting.\n");
}


UniMaildirGen::~UniMaildirGen()
{
    log("Stopping.\n");
}


WvString UniMaildirGen::filename(const UniConfKey &_key)
{
    WvString key(_key.first());
    if (dircache.exists(key))
	return dircache[key]->filename;
    else
	return WvString("%s/new/%s", dir, key);
}


static time_t mtime(WvStringParm filename)
{
    struct stat st;
    if (stat(filename, &st) == 0)
	return st.st_mtime;
    else
	return 0;
}


// handy shortcut
static inline time_t mtime(WVSTRING_FORMAT_DECL)
{
    return mtime(WvString(WVSTRING_FORMAT_CALL));
}


void UniMaildirGen::update_cache()
{
    int must_do = false;
    time_t t;
    
    if ((t=mtime("%s/new", dir)) > dir_mtime) must_do = dir_mtime = t;
    if ((t=mtime("%s/cur", dir)) > dir_mtime) must_do += dir_mtime = t;
    if ((t=mtime("%s/tmp", dir)) > dir_mtime) must_do += dir_mtime = t;
    
    // nothing changed! refresh complete.
    if (!must_do) return;
    
    hold_delta();
    
    // remove entries that no longer exist
    {
	DirCacheMap::Iter i(dircache);
	for (i.rewind(); i.next(); )
	{
	    if (!mtime(i->data->filename))
	    {
		WvString key(i->key);
		log("Uncached '%s' / '%s'\n", key, i->data->filename);
		dircache.remove(key);
		i.rewind();
		
		delta(key, WvString());
	    }
	}
    }
    
    // add new entries
    {
	WvDirIter i(dir, true);
	for (i.rewind(); i.next(); )
	{
	    if (!S_ISREG(i->st_mode))
		continue; // skip it if not a regular file
	    
	    WvString base(i->name);
	    char *cptr = strchr(base.edit(), ':');
	    if (cptr)
		*cptr = 0; // trim any status flags
	    
	    if (dircache.exists(base))
	    {
		DirCache *e = dircache[base];
		if (e->mtime != i->st_mtime)
		{
		    log("Recached '%s' / '%s'\n", base, i->fullname);
		    e->mtime = i->st_mtime;
		    delta(base, WvString());
		}
	    }
	    else
	    {
		log("Cached '%s' / '%s'\n", base, i->fullname);
		dircache.add(base, 
			     new DirCache(i->fullname, i->st_mtime), true);
		delta(base, WvString());
	    }
	}
    }
    
    log("Now %s entries in cache.\n", dircache.count());
    unhold_delta();
}


WvString UniMaildirGen::get(const UniConfKey &key)
{
    update_cache();

    UniConfKey msg(key.first()), part(key.last(key.numsegments() - 1));
    
    if (key.numsegments() == 0)
    {
	return "<MAILDIR>";
    }
    else if (part == "body:") // they want the body
    {
	if (!dircache.exists(msg)) return WvString();
	
	UniMailEmail email(filename(key), true);
	log("body used: %s\n", email.body.used());
	return WvString(email.bodystr());
    }
    else if (part.numsegments() == 0) // toplevel
    {
	if (dircache.exists(msg))
	    return "exists";
	else
	    return WvString(); // not there
    }
    else // they want a header
    {
	UniMailEmail email(filename(key), false);
	return WvString(email.getval(key.last(key.numsegments()-1)));
    }
}


void UniMaildirGen::set(const UniConfKey &key, WvStringParm value)
{
    update_cache();
    
    if (key.numsegments() < 2)
	return; // can't do it
    
    UniMailEmail email(filename(key), true);
    if (!email.getheader("From"))
	email.setheader("From", "UniConf Maildir Backend <unity@localhost>");

    if (!strcasecmp(key.last().printable(), "body:"))
    {
	email.body.zap();
	email.body.putstr(value);
    }
    else
	email.setheader(key.last(key.numsegments()-1), value);
    email.save();
    delta(key, value);
}


void UniMaildirGen::setv(const UniConfPairList &pairs)
{
    setv_naive(pairs);
}


bool UniMaildirGen::haschildren(const UniConfKey &key)
{
    update_cache();
    
    UniConfKey msg(key.first()), part(key.last(key.numsegments() - 1));
    
    if (key.numsegments() == 0)
	return true;
    else if (part.numsegments() == 0) // assume everything has a header
	return true;
    else if (part == "body:")
	return false;
    else
	return false; // assume headers have no children
}


bool UniMaildirGen::refresh()
{
    update_cache();
    return true;
}


UniConfGen::Iter *UniMaildirGen::iterator(const UniConfKey &key)
{
    update_cache();
    
    if (key.numsegments() == 0)
	return new ItemIter(*this);
    else if (key.numsegments() == 1)
	return new HeaderIter(*this, filename(key));
    else
	return new UniConfGen::NullIter(); // nothing underneath headers
}


UniMaildirGen::ItemIter::ItemIter(UniMaildirGen &_dir)
    : dir(_dir), i(list)
{
}


UniMaildirGen::ItemIter::~ItemIter()
{
}


void UniMaildirGen::ItemIter::rewind()
{
    list.zap();
    
    UniMaildirGen::DirCacheMap::Iter di(dir.dircache);
    for (di.rewind(); di.next(); )
	list.append(new WvString(di->key), true);
    
    i.rewind();
}


bool UniMaildirGen::ItemIter::next()
{
    return i.next();
}


UniConfKey UniMaildirGen::ItemIter::key() const
{
    return UniConfKey(*i).last();
}


WvString UniMaildirGen::ItemIter::value() const
{
    return dir.get(*i);
}


UniMaildirGen::HeaderIter::HeaderIter(UniMaildirGen &_dir, WvStringParm _filename)
    : dir(_dir), filename(_filename), email(_filename, false), i(email.headers)
{
    rewound = true;
}


UniMaildirGen::HeaderIter::~HeaderIter()
{
}


void UniMaildirGen::HeaderIter::rewind()
{
    i.rewind();
    rewound = true;
}


bool UniMaildirGen::HeaderIter::next()
{
    if (!rewound && !i.cur())
	return false; // already past the last one, so we already gave body:
    rewound = false;
    if (i.next())
	return true;
    return true;
}


UniConfKey UniMaildirGen::HeaderIter::key() const
{
    if (rewound)
	return "";
    if (i.cur())
	return i->name;
    else
	return "body:";
}


WvString UniMaildirGen::HeaderIter::value() const
{
    return dir.get(WvString("%s/%s", filename, key()));
}
