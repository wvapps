#include "uniconfroot.h"
#include "wvstream.h"


int main()
{
//    UniConfRoot cfg("mailwrap:maildir:/home/unity/Maildir");
    UniConfRoot cfg("mailwrap:unix:/tmp/uniconf/uniconfsocket");
    
    {
	UniConf::RecursiveIter i(cfg);
	for (i.rewind(); i.next(); )
	    wvcon->print("#1 '%s' = '%s'\n", i->fullkey(), i->getme());
    }
    
    {
	UniConf::RecursiveIter i(cfg);
	for (i.rewind(); i.next(); )
	    i->setme("%s.", i->getme());
    }
    
    {
	UniConf::RecursiveIter i(cfg);
	for (i.rewind(); i.next(); )
	    wvcon->print("#2 '%s' = '%s'\n", i->fullkey(), i->getme());
    }
    
    return 0;
}
