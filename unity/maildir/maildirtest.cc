#include "uniconfroot.h"
#include "wvstream.h"

int main()
{
    UniConfRoot cfg("maildir:../mail.test/");
    cfg["/foo"].getme();
    wvcon->write(cfg["/foo/Subject"].getme());
    wvcon->write(cfg["/foo/body:"].getme());
    
    UniConf::Iter i(cfg["/foo"]);
    for (i.rewind(); i.next(); )
	wvcon->print("%s\n", i->key());
    
    cfg["/boo/Subject"].setme("bonk");
    return 0;
}
