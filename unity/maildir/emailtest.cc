#include "unimailemail.h"

int main()
{
    UniMailEmail em1("../mail.test/cur/silly:2,S", true);
    UniMailEmail em2("../mail.test/cur/silly2:2,S", true);
    
    em1.addheader("From", "emailtest");
    em1.addheader("Subject", "mailtest/a/1");
    em1.body.putstr("This is the value #1.\n");
    em1.save();

    em2.addheader("From", "emailtest");
    em2.addheader("Subject", "mailtest/a/2");
    em2.body.putstr("This is the value #1.\n");
    em2.save();
    
    return 0;
}
