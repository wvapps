#include "unimailwrapgen.h"
#include "wvmoniker.h"
#include <time.h>
#include <unistd.h>


static IUniConfGen *creator(WvStringParm s, IObject *obj, void *)
{
    IUniConfGen *gen = NULL;

    if (obj)
        gen = mutate<IUniConfGen>(obj);
    if (!gen)
        gen = wvcreate<IUniConfGen>(s);

    return new UniMailWrapGen(gen);
}

static WvMoniker<IUniConfGen> reg("mailwrap", creator);



UniMailWrapGen::UniMailWrapGen(IUniConfGen *_inner) 
    : log("MailWrap", WvLog::Debug), msgname(100)
{
    inner = _inner;
    if (inner)
	inner->add_callback(this,
			    UniConfGenCallback(this,
		    			       &UniMailWrapGen::gencallback),
			    NULL);
    refresh();
}


UniMailWrapGen::~UniMailWrapGen()
{
    if (inner)
	inner->del_callback(this);
}


void UniMailWrapGen::gencallback(const UniConfKey &key, WvStringParm value,
			      void *userdata)
{
    // refresh();
}


bool UniMailWrapGen::refresh()
{
    log("Refreshing...");
    
    hold_delta();
    
    UniTempGen::set("/", WvString());
    msgname.zap();
    
    // iterate over the top level: individual "emails"
    UniConfGen::Iter *i = inner->iterator("/");
    for (i->rewind(); i->next(); )
    {
	WvString key(i->key());
	WvString subj(inner->get(UniConfKey(key, "Subject")));
	if (subj)
	{
	    msgname.add(new KeyMsg(subj, key), true);
	    UniTempGen::set(subj, inner->get(UniConfKey(key, "body:")));
	}
    }
    
    log("Refresh done.\n");

    unhold_delta();
    
    return true;
}


void UniMailWrapGen::set(const UniConfKey &key, WvStringParm value)
{
    static int cheesy = 0;
    
    hold_delta();
    
    log("Set...");
    log("Look for '%s': %s\n", key, !!msgname[key]);

    UniTempGen::set(key, value);
    
    if (!!msgname[key])
    {
	// key already exists - just change that one email
	inner->set(UniConfKey(msgname[key]->msg, "body:"), value);
    }
    else
    {
	// generate a new email for this one
	WvString msg("unity.%s.%s.%s", getpid(), time(NULL), ++cheesy);
	msgname.add(new KeyMsg(msg, key), true);
	inner->set(UniConfKey(msg, "Subject"), key);
	inner->set(UniConfKey(msg, "body:"), value);
    }
    
    log("Set done.\n");

    unhold_delta();
}


void UniMailWrapGen::setv(const UniConfPairList &pairs)
{
    setv_naive(pairs);
}
