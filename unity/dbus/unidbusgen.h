/* -*- mode: c++ -*-
 * Worldvisions Weaver Software:
 *   Copyright (C) 2003-2005 Net Integration Technologies, Inc.
 *
 * A generator to talk to a UniConf server over DBus.
 */

#ifndef __UNIDBUSGEN_H
#define __UNIDBUSGEN_H

#include "wvdbus.h"
#include "uniconfgen.h"
#include "wvlog.h"

class UniDBusGen : public UniConfGen
{
protected:
    WvLog log;
    WvDBus conn;

    class Iter;
    friend class Iter;

public:
    int version;

    UniDBusGen();
    virtual ~UniDBusGen();

    /***** Overridden members *****/
    virtual bool isok();

    virtual void commit();
    virtual bool refresh();
    virtual WvString get(const UniConfKey &key);

    virtual void set(const UniConfKey &key, WvStringParm value);
    virtual void setv(const UniConfPairList &pairs);
    virtual void flush_buffers() { }

    virtual bool haschildren(const UniConfKey &key);
    virtual IUniConfGen::Iter *iterator(const UniConfKey &key);
};


/**
 * Iterate through the items in your DBusGen.
 */
class UniDBusGen::Iter : public UniConfGen::Iter
{
protected:
    UniDBusGen *gen;
    UniConfKey top;
    WvStringList keys, values;
    WvStringList::Iter ki, vi;

public:
    Iter(UniDBusGen *_gen, const UniConfKey &_top);

    /***** Overridden members *****/
    virtual void rewind();
    virtual bool next();
    virtual UniConfKey key() const;
    virtual WvString value() const;
};


#endif // __UNIDBUSGEN_H
