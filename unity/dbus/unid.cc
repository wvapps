#include "uniconfroot.h"
#include "uniwatch.h"
#include "wvdbus.h"
#include "wvglibwrapper.h"
#include "wvstream.h"
#include "wvistreamlist.h"

void callback(const UniConf &changed, const UniConfKey &key)
{
    printf("NOTICE '%s' '%s'\n",
	   changed[key].fullkey().cstr(),
	   changed[key].getme().cstr());
}

int main()
{
    UniConfRoot cfg("dbus:");
    if (!cfg.isok())
    {
	fprintf(stderr, "Can't connect to uniconf at 'dbus:'\n");
	return 5;
    }

    UniWatch w(cfg, &callback);

    WvDBus conn;
    WvDBus::SignalMsg msg("/ca/nit/uniconf", "ca.nit.UniConf", "NOTICE");
    msg.append("d/c", "me");
    conn.send(msg);

    cfg["b"].remove();

    UniConf::RecursiveIter i(cfg);
    for (i.rewind(); i.next(); )
	printf("%s = %s\n", i->fullkey().printable().cstr(), i->getme().cstr());
    WvIStreamList &list(WvIStreamList::globallist);
    WvGlibWrapper glib(NULL);
    list.append(&glib, false);

    while (true)
	list.runonce();

    return 0;
}
