/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 2003-2005 Net Integration Technologies, Inc.
 *
 * A daemon that proxies between D-BUS clients and a UniConf server.
 */

#include "wvdaemon.h"
#include "wvdbus.h"
#include "wvglibwrapper.h"
#include "wvlog.h"
#include "wvistreamlist.h"
#include "uniconfroot.h"
#include "uniwatch.h"

#include <assert.h>

#define UNI_SVC "ca.nit.uniconfd"
#define UNI_IFC "ca.nit.UniConf"
#define UNI_OBJ "/ca/nit/uniconfd"

UniConfRoot cfg;

const int UNICONF_PROTOCOL_VERSION = 19;
#define is(c) dbus_message_is_method_call(msg, UNI_IFC, c)

static void myfilter(WvDBus &conn, const WvDBus::Msg &msg)
{
    // Ignore messages we sent ourselves
    WvString sender(dbus_message_get_sender(msg));
    if (sender == conn.unique_name())
	return;

    WvLog log("unidbusd", WvLog::Debug);

    WvDBus::ReplyMsg reply(msg);

    WvStringList args;
    msg.decode(args);

    log(WvLog::Debug5,
	"< %s '%s'\n", dbus_message_get_member(msg), args.join("' '"));

    if (is("get"))
    {
	WvString key;
	if (!args.isempty())
	    key = args.popstr();

	log("<< GET '%s'\n", key);
	reply.append("OK");
	WvString value(cfg.xget(key));
	if (value)
	    reply.append(value);
	conn.send(reply);
    }
    else if (is("set"))
    {
	WvString key, value;
	if (!args.isempty())
	    key = args.popstr();
	if (!args.isempty())
	    value = args.popstr();

	log("<< SET '%s' '%s'\n", key, value);
	cfg.xset(key, value);
	reply.append("OK");
	conn.send(reply);
    }
    else if (is("setv"))
    {
	for (;;)
	{
	    WvString key, value;
	    if (!args.isempty())
		key = args.popstr();
	    if (!args.isempty())
		value = args.popstr();

	    log("<< SETV '%s' '%s'\n", key, value);
	    if (!key.isnull())
		cfg.xset(key, value);
	    else
		break;
	}
	reply.append("OK");
	conn.send(reply);
    }
    else if (is("del"))
    {
	WvString key;
	if (!args.isempty())
	    key = args.popstr();

	log("<< DEL '%s'\n", key);
	cfg[key].remove();
	reply.append("OK");
	conn.send(reply);
    }
    else if (is("subt"))
    {
	WvString key;
	if (!args.isempty())
	    key = args.popstr();

	log("<< SUBT '%s'\n", key);
	reply.append("OK");

	UniConf::Iter i(cfg[key]);
	for (i.rewind(); i.next(); )
	    reply.append(i->fullkey(key), i->getme(""));
	conn.send(reply);
    }
    else if (is("hchild"))
    {
	WvString key;
	if (!args.isempty())
	    key = args.popstr();

	log("<< HCHILD '%s'\n", key);
	reply.append("OK", cfg[key].haschildren());
	conn.send(reply);
    }
    else if (is("commit"))
    {
	cfg.commit();
	log("<< COMMIT\n");
    }
    else if (is("refresh"))
    {
	cfg.refresh();
	log("<< REFRESH\n");
	reply.append("OK");
	conn.send(reply);
    }
    else if (is("HELLO"))
    {
	log("<< HELLO\n");
	reply.append("UniConf Server ready.", UNICONF_PROTOCOL_VERSION);
	conn.send(reply);
    }
    else if (dbus_message_is_signal(msg, UNI_IFC, "NOTICE"))
    {
	WvString key, newvalue;
	if (!args.isempty())
	    key = args.popstr();
	if (!args.isempty())
	    newvalue = args.popstr();

	if (newvalue.isnull())
	{
	    log("<< NOTICE '%s'\n", key);
	    cfg[key].remove();
	}
	else
	{
	    log("<< NOTICE '%s' '%s'\n", key, newvalue);
	    cfg.xset(key, newvalue);
	}
    }
    else if (dbus_message_get_type(msg) == DBUS_MESSAGE_TYPE_METHOD_CALL)
    {
	log("<< UNKNOWN '%s' '%s'\n",
	    dbus_message_get_member(msg), args.join("' '"));
	reply.append("FAIL",
		     WvString("unknown command: %s", args.popstr()));
	conn.send(reply);
    }
}


struct UniHandler
{
    WvDBus &conn;
    WvLog log;

    UniHandler(WvDBus &_conn)
	: conn(_conn), log("unidbusd", WvLog::Debug)
    {
    }

    void proxy(const UniConf &changed, const UniConfKey &key)
    {
	WvString value(changed[key].getme());

	WvDBus::SignalMsg msg(UNI_OBJ, UNI_IFC, "NOTICE");
	if (value.isnull())
	{
	    log(">> NOTICE '%s'\n", key);
	    msg.append(key);
	}
	else
	{
	    log(">> NOTICE '%s' '%s'\n", key, value);
	    msg.append(key, value);
	}
	conn.send(msg);
    }
};


void run(WvDaemon &daemon, void *)
{
    cfg.mount("tcp:localhost:11000");

    // link glib mainloop into WvStreams
    WvIStreamList &list(WvIStreamList::globallist);
    WvGlibWrapper glib(NULL);
    list.append(&glib, false);
    WvLog log("unidbusd", WvLog::Error);

    WvDBus conn;
    if (!conn.isok())
    {
	log("Have you started a `dbus-daemon-1 --session` for the current \n"
	    "login session?\n"
	    "Have you set DBUS_SESSION_BUS_ADDRESS to point to it?\n");
	daemon.die(1);
	return;
    }

    conn.setcallback(myfilter);
    conn.request_name(UNI_SVC);
    conn.add_match("type='signal',interface='" UNI_IFC "'");

    UniHandler handler(conn);
    UniWatch notifications(cfg,
			   UniConfCallback(&handler, &UniHandler::proxy));

    while (daemon.should_run())
	list.runonce();

    daemon.die();
}

int main(int argc, char **argv)
{
    WvDaemon daemon("unidbusd", "0.1",
		    WvDaemonCallback(), run, WvDaemonCallback());

    return daemon.run(argc, argv);
}
