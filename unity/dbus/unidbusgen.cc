/*
 * Worldvisions Weaver Software:
 *   Copyright (C) 2003-2005 Net Integration Technologies, Inc.
 *
 * A generator to talk to a UniConf server over DBus.
 */

#include "unidbusgen.h"
#include "wvmoniker.h"
#include "wvlog.h"
#include "wvdbus.h"

#define UNI_SVC "ca.nit.uniconfd"
#define UNI_IFC "ca.nit.UniConf"
#define UNI_OBJ "/ca/nit/uniconfd"


// Uncomment the following line to enable call-debugging.
// #define DEBUG_UNIDBUSGEN
#ifdef DEBUG_UNIDBUSGEN
# define debug(...) log(__VA_ARGS__)
#else // DEBUG_UNIDBUSGEN
# define debug(...)
#endif // DEBUG_UNIDBUSGEN


const int SENDSYNC_TIMEOUT = 10000;


static IUniConfGen *creator(WvStringParm s, IObject *obj, void *)
{
    return new UniDBusGen;
}

static WvMoniker<IUniConfGen> reg("dbus", creator);

static void myfilter(WvDBus &conn, const WvDBus::Msg &msg)
{
    // Ignore messages we sent ourselves
    if (!strcmp(dbus_message_get_sender(msg), conn.unique_name()))
	return;

    WvStringList args;
    msg.decode(args);

    if (dbus_message_is_signal(msg, UNI_IFC, "NOTICE"))
    {
	WvString key, newvalue;
	if (!args.isempty())
	    key = args.popstr();
	if (!args.isempty())
	    newvalue = args.popstr();

	//WvLog log("foo", WvLog::Info);
	//log("<< NOTICE '%s' '%s'\n", key, newvalue);

	UniDBusGen *self = reinterpret_cast<UniDBusGen *>(conn.get_userdata());
	if (self)
	    self->delta(key, newvalue);
    }
}

UniDBusGen::UniDBusGen()
    : log(WvString("UniDBusGen"), WvLog::Debug5),
      version(-1)
{
    log("Starting.\n");
    if (isok())
    {
	conn.set_userdata(this);

	WvDBus::CallMsg msg(UNI_SVC, UNI_OBJ, UNI_IFC, "HELLO");
	WvDBus::Msg reply(conn.sendsync(msg, SENDSYNC_TIMEOUT));

	WvString server(reply.arg(0));
	WvString version_string(reply.arg(1));
	if (server.isnull() || strncmp(server, "UniConf", 7))
	{
	    // wrong type of server!
	    log(WvLog::Error, "Connected to a non-UniConf server!\n");
	    conn.close();
	}
	else
	{
	    version = 0;
	    sscanf(version_string, "%d", &version);
	    log("UniConf version %s.\n", version);

	    // Set the notification callback
	    conn.setcallback(&myfilter);
	    conn.add_match("type='signal',interface='" UNI_IFC "'");
	}
    }
    else
	log(WvLog::Error, "Can't connect to D-BUS!\n");
}


UniDBusGen::~UniDBusGen()
{
    log("Stopping.\n");
}


bool UniDBusGen::isok()
{
    debug("%s()\n", __func__);
    return conn.isok();
}


void UniDBusGen::commit()
{
    debug("%s()\n", __func__);
    if (!isok())
	return;

    WvDBus::CallMsg msg(UNI_SVC, UNI_OBJ, UNI_IFC, "commit");
    conn.send(msg);
}


bool UniDBusGen::refresh()
{
    debug("%s()\n", __func__);
    if (!isok())
	return false;

    WvDBus::CallMsg msg(UNI_SVC, UNI_OBJ, UNI_IFC, "refresh");
    WvDBus::Msg reply(conn.sendsync(msg, SENDSYNC_TIMEOUT));
    if (reply.arg(0) == "OK")
	return true;
    else
	return false;
}


WvString UniDBusGen::get(const UniConfKey &key)
{
    debug("%s('%s')\n", __func__, key);
    if (!isok())
	return WvString::null;

    WvDBus::CallMsg msg(UNI_SVC, UNI_OBJ, UNI_IFC, "get");
    msg.append(key);
    WvDBus::Msg reply(conn.sendsync(msg, SENDSYNC_TIMEOUT));
    if (reply)
	return reply.arg(1);
    else
	return WvString();
}


void UniDBusGen::set(const UniConfKey &key, WvStringParm value)
{
    debug("%s('%s', '%s')\n", __func__, key, value.cstr());
    if (!isok())
	return;

    if (value.isnull())
    {
	WvDBus::CallMsg msg(UNI_SVC, UNI_OBJ, UNI_IFC, "del");
	msg.append(key, value);
	conn.send(msg);
    }
    else
    {
	WvDBus::CallMsg msg(UNI_SVC, UNI_OBJ, UNI_IFC, "set");
	msg.append(key, value);
	conn.send(msg);
    }
}


void UniDBusGen::setv(const UniConfPairList &pairs)
{
    debug("%s(...)\n", __func__);
    if (!isok())
	return;

    WvDBus::CallMsg msg(UNI_SVC, UNI_OBJ, UNI_IFC, "setv");
    UniConfPairList::Iter i(pairs);
    for (i.rewind(); i.next(); )
	msg.append(i->key(), i->value());
    conn.send(msg);
}


bool UniDBusGen::haschildren(const UniConfKey &key)
{
    debug("%s()\n", __func__);
    if (!isok())
	return false;

    WvDBus::CallMsg msg(UNI_SVC, UNI_OBJ, UNI_IFC, "hchild");
    msg.append(key);
    WvDBus::Msg reply(conn.sendsync(msg, SENDSYNC_TIMEOUT));
    if (reply)
	return reply.arg(1).num();
    else
	return false;
}


IUniConfGen::Iter *UniDBusGen::iterator(const UniConfKey &key)
{
    return new Iter(this, key);
}


UniDBusGen::Iter::Iter(UniDBusGen *_gen, const UniConfKey &_top)
    : top(_top), ki(keys), vi(values)
{
    gen = _gen;
}


void UniDBusGen::Iter::rewind()
{
    keys.zap();
    values.zap();

    if (gen->isok())
    {
	WvDBus::CallMsg msg(UNI_SVC, UNI_OBJ, UNI_IFC, "subt");
	msg.append(top);
	WvDBus::Msg reply(gen->conn.sendsync(msg, SENDSYNC_TIMEOUT));
	if (reply)
	{
	    WvStringList l;
	    reply.decode(l);
	    l.popstr(); // skip "OK"

	    if (l.count() % 2)
	    {
		WvLog log("UniDBusGen", WvLog::Debug5);
		log("Malformed response to 'subt'.\n");
	    }
	    else
	    {
		WvStringList::Iter i(l);
		for (i.rewind(); i.next(); )
		{
		    keys.append(new WvString(*i), true);
		    i.next();
		    values.append(new WvString(*i), true);
		}
	    }
	}
    }

    ki.rewind();
    vi.rewind();
}


bool UniDBusGen::Iter::next()
{
    ki.next();
    return vi.next();
}


UniConfKey UniDBusGen::Iter::key() const
{
    return *ki;
}


WvString UniDBusGen::Iter::value() const
{
    return *vi;
}
