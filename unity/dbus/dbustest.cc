#include "wvdbus.h"
#include "wvglibwrapper.h"
#include "wvistreamlist.h"
#include "wvtcp.h"
#include <assert.h>



static void myfilter(WvDBus &conn, const WvDBus::Msg &msg)
{
    bool match = 
	dbus_message_is_method_call(msg, "ca.nit.dbustest", "StupidMember");
    wvcon->print("filter_func: %s (matches=%s)\n    ",
		 (int)(void *)msg, match);
    
    WvStringList l;
    msg.decode(l);
    WvStringList::Iter i(l);
    for (i.rewind(); i.next(); )
	wvcon->print("'%s' ", *i);
    wvcon->print("\n");
    
    if (match)
    {
	WvDBus::ReplyMsg reply(msg);
	dbus_message_append_args(reply,
				 DBUS_TYPE_DOUBLE, 42.6,
				 DBUS_TYPE_STRING, "rstring1",
				 DBUS_TYPE_STRING, "rstring2",
				 DBUS_TYPE_INVALID);
	conn.send(reply);
	wvcon->print("sent reply.\n");
    }
}
    
    
int main()
{
    // link glib mainloop into WvStreams
    WvIStreamList &list(WvIStreamList::globallist);
    WvGlibWrapper glib(NULL);
    list.append(&glib, false);

    // running connection between wvcon and uniconf daemon, just for fun
    WvTCPConn tcp("127.0.0.1", 4111);
    list.append(&tcp, false);
    list.append(wvcon, false);
    tcp.autoforward(*wvcon);
    wvcon->autoforward(tcp);
    
    WvDBus conn;
    conn.setcallback(myfilter);
    conn.request_name("ca.nit.dbustest");
    
    WvDBus::CallMsg msg("ca.nit.dbustest", "/ca/nit/dbustest",
			"ca.nit.dbustest", "StupidMember");
    msg.append("yes", "hello", "world");
    dbus_message_append_args(msg,
			     DBUS_TYPE_DOUBLE, 42.6,
			     DBUS_TYPE_STRING, "string1",
			     DBUS_TYPE_STRING, "string2",
			     DBUS_TYPE_INVALID);
    dbus_message_append_args(msg,
			     DBUS_TYPE_DOUBLE, 42.6,
			     DBUS_TYPE_STRING, "string1",
			     DBUS_TYPE_STRING, "string2",
			     DBUS_TYPE_INVALID);
    
    {
	WvDBus::Msg reply(conn.sendsync(msg, 1000));
	wvcon->print("reply: %s\n", (int)(DBusMessage*)reply);
    }
    
    while (tcp.isok() && wvcon->isok())
	list.runonce();
    
    return 0;
}
