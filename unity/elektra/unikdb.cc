#include "uniconfroot.h"
#include "wvlog.h"

#undef PACKAGE_NAME
#undef PACKAGE_STRING
#undef PACKAGE_TARNAME
#undef PACKAGE_VERSION
#include "config.h"

extern "C"
{
#include "kdb.h"
#include "kdbbackend.h"

    KDBBackend *kdbBackendFactory(void);
};

#define BACKENDNAME "uniconf"

static UniConfRoot *uni;

// Uncomment the following for debugging
//#define DEBUG_UNIKDB
#ifdef DEBUG_UNIKDB
static WvLog log("kdb-uniconf", WvLog::Debug5);
#else /* DEBUG_UNIKDB */
#include "wvlogrcv.h"
static WvLogConsole logconsole(0, WvLog::Error);
#define log(...)
#endif /* DEBUG_UNIKDB */

// WvStreams 4 doesn't have UniConf::isok().  So here's one that we provide.
#ifdef HAVE_DECL_UNICONF_ISOK
static bool uniconf_isok(UniConf *u)
{
    return u->isok();
}
#else /* HAVE_DECL_UNICONF_ISOK */
static bool uniconf_isok(UniConf *u)
{
    IUniConfGen *gen = NULL;
    if (u)
	gen = u->whichmount();
    return gen && gen->isok();
}
#endif /* HAVE_DECL_UNICONF_ISOK */

struct AutoSave
{
    ~AutoSave()
    {
	if (uni)
	{
	    if (uniconf_isok(uni))
		uni->commit();
	    delete uni;
	}
    }
};
static AutoSave autosave;

static void init()
{
    if (uni && !uniconf_isok(uni))
    {
	delete uni;
	uni = NULL;
    }
    if (!uni)
    {
	const char *root = getenv("UNICONF");
	if (!root)
	    root = "ini:elektra.conf";

	uni = new UniConfRoot(root);
    }
    uni->refresh();
}


static WvString keyname(const Key *key)
{
    size_t size = keyGetNameSize(key);
    if (size < 0) size = 0;
    WvString name;
    name.setsize(size + 1);
    char *cptr = name.edit();
    keyGetName(key, cptr, size);
    cptr[size] = 0;
    return name;
}


/**
 * Initialize the backend.
 * This is the first method kdbOpenBackend() calls after dynamically loading
 * the backend library.
 *
 * This method is responsible of:
 * - backend's specific configuration gathering
 * - all backend's internal structs initialization
 * - initial setup of all I/O details such as opening a file, connecting to a
 *   database, etc
 *
 * @return 0 on success, anything else otherwise.
 * @see kdbOpenBackend()
 * @see kdbOpen()
 * @ingroup backend
 */
int kdbOpen_backend()
{
    init();
    if (uni)
	return 0;
    else
	return 1;
}


/**
 * All finalization logic of the backend should go here.
 *
 * Called prior to unloading the backend dynamic module. Should ensure
 * that no functions or static/global variables from the module will
 * ever be accessed again.  Should free any memory that the backend no
 * longer needs.  After this call, libelektra.so will unload the backend
 * library, so this is the point to shutdown any affairs with the
 * storage.
 *
 * @return 0 on success, anything else otherwise.
 * @see kdbClose()
 * @ingroup backend
 */
int kdbClose_backend()
{
    return 0; /* success */
}


/**
 * Implementation for kdbStatKey() method.
 *
 * This method is responsible of:
 * - make necessary I/O to retrieve @p key->name's metadata
 * - fill the @p key struct with its metadata
 *
 * @see kdbStatKey() for expected behavior.
 * @ingroup backend
 */
int kdbStatKey_backend(Key *key)
{
    init();
    WvString name(keyname(key));
    log("kdbStatKey('%s')\n", name);

    if (!(*uni)[name].exists())
	return -1;

    return 0;
}


/**
 * Implementation for kdbGetKey() method.
 *
 * This method is responsible of:
 * - make necessary I/O to retrieve all @p key->name's value and metadata
 * - fill the @p key struct with its value and metadata
 *
 * @see kdbGetKey() for expected behavior.
 * @ingroup backend
 */
int kdbGetKey_backend(Key *key)
{
    init();
    WvString name(keyname(key));
    log("kdbGetKey('%s')\n", name);

    WvString val = uni->xget(name);
    if (!val)
    {
	errno = KDB_RET_NOTFOUND;
	return -1;
    }

    keySetString(key, val);
    return 0;
}


/**
 * Implementation for kdbSetKey() method.
 *
 * This method is responsible of:
 * - check the existence of @p key->name on persistent storage
 * - prepare the backend to receive a new or updated key
 * - use value and metadata from @p key to store them in the backend storage
 * - fill the @p key struct with its value and metadata
 *
 * @see kdbSetKey() for expected behavior.
 * @ingroup backend
 */
int kdbSetKey_backend(Key *key)
{
    init();
    WvString name(keyname(key));

    char *buf = NULL;
    ssize_t buf_size = keyGetValueSize(key);
    buf = static_cast<char *>(malloc(buf_size));
    keyGetString(key, buf, buf_size);
    buf[buf_size-1] = '\0';

    log("kdbSetKey('%s') = '%s'\n", name, buf);
    uni->xset(name, buf);
    uni->commit();
    return 0;
}


template <typename Iter>
static void fillset(KeySet *returned, Iter *_i,
		    bool include_dirs, bool include_files,
		    bool stat_only, bool inactive)
{
    Iter &i = *_i;
    for (i.rewind(); i.next(); )
    {
	if ((include_dirs && i->haschildren())
	    || (include_files && !i->haschildren()))
	{
	    const char *name = i->key().printable();
	    if (!inactive && name[0] == '.')
		continue;
	    Key *k = keyNew(i->fullkey().printable(), KEY_SWITCH_END);
	    if (!stat_only)
	    {
		const char *val = i->getme();
		if (val)
		    keySetString(k, i->getme());
		else
		    keyDel(k);
	    }
	    ksAppend(returned, k);
	}
    }
    delete _i;
}


/**
 * Implementation for kdbRename() method.
 *
 * @see kdbRename() for expected behavior.
 * @ingroup backend
 */
int kdbRename_backend(Key *key, const char *newName)
{
    init();
    WvString name(keyname(key));
    log("kdbRename('%s', '%s')\n", name, newName);

    UniConf root((*uni)[name]);
    // Move over the root value first
    WvString val(root.getme());
    if (val == WvString::null)
    {
	errno = KDB_RET_NOTFOUND;
	return -1;
    }
    uni->xset(newName, val);

    // Now recursively move the rest of them over
    UniConf::RecursiveIter i(root);
    for (i.rewind(); i.next(); )
	uni->xset(WvString("%s/%s", newName, i->fullkey(name)), i->getme());

    root.remove();
    uni->commit();
    keySetName(key, newName);
    return 0;
}


/**
 * Implementation for kdbRemoveKey() method.
 *
 * @see kdbRemove() for expected behavior.
 * @ingroup backend
 */
int kdbRemoveKey_backend(const Key *key)
{
    init();
    WvString name(keyname(key));

    log("kdbRemoveKey('%s')\n", name);
    (*uni)[name].remove();
    uni->commit();
    return 0;
}


/**
 * Implementation for kdbGetKeyChildKeys() method.
 *
 * @see kdbGetKeyChildKeys() for expected behavior.
 * @ingroup backend
 */
ssize_t kdbGetKeyChildKeys_backend(const Key *parentKey, KeySet *returned,
				   unsigned long options)
{
    init();
    WvString name(keyname(parentKey));
    log("kdbGetKeyChildKeys('%s', &returned, %s)\n", name, options);

    bool dirs = options & KDB_O_DIR;
    bool files = !(options & KDB_O_DIRONLY);
    bool stat_only = options & KDB_O_STATONLY;
    bool inactive = options & KDB_O_INACTIVE;

    if (options & KDB_O_SORT)
    {
	if (options & KDB_O_RECURSIVE)
	    fillset(returned, new UniConf::SortedRecursiveIter((*uni)[name]),
		    dirs, files, stat_only, inactive);
	else
	    fillset(returned, new UniConf::SortedIter((*uni)[name]),
		    dirs, files, stat_only, inactive);
    }
    else // not sorting
    {
	if (options & KDB_O_RECURSIVE)
	    fillset(returned, new UniConf::RecursiveIter((*uni)[name]),
		    dirs, files, stat_only, inactive);
	else
	    fillset(returned, new UniConf::Iter((*uni)[name]),
		    dirs, files, stat_only, inactive);
    }
    return returned->size;
}


/**
 * The implementation of this method is optional.
 * The builtin inefficient implementation will use kdbGetKey() for each
 * key inside @p interests.
 *
 * @see kdbMonitorKeys() for expected behavior.
 * @ingroup backend
 */
u_int32_t kdbMonitorKeys_backend(KeySet *interests, u_int32_t diffMask,
				 unsigned long iterations, unsigned sleep)
{
    // We should probably implement this using UniConf notifications.
    // The default fallback doesn't seeem to work very well.
    return 0;
}


/**
 *
 * The implementation of this method is optional.
 * The builtin inefficient implementation will use kdbGetKey() for
 * @p interest.
 *
 * @see kdbMonitorKey() for expected behavior.
 * @ingroup backend
 */
u_int32_t kdbMonitorKey_backend(Key *interest, u_int32_t diffMask,
				unsigned long iterations, unsigned sleep)
{
    // We should probably implement this using UniConf notifications.
    // The default fallback doesn't seeem to work very well.
    WvString name(keyname(interest));
    log("kdbMonitorKey('%s', %s, %s, %s)\n", name, diffMask, iterations, sleep);


    Key *tested = keyNew(0);
    keyDup(interest, tested);
    log("interest is %s, tested is %s\n", keyname(interest), keyname(tested));
    return 0;
}


/**
 * All KeyDB methods implemented by the backend can have random names, except
 * kdbBackendFactory(). This is the single symbol that will be looked up
 * when loading the backend, and the first method of the backend
 * implementation that will be called.
 *
 * Its purpose is to "publish" the exported methods for libelektra.so. The
 * implementation inside the provided skeleton is usually enough: simply
 * call kdbBackendExport() with all methods that must be exported.
 *
 * @return whatever kdbBackendExport() returns
 * @see kdbBackendExport() for an example
 * @see kdbOpenBackend()
 * @ingroup backend
 */
KDBBackend *kdbBackendFactory(void)
{
    return kdbBackendExport(BACKENDNAME,
			    KDB_BE_OPEN,	   &kdbOpen_backend,
			    KDB_BE_CLOSE,	   &kdbClose_backend,
			    KDB_BE_GETKEY,	   &kdbGetKey_backend,
			    KDB_BE_SETKEY,	   &kdbSetKey_backend,
			    KDB_BE_STATKEY,	   &kdbStatKey_backend,
			    KDB_BE_RENAME,	   &kdbRename_backend,
			    KDB_BE_REMOVEKEY,      &kdbRemoveKey_backend,
			    KDB_BE_GETCHILD,       &kdbGetKeyChildKeys_backend,
// 			    KDB_BE_MONITORKEY,     &kdbMonitorKey_backend,
// 			    KDB_BE_MONITORKEYS,    &kdbMonitorKeys_backend,
			    /* set to default implementation: */
			    KDB_BE_MONITORKEY,     &kdbMonitorKey_default,
			    KDB_BE_MONITORKEYS,    &kdbMonitorKeys_default,
			    KDB_BE_SETKEYS,	   &kdbSetKeys_default,
			    KDB_BE_END);
}
