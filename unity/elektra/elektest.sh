#!/bin/bash
set -e

export KDB_BACKEND=uniconf
export UNICONF=ini:elektra.conf

SAME()
{
    echo -n "! elektest.sh  '$1' == '$2'  "
    if [ "$1" != "$2" ]; then
        echo "FAILED"
	exit 1
    else
        echo "ok"
	return 0
    fi
}

RUN ()
{
    echo "$@"
    "$@"
}

rm -f elektra.conf
rm -rf ~/.kdb   # no cheating!

RUN kdb set user/a/b/c "foo"
RUN kdb set user/a/b/d "more foo"
RUN kdb set user/a/b/x "x foo"
RUN kdb set user/a/b/y/y "y/y foo"
RUN kdb set user/a/b/y/z "y/z foo"
RUN kdb set user/a/b/.z "hidden"

SAME "$(kdb get user/a/b/c)" "foo"
SAME "$(kdb get user/a/b/y/y)" "y/y foo"
SAME "$(echo $(kdb ls user/a/b | sort))" \
	"user/a/b/c user/a/b/d user/a/b/x user/a/b/y"
SAME "$(echo $(kdb ls -a user/a/b | sort))" \
	"user/a/b/c user/a/b/d user/a/b/x user/a/b/y user/a/b/.z"
SAME "$(echo $(kdb ls -R user/a/b | sort))" \
	"user/a/b/c user/a/b/d user/a/b/x user/a/b/y user/a/b/y/y user/a/b/y/z"
SAME "$(echo $(kdb ls -a -R user/a/b | sort))" \
	"user/a/b/c user/a/b/d user/a/b/x user/a/b/y user/a/b/y/y user/a/b/y/z user/a/b/.z"

RUN kdb mv user/a user/b
SAME "$(kdb ls user)" "user/b"
SAME "$(kdb get user/b/b/c)" "foo"
SAME "$(kdb get user/b/b/y/y)" "y/y foo"
SAME "$(echo $(kdb ls user/b/b | sort))" \
	"user/b/b/c user/b/b/d user/b/b/x user/b/b/y"
SAME "$(echo $(kdb ls -a user/b/b | sort))" \
	"user/b/b/c user/b/b/d user/b/b/x user/b/b/y user/b/b/.z"
SAME "$(echo $(kdb ls -R user/b/b | sort))" \
	"user/b/b/c user/b/b/d user/b/b/x user/b/b/y user/b/b/y/y user/b/b/y/z"
SAME "$(echo $(kdb ls -a -R user/b/b | sort))" \
	"user/b/b/c user/b/b/d user/b/b/x user/b/b/y user/b/b/y/y user/b/b/y/z user/b/b/.z"

(RUN sleep 2; RUN kdb set user/b/b/c bar) &
SAME "$(kdb monitor user/b/b/c)" "New value is bar"

# if we get this far, we passed
echo "All tests passed!"
exit 0
