
#include "kconfigunibackend.h"
#include "uniconfroot.h"
#include "wvlog.h"

//static UniConfRoot root();
static UniConfRoot root;

class KUniPriv
{
public:
    WvLog log;
    UniConf cfg;
    KConfigBase *config;
    bool did_read;
    
    KUniPriv(KConfigBase *_config, WvStringParm _filename,
	     WvStringParm _restype, bool usekdeglobals);
    ~KUniPriv();
    void read();
    void write();
};


KUniPriv::KUniPriv(KConfigBase *_config, WvStringParm _filename,
		   WvStringParm _restype, bool usekdeglobals)
    : log(WvString("KU:%s:%s", _restype, _filename)), cfg(root)
{
    did_read = false;
    config = _config;
    log("Creating. (globals=%s)\n", usekdeglobals);
    
    static bool init = false;
    if (!init)
    {
	init = true;
	// root["kde/config"].mount("fast-reget:unix:/tmp/uniwrap");
	// root["kde/config"].mount("fstree:/home/apenwarr/.kde/share/config");
    }
    
    
    // FIXME: do something with usekdeglobals
    WvString restype(!!_restype ? _restype : WvString("NOTHING"));
    WvString filename(!!_filename ? _filename : WvString("NOTHING"));
    cfg = root["kde"][restype][filename];
    if (!cfg.ismountpoint())
	cfg.mount(WvString("ini:/home/apenwarr/.kde/share/%s/%s",
			   restype, filename), true);
    
//    UniConf cfgx(cfg["KUniPriv wuz heer"]);
//    cfgx.setmeint(cfgx.getmeint() + 1);

    log("Created.\n");
}


KUniPriv::~KUniPriv()
{
    log("Deleting.\n");
}


void KUniPriv::read()
{
    if (did_read)
	return; // UniConf doesn't need to reread more than once
    
    log("Reading...");
    
    // cfg.refresh();
    
    UniConf::Iter group(cfg);
    for (group.rewind(); group.next(); )
    {
	UniConf::RecursiveIter entry(*group);
	for (entry.rewind(); entry.next(); )
	{
	    QCString groupkey(group->key().printable().cstr());
	    QCString entkey(entry->fullkey(*group).printable().cstr());
	    
	    KEntryKey key(groupkey, entkey);
	    KEntry val;
	    val.mValue = entry._value();
	    
	    config->putData(key, val);
	}
    }
    
    did_read = true;
    
    log("read.\n");
}


void KUniPriv::write()
{
    log("Writing...");
    KEntryMap kmap = config->internalEntryMap();

    for (KEntryMapIterator i = kmap.begin(); i != kmap.end(); ++i)
    {
	const KEntry &ent = *i;
#if 0
	if (ent.bGlobal != bGlobal)
	{
	    // wrong "globality" - might have to be saved later
	    bEntriesLeft = true;
	    continue;
	}
#endif
	
	WvString group(i.key().mGroup), key(i.key().mKey), val(ent.mValue);
	if (val.isnull())
	    val = ""; // never accidentally *delete* keys
	if (!!group && !!key)
	{
	    if (cfg[group].xget(key) != val)
	    {
		log("'[%s]%s' = '%s'\n", group, key, val);
		cfg[group].xset(key, val);
	    }
	}
    }

    // cfg.commit();
    log("written.\n");
}






KConfigUniBackEnd::KConfigUniBackEnd(KConfigBase *_config,
	     const QString &_fileName, const char * _resType,
	     bool _useKDEGlobals)
    : KConfigBackEnd(_config, _fileName, _resType, _useKDEGlobals)
{
    priv = new KUniPriv(_config, _fileName, _resType, _useKDEGlobals);
}


KConfigUniBackEnd::~KConfigUniBackEnd()
{
    if (priv) delete priv;
}


bool KConfigUniBackEnd::parseConfigFiles()
{
    priv->read();
    return true;
}


void KConfigUniBackEnd::sync(bool merge)
{
    priv->write();
}


