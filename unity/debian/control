Source: uniconf-unity
Priority: optional
Maintainer: Simon Law <sfllaw@debian.org>
Build-Depends: debhelper (>= 4.0.0), autotools-dev, libwvstreams-dev, libgconf2-dev, kdelibs4-dev, dbus-glib-1-dev, libelektra-dev
Standards-Version: 3.6.1
Section: libs

Package: libuniconf-unity-dev
Section: libdevel
Architecture: all
Depends: libuniconf-dbus0.1, libuniconf-elektra0.1, libuniconf-gconf0.1, kconfigbackend-uniconf, libuniconf-maildir0.1
Description: Project Unity: Development files
 Project Unity is the glue that sticks together the various disjoint
 configuration systems in the world.  The central hub of this system
 is UniConf.
 .
 Included in this package are the pkg-config files that let you link in
 the various frontends and backends in the Unity collection.

Package: libuniconf-dbus0.1
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Project Unity: Use D-BUS servers through UniConf
 Project Unity is the glue that sticks together the various disjoint
 configuration systems in the world.  The central hub of this system
 is UniConf.
 .
 This package provides a UniConf generator that allows you to mount
 D-BUS servers.

Package: uniconf-unidbusd
Section: admin
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Project Unity: Use UniConf mounts through D-BUS
 Project Unity is the glue that sticks together the various disjoint
 configuration systems in the world.  The central hub of this system
 is UniConf.
 .
 This package contains a D-BUS daemon that stores data in a UniConf
 tree.  This lets D-BUS-enabled applications use UniConf.

Package: libelektra-uniconf
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Project Unity: Use Elektra key databases through UniConf
 Project Unity is the glue that sticks together the various disjoint
 configuration systems in the world.  The central hub of this system
 is UniConf.
 .
 This package contains an Elektra backend that stores data in a UniConf
 tree.  This lets Elektra-enabled applications use UniConf.

Package: libuniconf-gconf0.1
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Project Unity: Use GConf databases through UniConf
 Project Unity is the glue that sticks together the various disjoint
 configuration systems in the world.  The central hub of this system
 is UniConf.
 .
 This package provides a UniConf generator that allows you to mount
 GConf servers.

Package: gconf2-backend-uniconf
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Project Unity: Use UniConf mounts through GConf
 Project Unity is the glue that sticks together the various disjoint
 configuration systems in the world.  The central hub of this system
 is UniConf.
 .
 This package contains a GConf backend that stores data in a UniConf
 tree.  This lets GConf-enabled applications use UniConf.

Package: kconfigbackend-uniconf
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Project Unity: Use UniConf mounts through KConfig
 Project Unity is the glue that sticks together the various disjoint
 configuration systems in the world.  The central hub of this system
 is UniConf.
 .
 This package contains a KConfig backend that stores data in a UniConf
 tree.  This lets KConfig-enabled applications use UniConf.

Package: libuniconf-maildir0.1
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Project Unity: Use Maildir directories thorough UniConf
 Project Unity is the glue that sticks together the various disjoint
 configuration systems in the world.  The central hub of this system
 is UniConf.
 .
 This package provides a UniConf generator that allows you to mount
 Maildir directories.  This can be useful to notify applications by
 delivering e-mail to them.


