#include "twcgamecontroller.h"
#include "twcplayer.h"

TWCPlayer::TWCPlayer(UniConf _cfg, TWCGameController &_game)
    : cfg(_cfg), game(_game), id(cfg.key().printable().num())
{
}


void TWCPlayer::die()
{
    cfg["dead"].setmeint(1);
}


void TWCPlayer::deduct_turns(int turns)
{
    decr_uniconf(cfg["turns"], turns);
}


void TWCPlayer::add_cash(int howmuch)
{
    incr_uniconf(cfg["cash"], howmuch);
}


void TWCPlayer::ships(TWCShipList &shiplist)
{
    WvStringList shipnums;
    shipnums.split(cfg["ships"].getme());
    WvStringList::Iter i(shipnums);
    for (i.rewind(); i.next(); )
	shiplist.append(game.ships[i().num()], false);
}


void TWCPlayer::add_ship(TWCShip *ship)
{
    add_list_item_uniconf(cfg["ships"], ship->id);
}


void TWCPlayer::remove_ship(TWCShip *ship)
{
    del_list_item_uniconf(cfg["ships"], ship->id);
}
