#include "twcgamecontroller.h"
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <wvdaemon.h>
#include <wvistreamlist.h>
#include <wvverstring.h>

#define TWC_VER 0x00001000

WvString cfgpath("/usr/local/share/games/twc");

void daemon_start_cb(WvDaemon &daemon, void *userdata)
{
    srand(time(0));
    TWCGameController **game = (TWCGameController **)userdata;
    if (!*game)
        *game = new TWCGameController(cfgpath);
}

void daemon_stop_cb(WvDaemon &daemon, void *userdata)
{
    TWCGameController **game = (TWCGameController **)userdata;
    if (*game)
    {
        delete *game;
        *game = NULL;
    }
}

void daemon_run_cb(WvDaemon &daemon, void *game)
{
    while (daemon.should_run())
        WvIStreamList::globallist.runonce();
}    

int main(int argc, char **argv)
{
    TWCGameController *game = NULL;
    WvDaemon *daemon = new WvDaemon("twc", ver_to_string(TWC_VER),
                                    WvDaemonCallback(daemon_start_cb),
                                    WvDaemonCallback(daemon_run_cb),
                                    WvDaemonCallback(daemon_stop_cb),
                                    &game);

    daemon->args.add_option('c', "config", "Config path", "string", cfgpath);

    daemon->run(argc, argv);

    delete daemon;

    if (!WvIStreamList::globallist.isok())
    {
        int game_err = WvIStreamList::globallist.geterr();
        WvString game_err_str("");

        if (game_err)
            game_err_str = strerror(game_err);

        if (!!game_err_str)
            fprintf(stderr, "%s\n", game_err_str.cstr());

        return game_err;
    }
    
    return 0;
}
