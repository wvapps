/* -*- Mode: C++ -*- */
#ifndef __TWCBUYABLE_H
#define __TWCBUYABLE_H

/** Classes for the various types of things you can buy.
 * TWCGameController has a dictionary of all buyables, indexed by "<type><id>",
 * and there are dictionaries of all individual buyable types, indexed by ids.
 */

#include <uniconf.h>
#include <wvhashtable.h>

class TWCBuyable
{
protected:
    UniConf cfg;

public:
    TWCBuyable(UniConf _cfg)
	: cfg(_cfg), id(cfg.key().printable().num()),
	  key("%s%s", cfg.parent().key().printable(), id) {}
    virtual ~TWCBuyable() {}

    const int id;
    WvString key;

    WvString name()
    { return cfg["name"].getme(); }
    WvString type()
    { return cfg.parent().key().printable(); }
};

DeclareWvDict(TWCBuyable, WvString, key);

class TWCCommodity: public TWCBuyable
{
public:
    TWCCommodity(UniConf _cfg)
	: TWCBuyable(_cfg) {}
    virtual ~TWCCommodity() {}

    int pc_mod()
    { return cfg["pc modifier"].getmeint(); }
    bool illegal()
    { return cfg["illegal"].getmeint(); }
    bool dangerous()
    { return cfg["dangerous"].getmeint(); }
};

DeclareWvDict(TWCCommodity, int, id);

class TWCShipClass: public TWCBuyable
{
public:
    TWCShipClass(UniConf _cfg)
	: TWCBuyable(_cfg)
    { key = WvString("ships%s", id); }
    virtual ~TWCShipClass() {}

    int max_shield()
    { return cfg["max shield"].getmeint(); }
    int max_weapon()
    { return cfg["max weapon"].getmeint(); }
    int move_cost()
    { return cfg["move cost"].getmeint(); }
    int attack_cost()
    { return cfg["attack cost"].getmeint(); }
    int holds()
    { return cfg["commodities/max"].getmeint(); }
    int hull_strength()
    { return cfg["hull strength"].getmeint(); }
};

DeclareWvDict(TWCShipClass, int, id);

class TWCShieldClass: public TWCBuyable
{
public:
    TWCShieldClass(UniConf _cfg)
	: TWCBuyable(_cfg) {}
    ~TWCShieldClass() {}

    int strength()
    { return cfg["strength"].getmeint(); }
    int regen()
    { return cfg["regen"].getmeint(); }
};

DeclareWvDict(TWCShieldClass, int, id);

class TWCWeaponClass: public TWCBuyable
{
public:
    TWCWeaponClass(UniConf _cfg)
	: TWCBuyable(_cfg) {}
    ~TWCWeaponClass() {}

    int damage()
    { return cfg["damage"].getmeint(); }
    int accuracy()
    { return cfg["accuracy"].getmeint(); }
    int recharge_time()
    { return cfg["recharge time"].getmeint(); }
};

DeclareWvDict(TWCWeaponClass, int, id);

#endif // __TWCBUYABLE_H
