#include "twcgamecontroller.h"
#include "twcplayerclient.h"
#include "twcstardock.h"
#include <wvstrutils.h>

TWCObjectIf *TWCStarDock::create_if(TWCHumanPlayerClient &plyrcli)
{
    return new TWCStarDockIf(*this, plyrcli);
}


bool TWCStarDock::bank_transaction(TWCPlayer &player, int deposit)
{
    if ((player.cash() - deposit) < 0 || (bank_balance(player) + deposit) < 0)
	return false;

    player.deduct_cash(deposit);
    incr_uniconf(cfg["bank/accounts"][player.id]["balance"], deposit);
    return true;
}


int TWCStarDock::ship_value(TWCShip *ship)
{
    int ship_tradein = cfg["shipyard/ships"][ship->shipclass()->id].getmeint();
    int weapon_tradein = cfg["shipyard/weapons"][ship->weaponclass()->id].getmeint();
    int shield_tradein = cfg["shipyard/shields"][ship->shieldclass()->id].getmeint();
    return ship_tradein + weapon_tradein + shield_tradein;
}


TWCStarDockIf::TWCStarDockIf(TWCStarDock &_stardock,
			     TWCHumanPlayerClient &_plyrcli)
    : TWCObjectIf(_stardock, _plyrcli), stardock(_stardock),
      sublocation(TWCSDMain)
{
}


void TWCStarDockIf::print_menu()
{
    WvString screen;

    switch (sublocation)
    {
    case TWCSDBank:
	screen = "bank";
	break;
    case TWCSDPolice:
	screen = "police";
	break;
    case TWCSDShipYard:
	screen = "shipyard";
	break;
    default:
	screen = "stardock";
    }
    plyrcli.print(stardock.game->get_screen(screen));
}


void TWCStarDockIf::menu_input()
{
    switch (sublocation)
    {
    case TWCSDBank:
	bank_menu_input();
	return;
    default:
	break;
    }

    char *line = plyrcli.getline(0);
    if (!line)
	return;

    trim_string(line);

    switch (line[0])
    {
    case 'B':
    case 'b':
	sublocation = TWCSDBank;
	break;

    case 'Y':
    case 'y':
	sublocation = TWCSDShipYard;
	break;

    case 'P':
    case 'p':
	sublocation = TWCSDPolice;
	break;

#if 0
    case 'R':
    case 'r':
	location_type = StarDockBar;
	break;
#endif

    case '?':
	print_menu();
	break;

    case 'q':
    case 'Q':
	stardock.blastoff();
	break;

    default:
	break;
    }
}


WvString TWCStarDockIf::prompt()
{
    return WvString("%s< %sStarDock %s> %s%s%s/%s%s", ANSI_YELLOW,
		    ANSI_GREEN, ANSI_YELLOW, ANSI_GREEN, 
		    plyrcli.player.current_turns(), ANSI_YELLOW, ANSI_GREEN,
		    plyrcli.player.max_turns());
}


// StarDock bank
void TWCStarDockIf::bank_menu_input()
{
    char *line = plyrcli.getline(0);
    if (!line)
	return;
    trim_string(line);

    switch (line[0])
    {
    case 'd':
    case 'D':
	bank_account(true);
	break;
    case 'w':
    case 'W':
	bank_account(false);
	break;
    case 'i':
    case 'I':
	bank_info();
	break;
    case 'q':
    case 'Q':
	sublocation = TWCSDMain;
	break;
    }
}


void TWCStarDockIf::bank_account(bool deposit)
{
    int amount = 0;
    bool abort = false;

    plyrcli.print("\nYou have %s in cash and %s in your account.\n",
		  stardock.game->price_string(plyrcli.player.cash()),
		  stardock.game->price_string(stardock.bank_balance(plyrcli.player)));
    plyrcli.print("%s how much? ", deposit ? "Deposit" : "Withdraw");
    amount = plyrcli.get_int(abort);
    if (abort || !amount)
	return;

    if (!stardock.bank_transaction(plyrcli.player, deposit ? amount
				                           : amount * -1))
	plyrcli.print("You don't have that much %s.\n",
		      deposit ? "cash" : "in your account");
    else
	plyrcli.print("Transaction successful.\n");
}


void TWCStarDockIf::bank_info()
{
    plyrcli.print("\nAccount balance: %s\n",
	  stardock.game->price_string(stardock.bank_balance(plyrcli.player)));
    plyrcli.print("Interest rate: %s%% per day\n", stardock.bank_interest());
}

#if 0
// Star Dock shipyard
void TWCStarDockIf::shipyard_menu_input()
{
    char *line = plyrcli.getline(0);
    if (!line)
	return;
    trim_string(line);

    switch (line[0])
    {
    case 'w':
    case 'W':
	buy_weapon();
	break;
    case 's':
    case 'S':
	buy_shield();
	break;
    case 'l':
    case 'L':
	sell_ship();
	break;
    case 'q':
    case 'Q':
	sublocation = TWCSDMain;
	break;
    }
}


void TWCStarDockIf::sell_ship()
{
    TWCShipList ships;
    stardock.sector()->find_player_ships(ships, &plyrcli.player);
    TWCShipList::Iter i(ships);
    for (i.rewind(); i.next(); )
	if (i().docked_with)
	    i.xunlink();

    TWCShip *selling = plyrcli.ship_menu(ships, "Which ship would you like "
					        "to sell");
    if (!selling)
	return;

    int value = stardock.ship_value(selling);

    print("I can give you %s for the ship and everything on it.\n",
	  stardock.game->price_string(value));

    if (!areyousure())
	return;

    stardock.sell_ship(selling);
}


void TWCStarDock::sell_ship(TWCPlayer *player, TWCShip *ship)
{
    int value = stardock.ship_value(ship);
    plyrcli.player.add_cash(value);
    game->remove_ship(id, ship);
}


void TWCHumanPlayerClient::buy_weapon()
{
    buy_weaponshield("weapon");
}


void TWCHumanPlayerClient::buy_shield()
{
    buy_weaponshield("shield");
}


void TWCHumanPlayerClient::buy_weaponshield(WvStringParm type)
{
    WvString pluraltype("%ss", type);
    UniConf yardcfg(cfg["stardock shipyards"][location_id]);

    int docked_ship = find_docked_ship();
    if (!docked_ship)
    {
	print("You have no ships docked here!\n");
	return;
    }
    UniConf shipcfg(cfg["ships"][docked_ship]);
    
    print("You currently have a %s on your ship.\n",
	  cfg[pluraltype][shipcfg[type].getint()]["name"].get());

    int tradein_rate = yardcfg["tradein"].getint();
    log("Trade-in rate is %s\n", tradein_rate);
    if (!tradein_rate)
	tradein_rate = 50;
    log("Trade-in rate is %s\n", tradein_rate);
    int tradein = yardcfg["sell"][pluraltype][shipcfg[type].getint()].getint() 
                  * tradein_rate / 100;
    if (!tradein)
	print("I'm not interested in those, so I can't give you a trade-in "
	      "discount.  Sorry.\n\n");
    else
	print("I'll give you %s for it if you buy a new %s from "
	      "me.\n\n", price_string(tradein), type);

    if (!areyousure())
	return;

    Purchase p(pluraltype);
    handle_transaction(&p, false);

    if (!p.num)
	return;

    if (p.type > cfg["shipclasses"][shipcfg["class"].getint()]
	            [WvString("max %s", type)].getint())
    {
	print("Sorry, your ship can't handle that kind of %s.", type);
	return;
    }

    if (p.type == shipcfg[type].getint())
    {
	print("Your ship already has one of those.\n");
	return;
    }

    int realprice = p.price - tradein;
    print("That's %s.", price_string(p.price));
    if (tradein)
    {
	print(".. after trade-in of %s... ", price_string(tradein));
	if (realprice > 0)
	    print("that's %s, please.\n", price_string(realprice));
	else if (realprice < 0)
	    print("that'll be %s back to you.\n", price_string(-1*realprice));
	else
	    print("that's an even trade.\n");
    }
    
    p.price = realprice;

    if (!p.cash(cfg, id, this))
    {
	print("No money, no goods, pal.\n");
	return;
    }

    shipcfg[type].setint(p.type);
    if (type == "shield")
	shipcfg["shield strength"].set(cfg["shields"][p.type]["strength"].getint());
    
    // Adjust port's price...?
}


// StarDock bar
void TWCHumanPlayerClient::menu_starbar()
{
    char *line = getline(0);

    switch (line[0])
    {
    case 'b':
    case 'B':
	starbar_drink();
	break;
    case 'g':
    case 'G':
	starbar_grub();
	break;
    case 't':
    case 'T':
	starbar_talk();
	break;
    case 'e':
    case 'E':
	starbar_eavesdrop();
	break;
    case 'v':
    case 'V':
	starbar_visit();
	break;
    case 'u':
    case 'U':
	starbar_underground();
	break;
    case 'q':
    case 'Q':
	location_type = StarDock;
	break;
    }
}


// StarDock bar
void TWCPlayerClient::starbar_drink()
{
    msg("You order a Jupiter Julep... 'That'll be %s chum,'"
	"growls the bartender.\n", price_string(3));
    decr_uniconf(pcfg()["cash"], 2);
}


void TWCPlayerClient::starbar_grub()
{
    msg("You scarf down a Hyperburger... 'That'll be %s mate,'"
	"snaps the waiter.\n", price_string(5));
    msg("You get the impression that you'd better pay...\n");
    decr_uniconf(pcfg()["cash"], 5);
}


void TWCPlayerClient::starbar_talk()
{
}


void TWCPlayerClient::starbar_visit()
{
}


void TWCPlayerClient::starbar_eavesdrop()
{
}


void TWCPlayerClient::starbar_underground()
{
}


// StarDock police
void TWCHumanPlayerClient::menu_starcop()
{
    char *line = getline(0);

    switch (line[0])
    {
    case 'q':
    case 'Q':
	location_type = StarDock;
	break;
    }
}
#endif
