/* -*- Mode: C++ -*- */
#ifndef __TWCPLAYER_H
#define __TWCPLAYER_H

#include "twcship.h"

class TWCGameController;

class TWCPlayer
{
protected:
    UniConf cfg;
    TWCGameController &game;

public:
    TWCPlayer(UniConf _cfg, TWCGameController &_game);
    virtual ~TWCPlayer() {}

    const int id;
    WvString name() { return cfg["name"].getme(); }
    WvString password() { return cfg["password"].getme(); }

    void die();
    bool dead() { return cfg["dead"].getmeint(); }
    int experience() { return cfg["experience"].getmeint(); }

    int cash() { return cfg["cash"].getmeint(); }
    void add_cash(int howmuch);
    void deduct_cash(int howmuch) { add_cash(howmuch * -1); }

    void resurrect() { cfg["dead"].setmeint(0); }

    int max_turns() { return cfg["max turns"].getmeint(); }
    int current_turns() { return cfg["current turns"].getmeint(); }
    void deduct_turns(int turns);

    void ships(TWCShipList &shiplist);
    void add_ship(TWCShip *ship);
    void remove_ship(TWCShip *ship);
};

DeclareWvDict(TWCPlayer, int, id);

#endif // __TWCPLAYER_H
