#include "twcgamecontroller.h"
#include "twcplayerclient.h"
#include <strutils.h>

void TWCShipIf::transporter_menu_input()
{
    char *line = plyrcli.getline(0);
    if (!line)
	return;
    trim_string(line);

    switch (line[0])
    {
    case 's':
    case 'S':
    {
	TWCShip *target = plyrcli.acquire_target("Transport to which ship?",
						 "That ship is docked.");
	if (!target)
	    return;

	TWCTransportSelfResults res = target->transport_self(plyrcli);

	if (res == TSAlreadyCaptained)
	    plyrcli.print("That ship already has a captain.\n");
	else if (res != TSAccessDenied)
	{
	    ship.set_captain(NULL);
	    plyrcli.print("You beam aboard the %s.\n", target->name());
	    if (res == TSClaimedOwnership)
		plyrcli.print("You claim ownership of the %s.\n",
			      target->name());
	    plyrcli.set_location(target);
	}
	break;
    }

    case 'c':
    case 'C':
    {
	bool beam_to_ship = true;
	plyrcli.print("Transport cargo to or from this ship? (t/f) ");
	line = plyrcli.continue_getline();
	if (!line)
	    return;
	trim_string(line);
	if (line[0] == 'f' || line[0] == 'f')
	    beam_to_ship = false;
      
	TWCShip *target = plyrcli.acquire_target(
	    WvString("Transport cargo %s which ship?",
		     beam_to_ship ? "from" : "to"),
	    "That ship is docked.");
	if (!target)
	    return;

	TWCShip *from_ship, *to_ship;
	if (beam_to_ship)
	{
	    from_ship = target;
	    to_ship = &ship;
	}
	else
	{
	    from_ship = &ship;
	    to_ship = target;
	}

	plyrcli.display_inventory(from_ship, true);
	plyrcli.print("Beam which commodity aboard? ");
	bool abort = false;
	int comm_type = plyrcli.get_int(abort);
	if (abort || !comm_type)
	    return;
	ship.game->log("commodity type to beam is %s\n", comm_type);
	int max = from_ship->onboard("commodities", comm_type);
	if (max > to_ship->free_holds())
	    max = to_ship->free_holds();
	plyrcli.print("Beam how many aboard (max %s)? ", max);
	int num = plyrcli.get_int(abort);
	if (abort)
	    return;
	if (num > max)
	    plyrcli.print("You can't transport that many.\n");
	else if (target->sector() != ship.sector())
	    plyrcli.print("The ship has left the sector!\n");
	else if (!to_ship->beam_cargo_aboard(from_ship, comm_type, num))
	    plyrcli.print("Cargo transportation failed.\n");
	else
	    plyrcli.print("Cargo transferred.\n");

	break;
    }

    case 'q':
    case 'Q':
	transporter = false;
	break;

    default:
	break;
   
    }
}

#if 0
transport_comm_result TWCPlayerClient::transport_commodities(int from,
							     int to,
							     int comm_type,
							     int number)
{
    int target, actor = loccfg().key().printable().num();
    TWCEventType event_type;
    if (to == actor)
    {
	target = from;
	event_type = CargoBeamedTo;
    }
    else
    {
	target = to;
	event_type = CargoBeamedFrom;
    }

    log("transport_commodities() from [%s] to [%s] actor [%s] target [%s]\n",
	from, to, actor, target);
    log("number of [%s] on from [%s] is [%s] (want [%s])\n", comm_type, from,
	cfg["ships"][from]["holds"][comm_type].getint(), number);
    if (check_shields(target) != CSAccessGranted)
	return TCAccessDenied;

    log("shields are down on target\n");
    if (cfg["ships"][from]["holds"][comm_type].getint() < number)
	return TCNotEnough;

    log("doing actual transport\n");
    int holds = cfg["ships"][to]["total holds"].getint();
    UniConf::Iter i(cfg["ships"][to]["holds"]);
    for (i.rewind(); i.next(); )
	holds -= i().getint();
    if (holds < number)
	return TCFull;

    decr_uniconf(cfg["ships"][from]["holds"][comm_type], number);
    incr_uniconf(cfg["ships"][to]["holds"][comm_type], number);

    WvString beam_result("%s holds of %s.\n", number,
			  cfg["commodities"][comm_type]["name"].get());
    msg("%s beamed to %s from %s.\n", beam_result,
	cfg["ships"][to]["name"].get(),
	cfg["ships"][from]["name"].get());

    game->announce(event_type, actor, target,
		   WvString("%s beams cargo %s %s.\n",
			    loccfg()["name"].get(),
			    comm_type == CargoBeamedTo ? "from" : "to",
			    cfg["ships"][target]["name"].get()),
		   loccfg()["location"].getint());

    return TCSuccessful;
}
#endif


TWCTransportSelfResults TWCShip::transport_self(TWCPlayerClient &plyrcli)
{
    if (captain())
	return TSAlreadyCaptained;

    if (check_shields(plyrcli) == CSAccessGranted)
    {
	check_claim_ownership(&plyrcli.player);
	// player client must set location itself
	return TSSuccessful;
    }
    return TSAccessDenied;
}


TWCCheckShieldsResults TWCShip::check_shields(TWCPlayerClient &plyrcli)
{
    if (!are_shields_up() || owner() == &plyrcli.player)
	return CSAccessGranted;

    if (!password())
	return CSAccessForbidden;

    if (password() == plyrcli.try_passwd())
	return CSAccessGranted;

    return CSAccessDenied;
}


TWCCheckShieldsResults TWCShipIf::check_shields(TWCShip *target)
{
    TWCCheckShieldsResults res = target->check_shields(plyrcli);
    if (res == CSAccessForbidden)
	plyrcli.print("You don't own this ship, its shields are up, and the "
		      "owner has forbidden transports.\n");
    else if (res != CSAccessGranted)
	plyrcli.print("Access denied.\n");

    return res;
}
