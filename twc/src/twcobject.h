/* -*- Mode: C++ -*- */
#ifndef __TWCOBJECT_H
#define __TWCOBJECT_H

#include <uniconf.h>

class TWCGameController;
class TWCHumanPlayerClient;
class TWCPlayerClient;
class TWCObjectIf;

// Base class for all locations.
class TWCObject
{
    friend class TWCObjectIf;

protected:
    UniConf cfg;                    // UniConf tree for this location
    TWCGameController *game;
    TWCObject *location;
    
public:
    TWCObject(UniConf _cfg, TWCGameController *_game)
	: cfg(_cfg), game(_game), location(NULL),
	  id(cfg.key().printable().num()) {}
    virtual ~TWCObject() {}

    virtual void set_location(TWCObject *_location)
    { location = _location; }
    virtual void set_by_player(TWCPlayerClient *plyrcli) {}
    virtual WvString name() { return cfg["name"].getme(); }
    virtual bool on_ship() = 0;
    const int id;
    virtual TWCObjectIf *create_if(TWCHumanPlayerClient &plyrcli) = 0;
};


// Interface class for human players
class TWCObjectIf
{
protected:
    TWCObject &obj;
    TWCHumanPlayerClient &plyrcli;

public:
    TWCObjectIf(TWCObject &_obj, TWCHumanPlayerClient &_plyrcli)
	: obj(_obj), plyrcli(_plyrcli) {}
    virtual ~TWCObjectIf() {}

    virtual void print_menu() = 0;
    virtual void menu_input() = 0;
    virtual WvString prompt() = 0;
};

#endif // __TWCOBJECT_H
