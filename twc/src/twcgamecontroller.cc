#include "twcgamecontroller.h"
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <wvistreamlist.h>
#include <wvstrutils.h>
#include <wvstream.h>
#include <uniinigen.h>
#include <unilistgen.h>
#include <unireadonlygen.h>

int incr_uniconf(UniConf cfg, int incr)
{
    int value = cfg.getmeint() + incr;
    cfg.setmeint(value);
    return value;
}


void add_list_item_uniconf(UniConf cfg, WvStringParm item)
{
    WvString liststr(cfg.getme());
    WvStringList list;
    if (!!liststr)
    {
	list.split(liststr);
	WvStringList::Iter i(list);
	for (i.rewind(); i.next(); )
	{
	    if (i() == item)
		return;
	}
	liststr.append(" ");
    }
    liststr.append(item);

    cfg.setme(liststr);
}


void del_list_item_uniconf(UniConf cfg, WvStringParm item)
{
    WvString liststr(cfg.getme());
    WvStringList list;
    list.split(liststr);
    WvStringList::Iter i(list);
    for (i.rewind(); i.next(); )
    {
	if (i() == item)
	    i.xunlink();
    }

    if (list.count() > 0)
	liststr = list.join(" ");
    else
	liststr = "";

    cfg.setme(liststr);
}


// FIXME: default cfg file location probably should be elsewhere.
TWCGameController::TWCGameController(WvString cfgpath)
    : players(50), curplayers(50), curhumans(10), ships(100), ports(200),
      log("GameController", WvLog::Info), buyables(40), shipclasses(10),
      shieldclasses(10),  weaponclasses(10), commodities(10),
      _nowhere(cfg, this), sectors(100)
{
    log("Initializing...\n");
    // Set up UniConf
    UniIniGen *gen, *defgen;
    UniConfGenList *l;
    UniListGen *listgen;

    gen = new UniIniGen(WvString("%s/config.ini", cfgpath));
    defgen = new UniIniGen(WvString("%s/config.def.ini", cfgpath));
    l = new UniConfGenList;
    l->append(gen, true);
    l->append(defgen, true);
    listgen = new UniListGen(l);
    cfg["config"].mountgen(listgen);

    gen = new UniIniGen(WvString("%s/playerinfo.ini", cfgpath));
    defgen = new UniIniGen(WvString("%s/playerinfo.def.ini", cfgpath));
    l = new UniConfGenList;
    l->append(gen, true);
    l->append(defgen, true);
    listgen = new UniListGen(l);
    cfg["playerinfo"].mountgen(listgen);

    gen = new UniIniGen(WvString("%s/universe.ini", cfgpath));
    defgen = new UniIniGen(WvString("%s/universe.def.ini", cfgpath));
    l = new UniConfGenList;
    l->append(gen, true);
    l->append(defgen, true);
    listgen = new UniListGen(l);
    cfg["universe"].mountgen(listgen);

    listener = new WvTCPListener(8420);
    listener->setcallback(WvStreamCallback(
			      this, &TWCGameController::listener_callback),
			  NULL);
    WvIStreamList::globallist.append(listener, true);
    WvIStreamList::globallist.append(&events, false);
    events.add_event(TWCEventCallback(this, &TWCGameController::tick), 0,
		     20, false);
    events.add_event(TWCEventCallback(this, &TWCGameController::cfg_save),
		     0, 60*5, false);
    events.add_event(TWCEventCallback(this,
				      &TWCGameController::shield_recharge),
		     0, 60, false);
    events.set_timer(1000);

    // Initialize class dictionaries.
    UniConf::Iter comm(cfg["config/commodities"]);
    for (comm.rewind(); comm.next(); )
    {
	TWCCommodity *c = new TWCCommodity(comm());
	commodities.add(c, true);
	buyables.add(c, false);
    }

    UniConf::Iter shipcl(cfg["config/shipclasses"]);
    for (shipcl.rewind(); shipcl.next(); )
    {
	TWCShipClass *s = new TWCShipClass(shipcl()); 
	shipclasses.add(s, true);
	buyables.add(s, false);
    }

    UniConf::Iter shldcl(cfg["config/shields"]);
    for (shldcl.rewind(); shldcl.next(); )
    {
	TWCShieldClass *s = new TWCShieldClass(shldcl());
	shieldclasses.add(s, true);
	buyables.add(s, false);
    }

    UniConf::Iter weapcl(cfg["config/weapons"]);
    for (weapcl.rewind(); weapcl.next(); )
    {
	TWCWeaponClass *w = new TWCWeaponClass(weapcl());
	weaponclasses.add(w, true);
	buyables.add(w, false);
    }

    UniConf::Iter pli(cfg["playerinfo/players"]);
    for (pli.rewind(); pli.next(); )
	players.add(new TWCPlayer(pli(), *this), true);

    UniConf::Iter shpi(cfg["playerinfo/ships"]);
    for (shpi.rewind(); shpi.next(); )
	ships.add(new TWCShip(cfg["playerinfo/ships"][shpi().key().printable()],
			      this), true);

    UniConf::Iter prti(cfg["universe/ports"]);
    for (prti.rewind(); prti.next(); )
        ports.add(new TWCPort(cfg["universe/ports"][prti().key().printable()],
                              this), true);

    UniConf::Iter seci(cfg["universe/sectors"]);
    for (seci.rewind(); seci.next(); )
	sectors.add(new TWCSector(cfg["universe/sectors"]
                                     [seci().key().printable()], this), true);

    log("Starting game.\n");
}


TWCGameController::~TWCGameController()
{
    log("Shutting down...\n");
    WvIStreamList::globallist.unlink(&events);
    curplayers.zap();
    cfg.commit();
}


void TWCGameController::listener_callback(WvStream&, void *)
{
    log("Connection pending.\n");
    WvTCPConn *newconn = listener->accept();
    TWCLogin *login = new TWCLogin(newconn, *this);
    WvIStreamList::globallist.append(login, true);
}


TWCSectorCoord TWCGameController::find_route(TWCSectorCoord src,
					     TWCSectorCoord dest)
{
    if (!src.ok || !dest.ok)
	return TWCSectorCoord("");

    TWCSectorCoord next(src);
    int step;
    IntChar *ic;
    IntCharList icl;
    ic = new IntChar(dest.x - src.x, 'x');
    icl.append(ic, true);
    ic = new IntChar(dest.y - src.y, 'y');
    icl.append(ic, true);
    ic = new IntChar(dest.z - src.z, 'z');
    icl.append(ic, true);

    IntCharList::Sorter s(icl, IntChar::abs_sort);
    s.rewind();
    s.next();
    if (s().i > 0)
	step = 1;
    else if (s().i < 0)
	step = -1;
    else
	return next;  // already there

    switch (s().c)
    {
    case 'x': next.x += step; break;
    case 'y': next.y += step; break;
    default: next.z += step; break;
    }

    return next;
}


void TWCGameController::cfg_save(int)
{
    cfg.commit();
}


void TWCGameController::tick(int)
{
    int gametime = cfg["config/global/time"].getmeint();
    // 4320 ticks * 20 ticks/sec = 1 day (realtime)
    int day_length = cfg["config/global/day length"].getmeint(4320);
    gametime++;
    cfg["config/global/time"].setmeint(gametime);
    if (gametime % day_length == 0)
	new_day();
}


TWCSector *TWCGameController::get_sector(TWCSectorCoord coord)
{
    TWCSector *res = sectors[coord];
    if (!res)
    {
	res = new TWCSector(cfg["universe/sectors"][coord.str()], this);
	sectors.add(res, true);
    }
    return res;
}


void TWCGameController::new_day()
{
    log("Running daily events.\n");
    // bank interest
    UniConf::Iter i(cfg["universe/stardock bank"]);
    for (i.rewind(); i.next(); )
    {
	UniConf::Iter j(i()["accounts"]);
	for (j.rewind(); j.next(); )
	{
	    int interest = j()["balance"].getmeint() * i()["interest"].getmeint()
	                   / 100;
	    j()["balance"].setmeint(j()["balance"].getmeint() + interest);
	}
    }

    int turns = cfg["config/global/turns"].getmeint(100);
    UniConf::Iter i2(cfg["playerinfo/players"]);
    for (i2.rewind(); i2.next(); )
	i2()["turns"].setmeint(turns);

    // random events
}


void TWCGameController::shield_recharge(int)
{
    UniConf::Iter i(cfg["playerinfo/ships"]);

    for (i.rewind(); i.next(); )
    {
	int shield = i()["shield"].getmeint();
	int regen = cfg["config/shields"][shield]["regen"].getmeint();
	int str = i()["shield strength"].getmeint();
	int maxstr = cfg["config/shields"][shield]["strength"].getmeint();

	str += regen;
	if (str > maxstr)
	    str = maxstr;

	i()["shield strength"].setmeint(str);
    }
}


void TWCGameController::remove_ship(TWCShip *ship)
{
    TWCSectorCoord loc = cfg["playerinfo/ships"][ship->id]["location"].getme();
    del_list_item_uniconf(cfg["universe/sectors"][loc.str()]["ships"],
			  ship->id);
    if (ship->owner())
    {
	UniConf targetpcfg(cfg["playerinfo/players"][ship->owner()->id]);
	del_list_item_uniconf(targetpcfg["ships"], ship->id);
    }
}


TWCHumanPlayerClient *TWCGameController::new_human_player(WvStringParm username,
							  WvStringParm password,
							  WvTCPConn *conn)
{
    // Look for unused ID.
    int id = find_next_unused(cfg["playerinfo/players"]);
    log("First unused id is %s.\n", id);
    cfg["playerinfo/userids"][username].setmeint(id);

    UniConf pcfg(cfg["playerinfo/players"][id]);
    pcfg["name"].setme(username);
    pcfg["current turns"].setmeint(turns());
    pcfg["alignment"].setmeint(0);
    pcfg["max turns"].setmeint(turns());
    pcfg["password"].setme(password);
    pcfg["verbose"].setmeint(1);
    pcfg["cash"].setme(starting_cash());

    TWCPlayer *player = new TWCPlayer(pcfg, *this);
    players.add(player, true);

    TWCHumanPlayerClient *plcli = new TWCHumanPlayerClient(conn, *this,
							   *player);
    WvIStreamList::globallist.append(plcli, true);
    curhumans.add(plcli, false);
    curplayers.add(plcli, false);

    return plcli;
}


TWCShip *TWCGameController::new_ship(TWCPlayer *player,
				     TWCShipClass *shipclass, WvStringParm name,
				     TWCSectorCoord location)
{
    // Look for ununsed ID.
    log("Creating new ship (%s)\n", name);
    int ship_id = find_next_unused(cfg["playerinfo/ships"]);
    log("First unused id is %s.\n", ship_id);

    UniConf shipcfg(cfg["playerinfo/ships"][ship_id]);
    shipcfg["docked"].setmeint(0);
    shipcfg["class"].setmeint(shipclass->id);
    shipcfg["location"].setme(location);
    shipcfg["owner"].setmeint(player->id);
    shipcfg["total holds"].setmeint(shipclass->holds());
    shipcfg["name"].setme(name);
    shipcfg["hull strength"].setmeint(shipclass->hull_strength());
    shipcfg["shields up"].setmeint(1);

    add_list_item_uniconf(cfg["playerinfo/players"][player->id]["ships"],
			  ship_id);
    add_list_item_uniconf(cfg["universe/sectors"][location.str()]["ships"],
			  ship_id);

    TWCShip *ship = new TWCShip(shipcfg, this);
    ships.add(ship, true);
    
    return ship;
}


TWCHumanPlayerClient *TWCGameController::log_player_on(TWCPlayer *player,
						       WvTCPConn *conn)
{
    TWCHumanPlayerClient *client = new TWCHumanPlayerClient(conn, *this,
							    *player);

    WvIStreamList::globallist.append(client, true);
    curhumans.add(client, false);
    curplayers.add(client, false);

    return client;
}


TWCLogin::TWCLogin(WvTCPConn *_tcp, TWCGameController &_game)
    : WvStreamClone(_tcp), game(_game), logged_in(false), tcp(_tcp),
      log("Login", WvLog::Info)
{
    uses_continue_select = true;
    print(game.screen("title"));
    print("Your name: ");
}


TWCLogin::~TWCLogin()
{
    terminate_continue_select();
}


char *TWCLogin::continue_getline()
{
    char *line = NULL;
    while (isok() && !line)
    {
	continue_select(5000);
	line = getline(0);
    }

    return line;
}


void TWCLogin::new_user(WvStringParm username)
{
    char *line;
    log("New user logging on (username [%s]).\n", username);
    print(game.screen("newuser"));

    WvString pw;
    print("Enter a password: ");
    line = continue_getline();
    if (!line)
	return;

    trim_string(line);
    pw = line;

    game.new_human_player(username, pw, tcp);

    // set self-destruct
    cloned = tcp = NULL;
    logged_in = true;
}


void TWCLogin::execute()
{
    WvStreamClone::execute();
    char *line;
    line = getline(0);
    if (!line)
    {
	print("Your name: ");
	return;
    }

    WvString username = trim_string(line);
    if (!username)
    {
	print("Your name: ");
	return;
    }

    int id = game.playerid(username);

    if (!id)
    {
	print("The name %s doesn't exist.\nWould you like to "
	      "join TWC? ", username);

	line = continue_getline();
	if (!line)
	    return;

	if (line[0] == 'y' || line[0] == 'Y')
	    new_user(username);
    }
    else if (game.isloggedin(id))
	print("That user is already connected!\n\n");
    else
    {
	TWCPlayer *player = game.players[id];
	print("Password: ");
	line = continue_getline();
	if (!line)
	    return;

	trim_string(line);

	if (!player->password() || !strcmp(line, player->password()))
	{
	    log("User %s [%s] is logged in.\n", username, id);
	    print("Welcome!\n");
	    game.log_player_on(player, tcp);

	    // engage self-destruct
	    cloned = tcp = NULL;
	    logged_in = true;
	    return;
	}
	else
	{
	    log("User %s [%s] had wrong password.\n", username, id);
	    print("Sorry, wrong password.\n\n");
	}
    }
    print(game.screen("title"));
    print("Your name: ");
}

