/* -*- Mode: C++ -*- */
#ifndef __TWCUTILS_H
#define __TWCUTILS_H

/// Returns true if 's' is an integer (contains only digits).
bool isint(const char *s);

/// Generates a random number between 0 and 1, inclusive.
float ranf();

/// Generates a random number based on a gaussian distribution.
float box_muller(float mean, float std_dev);

#endif // __TWCUTILS_H

