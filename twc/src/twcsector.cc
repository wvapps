#include "twcgamecontroller.h"
#include "twcsector.h"
#include <wvlog.h>

TWCSectorCoord::TWCSectorCoord(WvStringParm coordstr)
    : x(0), y(0), z(0), ok(false)
{
    WvStringList l;
    l.split(coordstr, ", \t\r\n");
    if (l.count() == 3)
    {
	ok = true;
	x = l.popstr().num();
	y = l.popstr().num();
	z = l.popstr().num();
    }
}


TWCSectorCoord::TWCSectorCoord(const TWCSectorCoord &coord)
    : x(coord.x), y(coord.y), z(coord.z), ok(coord.ok)
{
}


WvString TWCSectorCoord::str() const
{
    if (!ok)
	return WvString();
	
    return WvString("%s %s %s", x, y, z);
}


TWCSector::TWCSector(UniConf _cfg, TWCGameController *_game)
    : TWCObject(_cfg, _game), id(0), coord(_cfg.key().printable())
{
    _port = game->ports[cfg["port"].getmeint()];
    if (_port)
	_port->set_location(this);

    TWCShip *curship;
    bool unknown_ship = false;
    WvStringList correct_ships;
    WvStringList shipnums;
    shipnums.split(cfg["ships"].getme());
    WvStringList::Iter si(shipnums);
    for (si.rewind(); si.next(); )
    {
	curship = game->ships[si().num()];
	if (curship)
	{
	    curship->set_location(this);
	    correct_ships.append(new WvString(si()), true);
	}
	else
	{
	    game->log(WvLog::Error, "Unknown ship %s while processing sector "
		      "%s.\n", si().num(), id);
	    unknown_ship = true;
	}
    }

    // Clean up unknown ships
    if (unknown_ship)
	cfg["ships"].setme(correct_ships.join());
}


TWCSector::~TWCSector()
{
}


void TWCSector::find_player_ships(TWCShipList &list, TWCShip *except)
{
    WvStringList ships;
    ships.split(cfg["ships"].getme());
    WvStringList::Iter i(ships);
    for (i.rewind(); i.next(); )
    {
	TWCShip *ship = game->ships[i().num()];
	if (ship && ship != except)
	    list.append(ship, false);
    }
}


void TWCSector::find_player_ships(TWCShipList &list, TWCPlayer *player,
				  bool except)
{
    WvStringList ships;
    ships.split(cfg["ships"].getme());
    WvStringList::Iter i(ships);
    for (i.rewind(); i.next(); )
    {
	TWCShip *ship = game->ships[i().num()];
	if (ship && (!player || ((ship->owner() == player) ^ except)))
	    list.append(ship, false);
    }
}


void TWCSector::ship_warp_out(TWCShip *ship)
{
    del_list_item_uniconf(cfg["ships"], ship->id);
    announce(WarpOutOf, ship->id, 0,
	     WvString("%s warps out of this sector.\n", ship->name()),
	     ship->id);
    if (ship->towing())
	ship_warp_out(ship->towing());
}


void TWCSector::ship_warp_in(TWCShip *ship)
{
    add_list_item_uniconf(cfg["ships"], ship->id);
    announce(WarpInto, ship->id, 0,
	     WvString("%s warps into this sector.\n", ship->name()),
	     ship->id);
    if (ship->towing())
	ship_warp_in(ship->towing());
}


bool TWCSector::on_ship()
{
    game->log(WvLog::Error, "Someone called TWCSector::on_ship()!\n");
    return false;
}


void TWCSector::announce(TWCEventType type, int actor, int target,
			 WvStringParm msg, int except)
{
    game->log(WvLog::Debug, "[%s] type [%s] actor [%s] target [%s]: %s\n",
	      name(), type, actor, target, msg);
    TWCShipList ships;
    find_player_ships(ships, game->players[except], true);
    if (!ships.count())
	return;

    TWCShipList::Iter i(ships);
    for (i.rewind(); i.next(); )
    {
	TWCPlayerClient *captain;
	if (i().captain() && (captain = game->curplayers[i().captain()->player.id]))
	    captain->event(type, actor, target, msg);
    }
}
