/* -*- Mode: C++ -*- */
#ifndef __TWCPLAYERCLIENT_H
#define __TWCPLAYERCLIENT_H

#include "twcevent.h"
#include "twcship.h"
#include <uniconf.h>
#include <wvlog.h>
#include <wvstreamclone.h>
#include <wvstringlist.h>
#include <wvtcp.h>

DeclareWvTable2(IntTable, int);

int find_next_unused(const UniConf &cfg);

class TWCGameController;
class TWCObject;
class TWCObjectIf;

typedef WvCallback<WvString, WvStringParm> TWCQnACallback;
typedef WvCallback<void> TWCEndConvCallback;

struct TWCQnA
{
    WvString question;
    WvString answer;
    WvString default_answer;
    TWCQnACallback check_answer;

    TWCQnA(WvStringParm _question, WvStringParm _default_answer,
           TWCQnACallback _check_answer)
        : question(_question), default_answer(_default_answer),
          check_answer(_check_answer) {}
};
DeclareWvList2(TWCConversation, TWCQnA);


class TWCPlayerClient
{
protected:
    WvLog log;
    TWCObject *location;

public:
    TWCGameController &game;
    TWCPlayer &player;

    TWCPlayerClient(TWCGameController &_game, TWCPlayer &_player);
    virtual ~TWCPlayerClient();

    virtual void die();

    // For mostly cosmetic messages.
    virtual void msg(WvStringParm s) = 0;
    virtual void msg(WVSTRING_FORMAT_DECL) = 0;

    virtual void set_location(TWCObject *_location);

    virtual bool areyousure() { return true; }
    
    // For events that take place in a particular sector
    // i.e. ships docking, warping, fighting, etc.
    virtual void event(TWCEventType type, int actor, int target,
		       WvStringParm eventmsg) = 0;
    virtual void weapon_recharged() = 0;

    virtual WvString try_passwd() = 0;

protected:
#if 0
    // twcstardock.cc
    void starbar_drink();
    void starbar_grub();
    void starbar_talk();
    void starbar_eavesdrop();
    void starbar_visit();
    void starbar_underground();
    void sell_ship(int ship);
#endif
};


class TWCHumanPlayerClient: public WvStreamClone, public TWCPlayerClient
{
protected:
    TWCObjectIf *interface;

public:
    TWCHumanPlayerClient(WvTCPConn *tcp, TWCGameController &_game,
			 TWCPlayer &_player);
    virtual ~TWCHumanPlayerClient();

    virtual void die();

    virtual void msg(WvStringParm s) { print(s); }
    virtual void msg(WVSTRING_FORMAT_DECL)
    { print(WvString(WVSTRING_FORMAT_CALL)); }

    virtual void set_location(TWCObject *_location);

    char *continue_getline();
    int get_int(bool &abort, int dflt = 0);
    void quit();

    int menu(const IntTable &items, WvStringParm prompt);

    virtual bool areyousure();
    virtual void weapon_recharged()
    { print("\nYour ship's weapon has recharged.\n"); }

    virtual void event(TWCEventType type, int actor, int target,
		       WvStringParm eventmsg);
    bool new_ship(bool first_ship);

    void print_ships_menu(const TWCShipList &ships, WvStringParm prompt);
    TWCShip *ship_menu(const TWCShipList &ships, WvStringParm prompt);
    TWCShip *acquire_target(WvStringParm prompt, WvStringParm docked);

    virtual WvString try_passwd();

    void display_inventory(TWCShip *ship, bool menu = false);

    void do_conversation(TWCConversation &_conv, TWCEndConvCallback _endconv);
    void do_conversation_callback(WvStream &, void *);

private:
    bool create_new_interface;
    TWCConversation conv;
    TWCEndConvCallback endconv;

    virtual void execute();
    void send_question();
};

DeclareWvList(TWCPlayerClient);
DeclareWvDict(TWCPlayerClient, int, player.id);
DeclareWvDict(TWCHumanPlayerClient, int, player.id);

#endif // __TWCPLAYERCLIENT_H
