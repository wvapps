#include <unistd.h>
#include <stdio.h>

#include "twcgenerator.h"
#include "twcutils.h"
#include "twccontainer.h"

#define DEFAULT_INI_FILE "twc.ini"

int main( int argc, char* argv[] )
{
    int c;
    
    unsigned int num_sectors = 500;
    unsigned int num_planets = 50;
    float        std_dev = 5.0;    
    float        prob_bi_dir = 0.95;
    unsigned int max_dist = 1;   
    char         outfile[80];

    sprintf( outfile, DEFAULT_INI_FILE );
    
    opterr = 0;
    
    while( (c = getopt( argc, argv, "hn:s:p:P:d:o:")) != -1 )
    {
	switch( c )
	{
	case 'h':
	    fprintf( stderr, "Usage:\n" );
	    fprintf( stderr, "%s [-h]\n", argv[0] );
	    fprintf( stderr, "\t[-n <num_sectors> (%d)]\n", num_sectors );
	    fprintf( stderr, "\t[-p <num_planets> (%d)]\n", num_planets );
	    fprintf( stderr, "\t[-s <standard deviation for additional path probability fn.> (%f)]\n", std_dev );
	    fprintf( stderr, "\t[-P <probability of bi-directional link> (%f)]\n", prob_bi_dir );
	    fprintf( stderr, "\t[-d <maximum distance between sectors> (%d)]\n", max_dist );
	    fprintf( stderr, "\t[-o <output file name> (%s)]\n", outfile );
	    exit(1);
	    break;
	case 'n':
	    num_sectors = atoi( optarg );
	    break;
	case 's':
	    std_dev = atoi( optarg );
	    break;
	case 'p':
	    num_planets =atoi( optarg );
	    break;
	case 'P':
	    prob_bi_dir = atof( optarg );
	    break;
	case 'd':
	    max_dist = (unsigned int)(atoi( optarg ) );
	    break;
	case 'o':
	    if( strlen( optarg ) >= 80 )
	    {
		fprintf( stderr, "Output filename is too large.\n" );
		exit(-1);
	    }
	    sprintf( outfile, "%s", optarg );
	    break;
	case '?':       
	    fprintf( stderr, "Unknown option: %c\n", (char)optopt );
	    fprintf( stderr, "Exiting.\n" );
	    exit(-1);
	    break;
	} // switch( c )
    } // while( (c = ...
    
    if( prob_bi_dir < 0. || prob_bi_dir > 1. )
    {
	fprintf( stderr, "Error.  The probability of a bidirectional link must" \
		 "be between 0 and 1 (inclusive).\n" );
	exit(-1);
    } // if( prob_bi_dir ...
    
    fprintf( stdout, "Creating a galaxy with:\n" );
    fprintf( stdout, "\tSectors: %d\n", num_sectors );
    fprintf( stdout, "\tStandard Deviation: %f\n", std_dev );
    fprintf( stdout, "\tProbability of bidirectional path: %f\n", prob_bi_dir );
    fprintf( stdout, "\tMaximum path distance: %d\n", max_dist );
    fprintf( stdout, "\tWriting results to: %s\n", outfile );
    
    TWCGenerator galaxy;
    
    galaxy. GenerateSectors( num_sectors );
    galaxy. ConnectAllSectors( std_dev, prob_bi_dir, max_dist );

    // well-connect sectors 1-10
    for( unsigned int from = 0; from < 10; ++from )
    {
	for( unsigned int to = 0; to < 10; ++to )
	{
	    if( from == to )
	    {
		continue;
	    }
	    else
	    {
		galaxy. Connect( from, to, 1 );
	    }
	}
    }

    // place planet at sector 1
    galaxy. PlacePlanet( 0 );

    // randomly place 'num_planets' other planets
    galaxy. PlaceRandomPlanets( num_planets );

    // place ports
    galaxy. PlacePortsAtPlanets();

    galaxy. DumpToFile( outfile );

    fprintf( stdout, "Done.\n" );

    return 0;
} // int main( ...

