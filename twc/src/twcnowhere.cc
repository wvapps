#include "twcnowhere.h"
#include "twcgamecontroller.h"
#include "twcplayerclient.h"

TWCObjectIf *TWCNowhere::create_if(TWCHumanPlayerClient &plyrcli)
{
    return new TWCNowhereIf(*this, plyrcli);
}


void TWCNowhereIf::print_menu()
{
    plyrcli.print(plyrcli.game.get_screen("nowhere"));
}


void TWCNowhereIf::menu_input()
{
    char *line = plyrcli.getline(0);
    if (!line)
	return;

    switch (line[0])
    {
    case 'q':
    case 'Q':
	plyrcli.quit();
	break;

    case 'E':
    case 'e':
    {
	bool wasdead = plyrcli.player.dead();
	if (wasdead)
	    plyrcli.player.resurrect();

	TWCShipList ships;
	plyrcli.player.ships(ships);
	if (!ships.count())
	{
	    plyrcli.new_ship(wasdead);
	    plyrcli.player.ships(ships);
	}

	plyrcli.print("You warm up the engines of %s and prepare for "
		      "your trading day.\n", ships.first()->name());
	plyrcli.set_location(ships.first());
#if 0
	int towed = loccfg()["being towed"].getint();
	if (towed)
	{
	    TWCPlayerClient *towing_player = game->players[
		cfg["ships"][towed]["captain"].getint()];
	    
	    if (towing_player)
		towing_player->event(Event, loccfg().key().printable().num(),
				     0, WvString("%s has just come online.\n",
						 loccfg()["name"].get()));
	    loccfg()["being towed"].setint(0);
	}
#endif
//	player.location->sector_scan();

	break;
    }

    default:
	break;

    }
}


WvString TWCNowhereIf::prompt()
{
    return WvString("%s< %sEntrance %s>", ANSI_YELLOW, ANSI_GREEN,
		    ANSI_YELLOW);
}
