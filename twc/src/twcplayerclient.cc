#include "twcgamecontroller.h"
#include <strutils.h>

static int numcmp(const UniConf &a, const UniConf &b)
{
    return (a.key().printable().num() > b.key().printable().num());
}


int find_next_unused(const UniConf &cfg)
{
    UniConf::SortedIter s(cfg, numcmp);

    // Look for ununsed ID.
    int id = 1;
    for (s.rewind(); s.next(); )
    {
	if (id != s->key().printable().num())
	    break;
	id++;	    
    }
    return id;
}


TWCPlayerClient::TWCPlayerClient(TWCGameController &_game, TWCPlayer &_player)
    :  log("Player", WvLog::Info), game(_game), player(_player)
{
    set_location(game.nowhere());
}


TWCPlayerClient::~TWCPlayerClient()
{
    game.curplayers.remove(this);
}


void TWCPlayerClient::die()
{
}


void TWCPlayerClient::set_location(TWCObject *_location)
{
    assert(_location);
    location = _location;
    location->set_by_player(this);
}


TWCHumanPlayerClient::TWCHumanPlayerClient(WvTCPConn *tcp, TWCGameController
					   &_game, TWCPlayer &_player)
    : WvStreamClone(tcp), TWCPlayerClient(_game, _player)
{
    uses_continue_select = true;
    interface = NULL;
    create_new_interface = false;

    set_location(game.nowhere());
}


TWCHumanPlayerClient::~TWCHumanPlayerClient()
{
    log("Connection to %s (%s) lost.\n", player.name(), player.id);
    terminate_continue_select();
    game.curhumans.remove(this);
}


void TWCHumanPlayerClient::die()
{
    player.die();
    close();
}


void TWCHumanPlayerClient::set_location(TWCObject *_location)
{
    log("Setting location to %s.\n", _location->name());
    TWCPlayerClient::set_location(_location);
    create_new_interface = true;
    alarm(0);
}


void TWCHumanPlayerClient::quit()
{
    print(game.get_screen("bye"));
    close();
}


// If this returns NULL, get out of execute() ASAP, as something has gone
// wrong (most like a dropped connection).
char *TWCHumanPlayerClient::continue_getline()
{
    char *line = NULL;
    while (isok() && !line)
    {
	continue_select(5000);
	line = getline(0);
    }

    return line;
}


int TWCHumanPlayerClient::get_int(bool &abort, int dflt)
{
    abort = false;
    char *line = continue_getline();
    if (!line)
    {
	abort = true;
	return 0;
    }

    trim_string(line);
    if (!strlen(line))
	return dflt;

    return atoi(line);
}


void TWCHumanPlayerClient::execute()
{
    WvStreamClone::execute();

    if (create_new_interface)
    {
	create_new_interface = false;
	if (interface)
	    delete interface;
	interface = location->create_if(*this);
    }

    interface->menu_input();
    
    if (!isok())
	return;

    interface->print_menu();

    WvString prompt = interface->prompt();

    if (!!prompt)
	print("\n%s %s:[0m ", prompt, ANSI_BLUE);
}


bool TWCHumanPlayerClient::areyousure()
{
    bool res = false;

    print("Are you sure? ");
    char *line = continue_getline();
    if (!line)
	return false;

    if (line[0] == 'y' || line[0] == 'Y')
	 res = true;

    print("\n");
    return res;
}


void TWCHumanPlayerClient::event(TWCEventType type, int actor, int target, 
				 WvStringParm eventmsg)
{
    print("\n%s\n", eventmsg);

#if 0
    switch (type)
    {
    case WarpOutOf:
	if (demanded_surrender == actor)
	    demanded_surrender = 0;
	break;
    case ShieldsDropped:
	if (demanded_surrender == actor)
	{
	    msg(WvString("%s has complied with our demands.  We can now "
			 "raid the ship!\n",
			 cfg["ships"][demanded_surrender]["name"].get()));
	    demanded_surrender = 0;
	    // adjust reputation and experience
	}
    default:
	break;
    }
#endif
}


bool TWCHumanPlayerClient::new_ship(bool first_ship)
{
    if (first_ship)
	print("Over the last couple years,\n");
    else
	print("After doing dishes on a world near where your last ship was "
	      "lost,\n");

    print("you've scraped together enough money for a %s.\n",
	  game.shipclasses[2]->name());

    print("What will you call your new ship? ");

    char *line = continue_getline();
    if (!line)
	return false;

    trim_string(line);
    game.new_ship(&player, game.shipclasses[2], line, TWCSectorCoord("0 0 0"));

    return true;
}


int TWCHumanPlayerClient::menu(const IntTable &items, WvStringParm prompt)
{
    msg("\n%s (0 to abort)? ", prompt);
    bool abort = false;
    int target = get_int(abort);
    if (abort)
	return 0;

    if (items[target])
	return *items[target];

    msg("Er, which?\n");
    return 0;
}


TWCShip *TWCHumanPlayerClient::ship_menu(const TWCShipList &ships,
					 WvStringParm prompt)
{
    IntTable choices(10);
    TWCShipList::Iter i(ships);
    for (i.rewind(); i.next(); )
    {
	print("%s) %s <%s>.\n", i().id, i().name(), i().shipclass()->name());
	choices.add(new int(i().id), true);
    }

    return game.ships[menu(choices, prompt)];
}


void TWCHumanPlayerClient::print_ships_menu(const TWCShipList &ships,
                                            WvStringParm prompt)
{
    TWCShipList::Iter i(ships);
    for (i.rewind(); i.next(); )
	print("%s) %s <%s>.\n", i().id, i().name(), i().shipclass()->name());
}


TWCShip *TWCHumanPlayerClient::acquire_target(WvStringParm prompt,
					      WvStringParm docked)
{
    TWCShipList ships;
    ((TWCShip*)location)->sector()->find_player_ships(ships, (TWCShip *)location);

    if (!ships.count())
    {
	print("There are no other ships in this sector.\n");
	return NULL;
    }

    TWCShip *target = ship_menu(ships, WvString("%s ", prompt));

    if (target->docked_with)
    {
	print("%s\n", docked);
	return NULL;
    }

    return target;
}


WvString TWCHumanPlayerClient::try_passwd()
{
    print("You don't own this ship.  Please enter authorization code: ");
    return continue_getline();
}


void TWCHumanPlayerClient::display_inventory(TWCShip *ship, bool menu)
{
    TWCShipInventoryList inv;
    ship->inventory(inv);

    print("Inventory of %s:\n\n", ship->name());
    TWCShipInventoryList::Iter i(inv);
    for (i.rewind(); i.next(); )
	print("%s%15s%4s\n", menu ? WvString("%3s. ", i().commodity->id)
	                          : WvString(""),
	      i().commodity->name(), i().number);
    print("\n");
}


void TWCHumanPlayerClient::send_question()
{
    if (conv.isempty())
    {
        setcallback(0, NULL);
        endconv();
        return;
    }

    print("%s", conv.first()->question);
    if (conv.first()->default_answer.len())
        print(" [%s]", conv.first()->default_answer);
    print("? ");

    setcallback(WvStreamCallback(this,
                                 &TWCHumanPlayerClient::do_conversation_callback),
                NULL);
}


void TWCHumanPlayerClient::do_conversation(TWCConversation &_conv,
                                           TWCEndConvCallback _endconv)
{
    if (_conv.isempty())
        return;

    endconv = _endconv;
    TWCConversation::Iter i(_conv);
    for (i.rewind(); i.next(); )
        conv.append(&i(), false);

    send_question();
}


void TWCHumanPlayerClient::do_conversation_callback(WvStream &, void *)
{
    char *line = getline(0);
    if (!line)
        return;

    if (!strlen(line))
        conv.first()->answer = conv.first()->default_answer;
    else
        conv.first()->answer = line;

    WvString inputerr(conv.first()->check_answer(conv.first()->answer));
    if (inputerr.len())
        send_question();
}
