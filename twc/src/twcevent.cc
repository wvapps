#include "twcevent.h"

int event_idx_sorter(const TWCEvent *a, const TWCEvent *b)
{
    return (a->idx - b->idx);
}


int event_sorter(const TWCEvent *a, const TWCEvent *b)
{
    return (a->next - b->next);
}


TWCEventHandler::TWCEventHandler()
    : log("TWCEventHandler", WvLog::Info)
{
}


TWCEventHandler::~TWCEventHandler()
{
}


int TWCEventHandler::add_event(TWCEventCallback _cb, int _id, time_t _interval,
			       bool _single)
{
    struct TWCEvent *new_event = new TWCEvent;

    new_event->interval = _interval;
    new_event->single =  _single;
    new_event->cb = _cb;
    new_event->id = _id;

    TWCEventList::Sorter s(events, event_idx_sorter);
    int i = 1;

    for (s.rewind(); s.next(); )
    {
	if (i != s->idx)
	    break;
	i++;
    }

    new_event->idx = i;
    new_event->next = time(0) + _interval;
    events.append(new_event, true);

    return i;
}


void TWCEventHandler::remove_event(int event_idx)
{
    TWCEventList::Sorter s(events, event_idx_sorter);
    for (s.rewind(); s.next(); )
    {
	if (s->idx == event_idx)
	{
	    events.unlink(s.ptr());
	    return;
	}
    }
}


void TWCEventHandler::execute()
{
    WvTimeStream::execute();

    time_t now = time(0);

    TWCEventList::Sorter s(events, event_sorter);

    for (s.rewind(); s.next(); )
    {
	if (now >= s->next)
	{
	    s->cb(s->id);
	    if (s->single)
	    {
		events.unlink(s.ptr());
		s.rewind();
	    }
	    else
		s->next = now + s->interval;
	}
	else
	    break;
    }
}
