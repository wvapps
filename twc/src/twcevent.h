/* -*- Mode: C++ -*- */
#ifndef __TWCEVENT_H
#define __TWCEVENT_H

#include <time.h>
#include <wvcallback.h>
#include <wvlinklist.h>
#include <wvlog.h>
#include <wvtimestream.h>

enum TWCEventType {
    Event = 0,
    DemandSurrender,
    WarpInto,
    WarpOutOf,
    ShipDestroyed,
    ShipCaptured,
    LaunchEscapePod,
    PodDestroyed,
    ShipAttackHit,
    ShipAttackMissed,
    ShieldsRaised,
    ShieldsDropped,
    ShipDocked,
    ShipUndocked,
    TractorBeamEngaged,
    TractorBeamDropped,
    CargoBeamedFrom,
    CargoBeamedTo
};

// int parameter is the id of whatever we're talking about in the callback:
// ship, player, planet, etc.
typedef WvCallback<void, int> TWCEventCallback;

struct TWCEvent
{
    int idx;
    time_t interval;
    bool single;
    time_t next;
    TWCEventCallback cb;
    int id;
};

DeclareWvList(TWCEvent);

int event_sorter(const TWCEvent *a, const TWCEvent *b);

class TWCEventHandler : public WvTimeStream
{
    TWCEventList events;
    WvLog log;
public:
    // use set_timer() to start the handler.
    TWCEventHandler();
    virtual ~TWCEventHandler();

    // Returns index of event.
    int add_event(TWCEventCallback _cb, int _id, time_t _interval,
		  bool _single);

    // Remove event by index.
    void remove_event(int event_idx);

    virtual void execute();
};

#endif // __TWCEVENT_H
