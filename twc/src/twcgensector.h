#ifndef __TWCSECTOR_H__
#define __TWCSECTOR_H__

#include <stdlib.h>

#include <wvlog.h>
#include <wvlogrcv.h>
#include <wvcrash.h>
#include <wvconf.h>
#include <signal.h>
#include <errno.h>
#include <time.h>

#include "twccontainer.h"

class Path;

class Sector
{
  public:
    unsigned int id;
    unsigned int port_type;

    Container<Path> exits;

    Sector();
    Sector( unsigned int );
    virtual ~Sector();
    unsigned int NumExits() const;
    void     AddExit( Sector*, unsigned int );
    void     RemoveExit( Sector* );
    bool     DoesExitExist( Sector*, unsigned int* );
};

class Path
{
  public:
    Sector      *destination;
    unsigned int distance;
    
    Path( Sector*, unsigned int );
    virtual ~Path();
};

#endif
