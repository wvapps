/* -*- Mode: C++ -*- */
#ifndef __TWCSHIP_H
#define __TWCSHIP_H

#include "twcevent.h"
#include "twcobject.h"

enum TWCDockResults {
    DockSuccess = 0,
    NoShip,
    NoPort,
    PortBusy
};

enum TWCAttackResults {
    Aborted = 0,
    Missed,
    Damaged,
    Destroyed
};

enum TWCCheckShieldsResults {
    CSAccessGranted,
    CSAccessDenied,
    CSAccessForbidden
};

enum TWCTransportSelfResults {
    TSSuccessful,
    TSAccessDenied,
    TSAlreadyCaptained,
    TSClaimedOwnership
};

enum TWCTransportCommResults {
    TCSuccessful,
    TCAccessDenied,
    TCFull,
    TCNotEnough
};

class TWCPlayer;
class TWCPlayerClient;
class TWCPort;
class TWCSector;
class TWCSectorCoord;
class TWCCommodity;
class TWCShipClass;
class TWCShieldClass;
class TWCWeaponClass;

struct TWCShipInventory {
    TWCCommodity *commodity;
    unsigned int number;
};
DeclareWvList(TWCShipInventory);


class TWCShip: public TWCObject
{
    friend class TWCShipIf;

protected:
    TWCSector *warping_to;
    TWCEventCallback post_warp;

public:
    TWCShip(UniConf _cfg, TWCGameController *_game);
    virtual ~TWCShip() {}

    TWCSector *sector();

    TWCPort *docked_with;

    virtual bool on_ship() { return true; }

    /** The player client currently piloting the ship.
     * Returns NULL if no one. */
    TWCPlayerClient *captain();

    TWCPlayer *owner();

    WvString password()
    { return cfg["password"].getme(); }

    bool are_shields_up()
    { return cfg["shields up"].getmeint() && shield_strength(); }

    int shield_strength()
    { return cfg["shield strength"].getmeint(); }

    int hull_strength()
    { return cfg["hull strength"].getmeint(); }

    /// Returns false if the ship has been destroyed.
    bool damage_ship(int dmg);

    TWCShipClass *shipclass();
    TWCShieldClass *shieldclass();
    TWCWeaponClass *weaponclass();
    bool install_weapon(TWCWeaponClass *weapon);
    bool install_shield(TWCShieldClass *shield);
    bool uninstall_weapon(TWCWeaponClass *weapon);
    bool uninstall_shield(TWCShieldClass *shield);
    bool weapon_charged();
    void charge_weapon(int);
    TWCAttackResults shoot(TWCShip *target, int power);

    int total_holds()
    { return cfg["commodities/max"].getmeint(); }

    int free_holds();
    bool load(int type, int num);
    bool unload(int type, int num);

    void inventory(TWCShipInventoryList &inv);
    int onboard(WvStringParm type, int subtype)
    { return cfg[type][subtype].getmeint(); }

    bool beam_cargo_aboard(TWCShip *from_ship, int type, int num);

    TWCShip *towing();
    TWCShip *towed_by();
    bool tow(TWCShip *target);

    /// called when 'by' attempts to tow this ship.
    bool tow_attempt(TWCShip *by);

    bool warping()
    { return warping_to; }

    virtual void set_by_player(TWCPlayerClient *plyrcli);

    virtual TWCObjectIf *create_if(TWCHumanPlayerClient &plyrcli);

    void set_captain(TWCPlayerClient *plyrcli);

    void raise_shields();
    void lower_shields();
    TWCDockResults dock();

    void do_warp(int);
    void start_warp(TWCEventCallback _post_warp, TWCSector *dest);

    TWCTransportSelfResults transport_self(TWCPlayerClient &plyrcli);
    TWCCheckShieldsResults check_shields(TWCPlayerClient &plyrcli);

    bool check_claim_ownership(TWCPlayer *player);

    WvString viable_target(WvStringParm shipnum);
};

DeclareWvList(TWCShip);
DeclareWvDict(TWCShip, int, id);


class TWCShipIf: public TWCObjectIf
{
protected:
    TWCShip &ship;
    bool autopilot;
    bool tactical;
    bool transporter;
    TWCSectorCoord *final_dest;

public:
    TWCShipIf(TWCShip &_ship, TWCHumanPlayerClient &_plyrcli);
    virtual ~TWCShipIf();

    virtual void print_menu();
    virtual void menu_input();
    virtual WvString prompt();
    void ship_move(const TWCSectorCoord dest_sector);
    void warping_menu_input();
    void tactical_menu_input();
    void transporter_menu_input();
    void post_warp(int);
    void sector_scan();
    bool checkmoveturns();
    TWCCheckShieldsResults check_shields(TWCShip *target);
};


#endif // __TWCSHIP_H
