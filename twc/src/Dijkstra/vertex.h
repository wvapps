//**************************************************************************
#include <stdlib.h>
//**************************************************************************
#ifndef VERTEX_H
#define VERTEX_H
//**************************************************************************
// Vertex class, holds the vertex's name, parent, distance between them.
class Vertex
{
   private:
	
   public:
    // Default constructor
	Vertex(){next=NULL;};
    // Overloaded constructor, initializes variables
	Vertex(int n,int p,int d, Vertex *pointer=NULL)
	{name=n;parent=p;distance=d;next=pointer;};

    // Variable retrieval methods
	void setName(int nme) {name = nme;};
	void setParent(int newParent) {parent = newParent;};
	void setDistance(int dist) {distance = dist;};

    // Overloaded equals operator
	void operator =(Vertex& b) {
	        name = b.name;
		parent = b.parent;
		distance = b.distance;
		};
	int operator >(Vertex& b) {return (distance > b.distance);};
	int operator >=(Vertex& b) {return (distance >= b.distance);};
	int operator <(Vertex& b) {return (distance < b.distance);};
	int operator <=(Vertex& b) {return (distance <= b.distance);};
    // Public pointer variable to another vertex
	Vertex *next;
	int name;
	int parent;
	int distance;
};
//**************************************************************************
#endif
//**************************************************************************
