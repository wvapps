//***************************************************************************
#ifndef HASH_H
#define HASH_H
//***************************************************************************
char *strsave(char *s);
int strsame(char *s, char *t);
const int H_Default_Size = 1001;
//***************************************************************************
class HashTable
{
  private:
    int H_Size;
    int Cur_Val;		// Keeps track of the last new ID

    struct Hash_Node {
	Hash_Node *Next;		// Next pointer (for collision)
	unsigned int ID;		// Actual consecutive ID returned

	Hash_Node(unsigned int id = 0, Hash_Node *N = NULL) :
	    Next (N), ID(id) { } // constructor
    } **Hash_Table;		// Identified by internal hash number

    struct Hash_Node_Consec {
	unsigned int Hash_int;	// Internal location in Hash Table
	char *Word;		// Actual word stored here

	Hash_Node_Consec(char *W = NULL, unsigned int id = 0) :
	    Hash_int (id), Word( strsave(W) ) { } // constructor
    } **Hash_Table_Consec;	// Identified by the actual ID used

    unsigned Hash (const char *s);	// Hashing function

  public:
    HashTable(int Size = H_Default_Size);	// Constructor
    ~HashTable( );				// Destructor
    int id_of (char *word);		// Inserts word if new and returns ID
    char *name_of (int id);		// Returns word corresponding to ID
    int nodes_of ();		// Returns number of words in table
    int find (char *word);		// Returns non-zero ID if word found

};
//***************************************************************************
#endif
//***************************************************************************
