//**************************************************************************
#ifndef DIJKSTRA_H
#define DIJKSTRA_H
//**************************************************************************
#include "vertex.h"
#include "adjlist.h"
#include "priqueue.h"
#include "hash.h"
#include <wvstream.h>
#include <wvstring.h>
//**************************************************************************
void dijkstra(int source, AdjList& G, Vertex *V, HashTable& T, int print_heap);
#if 1
WvString* Print_Path(int source, int dest, Vertex *V, AdjList& G, HashTable& T);
void Print_Path_Rec(int source, int dest, Vertex *V, AdjList& G, 
			HashTable& T, int *total, WvString*);
#else
void Print_Path(int source, int dest, Vertex *V, AdjList& G, HashTable& T);
void Print_Path_Rec(int source, int dest, Vertex *V, AdjList& G, 
			HashTable& T, int *total);
#endif
void Print_Dijkstra (int source, Vertex *V, HashTable& T);
void Print_Heap (PriQueue& Q, HashTable& T);
//**************************************************************************
#endif
//**************************************************************************
