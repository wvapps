/* -*- Mode: C++ -*- */
#ifndef __TWCSECTOR_H
#define __TWCSECTOR_H

#include "twcevent.h"
#include "twcobject.h"
//#include "twcport.h"
#include "twcship.h"
#include <wvlog.h>
#include <wvstringlist.h>

/** Easy way to ensure that a coordinate is always "x y z", with exact
 * spacing (needed for indexing in UniConf).
 */
class TWCSectorCoord
{
public:
    int x;
    int y;
    int z;
    bool ok;

    TWCSectorCoord(WvStringParm coordstr);
    TWCSectorCoord(const TWCSectorCoord &coord);
    virtual ~TWCSectorCoord() {}

    bool operator== (const TWCSectorCoord &a2) const
        { return (x == a2.x && y == a2.y && z == a2.z); }
    bool operator!= (const TWCSectorCoord &a2) const
        { return !(*this == a2); }

    operator WvString () const
        { return str(); }
    WvString str() const;
};


/** Represents one sector in space.
 * Note that 'id' is not meaningful in this class; use 'coord' instead.
 */
class TWCSector: public TWCObject
{
private:
    const int id;

protected:
    TWCPort *_port;

public:
    const TWCSectorCoord coord;
    
    TWCSector(UniConf _cfg, TWCGameController *_game);
    virtual ~TWCSector();

    virtual WvString name()
        { return WvString("Sector %s", coord); }
    virtual bool on_ship();
    virtual TWCObjectIf *create_if(TWCHumanPlayerClient &_player)
        { return NULL; }

    TWCPort *port()
        { return _port; }

    /** Finds a player's (or all but a player's) ships in this sector.
     * Populates 'list' with all ships belonging to (or not belonging to, if
     * 'except' is true) 'player' in this sector.
     */
    void find_player_ships(TWCShipList &list, TWCPlayer *player,
			   bool except = false);
    void find_player_ships(TWCShipList &list, TWCShip *except);

//    void port_destroyed();

    void ship_warp_out(TWCShip *ship);
    void ship_warp_in(TWCShip *ship);

    void announce(TWCEventType type, int actor, int target, WvStringParm msg,
		  int except);
};

DeclareWvList(TWCSector);
DeclareWvDict(TWCSector, TWCSectorCoord, coord);


#endif // __TWCSECTOR_H
