#include <wvlog.h>
#include <wvstring.h>
#include <uniconfroot.h>

#include <fcntl.h>
#include <unistd.h>

#include "twccontainer.h"
#include "twcgenerator.h"
#include "twcutils.h"

#include <strutils.h>

#define RANDOMIZE_SECTORS 1

TWCGenerator::TWCGenerator() : all_sectors(), planet_sectors(), port_sectors(), log("Generator", WvLog::Info)
{
    log(WvLog::Info, "And there was light...\n");
}


TWCGenerator::~TWCGenerator()
{
    Purge();

    log(WvLog::Info, "Universe Generator collapsing in upon itself...\n");
}


unsigned int TWCGenerator::Connect(unsigned int from, unsigned int to, unsigned int dist)
{
    if(from >= all_sectors.Count() || to >= all_sectors.Count())
    {
	log(WvLog::Error, "Out of range.\n");
	exit(-1);
    }

    if(IsConnected(from, to))
    {
	log(WvLog::Info, "Sector %s is already conected to Sector %s\n", from, to);
        return GENERATOR_SUCCESS;
    }
    else
    {
	log(WvLog::Info, "Connecting Sector %s to Sector %s\n", from, to);
	all_sectors[from]->AddExit(all_sectors[to], dist);
	return GENERATOR_SUCCESS;
    }
}


unsigned int TWCGenerator::ConnectAllSectors(float stdev, float prob_bi_dir, unsigned int max_distance)
{
    log(WvLog::Info, "Connecting Sectors.\n");
    
    // connect all of the sectors in 'all_sectors' in order, thereby
    // establishing a uni-directional, circular path through them.
    for (unsigned int n = 0; n < all_sectors.Count() - 1; ++n)
    {
	log(WvLog::Debug1, "Connecting sector %s to sector %s\n", n, n+1);

	if(Connect(n, n+1, 1) != GENERATOR_SUCCESS)
	{
	    assert(0);
	}
	//	all_sectors[i]->AddExit(all_sectors[i+1], 1);
    }
    // complete the circle...

    if(all_sectors.Count() > 1)
    {
	log(WvLog::Debug1, "Connecting sector %s to sector %s\n", all_sectors.Count()-1, 0);
	
	if(Connect(all_sectors.Count() - 1, 0, 1) != GENERATOR_SUCCESS)
	{
	    assert(0);
	}
    }
    
    // for each sector, establish a uni-directional link with other sectors
    // (the number of connections is dictated by a pdf)
    // (for now, just have each sector connected to 'degree'
    // other sectors)
    for (unsigned int n = 0; n < all_sectors.Count(); ++n)
    {
	log(WvLog::Debug, "Generating random connections for sector %s.\n", n);
	
	unsigned int j = abs((int)(box_muller(0, stdev/2)));
	
	// eliminate the chance that the generator will try to establish more
	// exits than exist sectors
	if(j >= all_sectors.Count() - 1)
	{
	    // -1 because it can't connect to itself 
	    j = all_sectors.Count() - 1;
	}
	
	// now, subtract the number of pre-existing connections
	if(all_sectors[n]->NumExits() > j)
	{
	    j = 0;
	}
	else
	{
	    j -= all_sectors[n]->NumExits();
	}
	
	log(WvLog::Debug1, "This sector will have %s additional exit(s).\n", j);
	
	for (; j > 0  ; --j)
	{
	    unsigned int rnd = (rand() % (all_sectors.Count() - 1));
	    
	    // don't let the sector connect to itself
	    if(rnd >= n)
	    {
		++rnd;
	    }
	    
	    // don't let the sector have multiple connections to the same
	    // destination
	    while(IsConnected(n, rnd) == true)
	    {
		log(WvLog::Debug1, "<!> Sector %s and sector %s are already connected. Try again.\n", n, rnd);
		log(WvLog::Debug1, "<!> -->Need to establish %s more connections.\n", j);
		
		rnd = (rand() % (all_sectors.Count() - 1));
		if(rnd >= n)
		{
		    ++rnd;
		}
	    }
	    
	    unsigned int dist;
	    dist = max_distance==0?0:(rand() % max_distance) + 1;

	    log(WvLog::Debug1, "Connecting sector %s to sector %s\n", n, rnd);
	    if(Connect(n, rnd, dist) != GENERATOR_SUCCESS)
	    {
		assert(0);
	    }
	    
#ifdef DEBUG
	    if(IsConnected(n, rnd) == false)
	    {
		log(WvLog::Error, "Internal error. Aborting.\n");
		abort();
	    }
#endif
	    // check for a bi-directional link
	    if(ranf() <= prob_bi_dir && IsConnected(rnd, n) == false)
	    {
		log(WvLog::Debug1, "A bidirectional link will be made...");
		log(WvLog::Debug1, "Connecting sector %s to sector %s\n", rnd, n);
		if(Connect(rnd, n, dist) != GENERATOR_SUCCESS)
		{
		    assert(0);
		}
#ifdef DEBUG
		if(IsConnected(rnd, n) == false)
		{
		    log(WvLog::Error, "Internal error. Aborting.\n");
		    abort();
		}
#endif
	    }
	    
	}
    }
    
#if RANDOMIZE_SECTORS
    // now, re-arrange the sectors to randomize their effective sector
    // numbers (which are determined by their offset into the array).

    log(WvLog::Debug1, "Reordering sectors...\n");

    for (unsigned int n = 0; n < all_sectors.Count() * 2; ++n)
    {	  
	unsigned int rnd = rand() % (all_sectors.Count());
	
	log(WvLog::Debug1, "%s ", rnd);
	
	all_sectors.Add(all_sectors.Remove(all_sectors[rnd]));
    }

    log(WvLog::Debug1, "\n");

#endif

    // renumber the sectors, starting at ID #1
    for (unsigned int n = 0; n < all_sectors.Count(); ++n)
    {
	all_sectors[n]->id = n+1;
    }

    
    return GENERATOR_SUCCESS;
}


unsigned int TWCGenerator::GenerateSectors(unsigned int num)
{    
    log(WvLog::Info, "Generating %s Sectors.\n", num);

    if(num == 0)
    {
	log(WvLog::Error, "How can I create 0 sectors?\n");
	return GENERATOR_FAILURE;
    }
    else if(all_sectors.Count() != 0)
    {
	log(WvLog::Error, "Sectors have already been generated.\n");
	return GENERATOR_FAILURE;
    }

    while(num > 0)
    {
	log(WvLog::Debug1, "Generating sector %s\n", --num);

	all_sectors.Add(new Sector(num));
    }
    
    return GENERATOR_SUCCESS;
}


void TWCGenerator::Purge(void)
{
    log(WvLog::Info, "Purging existing universe...\n");

    port_sectors.Purge();
    planet_sectors.Purge();

    // destroy all of the sectors contained in 'all_sectors'
    while(all_sectors.Count() != 0)
    {
	delete(all_sectors.Remove(all_sectors[0]));
    }
}

static int numcmp(const UniConf &a, const UniConf &b)
{
    return (a.key().printable().num() > b.key().printable().num());
}

WvString strip_slash(WvStringParm keyname)
{
    if (*keyname.cstr() == '/')
    {
	WvString newname;
	newname.setsize(keyname.len());
	strcpy(newname.edit(), keyname.cstr() + 1);
	return newname;
    }
    else
	return keyname;
}

void TWCGenerator::DumpToFile(char* galaxy_file)
{
    UniConfRoot cfg(WvString("ini:%s", galaxy_file));

    int num_old_ports = cfg["global"]["ports"].getmeint();

    cfg["global"]["sectors"].setmeint(all_sectors.Count());
    cfg["global"]["planets"].setmeint(planet_sectors.Count());
    cfg["global"]["ports"].setmeint(port_sectors.Count());
    
    // erase all of the existing, relevant entries
    // i.e.sectors, planets, ports
    cfg["sectors"].remove();
    cfg["planets"].remove();

    // erase extra ports
    for (int n = num_old_ports - port_sectors.Count(); n >= 0; --n)
    {
	cfg["ports"][num_old_ports--].remove();
    }

    // keep all other port info (buying, selling info) 
    // (Note: port location will be overwritten)

    // iterate and place all ships in sector 1
    // [sectors/1].ships ={...}
    // [ships/n].location = 1
    WvString ships("");    
    UniConf::SortedIter s(cfg["ships"], numcmp);
    for (s.rewind(); s.next();)
    {
	int id = strip_slash(s->key()).num();
	ships.append(WvString("%s ", id));
	s()["location"].setme(1);

	log(WvLog::Info, "Setting location of ship %s to %s\n",
	     id, cfg["ships"][WvString("%s",id)]["location"].getmeint());
    }

    ships.edit()[ships.len() - 1] = '\0';
    cfg["sectors"][1]["ships"].setme(ships);

    for (unsigned int from = 0; from < all_sectors.Count(); ++from)
    {
	WvString     exits("");
	WvString     distances("");

	// all sectors should have at least one exit.
	if (all_sectors[from]->exits.Count() == 0)
	{
	    log (WvLog::Error, "Sector %s has no exits!\n", WvString(all_sectors[from]->id));
	    exit(-1);
	}

	for (unsigned int exit_num = 0; exit_num < all_sectors[from]->exits.Count(); ++exit_num)
	{
	    unsigned int to = (all_sectors[from]->exits)[exit_num]->destination->id;
	    unsigned int distance = (all_sectors[from]->exits)[exit_num]->distance;
	    
	    log (WvLog::Info, "Sector %s is connected to Sector %s with distance %s\n", 
		 WvString(all_sectors[from]->id),
		 WvString(to),
		 distance);

	    exits.append(WvString("%s ", to));
	    distances.append(WvString("%s ", distance));
	}
	exits.edit()[exits.len() - 1] = '\0';
	distances.edit()[distances.len() - 1] = '\0';

	//wvcon->print("Exits are: %s\n", exits);
	//wvcon->print("Distances are: %s\n", distances);
	cfg["sectors"][WvString("%s", all_sectors[from]->id)]["exits"].setme(exits);
	cfg["sectors"][WvString("%s", all_sectors[from]->id)]["distances"].setme(distances);
    }

    for (unsigned int from = 0; from < planet_sectors.Count(); ++from)
    {
	cfg["sectors"][WvString("%s", planet_sectors[from]->id)]["planet"].setmeint(from + 1);
	cfg["planets"][WvString("%s", from + 1)]["name"].setme(WvString("Planet %s", from + 1));
	cfg["planets"][WvString("%s", from + 1)]["class"].setme("M");
    }

    for (unsigned int from = 0; from < port_sectors.Count(); ++from)
    {
	cfg["sectors"][WvString("%s", port_sectors[from]->id)]["port"].setmeint(from + 1);
	cfg["ports"][WvString("%s", from + 1)]["type"].setmeint(1);       
	cfg["ports"][WvString("%s", from + 1)]["name"].setme(WvString("Port %s", from + 1));
	cfg["ports"][WvString("%s", from + 1)]["location"].setme(WvString("%s", port_sectors[from]->id));
    }

    cfg.commit();
}

bool TWCGenerator::IsConnected(unsigned int from, unsigned int to)
{
    return IsConnected(from, to, NULL);
}

bool TWCGenerator::IsConnected(unsigned int from, unsigned int to, unsigned int *distance)
{
    if(from >= all_sectors.Count() || to >= all_sectors.Count())
    {
	return false;
    }
    
    return (IsConnected(all_sectors[from], all_sectors[to], distance));
}

bool TWCGenerator::IsConnected(Sector *from, Sector *to)
{
    return (IsConnected(from, to, NULL));
}

bool TWCGenerator::IsConnected(Sector *from, Sector *to, unsigned int *distance)
{
    assert(from);
    assert(to);
    
    if(from == to)
    {
	return false;
    }
    
    return (from->DoesExitExist(to, distance));
}

void TWCGenerator::PlaceRandomPlanets(unsigned int num)
{
    log(WvLog::Info, "Randomly placing %s planets.\n", num);

    if(num > all_sectors.Count())
    {
	log(WvLog::Error, "Cannot place %s planets in %s sectors.\n", num, all_sectors.Count());
	exit(-1);
    }    

    // place planets randomly -- later on I'll fix this up to make sure that they're
    // fairly well distributed over the sector-space
    while(num > 0)
    {
	unsigned int tmp = rand() % (all_sectors.Count());
	unsigned int index;

	while(planet_sectors.Contains(&index, all_sectors[tmp]))
	{
	    tmp = rand() % (all_sectors.Count());
	}

	log(WvLog::Info, "Placing planet at sector %s\n", tmp);

	PlacePlanet(tmp);

#ifdef DEBUG
	printf("all_sectors[%d] = %p, planet_sectors[%d] = %p\n",
		tmp, all_sectors[tmp],
		planet_sectors.Count() - 1, planet_sectors[planet_sectors.Count()-1]);
#endif

	--num;
    }
}

void TWCGenerator::PlacePlanet(unsigned int sector)
{
    planet_sectors.Add(all_sectors[sector]);
}

void TWCGenerator::PlacePortsAtPlanets()
{
    // for each of the planets, place a port of type 'type'
    for (int n = planet_sectors.Count() - 1; n >= 0; --n)
    {
	port_sectors.Add(planet_sectors[n]);
    }

}

