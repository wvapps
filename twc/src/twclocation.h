/* -*- Mode: C++ -*- */
#ifndef __TWCLOCATION_H
#define __TWCLOCATION_H

// Base class for all locations.
class TWCLocation
{
protected:
    UniConf cfg;                    // UniConf tree for this location
    TWCGameController &game;
    
public:
    TWCLocation(UniConf _cfg, TWCGameController &_game)
	: cfg(_cfg), game(_game) {}
    virtual ~TWCLocation() {}

    virtual WvString name() = 0;
    virtual bool on_ship() = 0;
};


// Interface class for human players
class TWCLocationIf
{
protected:
    TWCLocation *location;
public:
    TWCLocationIf(TWCLocation *_location)
	: location(_location) {}
    virtual ~TWCLocationIf() {}

    virtual void print_menu() = 0;
    virtual void menu_input() = 0;
};

#endif // __TWCLOCATION_H
