/* -*- Mode: C++ -*- */
#ifndef __TWCGAMECONTROLLER_H
#define __TWCGAMECONTROLLER_H

#include <wvlog.h>
#include <uniconfroot.h>
#include <wvtcp.h>
#include "twcbuyable.h"
#include "twcevent.h"
#include "twcnowhere.h"
#include "twcplayer.h"
#include "twcplayerclient.h"
#include "twcport.h"
#include "twcsector.h"
#include "twcship.h"

#define ANSI_BLACK    "\033[30m"
#define ANSI_RED      "\033[31m"
#define ANSI_GREEN    "\033[32m"
#define ANSI_YELLOW   "\033[33m"
#define ANSI_BLUE     "\033[34m"
#define ANSI_MAGENTA  "\033[35m"
#define ANSI_CYAN     "\033[36m"
#define ANSI_WHITE    "\033[37m"
#define ANSI_BG_BLACK   "\033[40m"
#define ANSI_BG_RED     "\033[41m"
#define ANSI_BG_GREEN   "\033[42m"
#define ANSI_BG_YELLOW  "\033[43m"
#define ANSI_BG_BLUE    "\033[44m"
#define ANSI_BG_MAGENTA "\033[45m"
#define ANSI_BG_CYAN    "\033[46m"
#define ANSI_BG_WHITE   "\033[47m"

/*
 * Extra UniConf functions.
 */

// Converts the value of 'cfg' to an int and increments it by 'incr'.
int incr_uniconf(UniConf cfg, int incr);

inline int decr_uniconf(UniConf cfg, int decr)
{ return incr_uniconf(cfg, -1 * decr); }

// Appends a space and the string 'item' to 'cfg', assuming it isn't already
// there.
// FIXME: probably should use wvtclstrings.
void add_list_item_uniconf(UniConf cfg, WvStringParm item);
void del_list_item_uniconf(UniConf cfg, WvStringParm item);

class IntChar
{
public:
    int i;
    char c;
    IntChar(int _i, char _c) : i(_i), c(_c) {}
    ~IntChar() {}

    static int abs_sort(const IntChar *a, const IntChar *b)
    { return (abs(a->i) < abs(b->i)); }

};
DeclareWvList(IntChar);


class TWCGameController
{
    friend class TWCEventHandler;
public:
    WvTCPListener *listener;
    UniConfRoot cfg;
    
    TWCPlayerDict players;
    TWCPlayerClientDict curplayers;
    TWCHumanPlayerClientDict curhumans;
    TWCShipDict ships;
    TWCPortDict ports;
    WvLog log;
    TWCEventHandler events;
    TWCBuyableDict buyables;
    TWCShipClassDict shipclasses;
    TWCShieldClassDict shieldclasses;
    TWCWeaponClassDict weaponclasses;
    TWCCommodityDict commodities;

    TWCGameController(WvString cfgpath);
    virtual ~TWCGameController();
    void listener_callback(WvStream&, void *);

    // A few common functions.
    TWCSectorCoord find_route(TWCSectorCoord src, TWCSectorCoord dest);
    void initialize_ship(int ship, int captain);
    void remove_ship(TWCShip *ship);

#if 0
    // twcattack.cc
    void launch_escape_pod(TWCPlayerClient *target_player, int target_player_id,
			   int sector);
#endif

    TWCHumanPlayerClient *new_player(WvStringParm username,
				     WvStringParm password, WvTCPConn *conn);

    TWCNowhere *nowhere() { return &_nowhere; }

    int turns() { return cfg["config/global/turns"].getmeint(100); }
    int starting_cash() { return cfg["config/global/starting cash"].getmeint(1000); }

    WvString screen(WvStringParm name)
    { return cfg["config/screens"][name].getme(); }

    int playerid(WvStringParm name)
    { return cfg["playerinfo/userids"][name].getmeint(); }

    bool isloggedin(int id)
    { return curhumans[id]; }

    TWCHumanPlayerClient *log_player_on(TWCPlayer *player, WvTCPConn *conn);
    TWCHumanPlayerClient *new_human_player(WvStringParm username,
					   WvStringParm password,
					   WvTCPConn *conn);

    TWCShip *new_ship(TWCPlayer *player, TWCShipClass *shipclass,
		      WvStringParm name, TWCSectorCoord location);

    WvString get_passwd(const int id)
    { return cfg["playerinfo/players"][id]["password"].getme(); }

    WvString get_screen(WvStringParm screen_name)
    { return cfg["config/screens"][screen_name].getme(); }

    TWCSector *get_sector(TWCSectorCoord coord);

    WvString price_string(int amount)
    {
	return WvString("%s %s%s", amount,
			cfg["config/global/currency"].getme(),
			amount == 1 ? "" : "s");
    }

private:
    TWCNowhere _nowhere;
    TWCSectorDict sectors;

    void tick(int);
    void new_day();
    void cfg_save(int);

    // twcattack.cc
    void shield_recharge(int);  // can be private since it's only started by
                                // TWCGameController
};


class TWCLogin: public WvStreamClone
{
protected:
    TWCGameController &game;
    bool logged_in;
    WvTCPConn *tcp;
    WvLog log;

    char *continue_getline();
    void new_user(WvStringParm username);
    
public:
    TWCLogin(WvTCPConn *_tcp, TWCGameController &_game);
    virtual ~TWCLogin();

    virtual bool isok() const { return !logged_in; }
    virtual void execute();
};

#endif // __TWCGAMECONTROLLER_H
