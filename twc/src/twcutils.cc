#include "twcutils.h"
#include <ctype.h>
#include <math.h>
#include <stdlib.h>

bool isint(const char *s)
{
    for (const char *p = s; *p; p++)
        if (!isdigit(*p))
            return false;
    return true;
}


float ranf()
{
    float r = rand() % 16384;
    return (r / 16384);
}


float box_muller(float mean, float std_dev)
{
    float x1, x2, w, y1;
    static float y2;
    static int use_last = 0;

    if (use_last)                   /* use value from previous call */
    {
        y1 = y2;
        use_last = 0;
    }
    else
    {
        do
	{
            x1 = 2.0 * ranf() - 1.0;
            x2 = 2.0 * ranf() - 1.0;
            w = x1 * x1 + x2 * x2;
        }
	while (w >= 1.0);

        w = sqrt((-2.0 * log(w)) / w);
        y1 = x1 * w;
        y2 = x2 * w;
        use_last = 1;
     }

     return(mean + y1 * std_dev);
}
