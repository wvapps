#include "twcattack.h"
#include "twcgamecontroller.h"
#include "twcrandom.h"
#include <strutils.h>

void TWCGameController::shield_recharge(int)
{
    UniConf::Iter i(cfg["ships"]);

    for (i.rewind(); i.next(); )
    {
	int shield = i()["shield"].getint();
	int regen = cfg["shields"][shield]["regen"].getint();
	int str = i()["shield strength"].getint();
	int maxstr = cfg["shields"][shield]["strength"].getint();

	str += regen;
	if (str > maxstr)
	    str = maxstr;

	i()["shield strength"].setint(str);
    }
}


void TWCGameController::weapon_recharge(int id)
{
    UniConf shipcfg(cfg["ships"][id]);
    
    int old_charge = shipcfg["weapon recharged"].getint();
    if (!old_charge)
    {
	shipcfg["weapon recharged"].setint(1);

	int captain = shipcfg["captain"].getint();
	TWCPlayerClient *player = players[captain];
	if (player)
	    player->weapon_recharged();
    }
}


void TWCGameController::launch_escape_pod(TWCPlayerClient *target_player,
					  int target_player_id, int sector)
{
    // Flee in escape pod.
    int pod_id = find_next_unused(cfg["ships"]);
    log("First unused id is %s.\n", pod_id);

    UniConf pod(cfg["ships"][pod_id]);
    pod["class"].setint(1);
    pod["name"].set(WvString("Escape pod %s", pod.key()));
    pod["commodities/max"].set(cfg["shipclasses"]["1"]["commodities/max"].get());
    pod["shield"].setint(1);
    pod["weapon"].setint(0);
    pod["shield strength"].set(cfg["shields"]["1"]["strength"].get());
    pod["docked"].setint(0);
    pod["owner"].setint(target_player_id);
    pod["location"].setint(sector);
    initialize_ship(pod_id, target_player_id);

    if (target_player)
    {
	target_player->location_id = pod_id;
	target_player->location_type = Ship;
    }

    // Move escape pod to a random nearby sector.
    int dest_sector(sector);
    WvStringList exits;
    exits.split(cfg["sectors"][dest_sector]["exits"].get());
    int which_sector = rand() % exits.count() + 1;
    WvStringList::Iter i2(exits);
    i2.rewind();
    for (int j = 0; j < which_sector; j++)
	i2.next();
    dest_sector = i2().num();
		    
    log("escape pod fleeing to sector %s.\n", dest_sector);
    ship_warp_to(pod.key().printable().num(), dest_sector, sector);
    int except = 0;
    if (target_player)
    {
	target_player->event(LaunchEscapePod, pod_id, 0,
			     "Luckily you made it to an escape pod in time.");
	except = target_player_id;
    }
    announce(LaunchEscapePod, pod_id, 0,
	     "An escape pod streaks out of the flaming hulk.\n",
	     sector, target_player_id);

//    if (target_player)
//	target_player->alarm(0);
}


void TWCGameController::remove_ship(int player, int ship)
{
    UniConf targetpcfg(cfg["players"][player]);
    int sector = cfg["ships"][ship]["location"].getint();
    del_list_item_uniconf(targetpcfg["ships"], ship);
    del_list_item_uniconf(cfg["sectors"][sector]["ships"], ship);
}


void attack_callback(const UniConf &cfg, const UniConfKey &)
{
    fprintf(stderr, "attack callback\n");
}


int TWCHumanPlayerClient::acquire_target(WvStringParm prompt, WvStringParm docked)
{
    int sector = loccfg()["location"].getint();

    WvStringList ships;
    game->find_player_ships(id * -1, sector, ships);

    if (!ships.count())
    {
	msg("There are no ships in this sector.\n");
	return 0;
    }

    int target = ship_menu(ships, WvString("%s ", prompt));
    if (!target)
	return 0;
    if (cfg["ships"][target]["location"].getint() != sector)
    {
	msg("That ship isn't here.\n");
	return 0;
    }

    if (!!docked && cfg["ships"][target]["docked"].getint())
    {
	msg("%s\n", docked);
	return 0;
    }

    return target;
}


TWCAttackResults TWCPlayerClient::shoot(TWCPlayerClient *player, int ship,
					int target, int power)
{
    UniConf shipcfg(game->cfg["ships"][ship]);
    if (!shipcfg["weapon recharged"].getint())
	return Aborted;

    int sector = shipcfg["location"].getint();
    int weapon = loccfg()["weapon"].getint();

    UniConf targetcfg(game->cfg["ships"][target]);
    int target_player_id = targetcfg["captain"].getint();
    UniConf targetpcfg(game->cfg["players"][target_player_id]);

    TWCPlayerClient *target_player = game->players[target_player_id];
    if (target_player)
	target_player->raise_shields(); // automatically raise shields
    int ship_weapon = shipcfg["weapon"].getint();
    game->log("%s is attacking %s at %s%% power.\n",
	      shipcfg["name"].get(), targetcfg["name"].get(), power);
    TWCAttackResults res = Damaged;

    int attack_roll = rand() % 100;
    game->log("Attacker rolls %s\n", attack_roll);
    if (attack_roll < game->cfg["weapons"][ship_weapon]["accuracy"].getint())
    {
	// Hit!
	int mean_damage = game->cfg["weapons"][ship_weapon]["damage"].getint();
	int damage = (int)box_muller(mean_damage, mean_damage * 0.1) * power / 100;
	if (!damage)
	    damage = 1;

	game->log("Attacker hit for %s damage (mean %s).\n", damage, mean_damage);
				
	int target_shields = targetcfg["shield strength"].getint();
	target_shields -= damage;
	WvString target_shield_type(targetcfg["shield"].get());
	game->announce(ShipAttackHit, ship, target,
		       WvString(
			   "%s blasts %s with its %s.\n",
			   shipcfg["name"].get(),
			   targetcfg["name"].get(),
			   game->cfg["weapons"][shipcfg["weapon"].get()]["name"].get()),
		       sector);

	int capture_threshold = 0;
	// escape pods can't be captured;
	if (targetcfg["class"].getint() != 1)
	    capture_threshold = game->cfg["global"]["capture threshold"].getint(5);

	if (target_shields > capture_threshold)
	{
	    if (target_player)
		target_player->msg(
		    WvString("Shields at %s percent, captain.\n",
			     target_shields * 100 /
			     game->cfg["shields"][target_shield_type]["strength"].getint()
			));
	    targetcfg["shield strength"].setint(target_shields);
	}
	else if (target_shields <= 0)
	{
	    res = Destroyed;
	    game->remove_ship(target_player_id, target);
	    int except = 0;
	    if (target_player)
	    {
		target_player->event(ShipDestroyed, ship, target,
				     "Your ship has been destroyed!");
		except = target_player_id;
	    }
	    game->announce(ShipDestroyed, ship, target,
			   WvString("%s has been destroyed!\n",
				    targetcfg["name"].get()),
			   sector, except);
	    if (targetcfg["class"].getint() != 1)
		game->launch_escape_pod(target_player, target_player_id,
					sector);
	    else
	    {
		// Boom!
		targetpcfg["turns"].setint(0);
		targetpcfg["dead"].setint(1);
		int except = 0;
		if (target_player)
		{
		    target_player->event(PodDestroyed, ship, target,
					 "Your escape pod has been destroyed!\n"
					 "You lose the rest of your turns.");
		    except = target_player_id;
		}
		game->announce(PodDestroyed, ship, target,
			       WvString("Escape pod %s has been destroyed!\n",
					targetcfg["name"].get()),
			       sector, except);

		if (target_player)
		    game->players.remove(target_player);
	    }
	}
	else
	{
	    res = Captured;
	    // Capture ship
	    targetcfg["owner"].setint(shipcfg["captain"].getint());
	    targetcfg["captain"].setint(0);
	    int except = 0;
	    if (target_player)
	    {
		target_player->event(ShipCaptured, ship, target,
				     "Your ship has been captured!");
		except = target_player_id;
	    }
	    game->announce(ShipCaptured, ship, target,
			   WvString("%s captures %s!\n", shipcfg["name"].get(),
				    targetcfg["name"].get()),
			   sector, except);

	    game->launch_escape_pod(target_player, target_player_id,
				    sector);
	}
    }
    else
    {
	res = Missed;
	game->log("attacker missed.\n");
	game->announce(ShipAttackMissed, ship, target,
		       WvString(
			   "%s misses %s with its %s.\n",
			   shipcfg["name"].get(),
			   targetcfg["name"].get(),
			   game->cfg["weapons"][shipcfg["weapon"].get()]["name"].get()),
		       sector);
    }

    loccfg()["weapon recharged"].setint(0);
    game->events.add_event(TWCEventCallback(game, &TWCGameController::weapon_recharge), id,
			   cfg["weapons"][weapon]["recharge time"].getint(),
			   true);
    UniConfCallback cb(attack_callback);
    int *i = new int;
    *i = 6703834;
    loccfg()["weapon recharged"].add_callback((void *)i, cb, false);

    return res;
}


void TWCHumanPlayerClient::menu_attack()
{
    char *line = continue_getline();
    if (!line)
	return;

    switch (line[0])
    {
    case '?':
	print_screen("attack");
	break;

    case 'a':
    case 'A':
    {
	if (!loccfg()["weapon"].getint())
	{
	    print("You have no weapon on this ship.\n");
	    return;
	}
	int current_turns = pcfg()["current turns"].getint();
	int attack_cost = cfg["shipclasses"][loccfg()["class"].getint()]
	                     ["attack cost"].getint();
	if (current_turns < attack_cost)
	{
	    print("Sorry, you don't have any turns left.\n");
	    return;
	}

	if (!loccfg()["weapon recharged"].getint())
	{
	    print("Your weapon is still charging.\n");
	    return;
	}

	int target = acquire_target("Target which ship", "The ship is "
				    "docked.  You can wait until the captain "
				    "returns or attack the port itself.");
	if (!target)
	    return;
	
	print("Power setting (1-100)? [100] ");
	bool abort = false;
	int power = get_int(abort, 100);
	if (abort)
	    return;

	if (power < 1 || power > 100)
	{
	    print("Power setting must be between 1 and 100.\n");
	    return;
	}

	TWCAttackResults res = shoot(this, location_id, target, power);

	switch (res)
	{
	case Damaged:
	    print("Their shields are still holding, captain.\n");
	    break;

	default:
	    break;
	}

	if (res != Aborted)
	{
	    print("Deducting %s turn%s.\n", attack_cost,
		  attack_cost == 1 ? "" : "s");
	    decr_uniconf(pcfg()["current turns"], attack_cost);
	}
    }
    break;

    case 'd':
    case 'D':
    {
	int target = acquire_target("Demand surrender from which ship",
				    "This ship is currently docked.  You can "
				    "wait for the captain to return.");
	if (!target)
	    return;

	UniConf targetcfg(cfg["ships"][target]);
	int target_player_id = targetcfg["captain"].getint();
	UniConf targetpcfg(cfg["players"][target_player_id]);

	TWCPlayerClient *target_player = game->players[target_player_id];
	if (!target_player)
	{
	    print("The ship is unoccupied.  You'll have to take it by force.\n");
	    return;
	}

	msg("You demand the surrender of %s.\n", targetcfg["name"].get());
	demanded_surrender = target;
	target_player->event(DemandSurrender, loccfg().key().printable().num(),
			     target,
			     WvString("\n%s demands you drop your shields and "
				      "surrender your cargo!  You have 10 "
				      "seconds to comply.",
				      loccfg()["name"].get()));
	game->events.add_event(
	    TWCEventCallback(this, &TWCHumanPlayerClient::surrender_timeout),
	    0, 10, true
	    );
	break;
    }

    case 'q':
    case 'Q':
	location_type = Ship;
	break;
    default:
	break;
    }
}

