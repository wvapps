#ifndef __TWCCONTAINER_H__
#define __TWCCONTAINER_H__

template <class T> class Container
{
    T** array;
    unsigned int count;
  public:
    Container();
    virtual ~Container();

    unsigned int Count() const;
    void         Add( T* );
    T*           Remove( T* );
    T*           Find( T ) const;
    bool         Contains( unsigned int*, T* ) const;
    T*           operator[]( unsigned int );

    void         Purge();
};

template <class T> Container<T>::Container() : array(NULL), count(0)
{}

template <class T> Container<T>::~Container()
{
    if( count > 0 )
	deletev array;
}

template <class T> void Container<T>::Purge()
{
    while( Count() != 0 )
    {
	(void)Remove( array[0] );
    }
}

template <class T> unsigned int Container<T>::Count() const
{
    return( count );
}

// search through container for an opject with a matching value
// and return a pointer to the first instance found.
template <class T> T* Container<T>::Find( T obj ) const
{
    if( count == 0 )
	return NULL;

    assert( array );

    for( unsigned int n = 0; n < count; ++n )
    {
	if( *array[n] == obj )
	    return array[n];
    }
    return NULL;
}
    

template <class T> void Container<T>::Add( T *ptr )
{
    assert( ptr );

    if( Contains( NULL, ptr ) )
    {
	return;
    }

    if( count == 0 )
    {
	array = new T*[1];
	array[0] = ptr;
	count = 1;
    }
    else
    {
	T** temp = new T*[count + 1];
	for( unsigned int n = 0; n < count; ++n )
	{
	    temp[n] = array[n];
	}
	temp[count] = ptr;
	++count;
	deletev array;
	array = temp;
    }
}

template <class T> bool Container<T>::Contains( unsigned int *index, T *ptr ) const
{
    if( count == 0 || ptr == NULL )
    {
	return false;
    }

    assert( array );
    for( unsigned int n = 0; n < count; ++n )
    {
	if( array[n] == ptr )
	{
	    if( index != NULL )
	    {
		*index = n;
	    }
	    return true;
	}
    }
    return false;
}

template <class T> T* Container<T>::Remove( T *ptr )
{
    assert( ptr );

    if( count == 0 )
	return NULL;

    assert( array );

    unsigned int index = 0;
    if( Contains( &index, ptr ) == true )
    {      
	if( count == 1 )
	{
	    count = 0;
	    deletev array;
	    array = NULL;
	    return ptr;
	}
	else
	{
	    T** temp = new T*[count - 1];
	    unsigned int n = 0;
	    for( unsigned int m = 0; m < count; ++m )
	    {
		if( m != index )
		{
		    temp[n] = array[m];
		    ++n;
		}
	    }
	    deletev array;
	    --count;
	    array = temp;
	    return ptr;
	}
    }
    else
    {
	return NULL;
    }
}

template <class T> T* Container<T>::operator[] ( unsigned int n )
{
    if( n >= count )
    {
	assert(0);
	return NULL;
    }
    assert( array );
    return( array[n] );
}

#endif

