/* -*- Mode: C++ -*- */
#ifndef __TWCNOWHERE_H
#define __TWCNOWHERE_H

#include "twcobject.h"

class TWCNowhere: public TWCObject
{
public:
    TWCNowhere(UniConf _cfg, TWCGameController *_game)
	: TWCObject(_cfg, _game) {}
    virtual ~TWCNowhere() {}

    virtual WvString name() { return WvString("Nowhere"); }
    virtual bool on_ship() { return false; }
    virtual TWCObjectIf *create_if(TWCHumanPlayerClient &plyrcli);
};


class TWCNowhereIf: public TWCObjectIf
{
protected:
    TWCNowhere &nowhere;

public:
    TWCNowhereIf(TWCNowhere &_nowhere, TWCHumanPlayerClient &_plyrcli)
	: TWCObjectIf(_nowhere, _plyrcli), nowhere(_nowhere) {}
    virtual ~TWCNowhereIf() {}

    virtual void print_menu();
    virtual void menu_input();
    virtual WvString prompt();
};

#endif // __TWCNOWHERE_H
