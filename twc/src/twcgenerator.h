#ifndef __TWCGENERATOR_H__
#define __TWCGENERATOR_H__

/* This is a class used to generate and maintain the game
   universe during game play.  It handles: universe generation
   (including sector generation and linking), setting up and
   maintaining the economics model, etc...
*/

#include <stdlib.h>

#include <wvlog.h>
#include <wvlogrcv.h>
#include <wvcrash.h>
#include <wvconf.h>
#include <signal.h>
#include <errno.h>
#include <time.h>
#include "twcgensector.h"
#include "twccontainer.h"

#define GENERATOR_SUCCESS 0
#define GENERATOR_FAILURE 1

class TWCGenerator {
  private:
    Container<Sector> all_sectors;
    Container<Sector> planet_sectors;
    Container<Sector> port_sectors;

    bool         IsConnected( Sector*, Sector*, unsigned int* );
    bool         IsConnected( Sector*, Sector* );
    
  public:
    WvLog log;

    TWCGenerator();
    virtual ~TWCGenerator();
    
    // Accessors
    void         DumpToFile( char* );
    
    bool         IsConnected( unsigned int, unsigned int );
    bool         IsConnected( unsigned int, unsigned int, unsigned int* );

    // Mutators
    void         Purge(); // erase everything
    unsigned int GenerateSectors( unsigned int );
    unsigned int ConnectAllSectors( float, float, unsigned int );
    unsigned int Connect( unsigned int, unsigned int, unsigned int );

    void         PlaceRandomPlanets( unsigned int );
    void         PlacePlanet( unsigned int );

    void         PlacePortsAtPlanets();
};

#endif
