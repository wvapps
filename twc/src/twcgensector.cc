#include "twcgensector.h"

Sector::Sector( unsigned int n ) : id(n), port_type(0), exits()
{}

Sector::~Sector()
{
    // destroy all of the Path objects contained 
    // in the 'exits' object
    while( exits. Count() != 0 )
    {
	delete( exits. Remove( exits[0] ) );
    }
}

void Sector::RemoveExit( Sector *destination_sector )
{
    for( unsigned int n = 0; n < exits. Count(); ++n )
    {
	if( exits[n]-> destination == destination_sector )
	{
	    exits. Remove( exits[n] );
	    return;
	}
    }
}

void Sector::AddExit( Sector *destination_sector, unsigned int distance )
{
    exits. Add( new Path( destination_sector, distance ) );
}

unsigned int Sector::NumExits() const
{
    return( exits. Count() );
}

bool Sector::DoesExitExist( Sector *destination_sector, unsigned int *distance )
{
    for( unsigned int n = 0; n < exits. Count(); ++n )
    {
	if( exits[n]-> destination == destination_sector )
	{
	    if( distance != NULL )
	    {
		*distance = exits[n]-> distance;
	    }
	    return true;
	}
    }
    return false;
}


Path::Path( Sector *dest, unsigned int dist ) : destination(dest), distance(dist)
{
    assert( dest );
}
 
 
Path::~Path()
{}






