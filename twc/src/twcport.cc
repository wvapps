#include "twcgamecontroller.h"
#include "twcport.h"
#include <strutils.h>

const char *products[] = { "Commodities", "Weapons", "Shields", "Ships", NULL };

void TWCPort::announce_dock()
{
    sector->announce(ShipDocked, docked.first()->id, id,
		     WvString("%s docks with %s.\n", docked.first()->name(),
			      name()), docked.first()->id);
}


void TWCPort::set_location(TWCObject *_sector)
{
//    sector = dynamic_cast<TWCSector*>(_sector);
    sector = (TWCSector*)_sector;
    TWCObject::set_location(sector);
}


void TWCPort::set_by_player(TWCPlayerClient *plyrcli)
{
}


TWCDockResults TWCPort::docking(TWCShip *ship)
{
    if (!ship)
	return NoShip;

    if (docked.count())
	return PortBusy;

    docked.append(ship, false);

    announce_dock();

    return DockSuccess;
}


void TWCPort::blastoff()
{
    assert(docked.count());
    game->curplayers[docked.first()->captain()->player.id]->set_location(docked.first());
    docked.zap();
}


TWCObjectIf *TWCPort::create_if(TWCHumanPlayerClient &plyrcli)
{
    return new TWCPortIf(*this, plyrcli);
}


void TWCPort::get_inventory(TWCPortInventoryList &inventory, WvStringParm type)
{
    UniConf::XIter i(cfg, WvString("inv/%s/*", type));
    for (i.rewind(); i.next(); )
    {
	TWCPortInventory *entry = new TWCPortInventory;
	entry->product_type = i().parent().key().printable();
	entry->product_id = i->key().printable().num();
	entry->purchase_price = i()["price"].getmeint();
	entry->sale_price = entry->purchase_price * cfg["profit"].getmeint()
	                    / 100;
	entry->number = i()["number"].getmeint();
	entry->max_number = i()["max number"].getmeint();
	inventory.append(entry, true);
    }
}


TWCPurchaseResults TWCPort::sell_okay(WvStringParm type, int subtype, int num,
				      int &price)
{
    TWCShip *ship = docked.first();
    if (!ship)
	return NoDockedShip;
    TWCPlayer &player = ship->captain()->player;

    price = get_selling_price(type, subtype, num);

    if (!price)
	return NotInterested;

    if (player.cash() < price)
	return NotEnoughCash;

    return PurchaseSuccess;
}


TWCPurchaseResults TWCPort::buy_okay(WvStringParm type, int subtype, int num,
				     int &price)
{
    TWCShip *ship = docked.first();
    if (!ship)
	return NoDockedShip;

    price = get_buying_price(type, subtype, num);

    if (!price)
	return NotInterested;

    return PurchaseSuccess;
}


int TWCPort::get_buying_price(WvStringParm type, int subtype, int num)
{
    return cfg["inv"][type][subtype]["price"].getmeint() * num;
}


TWCPurchaseResults TWCPort::buy_commodities(int type, int num)
{
    TWCShip *ship = docked.first();
    assert(ship);
    TWCPlayer *player = &ship->captain()->player;
    assert(player);
    UniConf comcfg(cfg["inv/commodities"][type]);

    int price = get_buying_price("commodities", type, num);

    if (!price)
	return NotInterested;
    
    if (comcfg["max number"].getmeint() < comcfg["number"].getmeint() + num)
	return TooManyRequested;

    if (!ship->unload(type, num))
	return NotEnoughOnShip;

    player->add_cash(price);
    incr_uniconf(comcfg["number"], num);

    // Adjust port's price...
    return PurchaseSuccess;
}


int TWCPort::get_selling_price(WvStringParm type, int subtype, int num)
{
    return cfg["inv"][type][subtype]["price"].getmeint() * num 
           * cfg["profit"].getmeint() / 100;
}


TWCPurchaseResults TWCPort::sell_commodities(int type, int num)
{
    int price;
    TWCPurchaseResults res = sell_okay("commodities", type, num, price);

    if (res != PurchaseSuccess)
	return res;
    
    UniConf comcfg(cfg["inv/commodities"][type]);
    TWCShip *ship = docked.first();
    TWCPlayer &player = ship->captain()->player;

    if (comcfg["number"].getmeint() < num)
	return TooManyRequested;

    if (!ship->load(type, num))
	return NoRoomOnShip;

    player.deduct_cash(price);
    decr_uniconf(comcfg["number"], num);

    // Adjust port's price...
    return PurchaseSuccess;
}


TWCPurchaseResults TWCPort::buy_weapon(TWCWeaponClass *weapon)
{
    int price;
    TWCPurchaseResults res = buy_okay("weapons", weapon->id, 1, price);
    if (res != PurchaseSuccess)
	return res;

    TWCShip *ship = docked.first();
    TWCPlayer &player = ship->captain()->player;

    if (!ship->uninstall_weapon(weapon))
	return NotEnoughOnShip;

    player.add_cash(price);

    return PurchaseSuccess;
}


TWCPurchaseResults TWCPort::sell_weapon(TWCWeaponClass *weapon)
{
    int price;
    TWCPurchaseResults res = sell_okay("weapons", weapon->id, 1, price);
    if (res != PurchaseSuccess)
	return res;

    TWCShip *ship = docked.first();
    TWCPlayer &player = ship->captain()->player;

    if (!ship->install_weapon(weapon))
	return NoRoomOnShip;

    player.deduct_cash(price);

    return PurchaseSuccess;
}


TWCPurchaseResults TWCPort::buy_shield(TWCShieldClass *shield)
{
    int price;
    TWCPurchaseResults res = buy_okay("shields", shield->id, 1, price);
    if (res != PurchaseSuccess)
	return res;

    TWCShip *ship = docked.first();
    TWCPlayer &player = ship->captain()->player;

    if (!ship->uninstall_shield(shield))
	return NotEnoughOnShip;

    player.add_cash(price);

    return PurchaseSuccess;
}


TWCPurchaseResults TWCPort::sell_shield(TWCShieldClass *shield)
{
    int price;
    TWCPurchaseResults res = sell_okay("shields", shield->id, 1, price);
    if (res != PurchaseSuccess)
	return res;

    TWCShip *ship = docked.first();
    TWCPlayer &player = ship->captain()->player;

    if (!ship->install_shield(shield))
	return NoRoomOnShip;

    player.deduct_cash(price);

    return PurchaseSuccess;
}


TWCShip *TWCPortIf::check_for_ship()
{
    TWCShip *ship = port.docked.first();
    if (!ship)
    {
	port.game->log(WvLog::Error, "Player %s's ship at port %s has "
		       "disappeared!\n", plyrcli.player.id, port.id);
	plyrcli.msg("Your ship has disappeared!  Something has gone "
		    "terribly wrong!\n");
    }
    return ship;
}


WvString TWCPortIf::purchase_results_string(TWCPurchaseResults res)
{
    WvString resstr("");
    switch (res)
    {
    case PurchaseSuccess: resstr = WvString(
	"You now have %s.\n", port.game->price_string(plyrcli.player.cash())
	);
	break;
    case NoDockedShip: resstr = "You don't have a docked ship here!\n";
	break;
    case NotInterested: resstr = "Sorry, we don't sell those.\n";
	break;
    case NoRoomOnShip: resstr = "You don't have enough free holds.\n";
	break;
    case TooManyRequested: resstr = "Sorry, we don't have that many.\n";
	break;
    case NotEnoughOnShip: resstr = "You don't have enough of that on board.\n";
	break;
    case NotEnoughCash: resstr = "You don't have enough cash.\n";
	break;
    case PurchaseAborted: resstr = "Aborted.\n";
    default:
	break;
    }

    return resstr;
}


void TWCPortIf::get_selling_info(WvStringParm type, int &subtype, int &num)
{
    bool abort = false;
    subtype = plyrcli.get_int(abort);
    if (abort)
    {
	subtype = num = 0;
	return;
    }

    if (num != -1)
    {
	plyrcli.msg("How many would you like? ");
	num = plyrcli.get_int(abort);
	if (abort)
	{
	    subtype = num = 0;
	    return;
	}
    }
    else
	num = 1;

    int price = port.get_selling_price(type, subtype, num);
    if (!price)
    {
	plyrcli.print("Sorry, we don't sell those.\n");
	subtype = num = 0;
	return;
    }

    plyrcli.print("That'll be %s.\n", port.game->price_string(price));
    if (!plyrcli.areyousure())
	subtype = num = 0;
}


void TWCPortIf::get_buying_info(WvStringParm type, int &subtype, int &num)
{
    bool abort = false;
    if (subtype > 0)
    {
	subtype = plyrcli.get_int(abort);
	if (abort)
	{
	    subtype = num = 0;
	    return;
	}
    }
    else
	subtype *= -1;

    if (num > 0)
    {
	plyrcli.msg("How many would you like to sell? ");
	num = plyrcli.get_int(abort);
	if (abort)
	{
	    subtype = num = 0;
	    return;
	}
    }
    else
	num *= -1;

    int price = port.get_buying_price(type, subtype, num);
    if (!price)
    {
	plyrcli.print("Sorry, we don't buy those.\n");
	subtype = num = 0;
	return;
    }

    plyrcli.print("I'll give you %s.\n", port.game->price_string(price));
    if (!plyrcli.areyousure())
	subtype = num = 0;
}


TWCPurchaseResults TWCPortIf::sell_commodities(TWCShip *ship)
{
    int free = ship->free_holds();
    int subtype, num;
	
    plyrcli.msg("\nYou have %s free hold%s and %s in cash.\n", free,
		free == 1 ? "" : "s",
		port.game->price_string(plyrcli.player.cash()));

    plyrcli.msg("Which commodity would you like to buy? ");
	
    get_selling_info("commodities", subtype, num);
    if (!subtype)
	return PurchaseAborted;

    return port.sell_commodities(subtype, num);
}


TWCPurchaseResults TWCPortIf::sell_weapons(TWCShip *ship)
{
    int subtype, num = -1;
    if (ship->weaponclass())
    {
	// Have to sell existing weapon first.
	buy_weapons(ship);
	if (ship->weaponclass())
	    return PurchaseAborted;
    }
    else
	plyrcli.msg("\nYour ship does not currently have a weapon.\n");

    plyrcli.msg("You have %s in cash.\n", 
		port.game->price_string(plyrcli.player.cash()));

    plyrcli.msg("Which weapon would you like to buy? ");
	
    get_selling_info("weapons", subtype, num);
    if (!subtype)
	return PurchaseAborted;

    return port.sell_weapon(port.game->weaponclasses[subtype]);
}


TWCPurchaseResults TWCPortIf::sell_shields(TWCShip *ship)
{
    int subtype, num = -1;
    if (ship->shieldclass())
    {
	buy_shields(ship);
	if (ship->shieldclass())
	    return PurchaseAborted;
    }
    else
	plyrcli.msg("\nYour ship does not currently have a shield.\n");

    plyrcli.msg("You have %s in cash.\n", 
		port.game->price_string(plyrcli.player.cash()));

    plyrcli.msg("Which shield would you like to buy? ");
	
    get_selling_info("shields", subtype, num);
    if (!subtype)
	return PurchaseAborted;

    return port.sell_shield(port.game->shieldclasses[subtype]);
}


TWCPurchaseResults TWCPortIf::buy_commodities(TWCShip *ship)
{
    int free = ship->free_holds();
    int subtype, num;
	
    plyrcli.msg("\nYou have %s free hold%s and %s in cash.\n", free,
		free == 1 ? "" : "s",
		port.game->price_string(plyrcli.player.cash()));

    plyrcli.msg("Which commodity would you like to sell? ");
	
    get_buying_info("commodities", subtype, num);
    if (!subtype)
	return PurchaseAborted;

    return port.buy_commodities(subtype, num);
}


TWCPurchaseResults TWCPortIf::buy_weapons(TWCShip *ship)
{
    if (ship->weaponclass())
	plyrcli.msg("\nYour ship currently has a %s.\n",
		    ship->weaponclass()->name());
    else
    {
	plyrcli.msg("\nYour ship does not currently have a weapon.\n");
	return PurchaseAborted;
    }

    plyrcli.msg("You have %s in cash.\n", 
		port.game->price_string(plyrcli.player.cash()));

    int subtype = -1, num = -1;
    get_buying_info("weapons", subtype, num);
    if (!subtype)
	return PurchaseAborted;

    return port.buy_weapon(ship->weaponclass());
}


TWCPurchaseResults TWCPortIf::buy_shields(TWCShip *ship)
{
    if (ship->shieldclass())
	plyrcli.msg("\nYour ship currently has a %s.\n",
		    ship->shieldclass()->name());
    else
    {
	plyrcli.msg("\nYour ship does not currently have a shield.\n");
	return PurchaseAborted;
    }

    plyrcli.msg("You have %s in cash.\n", 
		port.game->price_string(plyrcli.player.cash()));

    int subtype = -1, num = -1;
    get_buying_info("shields", subtype, num);
    if (!subtype)
	return PurchaseAborted;

    return port.buy_shield(ship->shieldclass());
}


TWCPurchaseResults TWCPortIf::sell_ship()
{
    return NotInterested;
}


TWCPurchaseResults TWCPortIf::buy_ship()
{
    return NotInterested;
}


void TWCPortIf::print_menu()
{
    plyrcli.print(port.game->get_screen("spaceport"));
}


void TWCPortIf::menu_input()
{
    char *line = plyrcli.getline(0);
    if (!line)
	return;

    trim_string(line);

    switch (line[0])
    {
    case 'b':
    case 'B':
    {
	TWCShip *ship = check_for_ship();
	if (!ship)
	    return;

	int type = select_product_type();
	print_price_list(type);

	TWCPurchaseResults res = PurchaseAborted;
	switch (type)
	{
	case 1: res = sell_commodities(ship);
	    break;
	case 2: res = sell_weapons(ship);
	    break;
	case 3: res = sell_shields(ship);
	    break;
	case 4: res = sell_ship();
	    break;
	default: break;
	}
	WvString resstr(purchase_results_string(res));
	plyrcli.print(resstr);
	break;
    }

    case 's':
    case 'S':
    {
	TWCShip *ship = check_for_ship();
	if (!ship)
	    return;

	int type = select_product_type();
	
	TWCPurchaseResults res = PurchaseAborted;
	switch (type)
	{
	case 1: print_price_list(1);
	    res = buy_commodities(ship);
	    break;
	case 2: res = buy_weapons(ship);
	    break;
	case 3: res = buy_shields(ship);
	    break;
	case 4: res = buy_ship();
	    break;
	default: break;
	}
	WvString resstr(purchase_results_string(res));
	plyrcli.print(resstr);
	break;
    }
    
    case 'd':
    case 'D':
	print_price_list(select_product_type());
	break;

    case 'q':
    case 'Q':
	port.blastoff();
	break;
    }
}


WvString TWCPortIf::prompt()
{
    return WvString("%s< %sPort %s> %s%s%s/%s%s", ANSI_YELLOW,
		    ANSI_GREEN, ANSI_YELLOW, ANSI_GREEN, 
		    plyrcli.player.current_turns(), ANSI_YELLOW, ANSI_GREEN,
		    plyrcli.player.max_turns());
}


int TWCPortIf::select_product_type()
{
    WvString menu("");
    int count = 0;
    int choice = 0;
    IntTable types(5);
    const char **p;
    for (p = products; *p; p++)
    {
	++count;
	if (port.cfg["inv"][*p].exists())
	{
	    menu.append("%s. %s\n", p-products+1, *p);
	    types.add(new int(p-products+1), true);
	}
    }

    if (types.isempty())
    {
	plyrcli.msg("We're not open for business yet.  Come back soon.\n");
	return 0;
    }
    else
    {
	plyrcli.msg(menu);
	choice = plyrcli.menu(types, "Which type of product are you "
			      "interested in?");
	if (!choice)
	    return 0;
    }
    return choice;
}


void TWCPortIf::print_price_list(int type)
{
    if (!type)
	return;

    TWCPortInventoryList inventory;
    port.get_inventory(inventory, products[type-1]);

    TWCShip *ship = port.docked.first();
    plyrcli.msg("\n                                     Buying  Selling%s\n",
		type != 3 ? " On board" : "");
    TWCPortInventoryList::Iter i(inventory);
    for (i.rewind(); i.next(); )
    {
	int onboard = ship->onboard(i().product_type, i().product_id);
	WvString key("%s%s", i().product_type, i().product_id);
	strlwr(key.edit());
	if (!port.game->buyables[key])
	    continue;
  	plyrcli.msg("<%2s> %-30s%8s %8s %8s\n", i().product_id,
		    port.game->buyables[key]->name(),
		    i().purchase_price, i().sale_price, 
		    onboard ? WvString(onboard) : WvString(""));
    }
}
