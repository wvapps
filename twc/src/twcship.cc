#include "twcgamecontroller.h"
#include "twcplayerclient.h"
#include "twcutils.h"
#include <wvstrutils.h>

TWCShip::TWCShip(UniConf _cfg, TWCGameController *_game)
    : TWCObject(_cfg, _game), warping_to(NULL)
{
    set_captain(NULL);
}

TWCSector *TWCShip::sector()
{
    return game->get_sector(TWCSectorCoord(cfg["location"].getme()));
}


TWCPlayerClient *TWCShip::captain()
{
    return game->curplayers[cfg["captain"].getmeint()];
}


TWCPlayer *TWCShip::owner()
{
    return game->players[cfg["owner"].getmeint()];
}


TWCShieldClass *TWCShip::shieldclass()
{
    // Eventually we'll support more than one shield.
    int shield = 0;
    UniConf::Iter i(cfg["shields"]);
    for (i.rewind(); i.next(); )
	if (i().getmeint())
	{
	    shield = i().key().printable().num();
	    break;
	}
	 
    return game->shieldclasses[shield];
}


TWCShipClass *TWCShip::shipclass()
{
    return game->shipclasses[cfg["class"].getmeint()];
}


TWCWeaponClass *TWCShip::weaponclass()
{
    // Eventually we'll support more than one weapon.
    int weapon = 0;
    UniConf::Iter i(cfg["weapons"]);
    for (i.rewind(); i.next(); )
	if (i().getmeint())
	{
	    weapon = i().key().printable().num();
	    break;
	}

    return game->weaponclasses[weapon];
}


bool TWCShip::weapon_charged()
{
    return cfg["weapon charged"].getmeint();
}


int TWCShip::free_holds()
{
    int res = total_holds();

    UniConf::Iter i(cfg["commodities"]);
    for (i.rewind(); i.next(); )
    {
	if (isint(i().key().printable()))
	    res -= i().getmeint();
    }

    return res;
}


bool TWCShip::install_weapon(TWCWeaponClass *weapon)
{
    if (!weapon || weaponclass())
	return false;

    cfg["weapons"][weapon->id].setmeint(1);
    return true;
}


bool TWCShip::uninstall_weapon(TWCWeaponClass *weapon)
{
    if (!weaponclass())
	return false;

    cfg["weapons"][weapon->id].remove();
    return true;
}


bool TWCShip::install_shield(TWCShieldClass *shield)
{
    if (!shield || shieldclass())
	return false;

    cfg["shields"][shield->id].setmeint(1);
    cfg["shield strength"].setmeint(shield->strength());
    return true;
}


bool TWCShip::uninstall_shield(TWCShieldClass *shield)
{
    if (!shieldclass())
	return false;

    cfg["shields"][shield->id].remove();
    return true;
}


bool TWCShip::load(int type, int num)
{
    game->log("Loading %s of type %s.\n", num, type);
    if (free_holds() < num)
	return false;

    incr_uniconf(cfg["commodities"][type], num);
    return true;
}


bool TWCShip::unload(int type, int num)
{
    if (cfg["commodities"][type].getmeint() < num)
	return false;

    decr_uniconf(cfg["commodities"][type], num);
    return true;
}


void TWCShip::inventory(TWCShipInventoryList &inv)
{
    TWCShipInventory *item;
    UniConf::Iter i(cfg["commodities"]);
    for (i.rewind(); i.next(); )
    {
	if (!i().getmeint())
	    continue;
	item = new TWCShipInventory;
	item->commodity = game->commodities[i().key().printable().num()];
	item->number = i().getmeint();
	inv.append(item, true);
    }
}


bool TWCShip::beam_cargo_aboard(TWCShip *from_ship, int type, int num)
{
    if (from_ship->sector() != sector())
	return false;
    if (!from_ship->unload(type, num))
	return false;
    if (!load(type, num))
    {
	from_ship->load(type, num);
	return false;
    }
    return true;
}


TWCShip *TWCShip::towing()
{
    return game->ships[cfg["towing"].getmeint()];
}


TWCShip *TWCShip::towed_by()
{
    return game->ships[cfg["towed by"].getmeint()];
}


bool TWCShip::tow(TWCShip *target)
{
    if (!target)
    {
	if (!towing())
	    return false;

	TWCShip *oldtowing = towing();
	cfg["towing"].setmeint(0);
        oldtowing->tow_attempt(NULL);
	sector()->announce(TractorBeamDropped, id, oldtowing->id,
			   WvString("%s deactivates the tractor beam on %s.\n",
				    name(), oldtowing->name()), id);
	return true;
    }

    if (target->tow_attempt(this))
    {
	cfg["towing"].setmeint(target->id);
	sector()->announce(TractorBeamEngaged, id, target->id,
			   WvString("%s locks %s in a tractor beam.\n",
				    name(), target->name()), id);
	return true;
    }
    return false;
}


bool TWCShip::tow_attempt(TWCShip *by)
{
    if (!by)
    {
	cfg["being towed"].setmeint(0);
	return true;
    }

    if (captain())
	return false;

    cfg["being towed"].setmeint(by->id);
    cfg["towing"].setmeint(0);  // just to be sure
    return true;
}


TWCObjectIf *TWCShip::create_if(TWCHumanPlayerClient &plyrcli)
{
    return new TWCShipIf(*this, plyrcli);
}


void TWCShip::set_captain(TWCPlayerClient *plyrcli)
{
    if (plyrcli)
	cfg["captain"].setmeint(plyrcli->player.id);
    else
	cfg["captain"].setme("");
}


void TWCShip::raise_shields()
{
    cfg["shields up"].setmeint(1);
    sector()->announce(ShieldsRaised, id, 0,
		       WvString("%s raises its shields.\n", name()), id);
}


void TWCShip::lower_shields()
{
    cfg["shields up"].setmeint(0);
    sector()->announce(ShieldsDropped, id, 0,
	     WvString("%s lowers its shields.\n", name()), id);
}


TWCDockResults TWCShip::dock()
{
    TWCPort *port = sector()->port();
    if (port)
    {
	TWCDockResults res = port->docking(this);
	if (res == DockSuccess)
	    game->curplayers[captain()->player.id]->set_location(port);

	return res;
    }
    return NoPort;
}


void TWCShip::charge_weapon(int)
{
    if (!weapon_charged())
    {
	cfg["weapon charged"].setmeint(1);
	if (captain())
	    captain()->msg("Weapon charged.\n");
    }
}


WvString TWCShip::viable_target(WvStringParm shipnum)
{
    TWCShip *ship = game->ships[shipnum.num()];
    if (!ship)
        return WvString("No such ship!\n");
    
    if (ship == this)
        return WvString("You can't target your own ship!\n");

    if (ship->sector() != sector())
        return WvString("This ship isn't here!\n");

    return WvString::null;
}


TWCAttackResults TWCShip::shoot(TWCShip *target, int power)
{
    if (!target)
	return Aborted;

    if (!weapon_charged())
	return Aborted;

    // automatically raise shields
    if (!target->are_shields_up())
	target->raise_shields();

    game->log("%s is attacking %s at %s%% power.\n", name(), target->name(),
	      power);
    TWCAttackResults res = Damaged;

    int attack_roll = rand() % 100;
    game->log("Attacker rolls %s\n", attack_roll);
    if (attack_roll < weaponclass()->accuracy())
    {
	// Hit!
	int mean_damage = weaponclass()->damage();
	int damage = (int)box_muller(mean_damage, mean_damage * 0.1) * power
	             / 100;
	if (!damage)
	    damage = 1;

	bool still_alive = target->damage_ship(damage);
	
	game->log("Attacker hit for %s damage (mean %s).\n", damage,
		  mean_damage);
				
	sector()->announce(ShipAttackHit, id, target->id,
			   WvString("%s blasts %s with its %s.\n",
				    name(), target->name(),
				    weaponclass()->name()),
			   captain()->player.id);

	if (!still_alive)
	    res = Destroyed;
    }
    else
    {
	res = Missed;
	game->log("attacker missed.\n");
	sector()->announce(ShipAttackMissed, id, target->id,
			   WvString("%s misses %s with its %s.\n",
				    name(), target->name(),
				    weaponclass()->name()),
			   captain()->player.id);
	captain()->msg("We missed, captain.\n");
    }

    cfg["weapon charged"].setmeint(0);
    game->events.add_event(TWCEventCallback(this, &TWCShip::charge_weapon),
			   0, weaponclass()->recharge_time(), true);

    return res;
}


bool TWCShip::damage_ship(int dmg)
{
    decr_uniconf(cfg["shield strength"], dmg);
    if (shield_strength() < 0)
    {
	decr_uniconf(cfg["hull strength"], shield_strength() * -1);
	cfg["shield strength"].setmeint(0);
    }
    if (hull_strength() <= 0)
    {
	sector()->announce(ShipDestroyed, id, 0, WvString("The %s explodes!\n",
							  name()),
			   captain() ? captain()->player.id : 0);
	if (captain())
	{
	    captain()->msg("Can't... hold it... together...\n");
	    captain()->die();
	}

	game->remove_ship(this);
	return false;
    }

    if (captain())
	if (shield_strength())
	    captain()->msg(WvString("Shields at %s percent, captain.\n",
				    shield_strength() * 100 /
				    shieldclass()->strength()));
	else
	    captain()->msg(WvString("Hull at %s percent, captain.\n",
				    hull_strength() * 100 /
				    shipclass()->hull_strength()));
    
    return true;
}


void TWCShip::do_warp(int)
{
    sector()->ship_warp_out(this);
    cfg["location"].setme(warping_to->coord);
    warping_to = NULL;
    sector()->ship_warp_in(this);
    post_warp(0);
}    


void TWCShip::start_warp(TWCEventCallback _post_warp, TWCSector *dest)
{
    post_warp = _post_warp;
    warping_to = dest;
    game->events.add_event(TWCEventCallback(this, &TWCShip::do_warp), 0, 2,
			   true);
}


void TWCShip::set_by_player(TWCPlayerClient *plyrcli)
{
    set_captain(plyrcli);
    docked_with = NULL;
}


bool TWCShip::check_claim_ownership(TWCPlayer *player)
{
    TWCPlayer *oldowner = owner();
    if (!oldowner || oldowner != player)
    {
	oldowner->remove_ship(this);
	cfg["owner"].setmeint(player->id);
	player->add_ship(this);
	return true;
    }
    return false;
}


TWCShipIf::TWCShipIf(TWCShip &_ship, TWCHumanPlayerClient &_plyrcli)
    : TWCObjectIf(_ship, _plyrcli), ship(_ship), autopilot(false),
      tactical(false), transporter(false), final_dest(NULL)
{
    sector_scan();
}


TWCShipIf::~TWCShipIf()
{
    if (final_dest)
	delete final_dest;
}


void TWCShipIf::print_menu()
{
    if (final_dest)
	return;

    if (tactical)
	plyrcli.print(ship.game->get_screen("tactical"));
    else if (transporter)
	plyrcli.print(ship.game->get_screen("transporter"));
    else
	plyrcli.print(ship.game->get_screen("ship"));
}


void TWCShipIf::menu_input()
{
    if (final_dest)
    {
	warping_menu_input();
	return;
    }
    else if (tactical)
    {
	tactical_menu_input();
	return;
    }
    else if (transporter)
    {
	transporter_menu_input();
	return;
    }

    char *line = plyrcli.getline(0);
    if (!line)
	return;

    trim_string(line);

    switch (line[0])
    {
    case 's':
    case 'S':
	if (ship.are_shields_up())
	{
	    plyrcli.print("You lower your ship's shields.\n");
	    ship.lower_shields();
	}
	else
	{
	    plyrcli.print("You raise your ship's shields.\n");
	    ship.raise_shields();
	}
	break;

    case 'm':
    case 'M':
    {
        plyrcli.print("To which sector (x y z)? ");
	line = plyrcli.continue_getline();
	if (!line)
	    return;
	ship_move(TWCSectorCoord(trim_string(line)));
	break;
    }

    case 'p':
    case 'P':
    {
	if (!plyrcli.player.current_turns())
	{
	    plyrcli.print("You don't have enough turns left.\n");
	    return;
	}
	
	TWCDockResults res = ship.dock();
	if (res == DockSuccess)
	{
	    ship.docked_with = ship.sector()->port();
	    plyrcli.print("Docked with %s.\n", ship.docked_with->name());
	    plyrcli.player.deduct_turns(1);
	}
	else if (res == NoPort)
	    plyrcli.print("[1;31mMove along... nothing to dock with "
			  "here.[0;37m\n");
	else
	    plyrcli.print("Only one ship can be docked with trading ports at a "
			  "time.\n");

	break;	
    }

    case 'a':
    case 'A':
	tactical = true;
	break;

    case 'l':
    case 'L':
	break;	

    case 'd':
    case 'D':
	sector_scan();
	break;

    case 'c':
    case 'C':
	break;

    case '/':
    {
	plyrcli.print("\n\n");
	plyrcli.print("[0;32mStats[0;37m\n");
	plyrcli.print("[1;33m-=-=-[0;37m\n");
	plyrcli.print("[0;32mMoves: [1;33m%s[0;32m/[1;33m%s\n"
		      "[0;32mExperience: [1;33m%s\n"
		      "[0;32mCash: [1;33m%s\n"
		      "[0;32mCurrent Ship: [1;33m%s [0;32m([0;33m%s"
		      "[0;32m)[0;37m\n"
		      "[0;32mShield: [1;33m%s %s[0;32m/[1;33m%s\n"
		      "[0;32mWeapon: [1;33m%s[0;37m\n\n"
		      "Press Enter to continue...\n",
		      plyrcli.player.current_turns(), plyrcli.player.max_turns(),
		      plyrcli.player.experience(),
		      ship.game->price_string(plyrcli.player.cash()),
		      ship.name(), ship.shipclass()->name(),
		      ship.shieldclass() ? ship.shieldclass()->name() : WvString("none"),
		      ship.shieldclass() ? ship.shield_strength() : WvString(0),
		      ship.shieldclass() ? ship.shieldclass()->strength() : WvString(0),
		      ship.weaponclass() ? ship.weaponclass()->name() : WvString("none")
	    );
	plyrcli.continue_getline();

	break;
    }

    case 'x':
    case 'X':
	transporter = true;
	break;

    case 'n':
    case 'N':
    {
	TWCShip *target = plyrcli.acquire_target("Tow which ship",
						 "You cannot scan a docked "
						 "ship.");
	if (target)
	    plyrcli.display_inventory(target);
	break;
    }

    case 't':
    case 'T':
    {
	TWCShip *towing = ship.towing();
	if (towing)
	{
	    plyrcli.print("Stop towing the %s? ", towing->name());
	    line = plyrcli.continue_getline();
	    if (!line)
		return;
	    line = trim_string(line);
	    if (line[0] == 'y' || line[0] == 'Y')
	    {
		plyrcli.print("Tractor beam deactivated.\n");
		ship.tow(NULL);
	    }
	    return;
	}

	TWCShip *target = plyrcli.acquire_target("Tow which ship",
						 "You cannot tow a docked "
						 "ship.");
	if (!target)
	    return;

	if (target->captain())
	{
	    plyrcli.print("You can only tow ships without an active "
			  "captain.\n");
	    return;
	}

	if (ship.tow(target))
	{
	    int move_cost = ship.shipclass()->move_cost() +
	                    target->shipclass()->move_cost();

	    plyrcli.print("You are now towing the %s.  Your new movement cost "
			  "is %s turns per sector.\n", target->name(),
			  move_cost);
	}
	else
	    plyrcli.print("Couldn't tow the %s.\n", target->name());

	break;
    }

    case 'q':
    case 'Q':
	ship.set_captain(NULL);
	plyrcli.set_location(ship.game->nowhere());
	break;

    case '?':
	plyrcli.print(ship.game->screen("ship"));
	break;

    default:
	break;
    }
}


WvString TWCShipIf::prompt()
{
    if (final_dest)
	return WvString("");

    return WvString("%s< %sShip %s> %s%s%s/%s%s%s%s", ANSI_YELLOW,
		    ANSI_GREEN, ANSI_YELLOW, ANSI_GREEN, 
		    plyrcli.player.current_turns(), ANSI_YELLOW, ANSI_GREEN,
		    plyrcli.player.max_turns(), ANSI_RED,
		    ship.cfg["shields up"].getmeint() ? "" : " *shields down*");
}


void TWCShipIf::ship_move(const TWCSectorCoord dest_sector)
{
    if (!dest_sector.ok)
    {
	plyrcli.print("Invalid sector.\n");
        return;
    }

    TWCSectorCoord cur_sector = ship.sector()->coord;
    if (cur_sector == dest_sector)
    {
        plyrcli.print("You're already in that sector.\n");
        return;
    }
    
    // check to see if the destination actually exists
    TWCSectorCoord next_hop = ship.game->find_route(cur_sector, dest_sector);
    if (next_hop == cur_sector)
    {
        plyrcli.print("Sector %s does not exist.\n", dest_sector);
	return;
    }

    // if no match was found (above) then we need to
    // find a path to the destination
    if (next_hop != dest_sector)
    {
	TWCSectorCoord chain = next_hop;
	
	plyrcli.print("Sector %s not adjacent; calculating route.\n[%s] ",
		     dest_sector, next_hop);
	
	while (chain != dest_sector)
	{
	    chain = ship.game->find_route(chain, dest_sector);
	    plyrcli.print("[%s] ", chain);
	}

	char *line;
	plyrcli.print("\nEngage [yna]? ");
	line = plyrcli.continue_getline();
	if (!line)
	    return;
	trim_string(line);
	if (line[0] == 'a' || line[0] == 'A')
	    autopilot = true;
	else if (line[0] != 'y' && line[0] != 'Y')
	    return;
    }

    final_dest = new TWCSectorCoord(dest_sector);
    if (checkmoveturns())
	ship.start_warp(TWCEventCallback(this, &TWCShipIf::post_warp), 
			ship.game->get_sector(next_hop));

}


void TWCShipIf::warping_menu_input()
{
    char *line = plyrcli.getline(0);
    if (!line || ship.warping())
	return;
    trim_string(line);

    switch (line[0])
    {
    case 'a':
    case 'A':
	autopilot = true;
	// fall through
    case 'y':
    case 'Y':
    {
	TWCSectorCoord next_hop = ship.game->find_route(ship.sector()->coord,
							*final_dest);
	ship.start_warp(TWCEventCallback(this, &TWCShipIf::post_warp),
			ship.game->get_sector(next_hop));
	break;
    }
    case 'n':
    case 'N':
//	ship.stop_warp();
	plyrcli.alarm(0);
	final_dest = NULL;
	break;
    default:
	plyrcli.print("Continue with trip? [yna]");
    }
}


void TWCShipIf::post_warp(int)
{
    sector_scan();
    if (ship.sector()->coord != *final_dest)
    {
	TWCSectorCoord next_hop = ship.game->find_route(ship.sector()->coord,
							*final_dest);
	if (!autopilot)
	    plyrcli.print("Continue with trip [yna]? ");
	else
	    ship.start_warp(TWCEventCallback(this, &TWCShipIf::post_warp),
			    ship.game->get_sector(next_hop));
	return;
    }

    plyrcli.alarm(0);
    final_dest = NULL;
}


void TWCShipIf::sector_scan()
{
    plyrcli.print("\n[1;33m-=- [0;35m %s [1;33m-=-[0;37m\n",
		 ship.sector()->name());

    TWCShipList ships;
    ship.sector()->find_player_ships(ships, &ship);
    if (ships.count())
    {
	plyrcli.print("Ships in this sector:\n");
	TWCShipList::Iter shi(ships);
	for (shi.rewind(); shi.next(); )
	    plyrcli.print("%s%s <%s>, %s\n", shi.ptr() == ship.towing() ?
			  "<TOWING> " : "", shi().name(),
			  shi().shipclass()->name(),
			  shi().captain() ? shi().captain()->player.name()
			                  : WvString("unoccupied"));
    }

    TWCPort *port = ship.sector()->port();
    if (port)
	plyrcli.print("A spaceport is in this sector: %s\n", port->name());
}


void TWCGameController::initialize_ship(int ship, int captain)
{
    UniConf shipcfg(cfg["ships"][ship]);
    shipcfg["captain"].setmeint(captain);
    shipcfg["shields up"].setmeint(1);
    shipcfg["weapon recharged"].setmeint(1);
}


bool TWCShipIf::checkmoveturns()
{
    int move_cost = ship.shipclass()->move_cost();
    if (ship.towing())
	move_cost += ship.towing()->shipclass()->move_cost();

    if (plyrcli.player.current_turns() < move_cost)
    {
	plyrcli.print("You don't have enough turns left.\n");
	return false;
    }
    else
    {
	plyrcli.player.deduct_turns(move_cost);
	plyrcli.print("%s turn%s deducted; %s remaining.\n", move_cost,
		      move_cost == 1 ? "" : "s",
		      plyrcli.player.current_turns());
	plyrcli.print("Warping...\n");
    }
    return true;
}


void TWCShipIf::tactical_menu_input()
{
    char *line = plyrcli.getline(0);
    if (!line)
	return;

    trim_string(line);

    switch (line[0])
    {
    case '?':
	plyrcli.print(ship.game->screen("tactical"));
	break;

    case 'a':
    case 'A':
    {
	if (!ship.weaponclass())
	{
	    plyrcli.print("You have no weapon on this ship.\n");
	    return;
	}

	if (!ship.weapon_charged())
	{
	    plyrcli.print("Your weapon is still charging.\n");
	    return;
	}

	int attack_cost = ship.shipclass()->attack_cost();
	if (plyrcli.player.current_turns() < attack_cost)
	{
	    plyrcli.print("Sorry, you don't have any turns left.\n");
	    return;
	}

	TWCShip *target = plyrcli.acquire_target(
	    "Target which ship", "The ship is docked.  You can wait until the "
	    "captain returns or attack the port itself."
	    );
	if (!target)
	    return;
	
	plyrcli.print("Power setting (1-100)? [100] ");
	bool abort = false;
	int power = plyrcli.get_int(abort, 100);
	if (abort)
	    return;

	if (power < 1 || power > 100)
	{
	    plyrcli.print("Power setting must be between 1 and 100.\n");
	    return;
	}

	TWCAttackResults res = ship.shoot(target, power);

	switch (res)
	{
	case Damaged:
	    plyrcli.print("Their shields are still holding, captain.\n");
	    break;

	case Destroyed:
	    plyrcli.print("We destroyed them!\n");

	default:
	    break;
	}

	if (res != Aborted)
	{
	    plyrcli.print("Deducting %s turn%s.\n", attack_cost,
			  attack_cost == 1 ? "" : "s");
	    plyrcli.player.deduct_turns(attack_cost);
	}
    }
    break;
#if 0
    case 'd':
    case 'D':
    {
	int target = acquire_target("Demand surrender from which ship",
				    "This ship is currently docked.  You can "
				    "wait for the captain to return.");
	if (!target)
	    return;

	UniConf targetcfg(cfg["ships"][target]);
	int target_player_id = targetcfg["captain"].getmeint();
	UniConf targetpcfg(cfg["players"][target_player_id]);

	TWCPlayerClient *target_player = game->players[target_player_id];
	if (!target_player)
	{
	    print("The ship is unoccupied.  You'll have to take it by force.\n");
	    return;
	}

	msg("You demand the surrender of %s.\n", targetcfg["name"].getme());
	demanded_surrender = target;
	target_player->event(DemandSurrender, loccfg().key().printable().num(),
			     target,
			     WvString("\n%s demands you drop your shields and "
				      "surrender your cargo!  You have 10 "
				      "seconds to comply.",
				      loccfg()["name"].getme()));
	game->events.add_event(
	    TWCEventCallback(this, &TWCHumanPlayerClient::surrender_timeout),
	    0, 10, true
	    );
	break;
    }
#endif
    case 'q':
    case 'Q':
	tactical = false;
	break;
    default:
	break;
    }
}
