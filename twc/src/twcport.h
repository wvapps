/* -*- Mode: C++ -*- */
#ifndef __TWCPORT_H
#define __TWCPORT_H

#include "twcobject.h"
#include "twcship.h"

extern const char *products[];

enum TWCPurchaseResults {
    PurchaseSuccess = 0,
    NoDockedShip,
    NotInterested,
    NoRoomOnShip,
    TooManyRequested,
    NotEnoughOnShip,
    NotEnoughCash,
    PurchaseAborted
};


struct TWCPortInventory
{
    WvString product_type;
    int product_id;
    int purchase_price;
    int sale_price;
    int number;
    int max_number;
};

DeclareWvList(TWCPortInventory);


class TWCPort: public TWCObject
{
    friend class TWCPortIf;

protected:
    TWCSector *sector;
    TWCShipList docked;

    virtual void announce_dock();

public:
    TWCPort(UniConf _cfg, TWCGameController *_game)
	: TWCObject(_cfg, _game) {}
    virtual ~TWCPort() {}

    virtual void set_location(TWCObject *_sector);
    virtual void set_by_player(TWCPlayerClient *plyrcli);
    virtual bool on_ship() { return true; }

    virtual TWCDockResults docking(TWCShip *ship);
    void blastoff();

    virtual void get_inventory(TWCPortInventoryList &inventory,
                               WvStringParm type = WvString::null);

    /// For buying from a player.
    TWCPurchaseResults sell_okay(WvStringParm type, int subtype, int num,
				 int &price);
    int get_buying_price(WvStringParm type, int subtype, int num);
    TWCPurchaseResults buy_commodities(int type, int num);
    TWCPurchaseResults buy_weapon(TWCWeaponClass *weapon);
    TWCPurchaseResults buy_shield(TWCShieldClass *shield);

    /// For selling to a player.
    TWCPurchaseResults buy_okay(WvStringParm type, int subtype, int num,
				int &price);
    int get_selling_price(WvStringParm type, int subtype, int num);
    TWCPurchaseResults sell_commodities(int type, int num);
    TWCPurchaseResults sell_weapon(TWCWeaponClass *weapon);
    TWCPurchaseResults sell_shield(TWCShieldClass *shield);

    virtual TWCObjectIf *create_if(TWCHumanPlayerClient &plyrcli);
};

DeclareWvDict(TWCPort, int, id);


class TWCPortIf: public TWCObjectIf
{
private:
    TWCPort &port;

public:
    TWCPortIf(TWCPort &_port, TWCHumanPlayerClient &_plyrcli)
	: TWCObjectIf(_port, _plyrcli), port(_port) {}
    virtual ~TWCPortIf() {}

    TWCShip *check_for_ship();
    WvString purchase_results_string(TWCPurchaseResults res);
    void get_buying_info(WvStringParm type, int &subtype, int &num);
    void get_selling_info(WvStringParm type, int &subtype, int &num);
    TWCPurchaseResults buy_commodities(TWCShip *ship);
    TWCPurchaseResults buy_weapons(TWCShip *ship);
    TWCPurchaseResults buy_shields(TWCShip *ship);
    TWCPurchaseResults buy_ship();
    TWCPurchaseResults sell_commodities(TWCShip *ship);
    TWCPurchaseResults sell_weapons(TWCShip *ship);
    TWCPurchaseResults sell_shields(TWCShip *ship);
    TWCPurchaseResults sell_ship();
    virtual void print_menu();
    virtual void menu_input();
    virtual WvString prompt();
    int select_product_type();
    void print_price_list(int type);
};

#endif // __TWCPORT_H
