#include "twcgamecontroller.h"
#include <strutils.h>

void TWCHumanPlayerClient::menu_computer()
{
    char *line = getline(0);

    switch (line[0])
    {
    case 'T':
    case 't':
	if (verbose)
	{
	    cfg["players"][id]["verbose"].setint(false);
	    verbose = false;
	}
	else
	{
	    cfg["players"][id]["verbose"].setint(true);
	    verbose = true;
	}
	break;

    case '?':
	print_screen("computer");
	break;

    case 'q':
    case 'Q':
	location_type = Ship;
	break;
    }
}
