#ifndef __TWCTRANSPORTER_H
#define __TWCTRANSPORTER_H

enum check_shields_result { CSAccessGranted, CSAccessDenied, CSAccessForbidden };
enum transport_self_result { TSSuccessful, TSAccessDenied, TSAlreadyCaptained,
			     TSClaimedOwnership };
enum transport_comm_result { TCSuccessful, TCAccessDenied, TCFull, TCNotEnough };

#endif // __TWCTRANSPORTER_H
