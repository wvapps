/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "zen.h"
#include "file_list.h"
#include "views.h"
#include "status_bar.h"

#include <gtk/gtkliststore.h>
#include <gtk/gtktreeview.h>
#include <strings.h>

enum
{
    FL_BUFFER
};

enum {
  BUFFER_FOCUS_SIGNAL,
  CUR_BUFFER_MODIFIED_CHANGED_SIGNAL,
  CUR_BUFFER_SELECTION_CHANGED_SIGNAL,
  CUR_BUFFER_CAN_UNDO_CHANGED_SIGNAL,
  CUR_BUFFER_CAN_REDO_CHANGED_SIGNAL,
  LAST_SIGNAL
};

static guint file_list_signals[LAST_SIGNAL] = { 0 };

static void file_list_class_init(FileListClass *klass)
{
    file_list_signals[BUFFER_FOCUS_SIGNAL] =
	    g_signal_new("buffer-focus",
			  G_TYPE_FROM_CLASS(klass),
			  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			  G_STRUCT_OFFSET(FileListClass, buffer_focus),
			  NULL, NULL,
			  g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1,
			  TYPE_BUFFER);
    file_list_signals[CUR_BUFFER_MODIFIED_CHANGED_SIGNAL] =
	    g_signal_new("cur-buffer-modified-changed",
			  G_TYPE_FROM_CLASS(klass),
			  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			  G_STRUCT_OFFSET(FileListClass, cur_buffer_modified_changed),
			  NULL, NULL,
			  g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1,
			  TYPE_BUFFER);
    file_list_signals[CUR_BUFFER_SELECTION_CHANGED_SIGNAL] =
	    g_signal_new("cur-buffer-selection-changed",
			  G_TYPE_FROM_CLASS(klass),
			  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			  G_STRUCT_OFFSET(FileListClass, cur_buffer_selection_changed),
			  NULL, NULL,
			  g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1,
			  TYPE_BUFFER);
    file_list_signals[CUR_BUFFER_CAN_UNDO_CHANGED_SIGNAL] =
	    g_signal_new("cur-buffer-can-undo-changed",
			  G_TYPE_FROM_CLASS(klass),
			  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			  G_STRUCT_OFFSET(FileListClass, cur_buffer_can_undo_changed),
			  NULL, NULL,
			  g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1,
			  TYPE_BUFFER);
    file_list_signals[CUR_BUFFER_CAN_REDO_CHANGED_SIGNAL] =
	    g_signal_new("cur-buffer-can-redo-changed",
			  G_TYPE_FROM_CLASS(klass),
			  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			  G_STRUCT_OFFSET(FileListClass, cur_buffer_can_redo_changed),
			  NULL, NULL,
			  g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1,
			  TYPE_BUFFER);
}

static void file_list_cursor_changed_cb(FileList *file_list, gpointer user_data)
{
    GtkTreePath *path;
    GtkTreeIter iter;
    Buffer *buffer;
    
    gtk_tree_view_get_cursor(GTK_TREE_VIEW(file_list), &path, NULL);
    
    gtk_tree_model_get_iter(file_list->model, &iter, path);
    
    buffer = file_list_tree_iter_get_buffer(file_list, &iter);
    if (buffer != NULL)
    {
    	View *view;
	if (file_list->button_press_modified)
	    view = views_set_split_view(buffer);
	else
	    view = views_set_one_view(buffer);
	view_focus(view);
    }
    else g_warning("file_list_row_activated: buffer == NULL");
    
    gtk_tree_path_free(path);
}

static gint file_list_sort_function(GtkTreeModel *model,
	GtkTreeIter *a, GtkTreeIter *b, gpointer _file_list)
{
    FileList *file_list = (FileList *)_file_list;
    
    return strcasecmp(
    	    buffer_get_desc(file_list_tree_iter_get_buffer(file_list, a)),
	    buffer_get_desc(file_list_tree_iter_get_buffer(file_list, b)));
}

static void file_list_render_filename(GtkTreeViewColumn *column,
		GtkCellRenderer *renderer, GtkTreeModel *model,
		GtkTreeIter *iter, gpointer _file_list)
{
    FileList *file_list = (FileList *)_file_list;

    Buffer *buffer = file_list_tree_iter_get_buffer(file_list, iter);
    if (buffer != NULL)
    {
	gboolean visible = views_get_top_buffer() == buffer
		|| views_get_bottom_buffer() == buffer;
	gchar *dispname;
	
	g_object_get(renderer, "text", &dispname, NULL);
	g_free(dispname);

	dispname = g_strconcat(buffer_get_desc(buffer),
		buffer_get_modified(buffer)? " *": "",
		NULL);
	g_object_set(renderer, "text", dispname, NULL);
	
	if (visible)
	{
	    g_object_set(renderer, "cell-background", "darkgray", NULL);
	    g_object_set(renderer, "cell-background-set", TRUE, NULL);
	}
	else
	    g_object_set(renderer, "cell-background-set", FALSE, NULL);
    }
    else g_warning("file_list_render_filename: buffer == NULL");
}

static gboolean file_list_button_press_event_cb(FileList *file_list,
	GdkEventButton *event, gpointer user_data)
{
    file_list->button_press_modified = event->state & (GDK_SHIFT_MASK | GDK_CONTROL_MASK);

    return FALSE;
}

static void file_list_show_hide(FileList *file_list)
{
    GtkWidget *parent = gtk_widget_get_parent(GTK_WIDGET(file_list));
    FileListIter this_iter;
    int count = 0;
    
    if (!GTK_IS_WIDGET(parent))
        return;
    
    for (file_list_iter_start(file_list, &this_iter);
	    !file_list_iter_done(&this_iter);
	    file_list_iter_next(&this_iter))
    {
	++count;
    }
    
    if (count > 1)
        gtk_widget_show(parent);
    else
        gtk_widget_hide(parent);
}


static void file_list_init(FileList *file_list)
{
    GtkCellRenderer *renderer;
    GtkTreeViewColumn *column;
    GtkTreeSelection *selection;
    GdkColormap *colormap = gdk_colormap_get_system();
    GdkColor color;

    file_list->button_press_modified = FALSE;

    renderer = gtk_cell_renderer_text_new();
    g_object_set(renderer, "text", g_strdup(""), NULL);

    column = gtk_tree_view_column_new();
    gtk_tree_view_column_pack_start(column, renderer, TRUE);
    gtk_tree_view_column_set_cell_data_func(column, renderer,
	    file_list_render_filename, file_list, NULL);

    file_list->model = GTK_TREE_MODEL(gtk_list_store_new(2, G_TYPE_POINTER, G_TYPE_POINTER));
    gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(file_list->model), 0,
	    file_list_sort_function, file_list, NULL);
    gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(file_list->model),
    	    0, GTK_SORT_ASCENDING);
    
    gtk_tree_view_set_model(GTK_TREE_VIEW(file_list),
    	    GTK_TREE_MODEL(file_list->model));
    gtk_tree_view_append_column(GTK_TREE_VIEW(file_list), column);
    gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(file_list), FALSE);
    gtk_widget_set(GTK_WIDGET(file_list), "can-focus", FALSE, NULL);
    g_signal_connect(G_OBJECT(file_list), "cursor-changed",
	    GTK_SIGNAL_FUNC(file_list_cursor_changed_cb), NULL);
    g_signal_connect(G_OBJECT(file_list), "button-press-event",
	    GTK_SIGNAL_FUNC(file_list_button_press_event_cb), NULL);
    gdk_color_parse("#C0C0C0", &color);
    gdk_colormap_alloc_color(colormap, &color, FALSE, FALSE);
    gtk_widget_modify_base(GTK_WIDGET(file_list), GTK_STATE_NORMAL, &color);
    
    selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(file_list));
    gtk_tree_selection_set_mode(selection, GTK_SELECTION_NONE);
    
    file_list_show_hide(file_list);
}

GType file_list_get_type(void)
{
    static GType type = 0;
    
    if (!type)
    {
    	static const GTypeInfo info =
    	{
    	    sizeof(FileListClass),
    	    NULL, /* base_init */
    	    NULL, /* base_finalize */
    	    (GClassInitFunc) file_list_class_init,
    	    NULL, /* class_finalize */
    	    NULL, /* class_data */
    	    sizeof(FileList),
    	    0,	  /* n_preallocs */
    	    (GInstanceInitFunc) file_list_init,
    	};
        
    	type = g_type_register_static (GTK_TYPE_TREE_VIEW,
		"FileList", &info, 0);
    }

    return type;
}

Buffer *file_list_tree_iter_get_buffer(FileList *file_list, GtkTreeIter *iter)
{
    Buffer *result;
    
    g_assert(file_list != NULL);
    
    gtk_tree_model_get(GTK_TREE_MODEL(file_list->model), iter,
	    FL_BUFFER, &result, -1);
    
    return result;
}

Buffer *file_list_iter_get_buffer(FileListIter *iter)
{
    return file_list_tree_iter_get_buffer(iter->file_list, &iter->iter);
}

void file_list_tree_iter_set_buffer(FileList *file_list,
    	GtkTreeIter *iter, Buffer *buffer)
{
    gtk_list_store_set(GTK_LIST_STORE(file_list->model),
    	    iter, FL_BUFFER, buffer, -1);
}

void file_list_iter_set_buffer(FileListIter *iter, Buffer *buffer)
{
    file_list_tree_iter_set_buffer(iter->file_list, &iter->iter, buffer);
}

gboolean file_list_get_cur_iter(FileList *file_list, FileListIter *iter)
{
    GtkTreePath *path;
    
    gtk_tree_view_get_cursor(GTK_TREE_VIEW(file_list), &path, NULL);
    if (path != NULL)
    {
    	iter->file_list = file_list;
	iter->done = FALSE;
	gtk_tree_model_get_iter(file_list->model, &iter->iter, path);

	return TRUE;
    }
    else return FALSE;
}

gboolean file_list_find_buffer(FileList *file_list,
    	const Buffer *buffer, FileListIter *iter)
{
    for (file_list_iter_start(file_list, iter);
    	    !file_list_iter_done(iter);
    	    file_list_iter_next(iter))
    {
	Buffer *this_buffer = file_list_iter_get_buffer(iter);

	if (buffer == this_buffer)
	    return TRUE;
    }
    
    return FALSE;
}

void file_list_iter_start(FileList *file_list, FileListIter *iter)
{
    iter->file_list = file_list;
    iter->done = !gtk_tree_model_get_iter_first(file_list->model, &iter->iter);
}

gboolean file_list_iter_done(const FileListIter *iter)
{
    return iter->done;
}

void file_list_iter_next(FileListIter *iter)
{
    iter->done = !gtk_tree_model_iter_next(iter->file_list->model, &iter->iter);
}

static void file_list_buffer_entry_changed(FileList *file_list, Buffer *buffer)
{
    FileListIter iter;
    
    for (file_list_iter_start(file_list, &iter);
    	    !file_list_iter_done(&iter);
    	    file_list_iter_next(&iter))
    {
    	if (file_list_iter_get_buffer(&iter) == buffer)
    	{
    	    GtkTreePath *path = gtk_tree_model_get_path(file_list->model,
    	    	    &iter.iter);
    	    GdkRectangle visible_rect, cell_rect;
    	
    	    gtk_tree_model_row_changed(file_list->model, path, &iter.iter);
    	    
    	    gtk_tree_view_get_visible_rect(GTK_TREE_VIEW(file_list),
                    &visible_rect);
            gtk_tree_view_get_background_area(GTK_TREE_VIEW(file_list),
            	    path, NULL, &cell_rect);
            if (cell_rect.y < 0
            	    || cell_rect.y + cell_rect.height > visible_rect.height)
    	    	gtk_tree_view_scroll_to_cell(GTK_TREE_VIEW(file_list),
            	    	path, NULL, TRUE, 1.0, 0.5);
    	    
    	    gtk_tree_path_free(path);
    	
    	    break;
    	}
    }
}

static void buffer_modified_changed_cb(Buffer *buffer, FileList *file_list)
{
    file_list_buffer_entry_changed(file_list, buffer);

    if (buffer == get_cur_buffer())
    {
    	g_signal_emit(G_OBJECT(file_list),
    	    	file_list_signals[CUR_BUFFER_MODIFIED_CHANGED_SIGNAL],
    	    	0, buffer);
    }
}

static void buffer_desc_changed_cb(Buffer *buffer, const gchar *desc,
    	FileList *file_list)
{
    file_list_buffer_entry_changed(file_list, buffer);
    
    // Force resort (hacky -- the obvious ways don't work)   
    gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(file_list->model),
    	    0, GTK_SORT_DESCENDING);
    gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(file_list->model),
    	    0, GTK_SORT_ASCENDING);
}

static void buffer_focus_in_cb(Buffer *buffer, View *view, FileList *file_list)
{
    file_list_buffer_entry_changed(file_list, buffer);
    
    g_signal_emit(G_OBJECT(file_list),
    	    file_list_signals[BUFFER_FOCUS_SIGNAL], 0, buffer);
}

static void buffer_mark_set_cb(Buffer *buffer, Iter *iter,
	Mark *mark, FileList *file_list)
{
    if (buffer == get_cur_buffer())
    {
    	Mark *insert = gtk_text_buffer_get_insert(GTK_TEXT_BUFFER(buffer));
    	Mark *selection_bound = gtk_text_buffer_get_selection_bound(GTK_TEXT_BUFFER(buffer));
        
    	if (mark == insert || mark == selection_bound)
    	{
    	    g_signal_emit(G_OBJECT(file_list),
    	    	    file_list_signals[CUR_BUFFER_SELECTION_CHANGED_SIGNAL], 0, buffer);
    	}
    }
}

static void buffer_can_undo_cb(Buffer *buffer,
    	gboolean can_undo, FileList *file_list)
{
    if (buffer == get_cur_buffer())
    {
    	g_signal_emit(G_OBJECT(file_list),
    	    	file_list_signals[CUR_BUFFER_CAN_UNDO_CHANGED_SIGNAL], 0, buffer);
    }
}

static void buffer_can_redo_cb(Buffer *buffer,
    	gboolean can_redo, FileList *file_list)
{
    if (buffer == get_cur_buffer())
    {
    	g_signal_emit(G_OBJECT(file_list),
    	    	file_list_signals[CUR_BUFFER_CAN_REDO_CHANGED_SIGNAL], 0, buffer);
    }
}

void file_list_add_buffer(FileList *file_list, Buffer *buffer)
{
    GtkTreeIter iter;
    
    g_object_ref(G_OBJECT(buffer));

    gtk_list_store_append(GTK_LIST_STORE(file_list->model), &iter);

    g_signal_connect(G_OBJECT(buffer), "modified-changed",
    	    GTK_SIGNAL_FUNC(buffer_modified_changed_cb), file_list);
    g_signal_connect(G_OBJECT(buffer), "desc-changed",
    	    GTK_SIGNAL_FUNC(buffer_desc_changed_cb), file_list);
    g_signal_connect(G_OBJECT(buffer), "focus-in",
    	    GTK_SIGNAL_FUNC(buffer_focus_in_cb), file_list);
    g_signal_connect(G_OBJECT(buffer), "mark-set",
		    GTK_SIGNAL_FUNC(buffer_mark_set_cb), file_list);
    g_signal_connect(G_OBJECT(buffer), "can-undo",
		    GTK_SIGNAL_FUNC(buffer_can_undo_cb), file_list);
    g_signal_connect(G_OBJECT(buffer), "can-redo",
		    GTK_SIGNAL_FUNC(buffer_can_redo_cb), file_list);
     
    file_list_tree_iter_set_buffer(file_list, &iter, buffer);
    
    file_list_show_hide(file_list);
}

Buffer *file_list_get_last_focused_buffer(FileList *file_list)
{
    time_t most_recent_time = 0;
    Buffer *most_recent_buffer = NULL;
    FileListIter this_iter;
    
    for (file_list_iter_start(file_list, &this_iter);
	    !file_list_iter_done(&this_iter);
	    file_list_iter_next(&this_iter))
    {
	Buffer *this_buffer = file_list_iter_get_buffer(&this_iter);
	time_t this_time = buffer_get_last_focus_time(this_buffer);
	if (this_time > most_recent_time || most_recent_buffer == NULL)
	{
	    most_recent_time = this_time;
	    most_recent_buffer = this_buffer;
	}
    }
    
    return most_recent_buffer;
}

void file_list_remove_buffer(FileList *file_list, Buffer *buffer)
{
    FileListIter iter;
    
    for (file_list_iter_start(file_list, &iter);
	    !file_list_iter_done(&iter);
	    file_list_iter_next(&iter))
    {
	if (file_list_iter_get_buffer(&iter) == buffer)
	{
	    int i;
	
	    gtk_list_store_remove(GTK_LIST_STORE(file_list->model),
	    	    &iter.iter);
	    
	    g_object_unref(G_OBJECT(buffer));

	    for (i=0; i<2; ++i)
	    {
		View *view = views_get_view(i);
		
		if (view != NULL)
		{
		    view_focus(view);
		    break;
		}
	    }
    
    	    if (file_list_num_buffers(file_list) == 0)
    	    {
	    	gtk_window_set_title(GTK_WINDOW(app), FRIENDLY_NAME);

    	        status_bar_hide_numbers();
    	        
    	    	g_signal_emit(G_OBJECT(file_list),
    	    	    	file_list_signals[BUFFER_FOCUS_SIGNAL], 0, NULL);
    	    }
    
            file_list_show_hide(file_list);

	    return;
	}
    }
    
    g_warning("file_list_remove_buffer: buffer not found");
}

gint file_list_num_buffers(FileList *file_list)
{
    return gtk_tree_model_iter_n_children(file_list->model, NULL);
}

GtkWidget* file_list_new(void)
{
    return g_object_new(TYPE_FILE_LIST, NULL);
}
