/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef VIEW_H
#define VIEW_H

#include "iter.h"

#include <gtk/gtktextview.h>
#include <gtk/gtkcontainer.h>
#include <gtksourceview/gtksourceview.h>

struct _Buffer;

#define TYPE_VIEW	    (view_get_type())
#define VIEW(obj)	    (G_TYPE_CHECK_INSTANCE_CAST((obj), TYPE_VIEW, View))
#define VIEW_CLASS(class)   (G_TYPE_CHECK_CLASS_CAST((class), TYPE_VIEW, ViewClass))
#define IS_VIEW(obj)		(G_TYPE_CHECK_INSTANCE_TYPE((obj), TYPE_VIEW))
#define IS_VIEW_CLASS(class)	(G_TYPE_CHECK_CLASS_TYPE((class), TYPE_VIEW))
#define VIEW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS((obj), TYPE_VIEW, ViewClass))

typedef struct _View	    View;
typedef struct _ViewClass ViewClass;

struct _ViewClass
{
    GtkSourceViewClass parent_class;
};

struct _View
{
    GtkSourceView parent;
    
    GtkWidget *containee;
    gboolean scroll_this_once;
};

GType view_get_type(void);

View *view_new(struct _Buffer *buffer);

GtkWidget *view_get_containee(View *view);

struct _Buffer *view_get_buffer(View *view);
void view_select(View *view, Iter *start, Iter *end);
void view_scroll_to_insert(View *view);
void view_scroll_next_resize(View *view);

View *get_cur_view(void);

void view_goto_line_number(View *view, gint line_number);
void view_focus(View *view);

#endif
