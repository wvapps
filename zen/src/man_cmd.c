/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "make_cmd.h"

#include "zen.h"

#include <gconf/gconf-client.h>

GtkEntry *page_entry;
GtkEntry *section_entry;

static void man_dialog_changed_cb(GtkWidget *entry, gpointer _dialog)
{
    GtkDialog *dialog = (GtkDialog *)_dialog;

    gboolean is_valid = gtk_entry_get_text(page_entry)[0];

    gtk_dialog_set_response_sensitive(dialog, GTK_RESPONSE_OK, is_valid);
}

#define FIND_CONF_PATH(key) 	CONF_PATH("Man") "/" key
#define CONF_LAST_PAGE	    	FIND_CONF_PATH("Page")
#define CONF_LAST_SECTION     	FIND_CONF_PATH("Section")

static GtkWidget *build_man_page(GtkDialog *dialog)
{
    GtkWidget *label = gtk_label_new(NULL);
    page_entry = GTK_ENTRY(gtk_entry_new());
    GtkWidget *hbox = gtk_hbox_new(FALSE, 8);
    GConfClient *client = gconf_client_get_default();
    gchar *last_man;

    last_man = gconf_client_get_string(client, CONF_LAST_PAGE, NULL);
    if (last_man != NULL)
    {
	gtk_entry_set_text(page_entry, last_man);
	g_free(last_man);
    }
    else gtk_entry_set_text(page_entry, "");

    g_signal_connect(GTK_OBJECT(page_entry), "changed",
		    G_CALLBACK(man_dialog_changed_cb), dialog);
    gtk_entry_set_activates_default(page_entry, TRUE);

    gtk_label_set_markup_with_mnemonic(GTK_LABEL(label), _("_Man page:"));
    gtk_label_set_mnemonic_widget(GTK_LABEL(label),
    	    GTK_WIDGET(page_entry));

    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(page_entry),
    	    TRUE, TRUE, 0);

    return hbox;
}

static GtkWidget *build_man_section(GtkDialog *dialog)
{
    GtkWidget *label = gtk_label_new(NULL);
    section_entry = GTK_ENTRY(gtk_entry_new());
    GtkWidget *hbox = gtk_hbox_new(FALSE, 8);
    GConfClient *client = gconf_client_get_default();
    gchar *last_section;

    last_section = gconf_client_get_string(client, CONF_LAST_SECTION, NULL);
    if (last_section != NULL)
    {
	gtk_entry_set_text(section_entry, last_section);
	g_free(last_section);
    }
    else gtk_entry_set_text(section_entry, "1");

    g_signal_connect(GTK_OBJECT(section_entry), "changed",
		    G_CALLBACK(man_dialog_changed_cb), dialog);
    gtk_entry_set_activates_default(section_entry, TRUE);

    gtk_label_set_markup_with_mnemonic(GTK_LABEL(label), _("In _section:"));
    gtk_label_set_mnemonic_widget(GTK_LABEL(label), GTK_WIDGET(section_entry));

    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(section_entry), TRUE, TRUE, 0);

    return hbox;
}

void man_cmd(void)
{
    GtkWidget *dialog = gtk_dialog_new_with_buttons(_("Man"), GTK_WINDOW(app),
	GTK_DIALOG_MODAL,
	_("_Get page"), GTK_RESPONSE_OK,
	GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	NULL);

    gtk_container_add(GTK_CONTAINER (GTK_DIALOG(dialog)->vbox),
		    build_man_page(GTK_DIALOG(dialog)));
    gtk_container_add(GTK_CONTAINER (GTK_DIALOG(dialog)->vbox),
		    build_man_section(GTK_DIALOG(dialog)));

    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);

    man_dialog_changed_cb(NULL, dialog);
    gtk_widget_show_all(dialog);

    if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK)
    {
    	GConfClient *client = gconf_client_get_default();
    	const gchar *section = gtk_entry_get_text(section_entry);
    	const gchar *page = gtk_entry_get_text(page_entry);
    	
    	const gchar *argv[256];
    	int argc = 0;
    	argv[argc++] = "man";
    	if (section && section[0])
    	    argv[argc++] = section;
    	argv[argc++] = page;
    	argv[argc] = NULL;
    	
    	gchar *title = g_strjoinv(" ", (gchar **)argv);
    	Make *man_make = make_new(argv, NULL, title, NULL, NULL);
    	g_free(title);
    	
    	man_make->started_browsing = TRUE;
	
	gconf_client_set_string(client, CONF_LAST_PAGE,
	    gtk_entry_get_text(page_entry), NULL);
	gconf_client_set_string(client, CONF_LAST_SECTION,
	    gtk_entry_get_text(section_entry), NULL);
    }

    gtk_widget_destroy(dialog);
}


