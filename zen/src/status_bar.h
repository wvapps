/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef STATUS_BAR_H
#define STATUS_BAR_H

#include <glib-2.0/glib.h>
#include <libgnomeui/gnome-app.h>

GtkWidget *status_bar_new(GtkWindow *window);

void status_bar_set(const gchar *format, ...);

void status_bar_set_line_num(gint line_num);
void status_bar_set_column_num(gint column_num);

void status_bar_show_numbers(void);
void status_bar_hide_numbers(void);

#endif
