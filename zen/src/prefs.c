/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "prefs.h"
#include "zen.h"
#include "prefs_file_type_options.h"

#include <glib/gprintf.h>
#include <gconf/gconf-client.h>

#undef DEBUG
#undef VIM_COLORS
#define CUSTOM_COLORS

GtkSourceLanguagesManager *source_languages_manager;

static gchar *fix_gconf_key(gchar *full_key)
{
    gchar *p;
    
    for (p=full_key; *p; ++p)
    {
    	if (*p == ' ' || *p == '+' || *p == '.' || *p == '#')
    	    *p = '_';
    }
    
    return full_key;
}

static const gchar *get_global_pref_key(const char *key)
{
    static gchar full_key[1024];
    
    g_sprintf(full_key, CONF_PATH("Global") "/%s", key);
    	       	    
    return fix_gconf_key(full_key);
} 

static const gchar *get_file_type_pref_key(const char *file_type,
    	const char *key)
{
    static gchar full_key[1024];
    
    g_sprintf(full_key, CONF_PATH("File Types") "/%s/%s",
    	    file_type, key);
    	    
    return fix_gconf_key(full_key);
} 

gboolean prefs_global_get_use_old_file_chooser(void)
{
    GConfClient *client = gconf_client_get_default();
        
    return gconf_client_get_bool(client,
	    get_global_pref_key("Use Old File Chooser"), NULL);
}

void prefs_global_set_use_old_file_chooser(gboolean use_old_file_chooser)
{
    GConfClient *client = gconf_client_get_default();
        
    gconf_client_set_bool(client,
	    get_global_pref_key("Use Old File Chooser"),
	    use_old_file_chooser, NULL);
}

void prefs_global_get_background_color(GdkColor *color)
{
    GConfClient *client = gconf_client_get_default();
    const gchar *str = gconf_client_get_string(client,
	    get_global_pref_key("Background Color"), NULL);
    
    if (str == NULL) str = "#A0A0A0";
    
    gdk_color_parse(str, color);
}

void prefs_global_set_background_color(const GdkColor *color)
{
    GConfClient *client = gconf_client_get_default();
    gchar str[8];
        
    g_sprintf(str, "#%02X%02X%02X", color->red >> 8,
    	    color->green >> 8, color->blue >> 8);
    	    
    gconf_client_set_string(client,
	    get_global_pref_key("Background Color"),
	    str, NULL);
}

void prefs_global_get_foreground_color(GdkColor *color)
{
    GConfClient *client = gconf_client_get_default();
    const gchar *str = gconf_client_get_string(client,
	    get_global_pref_key("Foreground Color"), NULL);
    
    if (str == NULL) str = "#000000";
    
    gdk_color_parse(str, color);
}

void prefs_global_set_foreground_color(const GdkColor *color)
{
    GConfClient *client = gconf_client_get_default();
    gchar str[8];
        
    g_sprintf(str, "#%02X%02X%02X", color->red >> 8,
    	    color->green >> 8, color->blue >> 8);
    	    
    gconf_client_set_string(client,
	    get_global_pref_key("Foreground Color"),
	    str, NULL);
}

static const gchar *prefs_get_file_type_from_mime_type(const char *mime_type)
{
    if (mime_type != NULL)
    {
	GtkSourceLanguage *source_language =
	    gtk_source_languages_manager_get_language_from_mime_type(
		    source_languages_manager, mime_type);
	
	if (source_language)
    	    return gtk_source_language_get_name(source_language);
    	else return NULL;
    }
    else return NULL;
}

const gchar *prefs_file_type_get_font_name(const gchar *file_type)
{
    static gchar *font_name = NULL;
    
    if (font_name != NULL)
    {
    	g_free(font_name);
    	font_name = NULL;
    }
        	
    if (file_type != NULL)
    {
    	GConfClient *client = gconf_client_get_default();
    
    	font_name = gconf_client_get_string(client,
	    	get_file_type_pref_key(file_type, "Font Name"), NULL);
    }
    	    
    if (font_name == NULL)
    	font_name = g_strdup("Monospace 10");
        
    return font_name;
}

const gchar *prefs_mime_type_get_font_name(const gchar *mime_type)
{
    return prefs_file_type_get_font_name(
    	    prefs_get_file_type_from_mime_type(mime_type));
}

void prefs_file_type_set_font_name(const gchar *file_type,
    	const gchar *font_name)
{
    if (file_type != NULL)
    {
    	GConfClient *client = gconf_client_get_default();
        
    	gconf_client_set_string(client,
	    	get_file_type_pref_key(file_type, "Font Name"),
	    	font_name, NULL);
    }
    else g_warning("file_type == NULL");
}

gint prefs_file_type_get_tab_size(const gchar *file_type)
{
    if (file_type != NULL)
    {
    	GConfClient *client = gconf_client_get_default();
    	gint result = gconf_client_get_int(client,
	    	get_file_type_pref_key(file_type, "Tab Size"), NULL);
    	    
    	if (result < 2 || result > 8)
    	    result = 8;
        
    	return result;
    }
    else return 8;
}

gint prefs_mime_type_get_tab_size(const gchar *mime_type)
{
    return prefs_file_type_get_tab_size(
    	    prefs_get_file_type_from_mime_type(mime_type));
}

void prefs_file_type_set_tab_size(const gchar *file_type,
    	gint tab_size)
{
    if (file_type != NULL)
    {
    	GConfClient *client = gconf_client_get_default();
        
    	gconf_client_set_int(client,
	    	get_file_type_pref_key(file_type, "Tab Size"), tab_size, NULL);
    }
    else g_warning("file_type == NULL");
}

gint prefs_file_type_get_indent_size(const gchar *file_type)
{
    if (file_type != NULL)
    {
    	GConfClient *client = gconf_client_get_default();
    	gint result = gconf_client_get_int(client,
	    	get_file_type_pref_key(file_type, "Indent Size"), NULL);
    	    
    	if (result < 2 || result > 8)
    	    result = 8;
        
    	return result;
    }
    else return 8;
}

gboolean prefs_file_type_get_use_tabs(const gchar *file_type)
{
    if (file_type != NULL)
    {
    	GConfClient *client = gconf_client_get_default();
    	gboolean result = gconf_client_get_bool(client,
	    	get_file_type_pref_key(file_type, "Use Tabs"), NULL);
    	    
    	return result;
    }
    else return FALSE;
}

gint prefs_mime_type_get_indent_size(const gchar *mime_type)
{
    return prefs_file_type_get_indent_size(
    	    prefs_get_file_type_from_mime_type(mime_type));
}

gboolean prefs_mime_type_get_use_tabs(const gchar *mime_type)
{
    return prefs_file_type_get_use_tabs(
    	    prefs_get_file_type_from_mime_type(mime_type));
}

void prefs_file_type_set_indent_size(const gchar *file_type,
    	gint indent_size)
{
    if (file_type != NULL)
    {
    	GConfClient *client = gconf_client_get_default();
        
    	gconf_client_set_int(client,
	    	get_file_type_pref_key(file_type, "Indent Size"), indent_size, NULL);
    }
    else g_warning("file_type == NULL");
}

void prefs_file_type_set_use_tabs(const gchar *file_type,
    	gboolean use_tabs)
{
    if (file_type != NULL)
    {
    	GConfClient *client = gconf_client_get_default();
        
    	gconf_client_set_bool(client,
	    	get_file_type_pref_key(file_type, "Use Tabs"), use_tabs, NULL);
    }
    else g_warning("file_type == NULL");
}

static void init_source_language(GtkSourceLanguage *source_language)
{
    const gchar *name = gtk_source_language_get_name(source_language);

#ifdef DEBUG
    {
	GSList *list = gtk_source_language_get_tags(source_language);
	g_message("Availible source tags for %s:", mime_type);
	while (list != NULL)
	{
	    GtkSourceTag *source_tag = (GtkSourceTag *)list->data;
	    gchar *property;
	    
	    g_object_get(G_OBJECT(source_tag), "id", &property, NULL);
	    
	    g_message("  %s", property);
    
	    list = g_slist_next(list);
	}
    }
#endif

    if (g_str_equal(name, "C") || g_str_equal(name, "C++"))
    {
	GSList *list = gtk_source_language_get_tags(source_language);
	while (list != NULL)
	{
	    GtkSourceTag *source_tag = (GtkSourceTag *)list->data;
	    gchar *id;
	    GtkSourceTagStyle *source_tag_style;
	    GtkSourceTagStyle *new_source_tag_style;
	    
	    g_object_get(G_OBJECT(source_tag), "id", &id, NULL);
	    
	    source_tag_style = gtk_source_language_get_tag_default_style(source_language, id);
#if defined(VIM_COLORS)
	    if (g_str_equal(id, "Keywords"))
	    {
		new_source_tag_style = gtk_source_tag_style_copy(source_tag_style);
		gdk_color_parse("#ffff63", &new_source_tag_style->foreground);
		new_source_tag_style->bold = FALSE;
		gtk_source_language_set_tag_style(source_language, id, new_source_tag_style);
	    }
	    if (g_str_equal(id, "Types"))
	    {
		new_source_tag_style = gtk_source_tag_style_copy(source_tag_style);
		gdk_color_parse("#63ff63", &new_source_tag_style->foreground);
		new_source_tag_style->bold = FALSE;
		gtk_source_language_set_tag_style(source_language, id, new_source_tag_style);
	    }
	    if (g_str_equal(id, "String") || g_str_equal(id, "Character@32@Constant")
		    || g_str_equal(id, "Decimal") || g_str_equal(id, "Floating@32@Point@32@Number")
		    || g_str_equal(id, "Hex@32@Number") || g_str_equal(id, "Octal@32@Number")
		    || g_str_equal(id, "Common@32@Macro"))
	    {
		new_source_tag_style = gtk_source_tag_style_copy(source_tag_style);
		gdk_color_parse("#ffa2a5", &new_source_tag_style->foreground);
		new_source_tag_style->bold = FALSE;
		gtk_source_language_set_tag_style(source_language, id, new_source_tag_style);
	    }
	    if (g_str_equal(id, "Preprocessor@32@Definitions") || g_str_equal(id, "Include@47@Pragma"))
	    {
		new_source_tag_style = gtk_source_tag_style_copy(source_tag_style);
		gdk_color_parse("#ff82ff", &new_source_tag_style->foreground);
		new_source_tag_style->bold = FALSE;
		gtk_source_language_set_tag_style(source_language, id, new_source_tag_style);
	    }
	    if (g_str_equal(id, "Line@32@Comment") || g_str_equal(id, "Block@32@Comment")
		    || g_str_equal(id, "@39@@35@if@32@0@39@@32@Comment"))
	    {
		new_source_tag_style = gtk_source_tag_style_copy(source_tag_style);
		gdk_color_parse("#84a2ff", &new_source_tag_style->foreground);
		new_source_tag_style->bold = FALSE;
		gtk_source_language_set_tag_style(source_language, id, new_source_tag_style);
	    }
#elif defined(CUSTOM_COLORS)
	    if (g_str_equal(id, "Keywords"))
	    {
		new_source_tag_style = gtk_source_tag_style_copy(source_tag_style);
		gdk_color_parse("#206020", &new_source_tag_style->foreground);
		new_source_tag_style->bold = FALSE;
		gtk_source_language_set_tag_style(source_language, id, new_source_tag_style);
	    }
	    if (g_str_equal(id, "Types"))
	    {
		new_source_tag_style = gtk_source_tag_style_copy(source_tag_style);
		gdk_color_parse("#2020A0", &new_source_tag_style->foreground);
		new_source_tag_style->bold = FALSE;
		gtk_source_language_set_tag_style(source_language, id, new_source_tag_style);
	    }
	    if (g_str_equal(id, "String") || g_str_equal(id, "Character@32@Constant")
		    || g_str_equal(id, "Decimal") || g_str_equal(id, "Floating@32@Point@32@Number")
		    || g_str_equal(id, "Hex@32@Number") || g_str_equal(id, "Octal@32@Number")
		    || g_str_equal(id, "Common@32@Macro"))
	    {
		new_source_tag_style = gtk_source_tag_style_copy(source_tag_style);
		gdk_color_parse("#A02020", &new_source_tag_style->foreground);
		new_source_tag_style->bold = FALSE;
		gtk_source_language_set_tag_style(source_language, id, new_source_tag_style);
	    }
	    if (g_str_equal(id, "Preprocessor@32@Definitions") || g_str_equal(id, "Include@47@Pragma"))
	    {
		new_source_tag_style = gtk_source_tag_style_copy(source_tag_style);
		gdk_color_parse("#602060", &new_source_tag_style->foreground);
		new_source_tag_style->bold = FALSE;
		gtk_source_language_set_tag_style(source_language, id, new_source_tag_style);
	    }
	    if (g_str_equal(id, "Line@32@Comment") || g_str_equal(id, "Block@32@Comment"))
	    {
		new_source_tag_style = gtk_source_tag_style_copy(source_tag_style);
		gdk_color_parse("#6c3500", &new_source_tag_style->foreground);
		new_source_tag_style->bold = FALSE;
		gtk_source_language_set_tag_style(source_language, id, new_source_tag_style);
	    }
	    if (g_str_equal(id, "@39@@35@if@32@0@39@@32@Comment"))
	    {
		new_source_tag_style = gtk_source_tag_style_copy(source_tag_style);
		gdk_color_parse("#606060", &new_source_tag_style->foreground);
		new_source_tag_style->bold = FALSE;
		gtk_source_language_set_tag_style(source_language, id, new_source_tag_style);
	    }
#endif
	
	    list = g_slist_next(list);
	}
    }
    
    if (g_str_equal(name, "Perl"))
    {
	GSList *list = gtk_source_language_get_tags(source_language);
	while (list != NULL)
	{
	    GtkSourceTag *source_tag = (GtkSourceTag *)list->data;
	    gchar *id;
	    GtkSourceTagStyle *source_tag_style;
	    GtkSourceTagStyle *new_source_tag_style;
	    
	    g_object_get(G_OBJECT(source_tag), "id", &id, NULL);
	    
	    source_tag_style = gtk_source_language_get_tag_default_style(source_language, id);
	    if (g_str_equal(id, "String") || g_str_equal(id, "String2")
		    || g_str_equal(id, "String3"))
	    {
		new_source_tag_style = gtk_source_tag_style_copy(source_tag_style);
		gdk_color_parse("#606000", &new_source_tag_style->foreground);
		new_source_tag_style->bold = FALSE;
		gtk_source_language_set_tag_style(source_language, id, new_source_tag_style);
	    }
	
	    list = g_slist_next(list);
	}
    }
}

void prefs_init(gboolean load_defaults)
{
    GConfClient *client = gconf_client_get_default();
    const GSList *list;
    
    source_languages_manager = gtk_source_languages_manager_new();
    
    gconf_client_add_dir(client, "/apps/" PACKAGE_NAME,
    	    GCONF_CLIENT_PRELOAD_RECURSIVE, NULL);
    
    list = gtk_source_languages_manager_get_available_languages(source_languages_manager);
    while (list != NULL)
    {
	GtkSourceLanguage *source_language =
	    (GtkSourceLanguage *)list->data;
	    
	init_source_language(source_language);

	list = g_slist_next(list);
    }
}

void prefs_free(void)
{
}

GtkSourceLanguage *prefs_mime_type_get_source_language(const char *mime_type)
{
    if (mime_type != NULL)
	return gtk_source_languages_manager_get_language_from_mime_type(
		source_languages_manager, mime_type);
    else return NULL;
}
