/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "prefs_global_options.h"

#include "zen.h"
#include "prefs.h"

#include <glib/gprintf.h>
#include <gconf/gconf-client.h>

static void prefs_global_options_apply_cb(PrefsOptions *_self)
{
    PrefsGlobalOptions *self = PREFS_GLOBAL_OPTIONS(_self);
    GdkColor color;
    
    prefs_global_set_use_old_file_chooser(
	    gtk_toggle_button_get_active(
	    	GTK_TOGGLE_BUTTON(self->old_file_chooser_checkbox)));

    gtk_color_button_get_color(GTK_COLOR_BUTTON(self->background_color),
    	    &color);	    	
    prefs_global_set_background_color(&color);
	    	
    gtk_color_button_get_color(GTK_COLOR_BUTTON(self->foreground_color),
    	    &color);	    	
    prefs_global_set_foreground_color(&color);
}

static void prefs_global_options_class_init(PrefsGlobalOptionsClass *class)
{
    PREFS_OPTIONS_CLASS(class)->apply = prefs_global_options_apply_cb;
}

GType prefs_global_options_get_type (void)
{
    static GType type = 0;
    
    if (!type)
    {
    	static const GTypeInfo info =
    	{
    	    sizeof(PrefsGlobalOptionsClass),
 	    NULL, /* base_init */
    	    NULL, /* base_finalize */
    	    (GClassInitFunc) prefs_global_options_class_init,
    	    NULL, /* class_finalize */
    	    NULL, /* class_data */
    	    sizeof(PrefsGlobalOptions),
    	    0,	  /* n_preallocs */
    	    NULL  /* (GInstanceInitFunc) */
    	};
        
    	type = g_type_register_static (TYPE_PREFS_OPTIONS,
	    	"PrefsGlobalOptions", &info, 0);
    }
    
    return type;
}

GtkWidget *prefs_global_options_new(void)
{
    PrefsGlobalOptions *self = g_object_new(TYPE_PREFS_GLOBAL_OPTIONS, NULL);
    int row = 0;
    GdkColor color;

    gtk_table_resize(GTK_TABLE(self), 3, 2);
    gtk_table_set_homogeneous(GTK_TABLE(self), FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(self), 4);
    gtk_table_set_col_spacings(GTK_TABLE(self), 8);
    
    self->old_file_chooser_checkbox =
    	    gtk_check_button_new_with_mnemonic(_("Use old style file chooser"));
    gtk_toggle_button_set_active(
    	    GTK_TOGGLE_BUTTON(self->old_file_chooser_checkbox),
    	    prefs_global_get_use_old_file_chooser());
    g_signal_connect_swapped(G_OBJECT(self->old_file_chooser_checkbox),
    	    "toggled", GTK_SIGNAL_FUNC(prefs_options_changed), self);
    prefs_options_add_row(PREFS_OPTIONS(self), NULL,
    	    self->old_file_chooser_checkbox, &row);
        
    prefs_global_get_background_color(&color);
    self->background_color = gtk_color_button_new_with_color(&color);
    g_signal_connect_swapped(G_OBJECT(self->background_color),
    	    "color-set", GTK_SIGNAL_FUNC(prefs_options_changed), self);
    prefs_options_add_row(PREFS_OPTIONS(self), _("Background color:"),
    	    self->background_color, &row);
    
    prefs_global_get_foreground_color(&color);
    self->foreground_color = gtk_color_button_new_with_color(&color);
    g_signal_connect_swapped(G_OBJECT(self->foreground_color),
    	    "color-set", GTK_SIGNAL_FUNC(prefs_options_changed), self);
    prefs_options_add_row(PREFS_OPTIONS(self), _("Foreground color:"),
    	    self->foreground_color, &row);
    
    return GTK_WIDGET(self);
}
