/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "cmd.h"

#include "zen.h"
#include "buffer.h"
#include "prefs_dialog.h"
#include "file_list.h"
#include "status_bar.h"
#include "views.h"
#include "find.h"
#include "make.h"
#include "prefs.h"
#include "make_cmd.h"
#include "man_cmd.h"
#include "find_in_files_cmd.h"

#include <string.h>
#include <stdlib.h>
#include <gdk-pixbuf/gdk-pixdata.h>
#include <gconf/gconf-client.h>

extern const GdkPixdata zen_logo;

void cmd_cut(void)
{
    Buffer *buffer = get_cur_buffer();
    
    if (buffer != NULL)
    {
    	GtkClipboard *clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
    
    	gtk_text_buffer_cut_clipboard(GTK_TEXT_BUFFER(buffer), clipboard, TRUE);
    }
    else g_warning("cmd_cut: buffer == NULL");
}

void cmd_copy(void)
{
    Buffer *buffer = get_cur_buffer();
    
    if (buffer != NULL)
    {
    	GtkClipboard *clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
    
    	gtk_text_buffer_copy_clipboard(GTK_TEXT_BUFFER(buffer), clipboard);
    }
    else g_warning("cmd_copy: buffer == NULL");
}

void cmd_paste(void)
{
    Buffer *buffer = get_cur_buffer();
    
    if (buffer != NULL)
    {
    	GtkClipboard *clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
    
    	gtk_text_buffer_paste_clipboard(GTK_TEXT_BUFFER(buffer),
	    	clipboard, NULL, TRUE);
    }
    else g_warning("cmd_paste: buffer == NULL");
}

void cmd_redo(void)
{
    Buffer *buffer = get_cur_buffer();
    
    if (buffer != NULL)
    {
    	gtk_source_buffer_redo(GTK_SOURCE_BUFFER(buffer));
    }
    else g_warning("cmd_redo: buffer == NULL");
}

void cmd_undo(void)
{
    Buffer *buffer = get_cur_buffer();
    
    if (buffer != NULL)
    {
    	gtk_source_buffer_undo(GTK_SOURCE_BUFFER(buffer));
    }
    else g_warning("cmd_undo: buffer == NULL");
}

void cmd_select_all(void)
{
    Buffer *buffer = get_cur_buffer();

    if (buffer != NULL)
    {
        Iter start, end;

        gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(buffer), &start, &end);
    
        buffer_select(buffer, &start, &end);
    }
    else g_warning("cmd_select_all: buffer == NULL");
}

void cmd_about_dlg(void)
{
    GtkWidget *dialog = gtk_dialog_new_with_buttons(_("About " FRIENDLY_NAME),
	    GTK_WINDOW(app), GTK_DIALOG_MODAL,
	    GTK_STOCK_OK, GTK_RESPONSE_ACCEPT, NULL);
    GtkWidget *hbox, *vbox;
    GError *error = NULL;
    GdkPixbuf *pixbuf = gdk_pixbuf_from_pixdata(&zen_logo, FALSE, &error);
    
    hbox = gtk_hbox_new(FALSE, 8);
    gtk_box_pack_start(GTK_BOX(hbox),
    	    gtk_image_new_from_pixbuf(pixbuf), FALSE, FALSE, 4);

    vbox = gtk_vbox_new(FALSE, 4);
    gtk_box_pack_start(GTK_BOX(vbox),
    	    gtk_label_new(_(FRIENDLY_NAME " version " VERSION_NAME)), TRUE, TRUE, 4);
    gtk_box_pack_start(GTK_BOX(vbox),
    	    gtk_label_new(AUTHORS), TRUE, TRUE, 4);
    gtk_box_pack_start(GTK_BOX(vbox),
    	    gtk_label_new(COPYRIGHT), TRUE, TRUE, 4);
    gtk_box_pack_start(GTK_BOX(vbox),
    	    gtk_label_new(LICENSE), TRUE, TRUE, 4);
    gtk_box_pack_start(GTK_BOX(hbox),
    	    vbox, FALSE, FALSE, 4);
    
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox),
    	    hbox, TRUE, TRUE, 4);

    gtk_widget_show_all(dialog);
    
    gtk_dialog_run(GTK_DIALOG(dialog));
    
    gtk_widget_destroy(dialog);
}

void cmd_preferences_dlg(void)
{
    GtkWidget *dialog = prefs_dialog_new();
    
//    g_signal_connect(GTK_OBJECT(dialog), "changed",
//	    G_CALLBACK(preferences_changed_cb), NULL);
    
    gtk_widget_show_all(dialog);
}

static gchar *get_cur_buffer_dirname(void)
{
    Buffer *buffer = get_cur_buffer();
    gchar *dirname_with_slash = NULL;
    
    if (buffer != NULL)
    {
	const gchar *filename = buffer_get_filename(buffer);
	if (filename != NULL)
	{
	    gchar *dirname = g_path_get_dirname(filename);
	    dirname_with_slash = g_strconcat(dirname, "/", NULL);
	    g_free(dirname);
    
    	    return dirname_with_slash;
	}
	else return NULL;
    }
    else return NULL;
}

static gboolean close_buffer(Buffer *buffer)
{
    gboolean really_close = FALSE;

    if (buffer_get_is_temp(buffer))
	really_close = TRUE;
    else if (buffer_get_modified(buffer))
    {
	GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(app),
		0, GTK_MESSAGE_QUESTION,
		GTK_BUTTONS_NONE,
		_("Save changes to %s?"),
		buffer_get_desc(buffer));
	gint result;

	gtk_dialog_add_buttons(GTK_DIALOG(dialog),
		GTK_STOCK_YES, GTK_RESPONSE_YES,
		GTK_STOCK_NO, GTK_RESPONSE_NO,
		GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
		NULL);

	result = gtk_dialog_run(GTK_DIALOG(dialog));
	really_close = result != GTK_RESPONSE_CANCEL;
	if (result == GTK_RESPONSE_YES)
	{
	    GError *error = NULL;
	    if (buffer_save(buffer, &error))
		really_close = TRUE;
	    else
		consume_error(error);
	}

	gtk_widget_destroy(dialog);
    }
    else really_close = TRUE;

    if (really_close)
    {
	buffer_free(buffer);
	
	if (views_get_top_view() == NULL && views_get_bottom_view() == NULL)
	{
	    Buffer *last_focused_buffer =
	    	    file_list_get_last_focused_buffer(FILE_LIST(file_list));
	    
	    if (last_focused_buffer != NULL)
	    	view_focus(views_set_one_view(last_focused_buffer));
	}
    }

    return really_close;
}

void cmd_close(void)
{
    Buffer *buffer = get_cur_buffer();
    if (buffer != NULL)
    {
    	const gchar *desc = buffer_get_filename(buffer);
    	if (!desc)
    	    desc = buffer_get_desc(buffer);
    	if (!desc)
    	    desc = "buffer";
	status_bar_set("Closed %s", desc);
	close_buffer(buffer);
    }
    else g_warning("cmd_close: buffer == NULL");
}

void cmd_close_all(void)
{
    FileListIter iter;
    gboolean really = TRUE;

    file_list_iter_start(FILE_LIST(file_list), &iter);
    while (!file_list_iter_done(&iter))
     {
	Buffer *buffer = file_list_iter_get_buffer(&iter);
	if (!close_buffer(buffer))
	{
	    really = FALSE;
	    break;
	}

	// We get the first again here since we removed something from the
	// file_list
    	file_list_iter_start(FILE_LIST(file_list), &iter);
    }

    if (really)
	status_bar_set(_("Closed all open files."));
    else
	status_bar_set(_("Cancelled!"));
}

Buffer *open_file(const gchar *filename, gboolean request_create)
{
    GnomeVFSFileInfo file_info;
    Buffer *buffer = buffer_new_with_filename(filename);
    gboolean (*buffer_load_func)(Buffer *, GError **) = buffer_load;
    filename = buffer_get_filename(buffer);
    if (buffer_have_newer_autosave(buffer))
    {
    	gchar *autosave_filename = buffer_get_autosave_filename(buffer);
    	GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(app),
	    	0, GTK_MESSAGE_QUESTION,
	    	GTK_BUTTONS_YES_NO,
	    	_("Autosave file is newer than %s; load autosave file?"),
	    	filename);
    	
    	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
    	    buffer_load_func = buffer_autoload;
    	
    	gtk_widget_destroy(dialog);
    	
    	g_free(autosave_filename);
    }
    else if (gnome_vfs_get_file_info(filename, &file_info,
    	    GNOME_VFS_FILE_INFO_FOLLOW_LINKS) != GNOME_VFS_OK)
    {
    	if (request_create)
    	{
    	    GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(app),
	    	    0, GTK_MESSAGE_QUESTION,
	    	    GTK_BUTTONS_YES_NO,
	    	    _("File %s not found; create?"),
	    	    filename);
        	
    	    if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
    	    	buffer_set_modified(buffer, FALSE);
    	    else buffer_free(buffer);
        	
    	    gtk_widget_destroy(dialog);
        	
    	    return buffer;
    	}
    	else
    	{
    	    buffer_free(buffer);
    	    return NULL;
    	}
    }
    
    const gchar *codesets[] = {
    	"UTF8", NULL, "ISO-8859-1", NULL
    };
    gint i;
    GError *error = NULL;
        
    g_get_charset(&codesets[1]);

    for (i=0; codesets[i] != NULL; ++i)
    {
        const gchar *codeset = codesets[i]; 
    	buffer_set_codeset(buffer, codeset);

    	if (buffer_load_func(buffer, &error))
    	{
    	    status_bar_set(_("Opened %s (codeset %s)"),
    	    		    buffer_get_filename(buffer), codeset);
    	    break;
    	}
    	else if (error && error->domain == G_CONVERT_ERROR)
    	{
    	    g_error_free(error);
	    error = NULL;
    	} 
    	else
    	{
    	    consume_error(error);
	    error = NULL;
    	    buffer_free(buffer);
    	    buffer = NULL;
    	}
    }
    	
    if (codesets[i] == NULL)
    {
    	GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(app),
	    	0, GTK_MESSAGE_ERROR,
	    	GTK_BUTTONS_OK,
	    	"Unable to open file %s: failed to determine codeset",
	    	filename);
        	
    	gtk_dialog_run(GTK_DIALOG(dialog));
        	
    	gtk_widget_destroy(dialog);

    	buffer_free(buffer);
    	buffer = NULL;
    }
    
    return buffer;
}

static void old_cmd_open_dlg(void)
{
    GtkFileSelection *file_selection =
	    GTK_FILE_SELECTION(gtk_file_selection_new(_("Open File...")));
    gchar *dirname_with_slash = get_cur_buffer_dirname();
    Buffer *last_buffer = NULL;
#if 0
    GtkWidget *codeset_list;
    GtkWidget *codeset_list_hbox;
#endif    
    
    if (dirname_with_slash != NULL)
	gtk_file_selection_set_filename(file_selection, dirname_with_slash);
    
    gtk_file_selection_set_select_multiple(file_selection, TRUE);
    
#if 0    
    codeset_list = gtk_combo_box_new_text();
    gtk_combo_box_append_text(GTK_COMBO_BOX(codeset_list), "UTF-8");
    if (g_str_equal(local_codeset, "utf8"))
        gtk_combo_box_set_active(GTK_COMBO_BOX(codeset_list), 0);
    gtk_combo_box_append_text(GTK_COMBO_BOX(codeset_list), "ISO-8859-1");
    if (g_str_equal(local_codeset, "ISO-8859-1"))
        gtk_combo_box_set_active(GTK_COMBO_BOX(codeset_list), 1);

    codeset_list_hbox = gtk_hbox_new(FALSE, 6);
    gtk_box_pack_start(GTK_BOX(codeset_list_hbox),
    	    gtk_label_new(_("Character set:")), FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(codeset_list_hbox), codeset_list,
    	    FALSE, FALSE, 0);
    gtk_widget_show_all(codeset_list_hbox);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(file_selection)->vbox),
    	    codeset_list_hbox, FALSE, FALSE, 0);
#endif

    for (;;)
    {
	gchar **filenames;
	int i, files_opened = 0;
	gint response = gtk_dialog_run(GTK_DIALOG(file_selection));
	
	if (response == GTK_RESPONSE_CANCEL
	    	|| response == GTK_RESPONSE_DELETE_EVENT)
	    break;

	filenames = gtk_file_selection_get_selections(file_selection);
	for (i=0; filenames[i] != NULL; ++i)
	{
	    GnomeVFSFileInfo file_info;
	    const gchar *filename = filenames[i];

    	    if (gnome_vfs_error(NULL,
	    	    gnome_vfs_get_file_info(filename, &file_info,
	    	    	GNOME_VFS_FILE_INFO_FOLLOW_LINKS),
	    	    filename))
	    	continue;
	    
	    if (file_info.type == GNOME_VFS_FILE_TYPE_DIRECTORY)
	    {
		g_free(dirname_with_slash);
		
		if (filename[strlen(filename)-1] == '/')
		    dirname_with_slash = g_strconcat(filename, NULL);
		else dirname_with_slash = g_strconcat(filename, "/", NULL);
		
		gtk_file_selection_set_filename(file_selection, dirname_with_slash);
	    }
	    else
	    {
	    	FileListIter iter;
	    	
	    	for (file_list_iter_start(FILE_LIST(file_list), &iter);
	    	    	!file_list_iter_done(&iter);
		    	file_list_iter_next(&iter))
	    	{
		    Buffer *buffer = file_list_iter_get_buffer(&iter);
		    const gchar *this_filename = buffer_get_filename(buffer);
		    if (this_filename != NULL && g_str_equal(this_filename, filename))
		    	break;
	    	}
	    	
	    	if (file_list_iter_done(&iter))
	    	{
	    	    Buffer *new_buffer = open_file(filename, TRUE);
		    if (new_buffer != NULL)
		    {
		    	last_buffer = new_buffer;
		    	++files_opened;
		    }
		}
	    	else
	    	{
		    last_buffer = file_list_iter_get_buffer(&iter);

		    status_bar_set(_("File %s already open"), filename);

		    ++files_opened;
	    	}
	    }
	}
	g_strfreev(filenames);
	
	if (files_opened > 0)
	    break;
    }
    gtk_widget_destroy(GTK_WIDGET(file_selection));
    
    if (last_buffer != NULL)
	view_focus(views_set_one_view(last_buffer));

    g_free(dirname_with_slash);
}

static void new_cmd_open_dlg(void)
{
    GtkWidget *dialog = gtk_file_chooser_dialog_new(_("Open File..."),
    	    GTK_WINDOW(app),
    	    GTK_FILE_CHOOSER_ACTION_OPEN,
	    GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	    GTK_STOCK_OPEN, GTK_RESPONSE_OK,
	    NULL);
    gchar *dirname_with_slash = get_cur_buffer_dirname();
    Buffer *last_buffer = NULL;
#if 0
    GtkWidget *codeset_list;
    GtkWidget *codeset_list_hbox;
#endif
         
    if (dirname_with_slash != NULL)
	gtk_file_chooser_set_current_folder_uri(GTK_FILE_CHOOSER(dialog), dirname_with_slash);
    gtk_file_chooser_set_local_only(GTK_FILE_CHOOSER(dialog), FALSE);
    
    gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(dialog), TRUE);
    
#if 0   
    codeset_list = gtk_combo_box_new_text();
    gtk_combo_box_append_text(GTK_COMBO_BOX(codeset_list), "UTF-8");
    if (g_str_equal(local_codeset, "utf8"))
        gtk_combo_box_set_active(GTK_COMBO_BOX(codeset_list), 0);
    gtk_combo_box_append_text(GTK_COMBO_BOX(codeset_list), "ISO-8859-1");
    if (g_str_equal(local_codeset, "ISO-8859-1"))
        gtk_combo_box_set_active(GTK_COMBO_BOX(codeset_list), 1);
    
    codeset_list_hbox = gtk_hbox_new(FALSE, 6);
    gtk_box_pack_start(GTK_BOX(codeset_list_hbox),
    	    gtk_label_new(_("Character set:")), FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(codeset_list_hbox), codeset_list,
    	    FALSE, FALSE, 0);
    gtk_widget_show_all(codeset_list_hbox);
    gtk_file_chooser_set_extra_widget(GTK_FILE_CHOOSER(dialog),
    	    codeset_list_hbox);
#endif

    for (;;)
    {
	GSList *filenames;
	GSList *p;
	int files_opened = 0;
        gint response = gtk_dialog_run(GTK_DIALOG(dialog));
        
        if (response == GTK_RESPONSE_DELETE_EVENT
            	|| response == GTK_RESPONSE_CANCEL)
            break;

	filenames = gtk_file_chooser_get_filenames(GTK_FILE_CHOOSER(dialog));
	for (p=filenames; p != NULL; p = p->next)
	{
	    const gchar *filename = (const gchar *)p->data;
	    GnomeVFSFileInfo file_info;

    	    if (gnome_vfs_error(NULL,
	    	    gnome_vfs_get_file_info(filename, &file_info,
	    	    	GNOME_VFS_FILE_INFO_FOLLOW_LINKS),
	    	    filename))
	    	continue;
	    
	    if (file_info.type == GNOME_VFS_FILE_TYPE_DIRECTORY)
	    {
		g_free(dirname_with_slash);
		
		if (filename[strlen(filename)-1] == '/')
		dirname_with_slash = g_strconcat(filename, NULL);
		else dirname_with_slash = g_strconcat(filename, "/", NULL);
		
		gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(dialog), dirname_with_slash);
	    }
	    else
	    {
	    	FileListIter iter;
	    	
	    	for (file_list_iter_start(FILE_LIST(file_list), &iter);
	    	    	!file_list_iter_done(&iter);
		    	file_list_iter_next(&iter))
	    	{
		    Buffer *buffer = file_list_iter_get_buffer(&iter);
		    const gchar *this_filename = buffer_get_filename(buffer);
		    if (this_filename != NULL && g_str_equal(this_filename, filename))
		    	break;
	    	}
	    	
	    	if (file_list_iter_done(&iter))
	    	{
	    	    Buffer *buffer_new = open_file(filename, TRUE);
		    if (buffer_new != NULL)
		    {
		    	last_buffer = buffer_new;
		    	++files_opened;
		    }
		}
	    	else
	    	{
		    last_buffer = file_list_iter_get_buffer(&iter);

		    status_bar_set(_("File %s already open"), filename);

		    ++files_opened;
	    	}
	    }
	    
	    g_free(p->data);
	}
	g_slist_free(filenames);
	
	if (files_opened > 0)
	    break;
    }
    gtk_widget_destroy(dialog);
    
    if (last_buffer != NULL)
	view_focus(views_set_one_view(last_buffer));

    g_free(dirname_with_slash);
}

void cmd_open_dlg(void)
{
    if (prefs_global_get_use_old_file_chooser())
    	old_cmd_open_dlg();
    else
    	new_cmd_open_dlg();
}

void cmd_save_as_dlg(void)
{
    Buffer *buffer = get_cur_buffer();
    GtkWidget *file_selector;
    if (prefs_global_get_use_old_file_chooser())
    	file_selector = gtk_file_selection_new(_("Save As..."));
    else
    	file_selector = gtk_file_chooser_dialog_new(_("Save As..."),
    	    	GTK_WINDOW(app), GTK_FILE_CHOOSER_ACTION_SAVE,
		GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
		GTK_STOCK_SAVE, GTK_RESPONSE_OK,
		NULL);
    const gchar *codeset = buffer_get_codeset(buffer);
#if 0
    GtkWidget *codeset_list;
    GtkWidget *codeset_list_hbox;
#endif

    const gchar *filename;
    if (buffer_get_filename(buffer) != NULL)
    	filename = buffer_get_filename(buffer);
    else
    	filename = buffer_get_last_dirname();
    
    if (filename != NULL)
    {
    	if (prefs_global_get_use_old_file_chooser())
    	    gtk_file_selection_set_filename(GTK_FILE_SELECTION(file_selector),
		    filename);
    	else
    	    gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(file_selector),
		    filename);
    }
		
#if 0
    codeset_list = gtk_combo_box_new_text();
    gtk_combo_box_append_text(GTK_COMBO_BOX(codeset_list), "UTF-8");	
    gtk_combo_box_append_text(GTK_COMBO_BOX(codeset_list), "ISO-8859-1");
   
    if (g_str_equal(codeset, "ISO-8859-1"))
    	gtk_combo_box_set_active(GTK_COMBO_BOX(codeset_list), 1);
    else gtk_combo_box_set_active(GTK_COMBO_BOX(codeset_list), 0);
    
    codeset_list_hbox = gtk_hbox_new(FALSE, 6);
    gtk_box_pack_start(GTK_BOX(codeset_list_hbox),
    	    gtk_label_new(_("Character set:")), FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(codeset_list_hbox), codeset_list,
    	    FALSE, FALSE, 0);
    gtk_widget_show_all(codeset_list_hbox);
    gtk_file_chooser_set_extra_widget(GTK_FILE_CHOOSER(file_selector),
    	    codeset_list_hbox);
#endif

    while (gtk_dialog_run(GTK_DIALOG(file_selector)) == GTK_RESPONSE_OK)
    {
	const gchar *filename;
	if (prefs_global_get_use_old_file_chooser())
    	    filename = gtk_file_selection_get_filename(GTK_FILE_SELECTION(file_selector));
    	else
    	    filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_selector));
	if (filename != NULL)
	{
	    gboolean save = FALSE;
	    GnomeVFSFileInfo file_info;

    	    if (!gnome_vfs_error(NULL,
	    	    gnome_vfs_get_file_info(filename, &file_info,
	    	    	GNOME_VFS_FILE_INFO_FOLLOW_LINKS),
	    	    filename))
	    {
		GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(file_selector),
		    0, GTK_MESSAGE_WARNING, GTK_BUTTONS_YES_NO,
		    _("The file %s already exists; replace?"),
		    filename);
		
		save = gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES;
		
		gtk_widget_destroy(dialog);
	    }
	    else save = TRUE;
	    
	    if (save)
	    {
		GError *error = NULL;
		
		buffer_set_filename(buffer, filename);
		
#if 0
	    	switch (gtk_combo_box_get_active(GTK_COMBO_BOX(codeset_list)))
	    	{
	    	    case 0:
	    	    default:
	    	    	codeset = "utf8";
	    	    	break;
	    	    	
	    	    case 1:
	    	    	codeset = "ISO-8859-1";
	    	    	break;
	    	}
	    	buffer_set_codeset(buffer, codeset);
#endif
	    	    
		if (buffer_save(buffer, &error))
		{
		    status_bar_set(_("Saved %s"), filename);
		    break;
		}
		else consume_error(error);
	    }
	}
	else g_warning("save_as_cb: filename == NULL");
    }
    
    gtk_widget_destroy(file_selector);
}

void cmd_save(void)
{
    Buffer *buffer = get_cur_buffer();
    
    if (buffer)
    {
    	GError *error = NULL;
	const gchar *filename = buffer_get_filename(buffer);
	if (filename != NULL)
	{
	    if (buffer_save(buffer, &error))
		status_bar_set(_("Saved %s"), buffer_get_filename(buffer));
	    else consume_error(error);
	}
	else cmd_save_as_dlg();
    }
    else g_warning("cmd_save: buffer == NULL");
}

void cmd_join_lines(void)
{
    Buffer *buffer = get_cur_buffer();
    if (buffer != NULL)
	buffer_join_lines(buffer);
    else g_warning("join_lines_cb: buffer == NULL"); 
}

void cmd_delete_lines(void)
{
    Buffer *buffer = get_cur_buffer();
    if (buffer != NULL)
	buffer_delete_lines(buffer);
    else g_warning("join_lines_cb: buffer == NULL"); 
}

void cmd_split_line(void)
{
    Buffer *buffer = get_cur_buffer();
    if (buffer != NULL)
    {
	buffer_split_lines(buffer);
    }
    else g_warning("split_lines_cb: buffer == NULL"); 
}

void cmd_save_all(void)
{
    FileListIter iter;
    GError *error = NULL;

    for (file_list_iter_start(FILE_LIST(file_list), &iter);
    	    !file_list_iter_done(&iter);
    	    file_list_iter_next(&iter))
    {
	Buffer *buffer = file_list_iter_get_buffer(&iter);

	if (!buffer_save(buffer, &error))
	    break;
    }

    if (error == NULL)
	status_bar_set(_("Saved all open files."));
    else consume_error(error);
}

void cmd_revert(void)
{
    Buffer *buffer = get_cur_buffer();
    if (buffer != NULL)
    {
	gboolean really = FALSE;

	if (buffer_get_modified(buffer))
	{
	    GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(app),
					0, GTK_MESSAGE_QUESTION,
					GTK_BUTTONS_NONE,
					_("Revert %s?  All changes will be lost!"),
					buffer_get_filename(buffer));

	    gtk_dialog_add_buttons(GTK_DIALOG(dialog),
		    GTK_STOCK_YES, GTK_RESPONSE_YES,
		    GTK_STOCK_NO, GTK_RESPONSE_CANCEL,
		    NULL);

	    really = gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES;

	    gtk_widget_destroy(dialog);
	}
	else g_warning("revert_cb: modified == FALSE");

	if (really)
	{
	    GError *error = NULL;
	    if (buffer_revert(buffer, &error))
		status_bar_set(_("Reverted %s"), buffer_get_filename(buffer));
	    else consume_error(error);
	}
    }
    else g_warning("revert_cb: get_cur_buffer() == NULL");
}

void cmd_find_dlg(void)
{
    GtkWidget *dialog = find_dialog_new(GTK_WINDOW(app));

    gtk_widget_show_all(dialog);
}

void cmd_find_next(void)
{
    find_next();
}

void cmd_find_previous(void)
{
    find_previous();
}

void cmd_replace_dlg(void)
{
    GtkWidget *dialog = replace_dialog_new(GTK_WINDOW(app));

    gtk_widget_show_all(dialog);
}

void cmd_replace_next(void)
{
    replace_next();
}

void cmd_new(void)
{
    view_focus(views_set_one_view(buffer_new()));
}

static unsigned long line_number;

static void goto_line_number_changed_cb(GtkWidget *entry, gpointer _dialog)
{
    GtkDialog *dialog = (GtkDialog *)_dialog;
    const gchar *text = gtk_entry_get_text(GTK_ENTRY(entry));
    char *bad_char_pos;
    line_number = strtoul(text, &bad_char_pos, 10) - 1;
    gboolean is_valid = *text && !*bad_char_pos;

    gtk_dialog_set_response_sensitive(dialog, GTK_RESPONSE_OK, is_valid);
}

static GtkWidget *build_goto_line_number(GtkDialog *dialog)
{
    GtkWidget *label = gtk_label_new(NULL);
    GtkWidget *entry = gtk_entry_new();
    GtkWidget *hbox = gtk_hbox_new(FALSE, 8);

    gtk_entry_set_activates_default(GTK_ENTRY(entry), TRUE);
    g_signal_connect(GTK_OBJECT(entry), "changed",
		    G_CALLBACK(goto_line_number_changed_cb), dialog);

    gtk_label_set_markup_with_mnemonic(GTK_LABEL(label), _("_Line number:"));
    gtk_label_set_mnemonic_widget(GTK_LABEL(label), GTK_WIDGET(entry));

    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(entry), TRUE, TRUE, 0);

    return hbox;
}

void cmd_goto_line_dlg(void)
{
    GtkWidget *dialog = gtk_dialog_new_with_buttons(_("Goto"),
    	GTK_WINDOW(app),
	GTK_DIALOG_MODAL,
	GTK_STOCK_OK, GTK_RESPONSE_OK,
	GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	NULL);

    gtk_container_add (GTK_CONTAINER (GTK_DIALOG(dialog)->vbox),
		    build_goto_line_number(GTK_DIALOG(dialog)));

    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);

    gtk_dialog_set_response_sensitive(GTK_DIALOG(dialog), GTK_RESPONSE_OK,
		    FALSE);
    gtk_widget_show_all(dialog);

    if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK)
	view_goto_line_number(get_cur_view(), (gint)line_number);

    gtk_widget_destroy(dialog);
}

void cmd_man_dlg(void)
{
    man_cmd();
}

void cmd_make_dlg(void)
{
    make_cmd();
}

void cmd_find_in_files_dlg(void)
{
    find_in_files_cmd();
}

void cmd_next_error(void)
{
    if (make != NULL)
    	make_next_error_cmd(make);
    else g_warning("cmd_next_error: no target!");
}

void cmd_previous_error(void)
{
    if (make != NULL)
    	make_previous_error_cmd(make);
    else g_warning("cmd_previous_error: no target!");
}

void cmd_find_next_in_files(void)
{
    if (find_in_files != NULL)
    	make_next_error_cmd(find_in_files);
    else g_warning("cmd_find_next_in_files: no target!");
}

void cmd_find_previous_in_files(void)
{
    if (find_in_files != NULL)
    	make_previous_error_cmd(find_in_files);
    else g_warning("cmd_find_previous_in_files: no target!");
}

void cmd_maximize_view(void)
{
    Buffer *buffer = get_cur_buffer();
    
    if (buffer != NULL)
	view_focus(views_set_one_view(buffer));
    else g_warning("cmd_maximize_view: buffer == NULL");
}

void cmd_split_view(void)
{
    Buffer *buffer = get_cur_buffer();
    if (buffer != NULL)
    {
    	view_focus(views_set_split_view(buffer));
    }
    else g_warning("cmd_split_view: buffer == NULL");
}

extern void save_app_conf(GtkWindow *app);
extern void save_paned_conf(GtkPaned *paned);

gboolean is_exiting = FALSE;

gboolean cmd_quit(void)
{
    gboolean really_quit = TRUE;
    FileListIter iter;
 
    is_exiting = TRUE;
    
    // We get the first again here since we removed something from the
    // file_list
    for (file_list_iter_start(FILE_LIST(file_list), &iter);
    	    !file_list_iter_done(&iter);
    	    file_list_iter_start(FILE_LIST(file_list), &iter))
    {
	Buffer *buffer = file_list_iter_get_buffer(&iter);
	
	if (!close_buffer(buffer))
	{
	    really_quit = FALSE;
	    break;
	}
    }

    is_exiting = really_quit;

    if (really_quit)
    {
    	save_app_conf(GTK_WINDOW(app));
    	save_paned_conf(GTK_PANED(paned));
    	
	gtk_main_quit();
	
	return TRUE;
    }
    else return FALSE;
}

void cmd_save_all_and_quit(void)
{
    cmd_save_all();
    cmd_quit();
}

