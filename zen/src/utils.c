/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "utils.h"

#include <limits.h>
#include <stdlib.h>

gchar *normalize_filename(const gchar *filename, const gchar *cwd)
{
    char *new_filename;
    gchar resolved_path[PATH_MAX];
	    
    if (filename[0] != '/')
	new_filename = g_strconcat(cwd, "/", filename, NULL);
    else new_filename = g_strdup(filename);
	    
    if (realpath(new_filename, resolved_path) != NULL)
    {
    	g_free(new_filename);
    	new_filename = g_strdup(resolved_path);
    }
    	    
    return new_filename;
}
