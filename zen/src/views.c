/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "views.h"
#include "file_list.h"
#include "view.h"

static GtkPaned *views_paned;
static GtkBox *boxes[2];
static Buffer *buffers[2] = {NULL, NULL};
static gint active_view_ind = 0;

GtkWidget *views_new(void)
{
    views_paned = GTK_PANED(gtk_vpaned_new());

    gtk_widget_show(GTK_WIDGET(views_paned));
    
    boxes[0] = GTK_BOX(gtk_vbox_new(TRUE, 0));
    gtk_paned_pack1(views_paned, GTK_WIDGET(boxes[0]), TRUE, TRUE);
    boxes[1] = GTK_BOX(gtk_vbox_new(TRUE, 0));
    gtk_paned_pack2(views_paned, GTK_WIDGET(boxes[1]), TRUE, TRUE);

    return GTK_WIDGET(views_paned);
}

void views_add_buffer(Buffer *buffer)
{
    int i;
    
    g_assert(buffer != NULL);

    for (i=0; i<2; ++i)
    {
    	View *view = buffer_get_view(buffer, i);
    	GtkWidget *containee = view_get_containee(view);
    	gtk_box_pack_start_defaults(boxes[i], containee);
    }
}

void views_remove_buffer(Buffer *buffer)
{
    int i;
    
    g_assert(buffer != NULL);
    
    for (i=0; i<2; ++i)
    {
    	View *view = buffer_get_view(buffer, i);
    	GtkWidget *containee = view_get_containee(view);
    	gtk_container_remove(GTK_CONTAINER(boxes[i]), containee);
    	
    	if (buffers[i] == buffer)
    	{
    	    buffers[i] = NULL;
    	    gtk_widget_hide(GTK_WIDGET(boxes[i]));
    	}
    }
}

static void hide_with_data(GtkWidget *widget, gpointer user_data)
{
    gtk_widget_hide_all(widget);
}

View *views_set_view(int i, Buffer *buffer)
{
    View *view = NULL;
    
    g_assert(i >= 0 && i < 2);
    
    if (buffers[i] == buffer)
    {
    	if (buffer)
    	    view = buffer_get_view(buffer, i);
    }
    else
    {
    	gtk_container_foreach(GTK_CONTAINER(boxes[i]), hide_with_data, NULL);
        
    	if (buffer != NULL)
    	{
    	    view = buffer_get_view(buffer, i);
    	    //view_scroll_next_resize(view);
    	    GtkWidget *containee = view_get_containee(view);
    	    gtk_widget_show_all(containee);
    	    active_view_ind = i;
    	}
    	else active_view_ind = 1-i;
        	
    	buffers[i] = buffer;
        
    	if (buffer != NULL)
            gtk_widget_show(GTK_WIDGET(boxes[i]));
    	else gtk_widget_hide(GTK_WIDGET(boxes[i]));
    }
    
    return view;
}

View *views_set_top_view(Buffer *buffer)
{
    return views_set_view(0, buffer);
}

View *views_set_bottom_view(Buffer *buffer)
{
    return views_set_view(1, buffer);
}

View *views_set_one_view(Buffer *buffer)
{
    gint old_active_view_ind = active_view_ind;
    views_set_view(1-old_active_view_ind, NULL);
    return views_set_view(old_active_view_ind, buffer);
}

View *views_set_split_view(Buffer *buffer)
{
    if (buffers[0] == NULL)
    	return views_set_view(0, buffer);
    else
    	return views_set_view(1, buffer);
}

Buffer *views_get_buffer(int i)
{
    g_assert(i >= 0 && i < 2);
    
    return buffers[i];
}

Buffer *views_get_top_buffer(void)
{
    return buffers[0];
}

Buffer *views_get_bottom_buffer(void)
{
    return buffers[1];
}

View *views_get_view(int i)
{
    g_assert(i >= 0 && i < 2);
    
    return buffers[i] != NULL? buffer_get_view(buffers[i], i): NULL;
}

View *views_get_top_view(void)
{
    return views_get_view(0);
}

View *views_get_bottom_view(void)
{
    return views_get_view(1);
}
