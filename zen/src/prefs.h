/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef PREFS_H
#define PREFS_H
#include <glib.h>
G_BEGIN_DECLS

#include <gtk/gtk.h>
#include <gtksourceview/gtksourcelanguagesmanager.h>
#include <gtksourceview/gtksourcelanguage.h>
#include <gtksourceview/gtksourcetag.h>

extern GtkSourceLanguagesManager *source_languages_manager;

void prefs_init(gboolean load_defaults);
void prefs_free(void);

GtkSourceLanguage *prefs_mime_type_get_source_language(const char *mime_type);

const gchar *prefs_file_type_get_font_name(const gchar *file_type);
const gchar *prefs_mime_type_get_font_name(const gchar *mime_type);

gboolean prefs_global_get_use_old_file_chooser(void);
void prefs_global_set_use_old_file_chooser(gboolean use_old_file_chooser);
void prefs_global_get_background_color(GdkColor *color);
void prefs_global_set_background_color(const GdkColor *color);
void prefs_global_get_foreground_color(GdkColor *color);
void prefs_global_set_foreground_color(const GdkColor *color);

void prefs_file_type_set_font_name(const gchar *file_type,
    	const char *font_name);

gint prefs_file_type_get_tab_size(const gchar *file_type);
gint prefs_mime_type_get_tab_size(const gchar *mime_type);

void prefs_file_type_set_tab_size(const gchar *file_type, gint tab_size);

gint prefs_file_type_get_indent_size(const gchar *file_type);
gint prefs_mime_type_get_indent_size(const gchar *mime_type);

void prefs_file_type_set_indent_size(const gchar *file_type, gint indent_size);

gboolean prefs_file_type_get_indent_size(const gchar *file_type);
gboolean prefs_mime_type_get_use_tabs(const gchar *mime_type);

void prefs_file_type_set_use_tabs(const gchar *file_type, gboolean use_tabs);

G_END_DECLS
#endif
