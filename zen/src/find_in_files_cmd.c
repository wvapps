/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "make_cmd.h"

#include "zen.h"

#include <gconf/gconf-client.h>

static gchar *last_regexp = NULL;
static gchar *last_dir = NULL;
static gboolean last_recursive = TRUE,
    	last_whole_words = FALSE, last_sensitive = FALSE;

static GtkEntry *dir_entry;
static GtkEntry *regexp_entry;
static GtkEntry *file_types_entry;
static GtkCheckButton *recursive_check_button;
static GtkCheckButton *sensitive_check_button;
static GtkCheckButton *whole_words_check_button;

static void find_in_files_dialog_changed_cb(GtkWidget *entry, gpointer _dialog)
{
    GtkDialog *dialog = (GtkDialog *)_dialog;

    gboolean is_valid = gtk_entry_get_text(regexp_entry)[0]
    	    && gtk_entry_get_text(file_types_entry)[0]
    	    && gtk_entry_get_text(dir_entry)[0]
    	    && g_file_test(gtk_entry_get_text(dir_entry),
    	    	    G_FILE_TEST_IS_DIR);

    gtk_dialog_set_response_sensitive(dialog, GTK_RESPONSE_OK, is_valid);
}

#define FIND_CONF_PATH(key) 	CONF_PATH("FindInFiles") "/" key
#define CONF_LAST_FILE_TYPES	FIND_CONF_PATH("FileTypes")
#define CONF_LAST_MAKE_DIR  	CONF_PATH("Make") "/Make/Directory"

static GtkWidget *build_find_in_files_regexp(GtkDialog *dialog)
{
    GtkWidget *label = gtk_label_new(NULL);
    regexp_entry = GTK_ENTRY(gtk_entry_new());
    GtkWidget *hbox = gtk_hbox_new(FALSE, 8);
    GConfClient *client = gconf_client_get_default();

    if (last_regexp == NULL)
    	last_regexp = g_strdup("");
    gtk_entry_set_text(regexp_entry, last_regexp);
    
    g_signal_connect(GTK_OBJECT(regexp_entry), "changed",
		    G_CALLBACK(find_in_files_dialog_changed_cb), dialog);
    gtk_entry_set_activates_default(regexp_entry, TRUE);

    gtk_label_set_markup_with_mnemonic(GTK_LABEL(label), _("_Find regexp:"));
    gtk_label_set_mnemonic_widget(GTK_LABEL(label),
    	    GTK_WIDGET(regexp_entry));

    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(regexp_entry),
    	    TRUE, TRUE, 0);

    return hbox;
}

static GtkWidget *build_find_in_files_file_types(GtkDialog *dialog)
{
    GtkWidget *label = gtk_label_new(NULL);
    file_types_entry = GTK_ENTRY(gtk_entry_new());
    GtkWidget *hbox = gtk_hbox_new(FALSE, 8);
    GConfClient *client = gconf_client_get_default();
    gchar *last_file_types;

    last_file_types = gconf_client_get_string(client,
    	    CONF_LAST_FILE_TYPES, NULL);
    if (last_file_types != NULL)
    {
	gtk_entry_set_text(file_types_entry, last_file_types);
	g_free(last_file_types);
    }
    else gtk_entry_set_text(file_types_entry, "*.c *.cc *.h *.sh *.cs");
    
    g_signal_connect(GTK_OBJECT(regexp_entry), "changed",
		    G_CALLBACK(find_in_files_dialog_changed_cb), dialog);
    gtk_entry_set_activates_default(regexp_entry, TRUE);

    gtk_label_set_markup_with_mnemonic(GTK_LABEL(label), _("File _types:"));
    gtk_label_set_mnemonic_widget(GTK_LABEL(label),
    	    GTK_WIDGET(file_types_entry));

    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(file_types_entry),
    	    TRUE, TRUE, 0);

    return hbox;
}

static GtkWidget *build_find_in_files_directory(GtkDialog *dialog)
{
    GtkWidget *label = gtk_label_new(NULL);
    dir_entry = GTK_ENTRY(gtk_entry_new());
    GtkWidget *dir = gtk_hbox_new(FALSE, 8);
    GtkWidget *hbox = gtk_hbox_new(FALSE, 8);
    GtkWidget *vbox = gtk_vbox_new(FALSE, 4);
    GConfClient *client = gconf_client_get_default();

    if (last_dir == NULL)
    {
    	last_dir = gconf_client_get_string(client, CONF_LAST_MAKE_DIR, NULL);
    	if (last_dir == NULL)
    	{
	    Buffer *buffer = get_cur_buffer();
	    last_dir = buffer? buffer_get_dirname(buffer): NULL;
    	    if (last_dir == NULL)
	    {
	    	gchar cwd[PATH_MAX];
    	    	getcwd(cwd, PATH_MAX);
    	    	last_dir = g_strdup(cwd);
	    }
	}
    }
    gtk_entry_set_text(dir_entry, last_dir);
    
    g_signal_connect(GTK_OBJECT(dir_entry), "changed",
		    G_CALLBACK(find_in_files_dialog_changed_cb), dialog);
    gtk_entry_set_activates_default(dir_entry, TRUE);

    gtk_label_set_markup_with_mnemonic(GTK_LABEL(label), _("In _directory:"));
    gtk_label_set_mnemonic_widget(GTK_LABEL(label), GTK_WIDGET(dir_entry));

    gtk_box_pack_start(GTK_BOX(dir), label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(dir), GTK_WIDGET(dir_entry), TRUE, TRUE, 0);

    recursive_check_button = GTK_CHECK_BUTTON(
    	    gtk_check_button_new_with_mnemonic(_("_Recurse into subdirectories")));
    gtk_toggle_button_set_active(
    	    GTK_TOGGLE_BUTTON(recursive_check_button), last_recursive);
    whole_words_check_button = GTK_CHECK_BUTTON(
    	    gtk_check_button_new_with_mnemonic(_("Match _whole words")));
    gtk_toggle_button_set_active(
    	    GTK_TOGGLE_BUTTON(whole_words_check_button),
    	    last_whole_words);
    sensitive_check_button = GTK_CHECK_BUTTON(
    	    gtk_check_button_new_with_mnemonic(_("_Case sensitive")));
    gtk_toggle_button_set_active(
    	    GTK_TOGGLE_BUTTON(sensitive_check_button),
    	    last_sensitive);
    
    gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(recursive_check_button),
    	    TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(whole_words_check_button),
    	    TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(sensitive_check_button),
    	    TRUE, TRUE, 0);

    gtk_box_pack_start(GTK_BOX(vbox), dir, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);

    return vbox;
}

Make *find_in_files = NULL;

static void destroy_cb(Buffer *buffer, void *userdata)
{
    find_in_files = NULL;
}

void find_in_files_cmd(void)
{
    GtkWidget *dialog = gtk_dialog_new_with_buttons (_("Find in files"),
    	GTK_WINDOW(app),
	GTK_DIALOG_MODAL,
	_("_Find"), GTK_RESPONSE_OK,
	GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	NULL);

    gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox),
		    build_find_in_files_regexp(GTK_DIALOG(dialog)));
    gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox),
		    build_find_in_files_file_types(GTK_DIALOG(dialog)));
    gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox),
		    build_find_in_files_directory(GTK_DIALOG(dialog)));

    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);

    find_in_files_dialog_changed_cb(NULL, dialog);
    gtk_widget_show_all(dialog);

    if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK)
    {
    	GConfClient *client = gconf_client_get_default();
    	gboolean recursive = gtk_toggle_button_get_active(
    	    	GTK_TOGGLE_BUTTON(recursive_check_button));
    	gboolean whole_words = gtk_toggle_button_get_active(
    	    	GTK_TOGGLE_BUTTON(whole_words_check_button));
    	gboolean sensitive = gtk_toggle_button_get_active(
    	    	GTK_TOGGLE_BUTTON(sensitive_check_button));
    	
    	if (find_in_files != NULL)
    	    buffer_free(BUFFER(find_in_files));
    	
    	const gchar *argv[256];
    	int argc = 0;
    	argv[argc++] = "find";
    	argv[argc++] = ".";
        argv[argc++] = "-follow";
        argv[argc++] = "-type";
        argv[argc++] = "f";
    	if (!recursive)
    	{
    	    argv[argc++] = "-maxdepth";
    	    argv[argc++] = "1";
    	}
        argv[argc++] = "(";
    	int i;
    	gchar **patterns =
    	    	g_strsplit_set(gtk_entry_get_text(file_types_entry),
    	    	    	    " ,;", -1);
    	gboolean first = TRUE;
    	for (i=0; patterns[i]; ++i)
    	{
    	    if (!patterns[i][0])
    	        continue;
    	    
    	    if (!first)
    	        argv[argc++] = "-o";
    	    argv[argc++] = "-name";
    	    argv[argc++] = patterns[i];
    	    first = FALSE;
    	}
        argv[argc++] = ")";
        argv[argc++] = "-exec";
    	argv[argc++] = "egrep";
        argv[argc++] = "-nH";
    	if (whole_words)
    	    argv[argc++] = "-w";
    	if (!sensitive)
    	    argv[argc++] = "-i";
    	argv[argc++] = (gchar *)gtk_entry_get_text(regexp_entry);
    	argv[argc++] = "{}";
    	argv[argc++] = ";";
    	argv[argc] = NULL;
    	    
    	find_in_files = make_new(argv, gtk_entry_get_text(dir_entry),
	    	_("Search Results"),
	    	g_strconcat(_("Searching for "),
	    	    gtk_entry_get_text(regexp_entry),
	    	    _(" in "),
	    	    gtk_entry_get_text(dir_entry),
	    	    NULL),
	    	g_strconcat(_("Search finished"), NULL));
	    	
        //g_strfreev(patterns);

	make_set_can_browse_errors(find_in_files, TRUE, FALSE);
	make_set_browse_errors(find_in_files,
	    	_("Already at first match"), _("No more matches"));
	find_in_files->started_browsing = TRUE;
	
    	g_signal_connect(G_OBJECT(find_in_files), "destroy",
    	    	GTK_SIGNAL_FUNC(destroy_cb), NULL);
	    
        last_recursive = recursive;
    	last_sensitive = sensitive;
    	last_whole_words = whole_words;
    	g_free(last_regexp); last_regexp =
    	    	g_strdup(gtk_entry_get_text(regexp_entry));
    	g_free(last_dir); last_dir =
            	g_strdup(gtk_entry_get_text(dir_entry));
	gconf_client_set_string(client, CONF_LAST_FILE_TYPES,
	    	gtk_entry_get_text(file_types_entry), NULL);
    }

    gtk_widget_destroy(dialog);
}
