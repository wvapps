/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef MAKE_H
#define MAKE_H
#include <glib.h>
G_BEGIN_DECLS

#include "buffer.h"

#define TYPE_MAKE		(make_get_type())
#define MAKE(obj)		(G_TYPE_CHECK_INSTANCE_CAST((obj), TYPE_MAKE, Make))
#define MAKE_CLASS(class)	(G_TYPE_CHECK_CLASS_CAST((class), TYPE_MAKE, MakeClass))
#define IS_MAKE(obj)		(G_TYPE_CHECK_INSTANCE_TYPE((obj), TYPE_MAKE))
#define IS_MAKE_CLASS(class)	(G_TYPE_CHECK_CLASS_TYPE((class), TYPE_MAKE))
#define MAKE_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), TYPE_MAKE, MakeClass))

typedef struct _Make	    Make;
typedef struct _MakeClass   MakeClass;

struct _MakeClass
{
    BufferClass parent_class;
    
    void (*can_browse_errors_changed)(Make *make);
};

struct _Make
{
    Buffer parent;
    
    int pipe[2];
    
    gchar *command;
    gchar *dir;
    
    guint timeout_id;
    
    gboolean can_browse_errors;
    gboolean search_for_directory;
    
    gchar *start_msg;
    gchar *end_msg;
    
    gboolean started_browsing;
    const gchar *browse_start_msg;
    const gchar *browse_end_msg;
};

Make *make_new(const gchar *argv[], const gchar *dir, const gchar *title,
    	gchar *start_msg, gchar *end_msg);

void make_set_can_browse_errors(Make *make, gboolean can_browse_errors,
    	gboolean search_for_directory);
gboolean make_can_browse_errors(Make *make);

void make_next_error_cmd(Make *make);
void make_previous_error_cmd(Make *make);

void make_set_browse_errors(Make *make,
    	const gchar *start_msg, const gchar *end_msg);

extern Make *make;

G_END_DECLS
#endif
