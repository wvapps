/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "iter.h"
#include "buffer.h"

gboolean iter_starts_word(Iter *iter)
{
    Buffer *buffer = iter_get_buffer(iter);
    Iter prev = *iter;
    return buffer_is_word_char(buffer, gtk_text_iter_get_char(iter))
	    && (!gtk_text_iter_backward_char(&prev)
		    || !buffer_is_word_char(buffer, gtk_text_iter_get_char(&prev)));
}

gboolean iter_ends_word(Iter *iter)
{
    Buffer *buffer = iter_get_buffer(iter);
    Iter prev = *iter;
    return (gtk_text_iter_is_end(iter)
		    || !buffer_is_word_char(buffer, gtk_text_iter_get_char(iter)))
	    && (!gtk_text_iter_backward_char(&prev)
		    || buffer_is_word_char(buffer, gtk_text_iter_get_char(&prev)));
}

void iter_swap(Iter *one, Iter *two)
{
    Iter tmp = *one;
    *two = *one;
    *one = tmp;
}

void iter_backward_to_line_start(Iter *iter)
{
    iter_goto_line_start(iter);
}

void iter_forward_to_line_end(Iter *iter)
{
    iter_goto_line_end(iter);
}

void iter_goto_line_start(Iter *iter)
{
    while (!gtk_text_iter_starts_line(iter))
    {
	if (!gtk_text_iter_backward_char(iter))
	    break;
    }
}

void iter_goto_line_end(Iter *iter)
{
    if (!gtk_text_iter_ends_line(iter))
	gtk_text_iter_forward_to_line_end(iter);
}

void iter_goto_visible_line_start(Iter *iter)
{
    iter_goto_line_start(iter);
    iter_goto_space_group_end(iter);
//    if (gtk_text_iter_ends_line(iter))
//        iter_goto_line_start(iter);
}

void iter_goto_visible_line_end(Iter *iter)
{
    iter_goto_line_end(iter);
    iter_goto_space_group_start(iter);
    if (gtk_text_iter_starts_line(iter))
        iter_goto_line_end(iter);
}

void iter_goto_space_group_start(Iter *iter)
{
    GtkTextIter maybe_new_space_group_start;
    
    maybe_new_space_group_start = *iter;
    while (!gtk_text_iter_starts_line(iter))
    {
	if (gtk_text_iter_backward_char(&maybe_new_space_group_start))
	{
	    gunichar ch = gtk_text_iter_get_char(&maybe_new_space_group_start);
	    if (ch == ' ' || ch == '\t')
		*iter = maybe_new_space_group_start;
	    else break;
	}
	else break;
    }
}

void iter_goto_space_group_end(Iter *iter)
{
    while (!gtk_text_iter_ends_line(iter))
    {
	gunichar ch = gtk_text_iter_get_char(iter);
	if (ch == ' ' || ch == '\t')
	{
	    if (!gtk_text_iter_forward_char(iter))
		break;
	}
	else break;
    }
}

Buffer *iter_get_buffer(Iter *iter)
{
    return BUFFER(gtk_text_iter_get_buffer(iter));
}
