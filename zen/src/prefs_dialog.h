/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef PREFS_DIALOG_H
#define PREFS_DIALOG_H
#include <glib.h>
G_BEGIN_DECLS

#include <gtk/gtk.h>

#define TYPE_PREFS_DIALOG	(prefs_dialog_get_type ())
#define PREFS_DIALOG(obj)	(G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_PREFS_DIALOG, PrefsDialog))
#define PREFS_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_PREFS_DIALOG, PrefsDialogClass))
#define IS_PREFS_DIALOG(obj)  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_PREFS_DIALOG))
#define IS_PREFS_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_PREFS_DIALOG))
#define PREFS_DIALOG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_PREFS_DIALOG, PrefsDialogClass))

typedef struct _PrefsDialog         PrefsDialog;
typedef struct _PrefsDialogClass    PrefsDialogClass;

struct _PrefsDialogClass
{
    GtkDialogClass parent_class;
};

struct _PrefsDialog
{
    GtkDialog parent;
    
    GtkTreeModel *categories;
    GtkWidget *view;
    
    GtkFrame *options_pane;
};

GType prefs_dialog_get_type(void);

GtkWidget *prefs_dialog_new(void);

G_END_DECLS
#endif
