/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "zen.h"
#include "make.h"
#include "buffer.h"
#include "views.h"
#include "mark.h"
#include "status_bar.h"
#include "file_list.h"
#include "utils.h"

#include <glib/gprintf.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>

enum {
    CAN_BROWSE_ERRORS_CHANGED_SIGNAL,
    LAST_SIGNAL
};

static guint make_signals[LAST_SIGNAL] = { 0 };

static Mark *make_get_search_mark(Make *make)
{
    Mark *search_mark;
    
    search_mark = gtk_text_buffer_get_mark(GTK_TEXT_BUFFER(make), "search-mark");
    if (search_mark == NULL)
    {	
	Iter start, end;
	gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(make), &start, &end);
	search_mark = gtk_text_buffer_create_mark(GTK_TEXT_BUFFER(make),
		"search-mark", &start, TRUE);
	if (search_mark == NULL)
	{
	    g_warning("make_get_search_mark: create_mark failed");
	    return NULL;
	}
    }
    
    return search_mark;
}

static Mark *make_get_insert_mark(Make *make)
{
    Mark *insert_mark;
    
    insert_mark = gtk_text_buffer_get_mark(GTK_TEXT_BUFFER(make), "insert-mark");
    if (insert_mark == NULL)
    {	
	Iter start, end;
	gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(make), &start, &end);
	insert_mark = gtk_text_buffer_create_mark(GTK_TEXT_BUFFER(make),
		"insert-mark", &start, FALSE);
	if (insert_mark == NULL)
	{
	    g_warning("make_get_insert_mark: create_mark failed");
	    return NULL;
	}
    }
    
    return insert_mark;
}

static void make_child_setup_cb(gpointer user_data)
{
    dup2(1, 2);
}

static void make_insert_in_results(Make *make, const gchar *format, ...)
{
    Mark *mark = make_get_insert_mark(make);
    Iter iter = buffer_get_iter_at_mark(BUFFER(make), mark);
	    
    gchar text[1024];
    va_list arg_list;

    va_start(arg_list, format);
    g_vsprintf(text, format, arg_list);
    va_end(arg_list);

    gchar fixed_text[1024];
    const gchar *p;
    gchar *q;
    
    for (p=text, q=fixed_text; *p; ++p)
    {
    	if (*p == '\b' && q != fixed_text)
    	    --q;
    	else *q++ = *p;
    }
    *q = '\0';
    	
    gtk_text_buffer_insert(GTK_TEXT_BUFFER(make), &iter, fixed_text,
    	    strlen(fixed_text));
    
    if (!make->started_browsing)
    {
    	int i;
    	for (i=0; i<2; ++i)
    	    gtk_text_view_scroll_mark_onscreen(
    	    	    GTK_TEXT_VIEW(buffer_get_view(BUFFER(make), i)), mark);
    }
}

static gboolean make_is_error_line(Make *make, const Iter *_iter,
	int max_filename_len, gchar *filename, int *line_number)
{
    Buffer *buffer = BUFFER(make);

    Iter iter = *_iter;
    gboolean have_filename = FALSE;
    gboolean have_line_number = FALSE;
    gboolean have_warning_error = FALSE;

    g_assert(buffer != NULL);
    g_assert(_iter != NULL);
    g_assert(max_filename_len > 0);
    g_assert(filename != NULL);
    g_assert(line_number != NULL);

    iter = *_iter;
    
    int used = 0;
    while (used < max_filename_len - 1
    	    && !have_filename && !gtk_text_iter_ends_line(&iter))
    {
	gunichar ch = gtk_text_iter_get_char(&iter);
	gtk_text_iter_forward_char(&iter);
	switch (ch)
	{
	    case ':':
		have_filename = TRUE;
		break;
	    
	    case ' ':
	    case '\t':
	    	have_filename = FALSE;
	    	used = 0;
	    	break;
	    
	    case '\0':
		return FALSE;

	    default:
	    {
	    	// This is wrong, it might write off the end!
	    	int count = g_unichar_to_utf8(ch, &filename[used]);
	    	if (used + count < max_filename_len - 1)
		    used += count;
		else used = max_filename_len - 1;
	    }
	    break;
	}
    }
    filename[used] = '\0';

    *line_number = 0;
    while (!have_line_number && !gtk_text_iter_ends_line(&iter))
    {
	gunichar ch = gtk_text_iter_get_char(&iter);
	gtk_text_iter_forward_char(&iter);
	switch (ch)
	{
	    case ':':
		have_line_number = TRUE;
		break;
	    
	    case '0':
	    case '1':
	    case '2':
	    case '3':
	    case '4':
	    case '5':
	    case '6':
	    case '7':
	    case '8':
	    case '9':
		*line_number = (*line_number * 10) + ch - '0';
		break;

	    default:
		return FALSE;
	}
    }
    
    have_warning_error = TRUE;
    
    return have_filename && have_line_number && have_warning_error;
}

static gboolean make_search_for_directory(Make *make, const Iter *_iter,
	int max_directory_len, gchar *directory)
{
    int skip = 0;
    Iter iter = *_iter;
    gboolean result = FALSE;
    
    while (!result && !gtk_text_iter_is_start(&iter))
    {
    	Iter next_iter = iter;
    	gchar *line;
    	int dummy;
    
	if (!gtk_text_iter_backward_line(&iter)) break;
	
	line = gtk_text_buffer_get_text(GTK_TEXT_BUFFER(make),
	    	&iter, &next_iter, FALSE);
	
	if (sscanf(line, "make[%d]: Entering directory `", &dummy) == 1)
	{
	    if (skip == 0)
	    {
	    	gchar *p = strchr(line, '`');
	    	strncpy(directory, p+1, max_directory_len);
	    	directory[max_directory_len-1] = '\0';
	    	p = strchr(directory, '\'');
	    	if (p != NULL) *p = '\0';
	    	result = TRUE;
	    }
	    else --skip;
	}
	else if (sscanf(line, "make[%d]: Leaving directory `", &dummy) == 1)
	{
	    ++skip;
	}
	
	g_free(line);
    }
    
    if (make->dir != NULL)
    {
    	strncpy(directory, make->dir, max_directory_len);
    	directory[max_directory_len-1] = '\0';
    	result = TRUE;
    }
    
    return result;
}

extern Buffer *open_file(const gchar *filename, gboolean request_create);

static void make_position_cursor(Make *make, Iter *make_iter,
    	const gchar *filename, int line_number)
{
    FileListIter iter;
    Buffer *buffer = NULL;
    gchar *normalized_filename = normalize_filename(filename, make->dir);
    
    for (file_list_iter_start(FILE_LIST(file_list), &iter);
	    !file_list_iter_done(&iter);
	    file_list_iter_next(&iter))
    {
	Buffer *this_buffer = file_list_iter_get_buffer(&iter);
	const gchar *this_filename = buffer_get_filename(this_buffer);

	if (this_filename != NULL
	    	&& g_str_equal(this_filename, normalized_filename))
	{
	    buffer = this_buffer;
	    break;
	}
    }
    
    if (buffer == NULL)
    	buffer = open_file(normalized_filename, FALSE);
    
    if (buffer == NULL)
    {
    	FileListIter iter;
    	for (file_list_iter_start(FILE_LIST(file_list), &iter);
	    	!file_list_iter_done(&iter);
	    	file_list_iter_next(&iter))
    	{
	    Buffer *this_buffer = file_list_iter_get_buffer(&iter);
	    const gchar *this_filename = buffer_get_filename(this_buffer);
    
	    if (this_filename != NULL
	    	    && g_str_has_suffix(this_filename, filename))
	    {
	    	buffer = this_buffer;
	    	break;
	    }
    	}
    }
    
    if (buffer != NULL)
    {
    	view_focus(views_set_top_view(buffer));
    	views_set_bottom_view(BUFFER(make));
	    
	buffer_goto_line_number_next_resize(buffer, line_number - 1);
	view_scroll_to_insert(views_get_bottom_view());

	status_bar_set(_("File %s, line %d"), filename, line_number);
	
	make->started_browsing = TRUE;
    }
    else
    {
    	view_focus(views_set_one_view(BUFFER(make)));
        
    	status_bar_set(_("Failed to open '%s'!"), normalized_filename);
    }
    
    g_free(normalized_filename);
}

gboolean make_can_browse_errors(Make *make)
{
    return make->can_browse_errors;
}

void make_next_error_cmd(Make *make)
{
    Mark *search_mark = make_get_search_mark(make);
    Iter iter;
    
    if (search_mark == NULL)
    {
	g_warning("next_run_error_cb: search_mark == NULL");
	return;
    }
    
    gtk_text_buffer_get_iter_at_mark(GTK_TEXT_BUFFER(make), &iter, search_mark);
    
    while (!gtk_text_iter_is_end(&iter))
    {
	gchar filename[1024];
	int line_number;
	
	if (make_is_error_line(make, &iter, 1024, filename, &line_number))
	{
	    buffer_select_line(BUFFER(make), &iter);
	    
	    if (make->search_for_directory && filename[0] != '/')
	    {
	    	gchar directory[1024];
	    	gchar *dir_and_file;
	    	Iter dir_iter = iter;
	    	
	    	if (make_search_for_directory(make, &iter, 1024, directory))
	    	{
	    	    dir_and_file = g_strconcat(directory, "/", filename, NULL);
        	    	
    	    	    make_position_cursor(make, &dir_iter, dir_and_file, line_number);
            	    	
    	    	    g_free(dir_and_file);
    	    	}
    	    	else make_position_cursor(make, &iter, filename, line_number);
	    }
    	    else make_position_cursor(make, &iter, filename, line_number);
	    
	    gtk_text_iter_forward_line(&iter);
	    gtk_text_buffer_move_mark(GTK_TEXT_BUFFER(make), search_mark, &iter);
	    
	    return;
	}
	else if (!gtk_text_iter_forward_line(&iter)) break;
    }
    
    make->started_browsing = FALSE;
    status_bar_set(make->browse_end_msg);
}

void make_previous_error_cmd(Make *make)
{
    Mark *search_mark = make_get_search_mark(make);
    Iter iter;
    
    if (search_mark == NULL)
    {
	g_warning("previous_run_error_cb: search_mark == NULL");
	return;
    }
    
    gtk_text_buffer_get_iter_at_mark(GTK_TEXT_BUFFER(make), &iter, search_mark);
    
    if (gtk_text_iter_backward_line(&iter))
    {
	while (!gtk_text_iter_is_start(&iter))
	{
	    gchar filename[1024];
	    int line_number;
	    
	    if (!gtk_text_iter_backward_line(&iter)) break;
	    
	    if (make_is_error_line(make, &iter, 1024, filename, &line_number))
	    {
	        buffer_select_line(BUFFER(make), &iter);
    
	    	if (make->search_for_directory && filename[0] != '/')
	    	{
	    	    gchar directory[1024];
	    	    gchar *dir_and_file;
	    	    Iter dir_iter = iter;
    	    	
	    	    if (make_search_for_directory(make, &iter, 1024, directory))
	    	    {
	    	    	dir_and_file = g_strconcat(directory, "/", filename, NULL);
        	    	
    	    	    	make_position_cursor(make, &dir_iter, dir_and_file, line_number);
            	    	
    	    	    	g_free(dir_and_file);
    	    	    }
    	    	    else make_position_cursor(make, &iter, filename, line_number);
	    	}
    	    	else make_position_cursor(make, &iter, filename, line_number);
	    
		gtk_text_iter_forward_line(&iter);
		gtk_text_buffer_move_mark(GTK_TEXT_BUFFER(make), search_mark, &iter);
	    
		return;
	    }
	}
    }
    
    status_bar_set(make->browse_start_msg);
}

static gboolean make_run_cb(gpointer _make)
{
    Make *make = (Make *)_make;

    if (make->pipe[0] == -1)
	return TRUE;
    
    for (;;)
    {
	fd_set fds;
	struct timeval tv;
	gchar buf[1024];
	int len, res;
	
	FD_ZERO(&fds);
	FD_SET(make->pipe[0], &fds);
	memset(&tv, 0, sizeof(tv));
	
	res = select(make->pipe[0]+1, &fds, NULL, NULL, &tv);
	if (res <= 0)
	    return TRUE;
	    
	len = read(make->pipe[0], buf, 1023);
	if (len <= 0)
	    break;
	if (len > 0)
	{
	    buf[len] = '\0';
	    
	    make_insert_in_results(make, "%s", buf);
	}
    }
    close(make->pipe[0]);
    make->pipe[0] = -1;
    
    if (make->end_msg)
    {
    	status_bar_set("%s", make->end_msg);
    	make_insert_in_results(make, "*** %s", make->end_msg);
    }

    return TRUE;
}

static void make_init(Make *make)
{
    make->command = NULL;
    make->dir = NULL;
    
    make->pipe[0] = -1;
    make->pipe[1] = -1;

    make->timeout_id = 0;
    
    make->can_browse_errors = FALSE;
    make->search_for_directory = FALSE;
    
    make->start_msg = NULL;
    make->end_msg = NULL;
    
    make->started_browsing = FALSE;
    make->browse_start_msg = _("Already at first error");
    make->browse_end_msg = _("No more errors");
}

static void make_class_init(MakeClass *class)
{
    make_signals[CAN_BROWSE_ERRORS_CHANGED_SIGNAL] =
	    g_signal_new("can-browse-errors-changed",
			  G_TYPE_FROM_CLASS(class),
			  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			  G_STRUCT_OFFSET(MakeClass, can_browse_errors_changed),
			  NULL, NULL,
			  g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}

static void make_finalize(GObject *_make)
{
    Make *make = (Make *)_make;
    
    if (make->timeout_id != 0)
    	g_source_remove(make->timeout_id);
    
    close(make->pipe[0]);
    
    g_free(make->command);
    g_free(make->dir);
    
    g_free(make->start_msg);
    g_free(make->end_msg);
    
    buffer_finalize((GObject *)_make);
}

static void make_base_init(BufferClass *base_class)
{
    G_OBJECT_CLASS(base_class)->finalize = make_finalize;
}

GType make_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
	static const GTypeInfo info =
	{
	    sizeof(MakeClass),
	    (GBaseInitFunc) make_base_init,
	    NULL, /* base_finalize */
	    (GClassInitFunc)make_class_init,
	    NULL, /* class_finalize */
	    NULL, /* class_data */
	    sizeof (Make),
	    0,	  /* n_preallocs */
	    (GInstanceInitFunc) make_init,
	};

	type = g_type_register_static(TYPE_BUFFER, "Make", &info, 0);
    }

    return type;
}

Make *make_new(const gchar *argv[], const gchar *dir, const gchar *title,
    	gchar *start_msg, gchar *end_msg)
{
    Make *make = g_object_new(TYPE_MAKE, NULL);
    gchar cwd[PATH_MAX];

    g_free(make->start_msg);
    make->start_msg = start_msg;
    
    g_free(make->end_msg);
    make->end_msg = end_msg;
    
    views_add_buffer(BUFFER(make));

    buffer_preferences_changed_cb(BUFFER(make), NULL);
    
    if (dir == NULL)
    {
    	getcwd(cwd, PATH_MAX);
    	dir = cwd;
    }
    
    if (pipe(make->pipe) == 0)
    {
	GError *error = NULL;
	Mark *search_mark;
	Iter start, end;
    	View *old_focus;
	
	if (!g_spawn_async_with_pipes(dir, (gchar **)argv, 
	    	NULL, G_SPAWN_SEARCH_PATH,
		make_child_setup_cb, NULL, NULL, NULL,
		&make->pipe[0], &make->pipe[1], &error))
	{
	    consume_error(error);
	    return;
	}
	
	buffer_set_is_temp(BUFFER(make), TRUE);
    	g_signal_emit(G_OBJECT(make),
    	    	make_signals[CAN_BROWSE_ERRORS_CHANGED_SIGNAL], 0);
	buffer_set_desc(BUFFER(make), title);
	
	gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(make), &start, &end);
	search_mark = make_get_search_mark(make);
	gtk_text_buffer_move_mark(GTK_TEXT_BUFFER(make), search_mark, &start);
	
	old_focus = get_cur_view();
	view_focus(views_set_bottom_view(BUFFER(make)));
	if (old_focus) view_focus(old_focus);
	
    	make->command = g_strjoinv(" ", (gchar **)argv);
    	make->dir = g_strdup(dir);
	
    	make->timeout_id = g_timeout_add(100, make_run_cb, make);
    	 
    	if (make->start_msg)
    	{
	    status_bar_set("%s", make->start_msg);
    	    make_insert_in_results(make, "*** %s\n", make->start_msg);
    	}
    }
    else g_warning("make_in_dir: failed to create pipe");

    return make;
}

void make_set_can_browse_errors(Make *make, gboolean can_browse_errors,
    	gboolean search_for_directory)
{
    make->can_browse_errors = can_browse_errors;
    make->search_for_directory = search_for_directory;
}

void make_set_browse_errors(Make *make,
    	const gchar *start_msg, const gchar *end_msg)
{
    make->browse_start_msg = start_msg;
    make->browse_end_msg = end_msg;
}

