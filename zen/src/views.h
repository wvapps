/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */
 
#ifndef VIEWS_H
#define VIEWS_H
#include <glib.h>
G_BEGIN_DECLS

#include "view.h"
#include "buffer.h"

#include <gtk/gtkcontainer.h>

GtkWidget *views_new(void);

void views_add_buffer(Buffer *buffer);
void views_remove_buffer(Buffer *buffer);

View *views_set_view(int i, Buffer *buffer);
View *views_set_top_view(Buffer *buffer);
View *views_set_bottom_view(Buffer *buffer);
View *views_set_one_view(Buffer *buffer);
View *views_set_split_view(Buffer *buffer);

Buffer *views_get_buffer(int i);
Buffer *views_get_top_buffer(void);
Buffer *views_get_bottom_buffer(void);

View *views_get_view(int i);
View *views_get_top_view(void);
View *views_get_bottom_view(void);

G_END_DECLS
#endif
