/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef BUFFER_H
#define BUFFER_H
#include <glib.h>
G_BEGIN_DECLS

#include "iter.h"
#include "mark.h"
#include "view.h"

#include <gtksourceview/gtksourcebuffer.h>
#include <time.h>

#define TYPE_BUFFER		(buffer_get_type())
#define BUFFER(obj)		(G_TYPE_CHECK_INSTANCE_CAST((obj), TYPE_BUFFER, Buffer))
#define BUFFER_CLASS(class)	(G_TYPE_CHECK_CLASS_CAST((class), TYPE_BUFFER, BufferClass))
#define IS_BUFFER(obj)		(G_TYPE_CHECK_INSTANCE_TYPE((obj), TYPE_BUFFER))
#define IS_BUFFER_CLASS(class)	(G_TYPE_CHECK_CLASS_TYPE((class), TYPE_BUFFER))
#define BUFFER_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), TYPE_BUFFER, BufferClass))

typedef struct _Buffer	    Buffer;
typedef struct _BufferClass BufferClass;

struct _BufferClass
{
    GtkSourceBufferClass parent_class;
    
    void (*filename_changed)(Buffer *buffer, const gchar *filename);
    void (*desc_changed)(Buffer *buffer, const gchar *desc);
    void (*focus_in)(Buffer *buffer, View *view);
    void (*focus_out)(Buffer *buffer, View *view);
    void (*destroy)(Buffer *buffer);
    
    GObject *(*chain_constructor)(GType type, guint, GObjectConstructParam *);
};

struct _Buffer
{
    GtkSourceBuffer parent;
    
    gchar *desc;
    gchar *filename;
    time_t last_mtime;
    View *views[2];
    gboolean is_temp;
    time_t last_focus_time;
    gint tab_size;
    gint indent_size; 
    gboolean use_tabs;
    gchar *codeset;
};

GType buffer_get_type(void);

Buffer *buffer_new(void);
Buffer *buffer_new_with_filename(const gchar *filename);
void buffer_free(Buffer *buffer);
void buffer_finalize(GObject *_buffer);

void buffer_select(Buffer *buffer, const Iter *start, const Iter *end);

void buffer_indent_line(Buffer *buffer, Iter *iter);
void buffer_exdent_line(Buffer *buffer, Iter *iter);

gboolean buffer_in_leading_space(Buffer *buffer, const Iter *iter);
gboolean buffer_in_trailing_space(Buffer *buffer, const Iter *iter);

void buffer_set_filename(Buffer *buffer, const gchar *filename);
const gchar *buffer_get_filename(const Buffer *buffer);

void buffer_set_desc(Buffer *buffer, const gchar *filename);
const gchar *buffer_get_desc(const Buffer *buffer);

gboolean buffer_join_lines(Buffer *buffer);
gboolean buffer_split_lines(Buffer *buffer);
gboolean buffer_delete_lines(Buffer *buffer);

gboolean buffer_load(Buffer *buffer, GError **error);
gboolean buffer_autoload(Buffer *buffer, GError **error);
gboolean buffer_have_newer_autosave(Buffer *buffer);
gboolean buffer_save(Buffer *buffer, GError **error);
gboolean buffer_autosave(Buffer *buffer, GError **error);
gboolean buffer_revert(Buffer *buffer, GError **error);
gchar *buffer_get_autosave_filename(Buffer *buffer);

gboolean buffer_replace_selection(Buffer *buffer, const gchar *str, gint len);

gboolean buffer_get_visual_selection_bounds(Buffer *buffer, Iter *start, Iter *end);

gint buffer_get_cursor_line_num(Buffer *buffer);
gint buffer_get_cursor_column_num(Buffer *buffer);

void buffer_goto_line_number(Buffer *buffer, gint line_number);
void buffer_goto_line_number_next_resize(Buffer *buffer, gint line_number);

gboolean buffer_is_word_char(const Buffer *buffer, gunichar ch);
gboolean buffer_is_word(const Buffer *buffer, const gchar *str);

gboolean buffer_get_selection_bounds(Buffer *buffer, Iter *start, Iter *end);
gboolean buffer_have_selection(Buffer *buffer);

gboolean buffer_get_modified(Buffer *buffer);
void buffer_set_modified(Buffer *buffer, gboolean modified);
gboolean buffer_has_file_changed(Buffer *buffer);
void buffer_freshen(Buffer *buffer);

View *buffer_get_view(Buffer *buffer, int i);
View *buffer_get_primary_view(Buffer *buffer);
View *buffer_get_secondary_view(Buffer *buffer);

void buffer_check_file_changed(Buffer *buffer);

Buffer *get_cur_buffer(void);

void buffer_select_line(Buffer *buffer, const Iter *iter);
void buffer_clear(Buffer *buffer);

void buffer_set_is_temp(Buffer *buffer, gboolean is_temp);
gboolean buffer_get_is_temp(Buffer *buffer);

time_t buffer_get_last_focus_time(Buffer *buffer);
gchar *buffer_get_dirname(Buffer *buffer);
const gchar *buffer_get_last_dirname();

Mark *buffer_get_insert_mark(Buffer *buffer);
Iter buffer_get_insert_iter(Buffer *buffer);

gboolean buffer_insert_on_first_line(Buffer *buffer);
gboolean buffer_insert_on_last_line(Buffer *buffer);

gboolean buffer_insert_at_line_start(Buffer *buffer);
gboolean buffer_insert_at_line_end(Buffer *buffer);

gboolean buffer_is_empty(Buffer *buffer);

Iter buffer_get_iter_at_mark(Buffer *buffer, Mark *mark);

gboolean buffer_can_undo(Buffer *buffer);
gboolean buffer_can_redo(Buffer *buffer);

gchar *buffer_get_mime_type(Buffer *buffer);

void buffer_set_codeset(Buffer *buffer, const gchar *codeset);
const gchar *buffer_get_codeset(Buffer *buffer);
void buffer_preferences_changed_cb(Buffer *buffer, gpointer user_data);

G_END_DECLS
#endif

