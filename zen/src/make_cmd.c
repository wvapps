/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "make_cmd.h"

#include "zen.h"

#include <gconf/gconf-client.h>

static GtkEntry *cmd_entry;
static GtkEntry *dir_entry;

static void make_dialog_changed_cb(GtkWidget *entry, gpointer _dialog)
{
    GtkDialog *dialog = (GtkDialog *)_dialog;
    
    gboolean is_valid = gtk_entry_get_text(cmd_entry)[0]
    	    && gtk_entry_get_text(dir_entry)[0]
    	    && g_file_test(gtk_entry_get_text(dir_entry),
    	    	    G_FILE_TEST_IS_DIR);

    gtk_dialog_set_response_sensitive(dialog, GTK_RESPONSE_OK, is_valid);
}

#define FIND_CONF_PATH(key) 	CONF_PATH("Make") "/" key
#define CONF_LAST_CMD       	FIND_CONF_PATH("Command")
#define CONF_LAST_DIR       	FIND_CONF_PATH("Directory")

static GtkWidget *build_make_command(GtkDialog *dialog)
{
    GtkWidget *label = gtk_label_new(NULL);
    cmd_entry = GTK_ENTRY(gtk_entry_new());
    GtkWidget *hbox = gtk_hbox_new(FALSE, 8);
    GConfClient *client = gconf_client_get_default();
    gchar *last_cmd;

    last_cmd = gconf_client_get_string(client, CONF_LAST_CMD, NULL);
    if (last_cmd != NULL)
    {
	gtk_entry_set_text(cmd_entry, last_cmd);
	g_free(last_cmd);
    }
    else gtk_entry_set_text(cmd_entry, "make -k");

    g_signal_connect(GTK_OBJECT(cmd_entry), "changed",
		    G_CALLBACK(make_dialog_changed_cb), dialog);
    gtk_entry_set_activates_default(cmd_entry, TRUE);

    gtk_label_set_markup_with_mnemonic(GTK_LABEL(label), _("_Run command:"));
    gtk_label_set_mnemonic_widget(GTK_LABEL(label), GTK_WIDGET(cmd_entry));

    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(cmd_entry), TRUE, TRUE, 0);

    return hbox;
}

static GtkWidget *build_make_directory(GtkDialog *dialog)
{
    GtkWidget *label = gtk_label_new(NULL);
    dir_entry = GTK_ENTRY(gtk_entry_new());
    GtkWidget *hbox = gtk_hbox_new(FALSE, 8);
    GConfClient *client = gconf_client_get_default();
    gchar *last_dir;

    last_dir = gconf_client_get_string(client, CONF_LAST_DIR, NULL);
    if (last_dir != NULL)
    {
	gtk_entry_set_text(dir_entry, last_dir);
	g_free(last_dir);
    }
    else
    {
	Buffer *buffer = get_cur_buffer();
	gchar *cur_buffer_dirname = buffer_get_dirname(buffer);
    
	if (cur_buffer_dirname != NULL)
	{
	    gtk_entry_set_text(dir_entry, cur_buffer_dirname);
	    g_free(cur_buffer_dirname);
	}
	else
	{
	    gchar cwd[PATH_MAX];
    	    getcwd(cwd, PATH_MAX);
	    gtk_entry_set_text(dir_entry, cwd);
	}
    }
    
    g_signal_connect(GTK_OBJECT(dir_entry), "changed",
		    G_CALLBACK(make_dialog_changed_cb), dialog);
    gtk_entry_set_activates_default(dir_entry, TRUE);

    gtk_label_set_markup_with_mnemonic(GTK_LABEL(label), "In _directory:");
    gtk_label_set_mnemonic_widget(GTK_LABEL(label), GTK_WIDGET(dir_entry));

    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(dir_entry), TRUE, TRUE, 0);

    return hbox;
}

Make *make = NULL;

static void destroy_cb(Buffer *buffer, void *userdata)
{
    if (make == MAKE(buffer))
    	make = NULL;
}

void make_cmd(void)
{
    GtkWidget *dialog = gtk_dialog_new_with_buttons(_("Make"), GTK_WINDOW(app),
	GTK_DIALOG_MODAL,
	_("_Make"), GTK_RESPONSE_OK,
	GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	NULL);

    gtk_container_add(GTK_CONTAINER (GTK_DIALOG(dialog)->vbox),
		    build_make_command(GTK_DIALOG(dialog)));
    gtk_container_add(GTK_CONTAINER (GTK_DIALOG(dialog)->vbox),
		    build_make_directory(GTK_DIALOG(dialog)));

    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);

    make_dialog_changed_cb(NULL, dialog);
    gtk_widget_show_all(dialog);

    if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK)
    {
    	GConfClient *client = gconf_client_get_default();
    	
    	if (make != NULL)
    	    buffer_free(BUFFER(make));
    	
    	const gchar *argv[256];
    	int argc = 0;
    	argv[argc++] = getenv("SHELL")? getenv("SHELL"): "sh";
    	argv[argc++] = "-c";
    	argv[argc++] = gtk_entry_get_text(cmd_entry);
    	argv[argc] = NULL;
    	
    	make = make_new(argv,
	    	gtk_entry_get_text(dir_entry), _("Make Output"),
	    	g_strconcat(_("Running "), gtk_entry_get_text(cmd_entry),
	    	    _(" in "), gtk_entry_get_text(dir_entry), NULL),
	    	g_strconcat(_("make finished"), NULL));
	make_set_can_browse_errors(make, TRUE, TRUE);
	
    	g_signal_connect(G_OBJECT(make), "destroy",
    	    	GTK_SIGNAL_FUNC(destroy_cb), NULL);
	
	gconf_client_set_string(client, CONF_LAST_CMD,
	    gtk_entry_get_text(cmd_entry), NULL);
	gconf_client_set_string(client, CONF_LAST_DIR,
	    gtk_entry_get_text(dir_entry), NULL);
    }

    gtk_widget_destroy(dialog);
}



