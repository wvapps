/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "view.h"
#include "buffer.h"
#include "mark.h"
#include "file_list.h"
#include "zen.h"
#include "prefs.h"

#include <string.h>
#include <gconf/gconf-client.h>

Buffer *view_get_buffer(View *view)
{
    return BUFFER(gtk_text_view_get_buffer(GTK_TEXT_VIEW(view)));
}

void view_select(View *view, Iter *start, Iter *end)
{
    Buffer *buffer = view_get_buffer(view);
    
    buffer_select(buffer, start, end);
}

void view_scroll_to_insert(View *view)
{
    if (view != NULL)
    {
	Buffer *buffer = view_get_buffer(view);
	Mark *insert = gtk_text_buffer_get_insert(GTK_TEXT_BUFFER(buffer));
	Iter iter;
	GdkRectangle iter_rect, visible;
	//gint y, height;
	
	gtk_text_view_get_visible_rect(GTK_TEXT_VIEW(view), &visible);
	gtk_text_buffer_get_iter_at_mark(GTK_TEXT_BUFFER(buffer), &iter, insert);
	gtk_text_view_get_iter_location(GTK_TEXT_VIEW(view),
                &iter, &iter_rect);
	//gtk_text_view_get_line_yrange(GTK_TEXT_VIEW(view), &iter, &y, &height);
	
	//g_warning("ir=(%d,%d:%d,%d); vis=(%d,%d:%d,%d)",
	//    iter_rect.x, iter_rect.y, iter_rect.width, iter_rect.height,
	//    visible.x, visible.y, visible.width, visible.height);
	if (iter_rect.y < visible.y
	    	|| iter_rect.y + iter_rect.height > visible.y + visible.height
	    	|| iter_rect.x < visible.x
	    	|| iter_rect.x + iter_rect.height > visible.x + visible.width)
	    gtk_text_view_scroll_to_mark(GTK_TEXT_VIEW(view),
		    insert, 0.0, TRUE, 0.99, 0.5);
    }
    else g_warning("view_scroll_to_insert: view == NULL");
}

void view_scroll_next_resize(View *view)
{
    if (view != NULL)
	view->scroll_this_once = TRUE;
    else g_warning("view_scroll_next_resize: view == NULL");
}

static void view_class_init(ViewClass *class)
{
}

static void view_init(View *view)
{
}

GType view_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
	static const GTypeInfo info =
	{
	    sizeof (ViewClass),
	    NULL, /* base_init */
	    NULL, /* base_finalize */
	    (GClassInitFunc) view_class_init,
	    NULL, /* class_finalize */
	    NULL, /* class_data */
	    sizeof(View),
	    0,	  /* n_preallocs */
	    (GInstanceInitFunc) view_init,
	};

	type = g_type_register_static (GTK_TYPE_SOURCE_VIEW,
		"View", &info, 0);
    }

    return type;
}

static gboolean size_allocate_cb(GtkWidget *_view, GtkAllocation *allocation,
    	gpointer user_data)
{
    View *view = VIEW(_view);

    if (view->scroll_this_once)
    {
        view_scroll_to_insert(view);
        view->scroll_this_once = FALSE;
    }
    
    return FALSE;
}

static void view_preferences_changed_cb(View *view, gpointer user_data)
{
    Buffer *buffer = view_get_buffer(view);
    gchar *mime_type = buffer_get_mime_type(buffer);
    const gchar *font_name = prefs_mime_type_get_font_name(mime_type);
    GdkColormap *colormap = gdk_colormap_get_system();
    GdkColor color;

    PangoFontDescription *font_desc =
    	    pango_font_description_from_string(font_name);
    	
    gtk_widget_modify_font(GTK_WIDGET(view), font_desc);
            
    pango_font_description_free(font_desc);
        
    gtk_source_view_set_tabs_width(GTK_SOURCE_VIEW(view),
    	    prefs_mime_type_get_tab_size(mime_type));

    prefs_global_get_background_color(&color);
    gdk_colormap_alloc_color(colormap, &color, FALSE, FALSE);
    gtk_widget_modify_base(GTK_WIDGET(view), GTK_STATE_NORMAL, &color);
    prefs_global_get_foreground_color(&color);
    gdk_colormap_alloc_color(colormap, &color, FALSE, FALSE);
    gtk_widget_modify_text(GTK_WIDGET(view), GTK_STATE_NORMAL, &color);

    g_free(mime_type);
}

static void view_gconf_value_changed_cb(GConfClient* client,
        const gchar* key, GConfValue* value, View *view)
{
    view_preferences_changed_cb(view, NULL);
}

static void view_filename_changed_cb(Buffer *buffer, const gchar *filename, View *view)
{
    view_preferences_changed_cb(view, NULL);
}

View *view_new(Buffer *buffer)
{
    GConfClient *client = gconf_client_get_default();
    View *view = g_object_new(TYPE_VIEW, NULL);
    GtkScrolledWindow *view_sw;

    gtk_text_view_set_buffer(GTK_TEXT_VIEW(view), GTK_TEXT_BUFFER(buffer));

    view_sw = GTK_SCROLLED_WINDOW(gtk_scrolled_window_new(NULL, NULL));
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(view_sw),
	    GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(view_sw),
	    GTK_SHADOW_IN);
    gtk_container_add(GTK_CONTAINER(view_sw), GTK_WIDGET(view));

    view->containee = GTK_WIDGET(view_sw);
    view->scroll_this_once = FALSE;

    g_object_ref(view->containee);

    view_preferences_changed_cb(view, NULL);

    g_signal_connect(G_OBJECT(view), "size-allocate",
	    GTK_SIGNAL_FUNC(size_allocate_cb), NULL);
    g_signal_connect(G_OBJECT(client), "value_changed",
    	    GTK_SIGNAL_FUNC(view_gconf_value_changed_cb), view);
    g_signal_connect(G_OBJECT(buffer), "filename-changed",
    	    GTK_SIGNAL_FUNC(view_filename_changed_cb), view);

    return view;
}

GtkWidget *view_get_containee(View *view)
{
    return view->containee;
}

View *get_cur_view(void)
{
    GtkWidget *focus_widget = gtk_window_get_focus(GTK_WINDOW(app));
    
    if (IS_VIEW(focus_widget))
    	return VIEW(focus_widget);
    else return NULL;
}

void view_goto_line_number(View *view, gint line_number)
{
    buffer_goto_line_number(view_get_buffer(view), line_number);
}

void view_focus(View *view)
{
    gtk_widget_grab_focus(GTK_WIDGET(view));
}
