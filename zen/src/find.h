/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef FIND_H
#define FIND_H
#include <glib.h>
G_BEGIN_DECLS

#include <gtk/gtkdialog.h>
#include <gtk/gtkcombo.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkcheckbutton.h>
#include <gtk/gtkradiobutton.h>
#include <gtk/gtkwindow.h>
#include <gtk/gtktextbuffer.h>
#include <gtk/gtktextview.h>

#undef USE_CONTEXT_SELECTION

typedef enum
{
    FIND_DIRECTION_UP,
    FIND_DIRECTION_DOWN
} FindDirection;

typedef enum
{
#ifdef USE_CONTEXT_SELECTION
    REPLACE_CONTEXT_SELECTION,
#endif    
    REPLACE_CONTEXT_THIS_BUFFER,
    REPLACE_CONTEXT_ALL_BUFFERS
} ReplaceContext;

typedef struct _FindReplaceOptions
{
    gchar *search_text;
    GSList *old_search_texts;
    gchar *replace_text;
    GSList *old_replace_texts;
    gboolean match_whole_words;
    gboolean match_case;
    gboolean regular_expression;
    gboolean wrap_searches;
    FindDirection direction;
    ReplaceContext context;
    gboolean extend_selection;
    gboolean in_all_documents;
} FindReplaceOptions;

/*****************************************************************************/

#define TYPE_FIND_REPLACE		(find_replace_get_type())
#define FIND_REPLACE(obj)		(G_TYPE_CHECK_INSTANCE_CAST((obj), TYPE_FIND_REPLACE, FindReplace))
#define FIND_REPLACE_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST((klass), TYPE_FIND_REPLACE, FindReplaceClass))
#define IS_FIND_REPLACE(obj)  		(G_TYPE_CHECK_INSTANCE_TYPE((obj), TYPE_FIND_REPLACE))
#define IS_FIND_REPLACE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), TYPE_FIND_REPLACE))
#define FIND_REPLACE_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), TYPE_FIND_REPLACE, FindReplaceClass))

typedef struct _FindReplace      	FindReplace;
typedef struct _FindReplaceClass	FindReplaceClass;

struct _FindReplaceClass
{
    GObjectClass parent_class;
    
    void (*options_changed)(FindReplace *find_replace);
};

struct _FindReplace
{
    GObject parent;
    
    FindReplaceOptions options;
};

GType find_replace_get_type(void);

extern FindReplace *find_replace;

FindReplace *find_replace_new(void);

gboolean find_replace_options_valid(const FindReplace *find_replace);

/*****************************************************************************/

#define TYPE_FIND_REPLACE_DIALOG	(find_replace_dialog_get_type ())
#define FIND_REPLACE_DIALOG(obj)	(G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_FIND_REPLACE_DIALOG, FindReplaceDialog))
#define FIND_REPLACE_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_FIND_REPLACE_DIALOG, FindReplaceDialogClass))
#define IS_FIND_REPLACE_DIALOG(obj)  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_FIND_REPLACE_DIALOG))
#define IS_FIND_REPLACE_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_FIND_REPLACE_DIALOG))
#define FIND_REPLACE_DIALOG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_FIND_REPLACE_DIALOG, FindReplaceDialogClass))

typedef struct _FindReplaceDialog      FindReplaceDialog;
typedef struct _FindReplaceDialogClass	FindReplaceDialogClass;

struct _FindReplaceDialogClass
{
    GtkDialogClass parent_class;
    
    void (*options_changed)(FindReplaceDialog *dialog, gpointer *options);
};

struct _FindReplaceDialog
{
    GtkDialog parent;
    
    GtkWidget *search;
    
    GtkCheckButton *match_whole_words;
    GtkCheckButton *match_case;
    
    GtkRadioButton *up;
    GtkRadioButton *down;
    
    GtkButton *find_next;
    GtkButton *cancel;
    
    struct _FindReplaceOptions *options;
};

GType find_replace_dialog_get_type(void);

/*****************************************************************************/

#define TYPE_FIND_DIALOG	(find_dialog_get_type())
#define FIND_DIALOG(obj)	(G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_FIND_DIALOG, FindDialog))
#define FIND_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_FIND_DIALOG, FindDialogClass))
#define IS_FIND_DIALOG(obj)  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_FIND_DIALOG))
#define IS_FIND_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_FIND_DIALOG))
#define FIND_DIALOG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_FIND_DIALOG, FindDialogClass))

typedef struct _FindDialog	FindDialog;
typedef struct _FindDialogClass FindDialogClass;

struct _FindDialogClass
{
    FindReplaceDialogClass parent_class;
};

struct _FindDialog
{
    FindReplaceDialog parent;
    
    GtkCheckButton *wrap_searches;
    
    GtkCheckButton *extend_selection;
};

GType find_dialog_get_type(void);
GtkWidget* find_dialog_new(GtkWindow *parent);

/*****************************************************************************/

#define TYPE_REPLACE_DIALOG	(replace_dialog_get_type())
#define REPLACE_DIALOG(obj)	(G_TYPE_CHECK_INSTANCE_CAST((obj), TYPE_REPLACE_DIALOG, ReplaceDialog))
#define REPLACE_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), TYPE_REPLACE_DIALOG, ReplaceDialogClass))
#define IS_REPLACE_DIALOG(obj)	     (G_TYPE_CHECK_INSTANCE_TYPE((obj), TYPE_REPLACE_DIALOG))
#define IS_REPLACE_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), TYPE_REPLACE_DIALOG))
#define REPLACE_DIALOG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_REPLACE_DIALOG, ReplaceDialogClass))

typedef struct _ReplaceDialog	   ReplaceDialog;
typedef struct _ReplaceDialogClass  ReplaceDialogClass;

struct _ReplaceDialogClass
{
    FindReplaceDialogClass parent_class;
};

struct _ReplaceDialog
{
    FindReplaceDialog parent;
    
    GtkWidget *replace;
    
    GtkRadioButton *selection;
    GtkRadioButton *this_buffer;
    GtkRadioButton *all_buffers;

    GtkButton *replace_next;
    GtkButton *replace_all;
};

GType replace_dialog_get_type(void);
GtkWidget* replace_dialog_new(GtkWindow *parent);

/*****************************************************************************/

void find_next(void);
void find_previous(void);
void replace_next(void);
	
G_END_DECLS
#endif
