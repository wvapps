/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "status_bar.h"
#include "zen.h"

#include <glib/gprintf.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkframe.h>
#include <gtk/gtkhbox.h>

static GtkWidget *status_bar;
static GtkLabel *status_bar_line_num, *status_bar_column_num;
static GtkBin *status_bar_num_bin;

GtkWidget *status_bar_new(GtkWindow *window)
{
    GtkWidget *hbox, *num_hbox;
    GtkWidget *frame, *num_frame;
    
    frame = gtk_frame_new(NULL);
    gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);

    status_bar = gtk_label_new(_("Welcome to " FRIENDLY_NAME));
    gtk_misc_set_alignment(GTK_MISC(status_bar), 0, 0.5);
    gtk_misc_set_padding(GTK_MISC(status_bar), 4, 0);
    gtk_container_add(GTK_CONTAINER(frame), status_bar);
    
    num_frame = gtk_frame_new(NULL);
    gtk_frame_set_shadow_type(GTK_FRAME(num_frame), GTK_SHADOW_IN);

    status_bar_line_num = GTK_LABEL(gtk_label_new("0"));
    status_bar_column_num = GTK_LABEL(gtk_label_new("0"));
    
    num_hbox = gtk_hbox_new(FALSE, 2);
    gtk_box_pack_start(GTK_BOX(num_hbox), GTK_WIDGET(status_bar_line_num), FALSE, FALSE, 2);
    gtk_box_pack_start_defaults(GTK_BOX(num_hbox), gtk_label_new(":"));
    gtk_box_pack_start(GTK_BOX(num_hbox), GTK_WIDGET(status_bar_column_num), FALSE, FALSE, 2);
    gtk_container_add(GTK_CONTAINER(num_frame), num_hbox);
    
    hbox = gtk_hbox_new(FALSE, 4);
    gtk_box_pack_start(GTK_BOX(hbox), frame, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), num_frame, FALSE, FALSE, 0);

    gtk_widget_show_all(hbox);
    
    status_bar_num_bin = GTK_BIN(num_frame);
    
    return hbox;
}

void status_bar_set(const gchar *format, ...)
{
    gchar text[1024];
    va_list arg_list;

    va_start(arg_list, format);
    g_vsprintf(text, format, arg_list);
    va_end(arg_list);

    gtk_label_set_text(GTK_LABEL(status_bar), text);
}

void status_bar_set_line_num(gint line_num)
{
    gchar text[10];
    
    g_sprintf(text, "%d", line_num+1);

    gtk_label_set_text(status_bar_line_num, text);
}

void status_bar_set_column_num(gint column_num)
{
    gchar text[10];
    
    g_sprintf(text, "%d", column_num+1);

    gtk_label_set_text(status_bar_column_num, text);
}

void status_bar_show_numbers(void)
{
    gtk_widget_show_all(GTK_WIDGET(status_bar_num_bin));
}

void status_bar_hide_numbers(void)
{
    gtk_widget_hide_all(GTK_WIDGET(status_bar_num_bin));
}
