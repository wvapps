/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "prefs_file_type_options.h"

#include "zen.h"
#include "prefs.h"

#include <glib/gprintf.h>
#include <gconf/gconf-client.h>

static void prefs_file_type_options_finalize(GObject *_self)
{
    PrefsFileTypeOptions *self = PREFS_FILE_TYPE_OPTIONS(_self);

    g_free(self->file_type);
}

static void prefs_file_type_options_apply_cb(PrefsOptions *_self)
{
    PrefsFileTypeOptions *self = PREFS_FILE_TYPE_OPTIONS(_self);

    prefs_file_type_set_font_name(self->file_type,
    	gtk_font_button_get_font_name(GTK_FONT_BUTTON(self->font_sel))); 

    prefs_file_type_set_use_tabs(self->file_type,
    	gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(self->use_tabs_checkbutton))); 

    prefs_file_type_set_tab_size(self->file_type,
    	(gint) gtk_spin_button_get_value(GTK_SPIN_BUTTON(self->tab_entry))); 

    prefs_file_type_set_indent_size(self->file_type,
    	(gint) gtk_spin_button_get_value(GTK_SPIN_BUTTON(self->indent_entry))); 
}

static void prefs_file_type_options_class_init(PrefsFileTypeOptionsClass *class)
{
    G_OBJECT_CLASS(class)->finalize = prefs_file_type_options_finalize;
    
    PREFS_OPTIONS_CLASS(class)->apply = prefs_file_type_options_apply_cb;
}

GType prefs_file_type_options_get_type (void)
{
    static GType type = 0;
    
    if (!type)
    {
    	static const GTypeInfo info =
    	{
    	    sizeof(PrefsFileTypeOptionsClass),
 	    NULL, /* base_init */
    	    NULL, /* base_finalize */
    	    (GClassInitFunc) prefs_file_type_options_class_init,
    	    NULL, /* class_finalize */
    	    NULL, /* class_data */
    	    sizeof(PrefsFileTypeOptions),
    	    0,	  /* n_preallocs */
    	    NULL  /* (GInstanceInitFunc) */
    	};
        
    	type = g_type_register_static (TYPE_PREFS_OPTIONS,
	    	"PrefsFileTypeOptions", &info, 0);
    }
    
    return type;
}

GtkWidget *prefs_file_type_options_new(const gchar *file_type)
{
    PrefsFileTypeOptions *self = g_object_new(TYPE_PREFS_FILE_TYPE_OPTIONS, NULL);
    int row = 0;

    self->file_type = g_strdup(file_type);

    gtk_table_resize(GTK_TABLE(self), 3, 2);
    gtk_table_set_homogeneous(GTK_TABLE(self), FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(self), 4);
    gtk_table_set_col_spacings(GTK_TABLE(self), 4);
    
    self->font_sel = gtk_font_button_new_with_font(prefs_file_type_get_font_name(file_type));
    g_signal_connect_swapped(G_OBJECT(self->font_sel), "font-set",
    	    GTK_SIGNAL_FUNC(prefs_options_changed), self);
    prefs_options_add_row(PREFS_OPTIONS(self), "Font:",
    	    self->font_sel, &row);
    
    self->tab_entry = gtk_spin_button_new_with_range(2,8,1);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(self->tab_entry),
	    prefs_file_type_get_tab_size(file_type));
    g_signal_connect_swapped(G_OBJECT(self->tab_entry), "value-changed",
    	    GTK_SIGNAL_FUNC(prefs_options_changed), self);
    prefs_options_add_row(PREFS_OPTIONS(self), _("Tab Size:"),
    	    self->tab_entry, &row);
    
    self->use_tabs_checkbutton = gtk_check_button_new_with_label("Use tabs to indent");
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(self->use_tabs_checkbutton),
	    prefs_file_type_get_use_tabs(file_type));
    g_signal_connect_swapped(G_OBJECT(self->use_tabs_checkbutton), "toggled",
    	    GTK_SIGNAL_FUNC(prefs_options_changed), self);
    prefs_options_add_row(PREFS_OPTIONS(self), _(""),
    	    self->use_tabs_checkbutton, &row);
    
    self->indent_entry = gtk_spin_button_new_with_range(2,8,1);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(self->indent_entry),
	    prefs_file_type_get_indent_size(file_type));
    g_signal_connect_swapped(G_OBJECT(self->indent_entry), "value-changed",
    	    GTK_SIGNAL_FUNC(prefs_options_changed), self);
    prefs_options_add_row(PREFS_OPTIONS(self), _("Indent Size:"),
    	    self->indent_entry, &row);
    
    return GTK_WIDGET(self);
}
