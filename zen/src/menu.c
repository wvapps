/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "menu.h"

#include "zen.h"
#include "cmd.h"
#include "file_list.h"
#include "buffer.h"
#include "make.h"
#include "make_cmd.h"
#include "find_in_files_cmd.h"
#include "find.h"

#include <gtk/gtk.h>

static GtkActionEntry menu_entries[] = {
  { "FileMenu", NULL, "_File" },
  { "EditMenu", NULL, "_Edit" },
  { "SearchMenu", NULL, "_Search" },
  { "RunMenu", NULL, "_Run" },
  { "ViewMenu", NULL, "_View" },
  { "HelpMenu", NULL, "_Help" }
};

static GtkActionEntry global_entries[] = {
  { "AboutDlg", NULL, _("_About"), NULL, _("Copyright and License Information"), cmd_about_dlg },
  { "ManDlg", GTK_STOCK_FIND, _("_Man Page..."), "F1", _("Fetch man page"), cmd_man_dlg },
  { "MakeDlg", GTK_STOCK_EXECUTE, _("_Make..."), "<control><shift>M", _("Run make, capturing output"), cmd_make_dlg },
  { "FindInFilesDlg", GTK_STOCK_FIND, _("Find in Files..."), "<control><shift>F", _("Find text in files on disk"), cmd_find_in_files_dlg },
  { "MaximizeView", NULL, _("_Maximize"), "<control>1", _("View current file alone"), cmd_maximize_view },
  { "New", GTK_STOCK_NEW, _("_New"), "", _("New file"), cmd_new },
  { "OpenDlg", GTK_STOCK_OPEN, _("_Open..."), "<control>O", _("Open a file"), cmd_open_dlg },
  { "PreferencesDlg", GTK_STOCK_PREFERENCES, _("Pre_ferences..."), NULL, _("Change Zen settings"), cmd_preferences_dlg },
  { "Quit", GTK_STOCK_QUIT, _("_Quit"), "<control>Q", _("Exit Zen"), cmd_quit },
  { "SaveAsDlg", GTK_STOCK_SAVE_AS, _("Save _As..."), "<control><shift>S", _("Save file, specifying name"), cmd_save_as_dlg },
};

static GtkActionEntry have_buffer_entries[] = {
  { "Close", GTK_STOCK_CLOSE, _("_Close"), "<control>W", _("Close current file"), cmd_close },
  { "CloseAll", NULL, _("Close All"), NULL, _("Close all files"), cmd_close_all },
  { "FindDlg", GTK_STOCK_FIND, _("_Find..."), "<control>F", _("Find text in current file"), cmd_find_dlg },
  { "GotoLineDlg", GTK_STOCK_JUMP_TO, _("_Goto Line..."), "<control>G", _("Jump to specified line number"), cmd_goto_line_dlg },
  { "JoinLines", NULL, _("_Join Lines"), "<control>J", _("Join current line and next line"), cmd_join_lines },
  { "DeleteLines", NULL, _("_Delete Lines"), "<control>U", _("Delete line(s) containing cursor or selection"), cmd_delete_lines },
  { "Paste", GTK_STOCK_PASTE, _("_Paste"), "<control>V", _("Paste clipboard contents into file"), cmd_paste },
  { "ReplaceDlg", GTK_STOCK_FIND_AND_REPLACE, _("_Replace..."), "<control>H", _("Replace text in current file"), cmd_replace_dlg },
  { "SaveAsDlg", GTK_STOCK_SAVE_AS, _("Save _As..."), "<control><shift>S", _("Save file, specifying name"), cmd_save_as_dlg },
  { "SaveAll", NULL, _("Save All"), NULL, _("Save all files"), cmd_save_all },
  { "SaveAllAndQuit", NULL, _("Save All and Quit"), "<control><shift>Q", _("Save all files then quit"), cmd_save_all_and_quit },
  { "SelectAll", NULL, _("Select _All"), "<control>A", _("Select entire file contents"), cmd_select_all },
  { "SplitLine", NULL, _("_Split Line"), "<control>K", _("Split current line at cursor"), cmd_split_line },
  { "SplitView", NULL, _("_Split"), "<control>2", _("Split view of current file into two"), cmd_split_view },
};

static GtkActionEntry have_modified_buffer_entries[] = {
  { "Save", GTK_STOCK_SAVE, _("_Save"), "<control>S", _("Save current file"), cmd_save },
  { "Revert", GTK_STOCK_REVERT_TO_SAVED, _("_Revert"), NULL, _("Revert current file to saved content"), cmd_revert },
};

static GtkActionEntry have_selection_entries[] = {
  { "Copy", GTK_STOCK_COPY, _("_Copy"), "<control>C", _("Copy current selection to clipboard"), cmd_copy },
  { "Cut", GTK_STOCK_CUT, _("Cu_t"), "<control>X", _("Cut current selection to clipboard"), cmd_cut },
};

static GtkActionEntry can_undo_entries[] = {
  { "Undo", GTK_STOCK_UNDO, _("_Undo"), "<control>Z", _("Undo last change"), cmd_undo },
};

static GtkActionEntry can_redo_entries[] = {
  { "Redo", GTK_STOCK_REDO, _("_Redo"), "<control>Y", _("Redo last undo"), cmd_redo },
};

static GtkActionEntry can_browse_make_errors_entries[] = {
  { "NextError", NULL, _("_Next error"), "F8", _("Jump to next make error"), cmd_next_error },
  { "PreviousError", NULL, _("_Previous error"), "F7", _("Jump to previous make error"), cmd_previous_error },
};

static GtkActionEntry can_browse_find_in_files_results_entries[] = {
  { "FindNextInFiles", NULL, _("Find Next in Files"), "<control><shift>N", _("Find next occurance of text in files"), cmd_find_next_in_files },
  { "FindPreviousInFiles", NULL, _("Find Previous in Files"), "<control><shift>P", _("Find previous occurance of text in files"), cmd_find_previous_in_files },
};

static GtkActionEntry can_find_replace_next_previous_entries[] = {
  { "FindNext", NULL, _("Find _Next"), "<control>N", _("Find next occurance of text"), cmd_find_next },
  { "FindPrevious", NULL, _("Find _Previous"), "<control>P", _("Find previous occurance of text"), cmd_find_previous },
  { "ReplaceNext", NULL, _("Replace Ne_xt"), "<control>R", _("Replace next occurance of text"), cmd_replace_next },
};

static const gchar *ui_description =
"<ui>"
"  <menubar name='MainMenu'>"
"    <menu action='FileMenu'>"
"      <menuitem action='New'/>"
"      <menuitem action='OpenDlg'/>"
"      <menuitem action='Close'/>"
"      <menuitem action='CloseAll'/>"
"      <separator/>"
"      <menuitem action='Save'/>"
"      <menuitem action='SaveAsDlg'/>"
"      <menuitem action='SaveAll'/>"
"      <menuitem action='Revert'/>"
"      <separator/>"
"      <menuitem action='SaveAllAndQuit'/>"
"      <menuitem action='Quit'/>"
"    </menu>"
"    <menu action='EditMenu'>"
"      <menuitem action='Undo'/>"
"      <menuitem action='Redo'/>"
"      <separator/>"
"      <menuitem action='Cut'/>"
"      <menuitem action='Copy'/>"
"      <menuitem action='Paste'/>"
"      <separator/>"
"      <menuitem action='SelectAll'/>"
"      <separator/>"
"      <menuitem action='JoinLines'/>"
"      <menuitem action='SplitLine'/>"
"      <menuitem action='DeleteLines'/>"
"      <separator/>"
"      <menuitem action='PreferencesDlg'/>"
"    </menu>"
"    <menu action='SearchMenu'>"
"      <menuitem action='FindDlg'/>"
"      <menuitem action='FindNext'/>"
"      <menuitem action='FindPrevious'/>"
"      <separator/>"
"      <menuitem action='FindInFilesDlg'/>"
"      <menuitem action='FindNextInFiles'/>"
"      <menuitem action='FindPreviousInFiles'/>"
"      <separator/>"
"      <menuitem action='ReplaceDlg'/>"
"      <menuitem action='ReplaceNext'/>"
"      <separator/>"
"      <menuitem action='GotoLineDlg'/>"
"    </menu>"
"    <menu action='RunMenu'>"
"      <menuitem action='MakeDlg'/>"
"      <separator/>"
"      <menuitem action='NextError'/>"
"      <menuitem action='PreviousError'/>"
"    </menu>"
"    <menu action='ViewMenu'>"
"      <menuitem action='MaximizeView'/>"
"      <menuitem action='SplitView'/>"
"    </menu>"
"    <menu action='HelpMenu'>"
"      <menuitem action='AboutDlg'/>"
"      <separator/>"
"      <menuitem action='ManDlg'/>"
"    </menu>"
"  </menubar>"
"</ui>";

static GtkActionGroup *have_buffer_action_group;
static GtkActionGroup *have_modified_buffer_action_group;
static GtkActionGroup *have_selection_action_group;
static GtkActionGroup *can_undo_action_group;
static GtkActionGroup *can_redo_action_group;
static GtkActionGroup *can_browse_make_errors_action_group;
static GtkActionGroup *can_browse_find_in_files_results_action_group;
static GtkActionGroup *can_find_replace_next_previous_action_group;

static void file_list_buffer_focus_cb(FileList *file_list, Buffer *buffer,
    	gpointer user_data)
{
    gtk_action_group_set_sensitive(have_buffer_action_group, buffer != NULL);

    gtk_action_group_set_sensitive(have_modified_buffer_action_group,
    	    buffer != NULL && buffer_get_modified(buffer));

    gtk_action_group_set_sensitive(have_selection_action_group,
    	    buffer != NULL && buffer_have_selection(buffer));
    	    
    gtk_action_group_set_sensitive(can_undo_action_group,
    	    buffer != NULL && buffer_can_undo(buffer));
    	    
    gtk_action_group_set_sensitive(can_redo_action_group,
    	    buffer != NULL && buffer_can_redo(buffer));
    	    
    gtk_action_group_set_sensitive(can_find_replace_next_previous_action_group,
    	    buffer != NULL && find_replace_options_valid(find_replace));

    gtk_action_group_set_sensitive(can_browse_make_errors_action_group,
    	    make != NULL && make_can_browse_errors(make));

    gtk_action_group_set_sensitive(can_browse_find_in_files_results_action_group,
    	    find_in_files != NULL && make_can_browse_errors(find_in_files));
}

static void file_list_cur_buffer_modified_changed_cb(FileList *file_list,
    	Buffer *buffer, gpointer user_data)
{
    gtk_action_group_set_sensitive(have_modified_buffer_action_group,
    	    buffer_get_modified(buffer));
}

static void file_list_cur_buffer_selection_changed_cb(FileList *file_list,
    	Buffer *buffer, gpointer user_data)
{
    gtk_action_group_set_sensitive(have_selection_action_group,
    	    buffer_have_selection(buffer));
}

static void file_list_cur_buffer_can_undo_changed_cb(FileList *file_list,
    	Buffer *buffer, gpointer user_data)
{
    gtk_action_group_set_sensitive(can_undo_action_group,
    	    buffer_can_undo(buffer));
}

static void file_list_cur_buffer_can_redo_changed_cb(FileList *file_list,
    	Buffer *buffer, gpointer user_data)
{
    gtk_action_group_set_sensitive(can_redo_action_group,
    	    buffer_can_redo(buffer));
}

static void find_replace_options_changed_cb(FindReplace *find_replace,
    	gpointer user_data)
{
    gtk_action_group_set_sensitive(can_find_replace_next_previous_action_group,
    	    find_replace_options_valid(find_replace));
}

GtkWidget *menu_new(GtkWindow *window)
{
    GtkActionGroup *global_action_group;
    GtkUIManager *ui_manager;
    GtkAccelGroup *accel_group;
    GError *error = NULL;

    ui_manager = gtk_ui_manager_new();

    global_action_group = gtk_action_group_new("GlobalActions");
    gtk_action_group_add_actions(global_action_group, menu_entries,
    	    G_N_ELEMENTS(menu_entries), window);
    gtk_action_group_add_actions(global_action_group, global_entries,
    	    G_N_ELEMENTS(global_entries), window);
    gtk_ui_manager_insert_action_group(ui_manager, global_action_group, 0);
    
    have_buffer_action_group = gtk_action_group_new("HaveBufferActions");
    gtk_action_group_add_actions(have_buffer_action_group, have_buffer_entries,
    	    G_N_ELEMENTS(have_buffer_entries), window);
    gtk_action_group_set_sensitive(have_buffer_action_group, FALSE);
    gtk_ui_manager_insert_action_group(ui_manager, have_buffer_action_group, 0);
    
    have_modified_buffer_action_group = gtk_action_group_new("HaveModifiedBufferActions");
    gtk_action_group_add_actions(have_modified_buffer_action_group,
    	    have_modified_buffer_entries,
    	    G_N_ELEMENTS(have_modified_buffer_entries), window);
    gtk_action_group_set_sensitive(have_modified_buffer_action_group, FALSE);
    gtk_ui_manager_insert_action_group(ui_manager,
    	    have_modified_buffer_action_group, 0);
    
    have_selection_action_group = gtk_action_group_new("HaveSelectionActions");
    gtk_action_group_add_actions(have_selection_action_group,
    	    have_selection_entries,
    	    G_N_ELEMENTS(have_selection_entries), window);
    gtk_action_group_set_sensitive(have_selection_action_group, FALSE);
    gtk_ui_manager_insert_action_group(ui_manager,
    	    have_selection_action_group, 0);
    
    can_undo_action_group = gtk_action_group_new("HaveUndoActions");
    gtk_action_group_add_actions(can_undo_action_group,
    	    can_undo_entries,
    	    G_N_ELEMENTS(can_undo_entries), window);
    gtk_action_group_set_sensitive(can_undo_action_group, FALSE);
    gtk_ui_manager_insert_action_group(ui_manager,
    	    can_undo_action_group, 0);
    
    can_redo_action_group = gtk_action_group_new("HaveRedoActions");
    gtk_action_group_add_actions(can_redo_action_group,
    	    can_redo_entries,
    	    G_N_ELEMENTS(can_redo_entries), window);
    gtk_action_group_set_sensitive(can_redo_action_group, FALSE);
    gtk_ui_manager_insert_action_group(ui_manager,
    	    can_redo_action_group, 0);
    
    can_browse_make_errors_action_group = gtk_action_group_new("CanBrowseMakeErrorsActions");
    gtk_action_group_add_actions(can_browse_make_errors_action_group,
    	    can_browse_make_errors_entries,
    	    G_N_ELEMENTS(can_browse_make_errors_entries), window);
    gtk_action_group_set_sensitive(can_browse_make_errors_action_group, FALSE);
    gtk_ui_manager_insert_action_group(ui_manager,
    	    can_browse_make_errors_action_group, 0);
    
    can_browse_find_in_files_results_action_group = gtk_action_group_new("CanBrowseFindInFilesResultActions");
    gtk_action_group_add_actions(can_browse_find_in_files_results_action_group,
    	    can_browse_find_in_files_results_entries,
    	    G_N_ELEMENTS(can_browse_find_in_files_results_entries), window);
    gtk_action_group_set_sensitive(can_browse_find_in_files_results_action_group, FALSE);
    gtk_ui_manager_insert_action_group(ui_manager,
    	    can_browse_find_in_files_results_action_group, 0);
    
    can_find_replace_next_previous_action_group =
    	    gtk_action_group_new("CanFindReplaceNextPreviousActions");
    gtk_action_group_add_actions(can_find_replace_next_previous_action_group,
    	    can_find_replace_next_previous_entries,
    	    G_N_ELEMENTS(can_find_replace_next_previous_entries), window);
    gtk_action_group_set_sensitive(can_find_replace_next_previous_action_group, FALSE);
    gtk_ui_manager_insert_action_group(ui_manager,
    	    can_find_replace_next_previous_action_group, 0);
    
    accel_group = gtk_ui_manager_get_accel_group(ui_manager);
    gtk_window_add_accel_group(window, accel_group);
    
    if (!gtk_ui_manager_add_ui_from_string(ui_manager, ui_description, -1, &error))
    {
    	consume_error(error);
    	return NULL;
    }
    
    g_signal_connect(G_OBJECT(file_list), "buffer-focus",
    	    GTK_SIGNAL_FUNC(file_list_buffer_focus_cb), NULL);
    g_signal_connect(G_OBJECT(file_list), "cur-buffer-modified-changed",
    	    GTK_SIGNAL_FUNC(file_list_cur_buffer_modified_changed_cb), NULL);
    g_signal_connect(G_OBJECT(file_list), "cur-buffer-selection-changed",
    	    GTK_SIGNAL_FUNC(file_list_cur_buffer_selection_changed_cb), NULL);
    g_signal_connect(G_OBJECT(file_list), "cur-buffer-can-undo-changed",
    	    GTK_SIGNAL_FUNC(file_list_cur_buffer_can_undo_changed_cb), NULL);
    g_signal_connect(G_OBJECT(file_list), "cur-buffer-can-redo-changed",
    	    GTK_SIGNAL_FUNC(file_list_cur_buffer_can_redo_changed_cb), NULL);
//    g_signal_connect(G_OBJECT(make), "can-browse-errors-changed",
//    	    GTK_SIGNAL_FUNC(make_can_browse_errors_changed_cb), NULL);
    g_signal_connect(G_OBJECT(find_replace), "options-changed",
    	    GTK_SIGNAL_FUNC(find_replace_options_changed_cb), NULL);

    return gtk_ui_manager_get_widget(ui_manager, "/MainMenu");
}
