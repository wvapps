/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "prefs_options.h"

#include <glib/gprintf.h>

enum {
    CHANGED_SIGNAL,
    LAST_SIGNAL
};

static guint prefs_options_signals[LAST_SIGNAL] = { 0 };

static void prefs_options_class_init(PrefsOptionsClass *class)
{
    class->apply = NULL;

    prefs_options_signals[CHANGED_SIGNAL] =
	    g_signal_new("changed",
			  G_TYPE_FROM_CLASS(class),
			  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			  G_STRUCT_OFFSET(PrefsOptionsClass, changed),
			  NULL, NULL,
			  g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}

GType prefs_options_get_type (void)
{
    static GType type = 0;
    
    if (!type)
    {
    	static const GTypeInfo info =
    	{
    	    sizeof(PrefsOptionsClass),
 	    NULL, /* base_init */
    	    NULL, /* base_finalize */
    	    (GClassInitFunc) prefs_options_class_init,
    	    NULL, /* class_finalize */
    	    NULL, /* class_data */
    	    sizeof(PrefsOptions),
    	    0,	  /* n_preallocs */
    	    NULL, /* instance_init */
    	};
        
    	type = g_type_register_static (GTK_TYPE_TABLE,
	    	"PrefsOptions", &info, 0);
    }
    
    return type;
}

void prefs_options_apply(PrefsOptions *self)
{
    PREFS_OPTIONS_GET_CLASS(self)->apply(self);
}

void prefs_options_changed(PrefsOptions *self)
{
    prefs_options_apply(self);

    g_signal_emit(G_OBJECT(self), prefs_options_signals[CHANGED_SIGNAL], 0);
}

void prefs_options_add_row(PrefsOptions *self,
    	const gchar *desc, GtkWidget *widget, int *row)
{
    GtkWidget *lalign = gtk_alignment_new(0, 0.5, 0, 0);
    GtkWidget *ralign = gtk_alignment_new(1, 0.5, 0, 0);
    
    if (desc != NULL)
    {
    	gtk_container_add(GTK_CONTAINER(ralign), gtk_label_new(desc));
    	gtk_container_add(GTK_CONTAINER(lalign), widget);
        
    	gtk_table_attach(GTK_TABLE(self), ralign,
    	    	0, 1, *row, *row+1, GTK_SHRINK | GTK_FILL, GTK_SHRINK | GTK_FILL, 0, 0);
    	gtk_table_attach(GTK_TABLE(self), lalign,
    	    	1, 2, *row, *row+1, GTK_SHRINK | GTK_FILL, GTK_SHRINK | GTK_FILL, 0, 0);
    }
    else
    {
    	gtk_container_add(GTK_CONTAINER(lalign), widget);
        
    	gtk_table_attach(GTK_TABLE(self), lalign,
    	    	0, 2, *row, *row+1, GTK_SHRINK | GTK_FILL, GTK_SHRINK | GTK_FILL, 0, 0);
    }
    	    
    ++*row;
}

