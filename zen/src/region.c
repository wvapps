/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "region.h"

Region *region_new(Buffer *buffer, const Iter *start, const Iter *end)
{
    Region *region = g_malloc(sizeof(Region));
    
    region->buffer = buffer;
    region->start_mark = gtk_text_buffer_create_mark(GTK_TEXT_BUFFER(buffer), "region-start", start, TRUE);
    region->end_mark = gtk_text_buffer_create_mark(GTK_TEXT_BUFFER(buffer), "region-end", end, FALSE);
    
    return region;
    
}

void region_get_bounds(Region *region, Iter *start, Iter *end)
{
    gtk_text_buffer_get_iter_at_mark(GTK_TEXT_BUFFER(region->buffer), start, region->start_mark); 
    gtk_text_buffer_get_iter_at_mark(GTK_TEXT_BUFFER(region->buffer), end, region->end_mark); 
}

void region_destroy(Region *region)
{
    gtk_text_buffer_delete_mark(GTK_TEXT_BUFFER(region->buffer), region->end_mark);
    gtk_text_buffer_delete_mark(GTK_TEXT_BUFFER(region->buffer), region->start_mark);
    
    g_free(region);
}
