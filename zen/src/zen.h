/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef ZED_H
#define ZED_H
#include <gtk/gtkwidget.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnome/gnome-i18n.h>

#define PACKAGE_NAME	"zen"

#define FRIENDLY_NAME	"Zen"
#define VERSION_NAME	"1.0.1"
#define AUTHORS 	_("By Peter Zion")
#define COPYRIGHT	_("Copyright 2004-2005 Net Integration Technologies")
#define LICENSE     	_("Licensed under the GPL")

#define CONF_PATH(key) "/apps/" PACKAGE_NAME "/" key

extern GtkWidget *app;
extern GtkWidget *file_list;
extern GtkWidget *paned;

void consume_error(GError *error);
gboolean gnome_vfs_error(GError **error, GnomeVFSResult result,
    	const char *uri);

#endif
