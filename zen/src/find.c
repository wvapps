/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "zen.h"
#include "find.h"
#include "buffer.h"
#include "region.h"
#include "view.h"
#include "status_bar.h"
#include "file_list.h"

#include <glib.h>
#include <gnome.h>
#include <gtksourceview/gtksourceview.h>
#include <gtksourceview/gtksourceiter.h>
#include <gconf/gconf-client.h>
#include <gtk/gtkcombobox.h>

enum {
  OPTIONS_CHANGED_SIGNAL,
  LAST_SIGNAL
};

static guint find_replace_dialog_signals[LAST_SIGNAL] = { 0 };

FindReplace *find_replace;

void find_load_conf(FindReplaceOptions *options, const gchar *key);
void find_save_conf(const FindReplaceOptions *options, const gchar *key);

static gboolean find(Buffer *buffer, const FindReplaceOptions *options,
	const Iter *_start, const Iter *_end, gboolean allow_extend)
{
    Iter match_start, match_end;
    gboolean (*search_func)(const Iter *iter,
			     const gchar *str,
			     GtkSourceSearchFlags flags,
			     Iter *match_start,
			     Iter *match_end,
			     const Iter *limit);
    GtkSourceSearchFlags flags = GTK_SOURCE_SEARCH_VISIBLE_ONLY | GTK_SOURCE_SEARCH_TEXT_ONLY;
    gboolean result = FALSE;
    gint direction_multiplier;
    Iter start = *_start, end = *_end;

    switch (options->direction)
    {
	case FIND_DIRECTION_DOWN:
	    search_func = gtk_source_iter_forward_search;
	    direction_multiplier = 1;	 
	    break;
		
	case FIND_DIRECTION_UP:
	    search_func = gtk_source_iter_backward_search;
	    direction_multiplier = -1;
	    break;
    }
		    
    if (!options->match_case)
	    flags |= GTK_SOURCE_SEARCH_CASE_INSENSITIVE;

    while (direction_multiplier * gtk_text_iter_compare(&start, &end) < 0)
		    
    {
	gboolean found = search_func(&start, options->search_text,
		flags, &match_start, &match_end, &end);
			
	if (found && (!options->match_whole_words
		|| (iter_starts_word(&match_start)
			&& iter_ends_word(&match_end))))
	{
	    result = TRUE;
	    break;
	}
	
	if (found)
	{
	    if (options->direction == FIND_DIRECTION_UP)
		start = match_start;
	    else
		start = match_end;
	}
	else break;
    }
		    
    if (result)
    {
	if (allow_extend && options->extend_selection)
	{
	    Iter sel_start, sel_end;

	    gtk_text_buffer_get_selection_bounds(GTK_TEXT_BUFFER(buffer), &sel_start, &sel_end); 
	    if (gtk_text_iter_compare(&sel_start, &match_start) < 0)
		match_start = sel_start;
	    if (gtk_text_iter_compare(&sel_end, &match_end) > 0)
		match_end = sel_end;
	}
	buffer_select(buffer, &match_start, &match_end);
    }
    
    return result;
}

static GSList *history_list_prepend(GSList *history_list, const gchar *item,
	gint max_history_list_len)
{
    GSList *p;
    gint i;

    p = history_list;
    i = 0;
    while (p != NULL)
    {
	gchar *old_item = (gchar *)p->data;

	if (i >= max_history_list_len
		|| g_str_equal(item, old_item))
	{
	    p = history_list = g_slist_delete_link(history_list, p);
	    g_free(old_item);
	}
	else
	{
	    p = g_slist_next(p);
	    ++i;
	}
    }

    return g_slist_prepend(history_list, g_strdup(item));
} 

const gint max_num_old_search_texts = 16;
const gint max_num_old_replace_texts = 16;

gboolean find_next_in_dir(const FindReplaceOptions *options, Buffer *buffer)
{
    Iter buf_start, buf_end;
    Iter sel_start, sel_end;

    if (buffer == NULL)
    {
	g_warning("find_next_in_dir: buffer == NULL");
	return FALSE;
    }

    gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(buffer), &buf_start, &buf_end);
    gtk_text_buffer_get_selection_bounds(GTK_TEXT_BUFFER(buffer), &sel_start, &sel_end);

    if (options->direction == FIND_DIRECTION_UP)
    {
	iter_swap(&buf_start, &buf_end);
	iter_swap(&sel_start, &sel_end);
    }
    
    if (!find(buffer, options, &sel_end, &buf_end, TRUE))
    {
	status_bar_set(_("Search wrapped"));
	if (!find(buffer, options, &buf_start, &sel_start, TRUE))
	{
	    status_bar_set(_("Not found!"));
	    return FALSE;
	}
	else return TRUE;	  
    }
    else return TRUE;
}
    
static void search_text_changed_cb(GtkEntry *entry, gpointer _dialog)
{
    FindReplaceDialog *dialog = (FindReplaceDialog *)_dialog;
    FindReplaceOptions *options = dialog->options;
    const gchar *text = gtk_entry_get_text(entry);

    if (strcmp(options->search_text, text) != 0)
    {
	g_free(options->search_text);
	options->search_text = g_strdup(text);
    
	g_signal_emit(G_OBJECT(dialog), find_replace_dialog_signals[OPTIONS_CHANGED_SIGNAL], 0, options);
    }
}

static void replace_text_changed_cb(GtkEntry *entry, gpointer _dialog)
{
    ReplaceDialog *dialog = (ReplaceDialog *)_dialog;
    FindReplaceOptions *options = FIND_REPLACE_DIALOG(dialog)->options;
    const gchar *text = gtk_entry_get_text(GTK_ENTRY(entry));

    if (strcmp(options->replace_text, text) != 0)
    {
	g_free(options->replace_text);
	options->replace_text = g_strdup(text);
	    
	g_signal_emit(G_OBJECT(dialog), find_replace_dialog_signals[OPTIONS_CHANGED_SIGNAL], 0, options);
    }
}

static void find_replace_dialog_set_items_from_options(FindReplaceDialog *dialog)
{
    const FindReplaceOptions *options = dialog->options;
    Buffer *buffer = get_cur_buffer();

    gtk_entry_set_text(GTK_ENTRY(GTK_BIN(dialog->search)->child), options->search_text);
			    
    gtk_widget_set_sensitive(GTK_WIDGET(dialog->match_whole_words),
	    options->search_text == NULL || buffer_is_word(buffer, options->search_text));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dialog->match_whole_words), options->match_whole_words);
    
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dialog->match_case), options->match_case);
	
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dialog->up), options->direction == FIND_DIRECTION_UP);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dialog->down), options->direction == FIND_DIRECTION_DOWN);

    gtk_widget_set_sensitive(GTK_WIDGET(dialog->find_next), options->search_text[0] != '\0');
}

static guint find_replace_signals[LAST_SIGNAL] = { 0 };

static void find_replace_dialog_options_changed_cb(FindDialog *dialog, gpointer _options, gpointer user_data)
{
    find_replace_dialog_set_items_from_options(FIND_REPLACE_DIALOG(dialog));
    
    g_signal_emit(G_OBJECT(find_replace),
    	    find_replace_signals[OPTIONS_CHANGED_SIGNAL], 0);
}

static void find_dialog_set_items_from_options(FindDialog *dialog)
{
    const FindReplaceOptions *options = FIND_REPLACE_DIALOG(dialog)->options;

    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dialog->wrap_searches), options->wrap_searches);

    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dialog->extend_selection), options->extend_selection);
}

static void find_dialog_options_changed_cb(FindDialog *dialog, gpointer _options, gpointer user_data)
{
    find_dialog_set_items_from_options(dialog);
}

static void replace_dialog_set_items_from_options(ReplaceDialog *dialog)
{
    const FindReplaceOptions *options = FIND_REPLACE_DIALOG(dialog)->options;
    Buffer *buffer = get_cur_buffer();
    gboolean have_selection = gtk_text_buffer_get_selection_bounds(GTK_TEXT_BUFFER(buffer), NULL, NULL);

    gtk_entry_set_text(GTK_ENTRY(GTK_BIN(dialog->replace)->child), options->replace_text);
			    
#ifdef USE_CONTEXT_SELECTION
    gtk_widget_set_sensitive(GTK_WIDGET(dialog->selection), have_selection);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dialog->selection), options->context == REPLACE_CONTEXT_SELECTION);
#endif    
    gtk_widget_set_sensitive(GTK_WIDGET(dialog->this_buffer), TRUE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dialog->this_buffer), options->context == REPLACE_CONTEXT_THIS_BUFFER);
    gtk_widget_set_sensitive(GTK_WIDGET(dialog->all_buffers), TRUE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dialog->all_buffers), options->context == REPLACE_CONTEXT_ALL_BUFFERS);
	
    gtk_widget_set_sensitive(GTK_WIDGET(dialog->replace_next), options->search_text[0] != '\0' && have_selection);
    gtk_widget_set_sensitive(GTK_WIDGET(dialog->replace_all), options->search_text[0] != '\0');
}

static void replace_dialog_options_changed_cb(ReplaceDialog *dialog, gpointer _options, gpointer user_data)
{
    replace_dialog_set_items_from_options(dialog);
}

static GtkWidget *build_find_replace_dialog_search_text(FindReplaceDialog *dialog)
{
    GtkWidget *label = gtk_label_new(NULL);
    GtkWidget *hbox = gtk_hbox_new(FALSE, 8);

    dialog->search = gtk_combo_box_entry_new_text();
    gtk_entry_set_activates_default(GTK_ENTRY(GTK_BIN(dialog->search)->child), TRUE);
    g_signal_connect(GTK_OBJECT(GTK_BIN(dialog->search)->child), "changed",
		G_CALLBACK(search_text_changed_cb), dialog);

    gtk_label_set_markup_with_mnemonic(GTK_LABEL(label), _("Find what:"));
    gtk_label_set_mnemonic_widget(GTK_LABEL(label), dialog->search);

    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), dialog->search, TRUE, TRUE, 0);

    return hbox;
}

static GtkWidget *build_replace_dialog_replace_text(ReplaceDialog *dialog)
{
    GtkWidget *label = gtk_label_new(NULL);
    GtkWidget *hbox = gtk_hbox_new(FALSE, 8);

    gtk_entry_set_activates_default(GTK_ENTRY(GTK_BIN(FIND_REPLACE_DIALOG(dialog)->search)->child), FALSE);

    dialog->replace = gtk_combo_box_entry_new_text();
    gtk_entry_set_activates_default(GTK_ENTRY(GTK_BIN(dialog->replace)->child), TRUE);
    g_signal_connect(GTK_OBJECT(GTK_BIN(dialog->replace)->child), "changed",
		G_CALLBACK(replace_text_changed_cb), dialog);

    gtk_label_set_markup_with_mnemonic(GTK_LABEL(label), _("Replace with:"));
    gtk_label_set_mnemonic_widget(GTK_LABEL(label), GTK_WIDGET(dialog->replace));

    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(dialog->replace), TRUE, TRUE, 0);

    return hbox;
}

static void match_whole_words_button_toggle_cb(GtkToggleButton *toggle_button, gpointer _dialog)
{
    FindReplaceDialog *dialog = (FindReplaceDialog *)_dialog;

    dialog->options->match_whole_words = gtk_toggle_button_get_active(toggle_button);
    
    g_signal_emit(G_OBJECT(dialog), find_replace_dialog_signals[OPTIONS_CHANGED_SIGNAL], 0, dialog->options);
}

static void match_case_button_toggle_cb(GtkToggleButton *toggle_button, gpointer _dialog)
{
    FindReplaceDialog *dialog = (FindReplaceDialog *)_dialog;

    dialog->options->match_case = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(dialog->match_case));
    
    g_signal_emit(G_OBJECT(dialog), find_replace_dialog_signals[OPTIONS_CHANGED_SIGNAL], 0, dialog->options);
}

static void wrap_searches_button_toggle_cb(GtkToggleButton *toggle_button, gpointer _dialog)
{
    FindReplaceDialog *dialog = (FindReplaceDialog *)_dialog;

    dialog->options->wrap_searches = gtk_toggle_button_get_active(toggle_button);
    
    g_signal_emit(G_OBJECT(dialog), find_replace_dialog_signals[OPTIONS_CHANGED_SIGNAL], 0, dialog->options);
}

static GtkVBox *build_find_replace_dialog_conditions(FindReplaceDialog *dialog)
{
    GtkVBox *vbox = GTK_VBOX(gtk_vbox_new(FALSE, 2));

    dialog->match_whole_words = GTK_CHECK_BUTTON(gtk_check_button_new_with_mnemonic(_("Match whole words")));
    g_signal_connect(GTK_OBJECT(dialog->match_whole_words), "toggled",
	     G_CALLBACK(match_whole_words_button_toggle_cb), dialog);
    gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(dialog->match_whole_words),
	    FALSE, FALSE, 0);

    dialog->match_case = GTK_CHECK_BUTTON(gtk_check_button_new_with_mnemonic(_("Match case")));
    g_signal_connect(GTK_OBJECT(dialog->match_case), "toggled",
	     G_CALLBACK(match_case_button_toggle_cb), dialog);
    gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(dialog->match_case),
	    FALSE, FALSE, 0);

#if 0
    GtkWidget *regular_expression = gtk_check_button_new_with_mnemonic(_("Regular _expression"));
    g_signal_connect(GTK_OBJECT(regular_expression), "toggled",
	    G_CALLBACK(check_button_toggle_cb), &options->regular_expression);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(regular_expression), options->regular_expression);
    gtk_box_pack_start(GTK_BOX(vbox), regular_expression,
	    FALSE, FALSE, 0);
#endif
    
    return vbox;
}

static GtkWidget *build_find_dialog_conditions(FindDialog *dialog)
{
    GtkWidget *frame = gtk_frame_new(_("Conditions:"));
    GtkVBox *vbox = build_find_replace_dialog_conditions(FIND_REPLACE_DIALOG(dialog));

    dialog->wrap_searches =
    	    GTK_CHECK_BUTTON(gtk_check_button_new_with_mnemonic(_("Wrap _searches")));
    g_signal_connect(GTK_OBJECT(dialog->wrap_searches), "toggled",
	    G_CALLBACK(wrap_searches_button_toggle_cb), dialog);
    gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(dialog->wrap_searches),
	    FALSE, FALSE, 0);
    
    gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_ETCHED_IN);
    gtk_container_add(GTK_CONTAINER(frame), GTK_WIDGET(vbox));
    
    return frame;
}

static GtkWidget *build_replace_dialog_conditions(ReplaceDialog *dialog)
{
    GtkWidget *frame = gtk_frame_new(_("Conditions:"));
    GtkVBox *vbox = build_find_replace_dialog_conditions(FIND_REPLACE_DIALOG(dialog));

    gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_ETCHED_IN);
    gtk_container_add(GTK_CONTAINER(frame), GTK_WIDGET(vbox));
    
    return frame;
}

static void direction_toggled_cb(GtkRadioButton *radio_button, gpointer _dialog)
{
    FindReplaceDialog *dialog = (FindReplaceDialog *)_dialog;

    dialog->options->direction =
	    gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(dialog->up))?
		    FIND_DIRECTION_UP:
		    FIND_DIRECTION_DOWN;
		    
    g_signal_emit(G_OBJECT(dialog), find_replace_dialog_signals[OPTIONS_CHANGED_SIGNAL], 0, dialog->options);
}

static GtkWidget *build_find_replace_dialog_direction(FindReplaceDialog *dialog)
{
    GtkWidget *vbox = gtk_vbox_new(FALSE, 2);
    GtkWidget *frame = gtk_frame_new(_("Direction:"));

    dialog->up = GTK_RADIO_BUTTON(gtk_radio_button_new_with_mnemonic(NULL, _("Up")));
    g_signal_connect(GTK_OBJECT(dialog->up), "toggled",
	    G_CALLBACK(direction_toggled_cb), dialog);
    gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(dialog->up),
	    FALSE, FALSE, 0);

    dialog->down = GTK_RADIO_BUTTON(gtk_radio_button_new_with_mnemonic_from_widget(dialog->up, _("Down")));
    g_signal_connect(GTK_OBJECT(dialog->down), "toggled",
	    G_CALLBACK(direction_toggled_cb), dialog);
    gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(dialog->down),
	    FALSE, FALSE, 0);

    gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_ETCHED_IN);
    gtk_container_add(GTK_CONTAINER(frame), vbox);

    return frame;
}

static void context_toggled_cb(GtkRadioButton *radio_button, gpointer _dialog)
{
    ReplaceDialog *dialog = (ReplaceDialog *)_dialog;
    FindReplaceOptions *options = FIND_REPLACE_DIALOG(dialog)->options;

#ifdef USE_CONTEXT_SELECTION
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(dialog->selection)))
	options->context = REPLACE_CONTEXT_SELECTION;
    else
#endif
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(dialog->this_buffer)))
	options->context = REPLACE_CONTEXT_THIS_BUFFER;
    else
	options->context = REPLACE_CONTEXT_ALL_BUFFERS;
		    
    g_signal_emit(G_OBJECT(dialog), find_replace_dialog_signals[OPTIONS_CHANGED_SIGNAL], 0, options);
}

static GtkWidget *build_replace_dialog_context(ReplaceDialog *dialog)
{
    GtkWidget *vbox = gtk_vbox_new(FALSE, 2);
    GtkWidget *frame = gtk_frame_new(_("Context:"));

#ifdef USE_CONTEXT_SELECTION
    dialog->selection = GTK_RADIO_BUTTON(gtk_radio_button_new_with_mnemonic(NULL, _("Selection")));
    g_signal_connect(GTK_OBJECT(dialog->selection), "toggled",
	    G_CALLBACK(context_toggled_cb), dialog);
    gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(dialog->selection),
	    FALSE, FALSE, 0);
#endif

#ifdef USE_CONTEXT_SELECTION
    dialog->this_buffer = GTK_RADIO_BUTTON(gtk_radio_button_new_with_mnemonic_from_widget(GTK_RADIO_BUTTON(dialog->selection), _("This buffer")));
#else
    dialog->this_buffer = GTK_RADIO_BUTTON(gtk_radio_button_new_with_mnemonic_from_widget(NULL, _("This buffer")));
#endif
    g_signal_connect(GTK_OBJECT(dialog->this_buffer), "toggled",
	    G_CALLBACK(context_toggled_cb), dialog);
    gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(dialog->this_buffer),
	    FALSE, FALSE, 0);

#ifdef USE_CONTEXT_SELECTION
    dialog->all_buffers = GTK_RADIO_BUTTON(gtk_radio_button_new_with_mnemonic_from_widget(GTK_RADIO_BUTTON(dialog->selection), _("All buffers")));
#else
    dialog->all_buffers = GTK_RADIO_BUTTON(gtk_radio_button_new_with_mnemonic_from_widget(GTK_RADIO_BUTTON(dialog->this_buffer), _("All buffers")));
#endif
    g_signal_connect(GTK_OBJECT(dialog->all_buffers), "toggled",
	    G_CALLBACK(context_toggled_cb), dialog);
    gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(dialog->all_buffers),
	    FALSE, FALSE, 0);

    gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_ETCHED_IN);
    gtk_container_add(GTK_CONTAINER(frame), vbox);

    return frame;
}

static void extend_selection_button_toggle_cb(GtkToggleButton *toggle_button, gpointer _dialog)
{
    FindDialog *dialog = (FindDialog *)_dialog;
    FindReplaceOptions *options = FIND_REPLACE_DIALOG(dialog)->options;

    options->extend_selection = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(dialog->extend_selection));
    
    g_signal_emit(G_OBJECT(dialog), find_replace_dialog_signals[OPTIONS_CHANGED_SIGNAL], 0, options);
}

static GtkWidget *build_find_dialog_extras(FindDialog *dialog)
{
    GtkWidget *vbox = gtk_vbox_new(FALSE, 2);

    dialog->extend_selection = GTK_CHECK_BUTTON(gtk_check_button_new_with_mnemonic("Extend selection"));
    g_signal_connect(GTK_OBJECT(dialog->extend_selection), "toggled",
	    G_CALLBACK(extend_selection_button_toggle_cb), dialog);
    gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(dialog->extend_selection), FALSE, FALSE, 0);

#if 0
    GtkWidget *in_all_documents = gtk_check_button_new_with_mnemonic(_("In _all documents"));
    g_signal_connect(GTK_OBJECT (in_all_documents), "toggled",
	    G_CALLBACK(check_button_toggle_cb), &options->in_all_documents);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(in_all_documents), options->in_all_documents);
    gtk_box_pack_start(GTK_BOX(vbox), in_all_documents, FALSE, FALSE, 0);
#endif

    return vbox;
}

static GtkWidget *build_find_dialog_direction_and_extras(FindDialog *dialog)
{
    GtkWidget *vbox = gtk_vbox_new(FALSE, 4);

    gtk_box_pack_start(GTK_BOX(vbox), build_find_replace_dialog_direction(FIND_REPLACE_DIALOG(dialog)),
	    FALSE, FALSE, 0);
	    
    gtk_box_pack_start(GTK_BOX(vbox), build_find_dialog_extras(dialog),
	    FALSE, FALSE, 0);

    return vbox;
}

static GtkWidget *build_replace_dialog_direction_and_context(ReplaceDialog *dialog)
{
    GtkWidget *vbox = gtk_vbox_new(FALSE, 4);

    gtk_box_pack_start(GTK_BOX(vbox), build_find_replace_dialog_direction(FIND_REPLACE_DIALOG(dialog)),
	    FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), build_replace_dialog_context(dialog),
	    FALSE, FALSE, 0);

    return vbox;
}

static GtkWidget *build_find_dialog_options(FindDialog *dialog)
{
    GtkWidget *hbox = gtk_hbox_new(FALSE, 4);

    gtk_box_pack_start(GTK_BOX(hbox), build_find_dialog_conditions(dialog),
	    FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), build_find_dialog_direction_and_extras(dialog),
	    FALSE, FALSE, 0);

    return hbox;
}

static GtkWidget *build_replace_dialog_options(ReplaceDialog *dialog)
{
    GtkWidget *hbox = gtk_hbox_new(FALSE, 4);

    gtk_box_pack_start(GTK_BOX(hbox), build_replace_dialog_conditions(dialog),
	    TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), build_replace_dialog_direction_and_context(dialog),
	    TRUE, TRUE, 0);

    return hbox;
}

enum
{
    RESPONSE_FIND_NEXT,
    RESPONSE_REPLACE_NEXT,
    RESPONSE_REPLACE_ALL
};

static void find_replace_dialog_response_cb(FindReplaceDialog *dialog,
	gint response, gpointer user_data)
{
    FindReplaceOptions *options = dialog->options;
    Buffer *buffer = get_cur_buffer();

    switch (response)
    {
	case RESPONSE_FIND_NEXT:
	case GTK_RESPONSE_OK:
	    options->old_search_texts =
		    history_list_prepend(options->old_search_texts,
			options->search_text, max_num_old_search_texts);
    	    gtk_combo_box_prepend_text(GTK_COMBO_BOX(dialog->search),
    	    	    options->search_text);

	    find_next_in_dir(options, buffer);
	    
	    break;

	case GTK_RESPONSE_CLOSE:
	case GTK_RESPONSE_CANCEL:
	case GTK_RESPONSE_REJECT:
	    gtk_widget_destroy(GTK_WIDGET(dialog));
	case GTK_RESPONSE_DELETE_EVENT:
	    break;

	default:
	    g_warning("find_replace_dialog_response_cb: received unexpected response %d (dialog=%p)", response, dialog);
	    break;
    }
}

static gint replace_all_in_range(Buffer *buffer, FindReplaceOptions *options,
    	Iter *start, Iter *end)
{
    Mark *end_mark = gtk_text_buffer_create_mark(GTK_TEXT_BUFFER(buffer),
    	    "replace-end", end, FALSE);
    gint occurances = 0;
    
    gtk_text_buffer_begin_user_action(GTK_TEXT_BUFFER(buffer));
    while (find(buffer, options, start, end, TRUE))
    {
	buffer_replace_selection(buffer, options->replace_text, -1);
	++occurances;
		    
	gtk_text_buffer_get_selection_bounds(GTK_TEXT_BUFFER(buffer),
	    	NULL, start);
		
	gtk_text_buffer_get_iter_at_mark(GTK_TEXT_BUFFER(buffer),
	    	end, end_mark); 
    }
    gtk_text_buffer_end_user_action(GTK_TEXT_BUFFER(buffer));
	    
    gtk_text_buffer_delete_mark(GTK_TEXT_BUFFER(buffer), end_mark);
    
    return occurances;
}

static void replace_dialog_response_cb(ReplaceDialog *dialog,
	gint response, gpointer user_data)
{
    FindReplaceOptions *options = FIND_REPLACE_DIALOG(dialog)->options;
    Buffer *buffer = get_cur_buffer();
    Iter start, end;
    gint occurances;
    
    switch (response)
    {
	case RESPONSE_REPLACE_NEXT:
	    options->old_search_texts =
		    history_list_prepend(options->old_search_texts,
			options->search_text, max_num_old_search_texts);
    	    gtk_combo_box_prepend_text(
    	    	    GTK_COMBO_BOX(FIND_REPLACE_DIALOG(dialog)->search),
    	    	    options->search_text);

	    options->old_replace_texts =
		    history_list_prepend(options->old_replace_texts,
			options->replace_text, max_num_old_replace_texts);
    	    gtk_combo_box_prepend_text(
    	    	    GTK_COMBO_BOX(dialog->replace),
    	    	    options->search_text);

	    gtk_text_buffer_begin_user_action(GTK_TEXT_BUFFER(buffer));
	    buffer_replace_selection(buffer, options->replace_text, -1);
	    gtk_text_buffer_end_user_action(GTK_TEXT_BUFFER(buffer));

	    find_next_in_dir(options, buffer);

	    break;

	case RESPONSE_REPLACE_ALL:
	    options->old_search_texts =
		    history_list_prepend(options->old_search_texts,
			options->search_text, max_num_old_search_texts);
#if 0
	    if (options->old_search_texts != NULL)
		combo_set_popdown_strings(FIND_REPLACE_DIALOG(dialog)->search, options->old_search_texts);
#endif

	    options->old_replace_texts =
		    history_list_prepend(options->old_replace_texts,
			options->replace_text, max_num_old_replace_texts);
#if 0
	    if (options->old_replace_texts != NULL)
		combo_set_popdown_strings(dialog->replace, options->old_replace_texts);
#endif

    	    switch (options->context)
    	    {
#if USE_CONTEXT_SELECTION
    	    	case REPLACE_CONTEXT_SELECTION:
    	    	{
    	    	    Region *old_selection;
    	    	    
        	    gtk_text_buffer_get_selection_bounds(GTK_TEXT_BUFFER(buffer), &start, &end);
    	    	    old_selection = region_new(buffer, &start, &end);
    	    	    
    	    	    occurances = replace_all_in_range(buffer, &start, &end);
    	    	    
		    region_get_bounds(old_selection, &start, &end);
		    region_destroy(old_selection);	    
		    buffer_select(buffer, &start, &end);
	    	}
	    	break;
#endif

    	    	case REPLACE_CONTEXT_THIS_BUFFER:
    	    	{
    		    gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(buffer), &start, &end);

    	    	    occurances = replace_all_in_range(buffer, options,
    	    	    	    &start, &end);
    		}
    		break;
    		
	    	case REPLACE_CONTEXT_ALL_BUFFERS:
	    	{
	    	    FileListIter iter;
	    	    
	    	    occurances = 0;
	    	    
	    	    for (file_list_iter_start(FILE_LIST(file_list), &iter);
	    	    	    !file_list_iter_done(&iter);
	    	    	    file_list_iter_next(&iter))
	    	    {
	    	    	Buffer *buffer = file_list_iter_get_buffer(&iter);
	    	    	
    		    	gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(buffer), &start, &end);

    	    	    	occurances += replace_all_in_range(buffer, options,
    	    	    	    	&start, &end);
	    	    }
	    	}
	    	break;
	    }

	    status_bar_set(_("Replaced %d occurance(s)"), occurances);

	    break;

	case RESPONSE_FIND_NEXT:
	    options->old_replace_texts =
		    history_list_prepend(options->old_replace_texts,
			options->replace_text, max_num_old_replace_texts);
    	    gtk_combo_box_prepend_text(
    	    	    GTK_COMBO_BOX(dialog->replace),
    	    	    options->search_text);

	    find_replace_dialog_response_cb(FIND_REPLACE_DIALOG(dialog),
		    response, user_data);

	    find_replace_dialog_set_items_from_options(FIND_REPLACE_DIALOG(dialog));
	    replace_dialog_set_items_from_options(dialog);

	    break;
	    
	default:
	    find_replace_dialog_response_cb(FIND_REPLACE_DIALOG(dialog),
		    response, user_data);
	    break;
    }
}

void find_next(void)
{
    if (find_replace_options_valid(find_replace))
	find_next_in_dir(&find_replace->options, get_cur_buffer());
    else g_warning("find_next: options invalid");
}

void find_previous(void)
{
    if (find_replace_options_valid(find_replace))
    {
    	FindDirection old_direction = find_replace->options.direction;
    	if (find_replace->options.direction == FIND_DIRECTION_UP)
	    find_replace->options.direction = FIND_DIRECTION_DOWN;
    	else
	    find_replace->options.direction = FIND_DIRECTION_UP;
	find_next_in_dir(&find_replace->options, get_cur_buffer());
    	find_replace->options.direction = old_direction; 
    }
    else g_warning("find_previous: options invalid");
}

void replace_next(void)
{
    if (find_replace_options_valid(find_replace))
    {
	Buffer *buffer = get_cur_buffer();
	
	gtk_text_buffer_begin_user_action(GTK_TEXT_BUFFER(buffer));
	buffer_replace_selection(buffer, find_replace->options.replace_text, -1);
	gtk_text_buffer_end_user_action(GTK_TEXT_BUFFER(buffer));
    
	find_next_in_dir(&find_replace->options, get_cur_buffer());
    }
    else g_warning("replace_next: options invalid");
}

#define FIND_CONF_PATH(key) CONF_PATH("FindOptions") "/" key

#define CONF_SEARCH_TEXT	FIND_CONF_PATH("SearchText")
#define CONF_OLD_SEARCH_TEXT	FIND_CONF_PATH("OldSearchText")
#define CONF_REPLACE_TEXT	FIND_CONF_PATH("ReplaceText")
#define CONF_OLD_REPLACE_TEXT	FIND_CONF_PATH("OldReplaceText")
#define CONF_MATCH_WHOLE_WORDS	FIND_CONF_PATH("MatchWholeWords")
#define CONF_MATCH_CASE 	FIND_CONF_PATH("MatchCase")
#define CONF_MATCH_WHOLE_WORDS	FIND_CONF_PATH("MatchWholeWords")
#define CONF_REGULAR_EXPRESSION FIND_CONF_PATH("RegularExpression")
#define CONF_WRAP_SEARCHES	FIND_CONF_PATH("WrapSearches")
#define CONF_DIRECTION		FIND_CONF_PATH("Direction")
#define CONF_CONTEXT		FIND_CONF_PATH("Context")
#define CONF_EXTEND_SELECTION	FIND_CONF_PATH("ExtendSelection")
#define CONF_IN_ALL_DOCUMENTS	FIND_CONF_PATH("InAllDocuments")

void find_load_conf(FindReplaceOptions *options, const gchar *key)
{
    GConfClient *client = gconf_client_get_default();
    gchar *direction, *context;
    
    if (client == NULL)
    {
	g_warning("find_load_conf: client == NULL");
	return;
    }

    options->search_text = gconf_client_get_string(client,
	    CONF_SEARCH_TEXT, NULL);
    if (options->search_text == NULL)
	options->search_text = g_strdup("");
    
    options->old_search_texts = gconf_client_get_list(client,
	    CONF_OLD_SEARCH_TEXT, GCONF_VALUE_STRING, NULL);

    options->replace_text = gconf_client_get_string(client,
	    CONF_REPLACE_TEXT, NULL);
    if (options->replace_text == NULL)
	options->replace_text = g_strdup("");
    
    options->old_replace_texts = gconf_client_get_list(client,
	    CONF_OLD_REPLACE_TEXT, GCONF_VALUE_STRING, NULL);

    options->match_whole_words = gconf_client_get_bool(client,
	    CONF_MATCH_WHOLE_WORDS, NULL);
    
    options->match_case = gconf_client_get_bool(client,
	    CONF_MATCH_CASE, NULL);
    
    options->regular_expression = gconf_client_get_bool(client,
	    CONF_REGULAR_EXPRESSION, NULL);
    
    options->wrap_searches = gconf_client_get_bool(client,
	    CONF_WRAP_SEARCHES, NULL);
    
    direction = gconf_client_get_string(client, CONF_DIRECTION, NULL);
    if (direction == NULL) options->direction = FIND_DIRECTION_DOWN;
    else
    {
	if (direction[0] == 'u' || direction[0] == 'U')
		options->direction = FIND_DIRECTION_UP;
	else
		options->direction = FIND_DIRECTION_DOWN;     
	g_free(direction);
    }
	    
    context = gconf_client_get_string(client, CONF_CONTEXT, NULL);
    if (context == NULL) options->context = REPLACE_CONTEXT_THIS_BUFFER;
    else
    {
#ifdef USE_CONTEXT_SELECTION
	if (context[0] == 's' || context[0] == 'S')
	    options->context = REPLACE_CONTEXT_SELECTION;
	else
#endif
	if (context[0] == 't' || context[0] == 'T')
	    options->context = REPLACE_CONTEXT_THIS_BUFFER;
	else
	    options->context = REPLACE_CONTEXT_ALL_BUFFERS;     
	g_free(context);
    }
    if (options->context == REPLACE_CONTEXT_ALL_BUFFERS)
    	options->context = REPLACE_CONTEXT_THIS_BUFFER;
	    
    options->extend_selection = gconf_client_get_bool(client,
	    CONF_EXTEND_SELECTION, NULL);
    
    options->in_all_documents = gconf_client_get_bool(client,
	    CONF_IN_ALL_DOCUMENTS, NULL);
}

void find_save_conf(const FindReplaceOptions *options, const gchar *key)
{
    GConfClient *client = gconf_client_get_default();

    gconf_client_set_string(client, CONF_SEARCH_TEXT,
	    options->search_text, NULL);

    gconf_client_set_list(client, CONF_OLD_SEARCH_TEXT,
	    GCONF_VALUE_STRING, options->old_search_texts, NULL);
	    
    gconf_client_set_string(client, CONF_REPLACE_TEXT,
	    options->replace_text, NULL);

    gconf_client_set_list(client, CONF_OLD_REPLACE_TEXT,
	    GCONF_VALUE_STRING, options->old_replace_texts, NULL);
	    
    gconf_client_set_bool(client, CONF_MATCH_WHOLE_WORDS,
	    options->match_whole_words, NULL);
    gconf_client_set_bool(client, CONF_MATCH_CASE,
	    options->match_case, NULL);
    gconf_client_set_bool(client, CONF_REGULAR_EXPRESSION,
	    options->regular_expression, NULL);
    gconf_client_set_bool(client, CONF_WRAP_SEARCHES,
	    options->wrap_searches, NULL);

#ifdef USE_CONTEXT_SELECTION
    if (options->context == REPLACE_CONTEXT_SELECTION)
	gconf_client_set_string(client, CONF_CONTEXT, "Selection", NULL);
    else
#endif
    if (options->direction == REPLACE_CONTEXT_THIS_BUFFER)
	gconf_client_set_string(client, CONF_CONTEXT, "This Buffer", NULL);
    else
	gconf_client_set_string(client, CONF_CONTEXT, "All Buffers", NULL);

    if (options->direction == FIND_DIRECTION_UP)
	gconf_client_set_string(client, CONF_DIRECTION, "Up", NULL);
    else
	gconf_client_set_string(client, CONF_DIRECTION, "Down", NULL);

    gconf_client_set_bool(client, CONF_EXTEND_SELECTION,
	    options->extend_selection, NULL);
    gconf_client_set_bool(client, CONF_IN_ALL_DOCUMENTS,
	    options->in_all_documents, NULL);
}

/*****************************************************************************/

static void find_replace_dialog_class_init(FindReplaceDialogClass *class)
{
    find_replace_dialog_signals[OPTIONS_CHANGED_SIGNAL] =
	    g_signal_new("options-changed",
			  G_TYPE_FROM_CLASS(class),
			  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			  G_STRUCT_OFFSET(FindReplaceDialogClass, options_changed),
			  NULL, NULL,
			  g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1,
			  G_TYPE_POINTER);
}

static void find_replace_dialog_init(FindDialog *dialog)
{
    g_signal_connect(GTK_OBJECT(dialog), "options-changed",
	    G_CALLBACK(find_replace_dialog_options_changed_cb), NULL);
}

GType find_replace_dialog_get_type (void)
{
  static GType type = 0;

  if (!type)
    {
      static const GTypeInfo info =
      {
    sizeof (FindReplaceDialogClass),
    NULL, /* base_init */
    NULL, /* base_finalize */
    (GClassInitFunc) find_replace_dialog_class_init,
    NULL, /* class_finalize */
    NULL, /* class_data */
    sizeof (FindReplaceDialog),
    0,	  /* n_preallocs */
    (GInstanceInitFunc) find_replace_dialog_init,
      };

      type = g_type_register_static (GTK_TYPE_DIALOG,
					 "FindReplaceDialog",
					 &info,
					 0);
    }

  return type;
}

/*****************************************************************************/

static void find_dialog_class_init(FindDialogClass *class)
{
}

static void find_dialog_init(FindDialog *dialog)
{
    gtk_window_set_title(GTK_WINDOW(dialog), _("Find"));

    FIND_REPLACE_DIALOG(dialog)->find_next = GTK_BUTTON(gtk_dialog_add_button(GTK_DIALOG(dialog), _("_Find Next"), RESPONSE_FIND_NEXT));
    FIND_REPLACE_DIALOG(dialog)->cancel = GTK_BUTTON(gtk_dialog_add_button(GTK_DIALOG(dialog), GTK_STOCK_CLOSE, GTK_RESPONSE_CANCEL));

    gtk_dialog_set_default_response(GTK_DIALOG(dialog), RESPONSE_FIND_NEXT);

    gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), build_find_replace_dialog_search_text(FIND_REPLACE_DIALOG(dialog)));
    gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), build_find_dialog_options(dialog));

    g_signal_connect(GTK_OBJECT(dialog), "response",
	    G_CALLBACK(find_replace_dialog_response_cb), NULL);
    g_signal_connect(GTK_OBJECT(dialog), "options-changed",
	    G_CALLBACK(find_dialog_options_changed_cb), NULL);
}

GType find_dialog_get_type (void)
{
  static GType type = 0;

  if (!type)
    {
      static const GTypeInfo info =
      {
    sizeof (FindDialogClass),
    NULL, /* base_init */
    NULL, /* base_finalize */
    (GClassInitFunc) find_dialog_class_init,
    NULL, /* class_finalize */
    NULL, /* class_data */
    sizeof (FindDialog),
    0,	  /* n_preallocs */
    (GInstanceInitFunc) find_dialog_init,
      };

      type = g_type_register_static (TYPE_FIND_REPLACE_DIALOG,
					 "FindDialog",
					 &info,
					 0);
    }

  return type;
}

GtkWidget* find_dialog_new(GtkWindow *parent)
{
    FindReplaceOptions *options = &find_replace->options;
    FindDialog *dialog = g_object_new(TYPE_FIND_DIALOG, NULL);
    Buffer *buffer = get_cur_buffer();
    Iter start, end;

    gtk_window_set_transient_for(GTK_WINDOW(dialog), parent);

    FIND_REPLACE_DIALOG(dialog)->options = options;

    if (options->old_search_texts != NULL)
    {
    	GSList *p = options->old_search_texts;
        
    	while (p != NULL)
    	{
    	    gtk_combo_box_append_text(
    	    	    GTK_COMBO_BOX(FIND_REPLACE_DIALOG(dialog)->search),
    	    	    p->data);
	    p = g_slist_next(p);
    	}
    }

    if (buffer != NULL)
    {
	if (gtk_text_buffer_get_selection_bounds(GTK_TEXT_BUFFER(buffer), &start, &end)
		&& gtk_text_iter_get_line(&start) == gtk_text_iter_get_line(&end))
	{
	    gchar *selected_text =
		    gtk_text_buffer_get_text(GTK_TEXT_BUFFER(buffer), &start,  &end, FALSE);
	    if (selected_text != NULL)
	    {
		g_free(options->search_text);
		options->search_text = selected_text;
	    }
	    else g_warning("find_dialog_new: selected_text == NULL");
	}
    }

    find_replace_dialog_set_items_from_options(FIND_REPLACE_DIALOG(dialog));
    find_dialog_set_items_from_options(dialog);

    return GTK_WIDGET(dialog);
}

/*****************************************************************************/

static void replace_dialog_class_init(ReplaceDialogClass *class)
{
}

static void replace_dialog_init(ReplaceDialog *dialog)
{
    gtk_window_set_title(GTK_WINDOW(dialog), _("Replace"));

    FIND_REPLACE_DIALOG(dialog)->find_next = GTK_BUTTON(gtk_dialog_add_button(GTK_DIALOG(dialog), _("_Find Next"), RESPONSE_FIND_NEXT));
    dialog->replace_next = GTK_BUTTON(gtk_dialog_add_button(GTK_DIALOG(dialog), _("_Replace Next"), RESPONSE_REPLACE_NEXT));
    dialog->replace_all = GTK_BUTTON(gtk_dialog_add_button(GTK_DIALOG(dialog), _("Replace _All"), RESPONSE_REPLACE_ALL));
    FIND_REPLACE_DIALOG(dialog)->cancel = GTK_BUTTON(gtk_dialog_add_button(GTK_DIALOG(dialog), GTK_STOCK_CLOSE, GTK_RESPONSE_CANCEL));

    gtk_dialog_set_default_response(GTK_DIALOG(dialog), RESPONSE_FIND_NEXT);

    gtk_container_add (GTK_CONTAINER (GTK_DIALOG(dialog)->vbox),
	    build_find_replace_dialog_search_text(FIND_REPLACE_DIALOG(dialog)));
    gtk_container_add (GTK_CONTAINER (GTK_DIALOG(dialog)->vbox),
	    build_replace_dialog_replace_text(dialog));
    gtk_container_add (GTK_CONTAINER (GTK_DIALOG(dialog)->vbox),
	    build_replace_dialog_options(dialog));

    g_signal_connect(GTK_OBJECT(dialog), "response",
	    G_CALLBACK(replace_dialog_response_cb), NULL);
    g_signal_connect(GTK_OBJECT(dialog), "options-changed",
	    G_CALLBACK(replace_dialog_options_changed_cb), NULL);
}

GType replace_dialog_get_type (void)
{
  static GType type = 0;

  if (!type)
    {
      static const GTypeInfo info =
      {
    sizeof (ReplaceDialogClass),
    NULL, /* base_init */
    NULL, /* base_finalize */
    (GClassInitFunc) replace_dialog_class_init,
    NULL, /* class_finalize */
    NULL, /* class_data */
    sizeof (ReplaceDialog),
    0,	  /* n_preallocs */
    (GInstanceInitFunc) replace_dialog_init,
      };

      type = g_type_register_static (TYPE_FIND_REPLACE_DIALOG,
					 "ReplaceDialog",
					 &info,
					 0);
    }

  return type;
}

GtkWidget* replace_dialog_new(GtkWindow *parent)
{
    FindReplaceOptions *options = &find_replace->options;
    ReplaceDialog *dialog = g_object_new(TYPE_REPLACE_DIALOG, NULL);
    Buffer *buffer = get_cur_buffer();
    Iter start, end;

    gtk_window_set_transient_for(GTK_WINDOW(dialog), parent);

    FIND_REPLACE_DIALOG(dialog)->options = options;

    if (options->old_search_texts != NULL)
    {
    	GSList *p = options->old_search_texts;
        
    	while (p != NULL)
    	{
    	    gtk_combo_box_append_text(
    	    	    GTK_COMBO_BOX(FIND_REPLACE_DIALOG(dialog)->search),
    	    	    p->data);
	    p = g_slist_next(p);
    	}
    }
    if (options->old_replace_texts != NULL)
    {
    	GSList *p = options->old_replace_texts;
        
    	while (p != NULL)
    	{
    	    gtk_combo_box_append_text(
    	    	    GTK_COMBO_BOX(REPLACE_DIALOG(dialog)->replace),
    	    	    p->data);
	    p = g_slist_next(p);
    	}
    }

    if (buffer != NULL && gtk_text_buffer_get_selection_bounds(GTK_TEXT_BUFFER(buffer), &start, &end))
    {
#ifdef USE_CONTEXT_SELECTION
	if (gtk_text_iter_get_line(&start) == gtk_text_iter_get_line(&end))
	{
#endif
	    gchar *selected_text = gtk_text_buffer_get_text(GTK_TEXT_BUFFER(buffer),
			    &start,  &end, FALSE);
	    if (selected_text != NULL)
	    {
		g_free(options->search_text);
		options->search_text = selected_text;
	    }
	    else g_warning("find_popup: selected_text == NULL");
	    
#ifdef USE_CONTEXT_SELECTION
	    if (options->context == REPLACE_CONTEXT_SELECTION)
    	        options->context = REPLACE_CONTEXT_THIS_BUFFER;
	}
	else options->context = REPLACE_CONTEXT_SELECTION;
#endif
    }
    else
    {
#ifdef USE_CONTEXT_SELECTION
	if (options->context == REPLACE_CONTEXT_SELECTION)
    	    options->context = REPLACE_CONTEXT_THIS_BUFFER;
#endif
    }

    find_replace_dialog_set_items_from_options(FIND_REPLACE_DIALOG(dialog));
    replace_dialog_set_items_from_options(dialog);

    return GTK_WIDGET(dialog);
}

/*****************************************************************************/

static void find_replace_class_init(FindReplaceClass *klass)
{
    find_replace_signals[OPTIONS_CHANGED_SIGNAL] =
	    g_signal_new("options-changed",
			  G_TYPE_FROM_CLASS(klass),
			  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			  G_STRUCT_OFFSET(FindReplaceClass, options_changed),
			  NULL, NULL,
			  g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}

static void find_replace_finalize(GObject *_find_replace)
{
    FindReplace *find_replace = (FindReplace *)_find_replace;
    
    find_save_conf(&find_replace->options, "ignored");
}

static void find_replace_base_init(GObjectClass *base_class)
{
    G_OBJECT_CLASS(base_class)->finalize = find_replace_finalize;
}

static void find_replace_init(FindReplace *find_replace)
{
    find_load_conf(&find_replace->options, "ignored");
}

GType find_replace_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
    	static const GTypeInfo info =
    	{
    	    sizeof(FindReplaceClass),
    	    (GBaseInitFunc) find_replace_base_init, /* base_init */
    	    NULL, /* base_finalize */
    	    (GClassInitFunc) find_replace_class_init,
    	    NULL, /* class_finalize */
    	    NULL, /* class_data */
    	    sizeof(FindReplace),
    	    0,	  /* n_preallocs */
    	    (GInstanceInitFunc) find_replace_init,
    	};

    	type = g_type_register_static(G_TYPE_OBJECT,
      	    	"FindReplace", &info, 0);
    }

    return type;
}

FindReplace *find_replace_new(void)
{
    FindReplace *find_replace = g_object_new(TYPE_FIND_REPLACE, NULL);

    return find_replace;
}

gboolean find_replace_options_valid(const FindReplace *find_replace)
{
    return find_replace->options.search_text[0] != '\0';
}
