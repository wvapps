/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef FILE_LIST_H
#define FILE_LIST_H
#include <glib.h>
G_BEGIN_DECLS

#include "buffer.h"
#include "view.h"

#include <gtk/gtktreemodel.h>
#include <gtk/gtktextbuffer.h>
#include <gtk/gtktextview.h>

#define TYPE_FILE_LIST	    	    (file_list_get_type())
#define FILE_LIST(obj)	    	    (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_FILE_LIST, FileList))
#define FILE_LIST_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_FILE_LIST, FileListClass))
#define IS_FILE_LIST(obj)   	    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_FILE_LIST))
#define IS_FILE_LIST_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_FILE_LIST))
#define FILE_LIST_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_FILE_LIST, FileListClass))

typedef struct _FileList      	FileList;
typedef struct _FileListClass	FileListClass;

struct _FileListClass
{
    GtkTreeViewClass parent_class;
    
    void (*buffer_focus)(FileList *file_list, Buffer *buffer);
    void (*cur_buffer_modified_changed)(FileList *file_list, Buffer *buffer);
    void (*cur_buffer_selection_changed)(FileList *file_list, Buffer *buffer);
    void (*cur_buffer_can_undo_changed)(FileList *file_list, Buffer *buffer);
    void (*cur_buffer_can_redo_changed)(FileList *file_list, Buffer *buffer);
};

struct _FileList
{
    GtkTreeView parent;
    
    GtkTreeModel *model;
    
    gboolean button_press_modified;
};

GType file_list_get_type(void);

typedef struct
{
    FileList *file_list;
    gboolean done;
    GtkTreeIter iter;
} FileListIter;

GtkWidget *file_list_new(void);

gboolean file_list_find_buffer(FileList *file_list,
    	const Buffer *buffer, FileListIter *iter);
	
void file_list_iter_start(FileList *file_list, FileListIter *iter);
gboolean file_list_iter_done(const FileListIter *iter);
void file_list_iter_next(FileListIter *iter);

Buffer *file_list_tree_iter_get_buffer(FileList *file_list, GtkTreeIter *iter);
Buffer *file_list_iter_get_buffer(FileListIter *iter);

void file_list_tree_iter_set_buffer(FileList *file_list,
    	GtkTreeIter *iter, Buffer *buffer);
void file_list_iter_set_buffer(FileListIter *iter, Buffer *buffer);

void file_list_add_buffer(FileList *file_list, Buffer *buffer);
void file_list_remove_buffer(FileList *file_list, Buffer *buffer);

Buffer *file_list_get_last_focused_buffer(FileList *file_list);

gint file_list_num_buffers(FileList *file_list);

G_END_DECLS
#endif
