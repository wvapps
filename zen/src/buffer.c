/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "buffer.h"

#include "zen.h"
#include "mark.h"
#include "file_list.h"
#include "views.h"
#include "prefs.h"
#include "status_bar.h"
#include "utils.h"
#include "cmd.h"

#include <string.h>
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>     
#include <sys/stat.h>
#include <unistd.h>
#include <gdk/gdkkeysyms.h>
#include <gtksourceview/gtksourceview.h>
#include <gconf/gconf-client.h>

enum {
    FILENAME_CHANGED_SIGNAL,
    DESC_CHANGED_SIGNAL,
    FOCUS_IN_SIGNAL,
    FOCUS_OUT_SIGNAL,
    DESTROY_SIGNAL,
    LAST_SIGNAL
};

static guint buffer_signals[LAST_SIGNAL] = { 0 };

static gchar *last_dirname = NULL;

void buffer_select(Buffer *buffer, const Iter *start, const Iter *end)
{
    GtkTextMark *selection_bound = gtk_text_buffer_get_selection_bound(GTK_TEXT_BUFFER(buffer));
            
    gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), start);
    gtk_text_buffer_move_mark(GTK_TEXT_BUFFER(buffer), selection_bound, end);
}

gboolean buffer_in_leading_space(Buffer *buffer, const GtkTextIter *iter)
{
    GtkTextIter i = *iter;
    gboolean result = TRUE;
                
    while (!gtk_text_iter_starts_line(&i))
    {
    	gunichar ch;

	if (!gtk_text_iter_backward_char(&i))
	    break;
    	    	    	    
	ch = gtk_text_iter_get_char(&i);
	if (!isspace(ch))
        {
	    result = FALSE;
	    break;
        }
    }
                
    return result;
}

gboolean buffer_in_trailing_space(Buffer *buffer, const GtkTextIter *iter)
{
    GtkTextIter i = *iter;
    gboolean result = TRUE;
                
    while (!gtk_text_iter_ends_line(&i))
    {
    	gunichar ch;

	if (!gtk_text_iter_forward_char(&i))
	    break;
    	    	    	    
	ch = gtk_text_iter_get_char(&i);
	if (!isspace(ch))
        {
	    result = FALSE;
	    break;
        }
    }
                
    return result;
}

const gchar *buffer_get_filename(const Buffer *buffer)
{
    return buffer->filename;
}

static void buffer_set_title(Buffer *buffer)
{
    if (buffer != NULL)
    {
	const gchar *desc = buffer_get_desc(buffer);
	gchar *title;

        if (gtk_text_buffer_get_modified(GTK_TEXT_BUFFER(buffer)))
	    title = g_strconcat(desc, " ",  _("(modified)"), " - " FRIENDLY_NAME, NULL);
	else
	    title = g_strconcat(desc, " - " FRIENDLY_NAME, NULL);

    	gtk_window_set_title(GTK_WINDOW(app), title);

	g_free(title);
    }
    else gtk_window_set_title(GTK_WINDOW(app), FRIENDLY_NAME);
}

void buffer_preferences_changed_cb(Buffer *buffer, gpointer user_data)
{
    gchar *mime_type = buffer_get_mime_type(buffer);
	
    GtkSourceLanguage *source_language =
    	    prefs_mime_type_get_source_language(mime_type);
	
    gtk_source_buffer_set_language(GTK_SOURCE_BUFFER(buffer), source_language);
    gtk_source_buffer_set_highlight(GTK_SOURCE_BUFFER(buffer), source_language != NULL);
    	
    buffer->tab_size = prefs_mime_type_get_tab_size(mime_type);
    buffer->indent_size = prefs_mime_type_get_indent_size(mime_type);
    buffer->use_tabs = prefs_mime_type_get_use_tabs(mime_type);
    	
    g_free(mime_type);
}

void buffer_set_filename(Buffer *buffer, const gchar *filename)
{
    if (buffer->filename == NULL?
	    filename != NULL:
	    filename == NULL || !g_str_equal(filename, buffer->filename))
    {
	g_free(buffer->filename);

	if (filename != NULL)
	{
	    gchar cwd[PATH_MAX];
	    
	    getcwd(cwd, PATH_MAX);
    	    buffer->filename = normalize_filename(filename, cwd);
	}
	else buffer->filename = NULL;
    
    	buffer_set_title(buffer);
    	buffer_preferences_changed_cb(buffer, NULL);
    
	g_signal_emit(G_OBJECT(buffer), buffer_signals[FILENAME_CHANGED_SIGNAL],
		0, buffer->filename);
	if (buffer->desc == NULL)
	    g_signal_emit(G_OBJECT(buffer), buffer_signals[DESC_CHANGED_SIGNAL],
		    0, buffer_get_desc(buffer));
    } 
}

const gchar *buffer_get_desc(const Buffer *buffer)
{
    if (buffer == NULL)
    {
	g_warning("buffer_get_short_filename: buffer == NULL");
	return NULL;
    }

    if (buffer->desc != NULL)
	return buffer->desc;
    else if (buffer->filename != NULL)
    {
	const gchar *basename;

	if ((basename = g_utf8_strrchr(buffer->filename, -1, '/')) != NULL)
	    basename++;
	else
	    basename = buffer->filename; 

	return basename;
    }
    else return _("New document");
}

static const char spaces[] = "                                       ";

void buffer_set_desc(Buffer *buffer, const gchar *desc)
{
    if (buffer->desc == NULL?
	    desc != NULL:
	    desc == NULL || !g_str_equal(desc, buffer->desc))
    {
	g_free(buffer->desc);

    	if (desc != NULL)
    	    buffer->desc = g_strdup(desc);
	else buffer->desc = NULL;
    
	g_signal_emit(G_OBJECT(buffer), buffer_signals[DESC_CHANGED_SIGNAL],
    	    	0, buffer->desc);
    } 
}

static gint buffer_get_column(Buffer *buffer, Iter *iter)
{
    Iter start = *iter;
    gint col = 0;
    
    iter_goto_line_start(&start);
    while (!gtk_text_iter_equal(&start, iter))
    {
	switch (gtk_text_iter_get_char(&start))
	{
	    case '\t':
		col = (col + buffer->tab_size) / buffer->tab_size * buffer->tab_size;
		break;
		
	    default:
		++col;
		break;
	}
	
	gtk_text_iter_forward_char(&start);
    }
    
    return col;
}

void buffer_indent(Buffer *buffer, Iter *iter)
{
    gint col;
    
    col = buffer_get_column(buffer, iter);
    
    if (buffer->use_tabs
    	    && (col + buffer->indent_size) / buffer->indent_size * buffer->indent_size
    	    	== (col + buffer->tab_size) / buffer->tab_size * buffer->tab_size)
	gtk_text_buffer_insert(GTK_TEXT_BUFFER(buffer), iter, "\t", 1);
    else
    {
	gint num_spaces = ((col + buffer->indent_size) / buffer->indent_size * buffer->indent_size - col);
	gtk_text_buffer_insert(GTK_TEXT_BUFFER(buffer), iter,
		&spaces[sizeof(spaces) - num_spaces - 1], num_spaces);
    }
}

void buffer_indent_at_cursor(Buffer *buffer)
{
    Iter iter = buffer_get_insert_iter(buffer);

    buffer_indent(buffer, &iter);
}

gboolean buffer_backspace(Buffer *buffer, Iter *iter)
{
    Iter start = *iter;
    if (!gtk_text_iter_starts_line(&start))
    {
    	gint end_col, col;
        
    	end_col = col = buffer_get_column(buffer, iter);

     	while (!gtk_text_iter_starts_line(&start))
    	{
	    Iter maybe_new_start = start;
	    if (gtk_text_iter_backward_char(&maybe_new_start))
	    {
	    	gunichar ch = gtk_text_iter_get_char(&maybe_new_start);
	    	if (ch == ' ')
	    	{
		    --col;
		    start = maybe_new_start;
	    	}
	    	else if (ch == '\t')
	    	{
		    start = maybe_new_start;                      
    	    	    col = buffer_get_column(buffer, &start);		
	    	}
	    	else return FALSE;
    	    
	    	if (col <= (end_col - buffer->indent_size)
	    	    	/ buffer->indent_size * buffer->indent_size)
		    break;
	    }
	    else break;
    	}
        
    	if (!gtk_text_iter_equal(&start, iter))
    	{
    	    Mark *mark = gtk_text_buffer_create_mark(GTK_TEXT_BUFFER(buffer),
    	    	    "temp-mark", &start, FALSE);
        	    	
    	    gint extra_spaces = (end_col - buffer->indent_size)
    	    	    / buffer->indent_size * buffer->indent_size - col;

    	    gtk_text_buffer_delete(GTK_TEXT_BUFFER(buffer), &start, iter);
	    gtk_text_buffer_insert(GTK_TEXT_BUFFER(buffer), iter,
	    	    &spaces[sizeof(spaces) - extra_spaces - 1], extra_spaces);

    	    gtk_text_buffer_get_iter_at_mark(GTK_TEXT_BUFFER(buffer),
    	    	    iter, mark);
	    gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), iter);
    	    	
	    gtk_text_buffer_delete_mark(GTK_TEXT_BUFFER(buffer), mark);
    	}
    }
    else
    {
    	Iter start = *iter;
    	if (gtk_text_iter_backward_char(&start))
    	{
	    Iter maybe_new_start = start;
    	    while (gtk_text_iter_backward_char(&maybe_new_start))
    	    {
	    	gunichar ch = gtk_text_iter_get_char(&maybe_new_start);
	    	if (ch == ' ' || ch == '\t')
		    start = maybe_new_start;                      
	    	else break;
    	    }
            
    	    if (!gtk_text_iter_equal(&start, iter))
	    	gtk_text_buffer_delete(GTK_TEXT_BUFFER(buffer), &start, iter);
	}
    }
    
    return TRUE;
}

void buffer_exdent(Buffer *buffer, Iter *iter)
{
    iter_goto_visible_line_start(iter);

    buffer_backspace(buffer, iter);
}

void buffer_exdent_at_cursor(Buffer *buffer)
{
    Mark *insert = gtk_text_buffer_get_insert(GTK_TEXT_BUFFER(buffer));
    Iter iter;
    
    gtk_text_buffer_get_iter_at_mark(GTK_TEXT_BUFFER(buffer), &iter, insert);
    
    buffer_exdent(buffer, &iter);
}

void buffer_backspace_at_cursor(Buffer *buffer)
{
    Mark *insert = gtk_text_buffer_get_insert(GTK_TEXT_BUFFER(buffer));
    Iter iter;
    
    gtk_text_buffer_get_iter_at_mark(GTK_TEXT_BUFFER(buffer), &iter, insert);
    
    buffer_backspace(buffer, &iter);
}

gboolean buffer_delete(Buffer *buffer, Iter *iter)
{
    Iter start = *iter, end;
    gint start_col, col;
    
    start_col = col = buffer_get_column(buffer, &start);
    
    end = start;
    while (!gtk_text_iter_ends_line(&end))
    {
	Iter maybe_new_end = end;
	if (gtk_text_iter_forward_char(&maybe_new_end))
	{
	    gunichar ch = gtk_text_iter_get_char(&end);
	    if (ch == ' ')
	    {
	    	++col;
	    	end = maybe_new_end;
	    }
	    else if (ch == '\t')
	    {
	    	col = (col + buffer->tab_size) / buffer->tab_size * buffer->tab_size;
	    	end = maybe_new_end;
	    }
	    else return FALSE;
	    
	    if (col >= (start_col + buffer->indent_size) / buffer->indent_size * buffer->indent_size)
	    {
		break;
	    }
	}
	else break;
    }
    
    if (!gtk_text_iter_equal(&start, &end))
    {
    	Mark *mark = gtk_text_buffer_create_mark(GTK_TEXT_BUFFER(buffer),
    	    	"temp-mark", &start, TRUE);
    	    	
    	gint extra_spaces = col - (start_col + buffer->indent_size) / buffer->indent_size * buffer->indent_size;
    	
	gtk_text_buffer_delete(GTK_TEXT_BUFFER(buffer), &start, &end);
	gtk_text_buffer_insert(GTK_TEXT_BUFFER(buffer), &end,
	    	&spaces[sizeof(spaces) - extra_spaces - 1], extra_spaces);
	    	
	gtk_text_buffer_get_iter_at_mark(GTK_TEXT_BUFFER(buffer), &start,
	    	mark);
	gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), &start);
	    	
	gtk_text_buffer_delete_mark(GTK_TEXT_BUFFER(buffer), mark);
    }

    return TRUE;
}

void buffer_delete_at_cursor(Buffer *buffer)
{
    Iter iter = buffer_get_insert_iter(buffer);
    
    buffer_delete(buffer, &iter);
}

void buffer_indent_line(Buffer *buffer, GtkTextIter *iter)
{
    iter_goto_visible_line_start(iter);
    buffer_indent(buffer, iter);
}

void buffer_exdent_line(Buffer *buffer, GtkTextIter *iter)
{
    iter_goto_visible_line_start(iter);
    buffer_exdent(buffer, iter);
}

gboolean buffer_join_lines(Buffer *buffer)
{
    GtkTextIter start, end;
    if (gtk_text_buffer_get_selection_bounds(GTK_TEXT_BUFFER(buffer), &start, &end))
    {
	return FALSE;
    }
    else
    {
	iter_goto_visible_line_end(&start);
	if (gtk_text_iter_forward_line(&end))
	{
	    iter_goto_visible_line_start(&end);
	    
	    gtk_text_buffer_begin_user_action(GTK_TEXT_BUFFER(buffer));
	    gtk_text_buffer_delete(GTK_TEXT_BUFFER(buffer), &start, &end);
	    gtk_text_buffer_insert(GTK_TEXT_BUFFER(buffer), &start, " ", 1);
	    gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), &start);
	    gtk_text_buffer_end_user_action(GTK_TEXT_BUFFER(buffer));
	}
	else return FALSE;
    }
    
    return TRUE;
}

gboolean buffer_split_lines(Buffer *buffer)
{
    GtkTextIter start, end;
    if (gtk_text_buffer_get_selection_bounds(GTK_TEXT_BUFFER(buffer), &start, &end))
    {
	return FALSE;
    }
    else
    {
	GtkTextIter line_sg_start = start, line_sg_end = end;
	gchar *indenting;
	
	iter_goto_space_group_start(&start);
	iter_goto_space_group_end(&end);

	iter_goto_line_start(&line_sg_start);
	iter_goto_visible_line_start(&line_sg_end);
	indenting = gtk_text_buffer_get_text(GTK_TEXT_BUFFER(buffer),
		&line_sg_start, &line_sg_end, FALSE);
	
	gtk_text_buffer_begin_user_action(GTK_TEXT_BUFFER(buffer));
	gtk_text_buffer_delete(GTK_TEXT_BUFFER(buffer), &start, &end);
	gtk_text_buffer_insert(GTK_TEXT_BUFFER(buffer), &start, "\n", 1);
	gtk_text_buffer_insert(GTK_TEXT_BUFFER(buffer), &start, indenting, -1);
	gtk_text_buffer_end_user_action(GTK_TEXT_BUFFER(buffer));
	
	g_free(indenting);
	
	return TRUE;
    }
}

gboolean buffer_delete_lines(Buffer *buffer)
{
    GtkTextIter start, end;

    gtk_text_buffer_get_selection_bounds(GTK_TEXT_BUFFER(buffer), &start, &end);
    
    iter_goto_line_start(&start);
    iter_goto_line_end(&end);
    if (gtk_text_iter_forward_line(&end))
        iter_goto_line_start(&end);
    else
    {
        /* on last line */
        gtk_text_iter_backward_line(&start);
        iter_goto_line_end(&start);
    }
    
    gtk_text_buffer_begin_user_action(GTK_TEXT_BUFFER(buffer));
    gtk_text_buffer_delete(GTK_TEXT_BUFFER(buffer), &start, &end);
    gtk_text_buffer_end_user_action(GTK_TEXT_BUFFER(buffer));
    
    return TRUE;
}

#if 0
static gboolean convert_tabs(const gchar *src, gint src_tab_len,
	gchar *dst, gint *dst_size, gint dst_tab_len)
{
    gint dst_len = *dst_size;
    gint src_col = 0;
    gint dst_col = 0;
    
    *dst_size = 0;
    
    while (*src != '\0')
    {
	gchar *src_next;
	gint size;
	
	switch (*src)
	{
	    case ' ':
	    case '\t':
		if (*src == '\t')
		{
		    src_col = (src_col + src_tab_len) / src_tab_len * src_tab_len;
		}
		else
		{
		    src_col += 1;
		}
		
		src++;
		
		while ((dst_col + dst_tab_len) / dst_tab_len * dst_tab_len <= src_col)
		{
		    if ((dst_col + dst_tab_len) / dst_tab_len * dst_tab_len == dst_col + 1)
		    {
			*dst++ = ' ';
			*dst_size += 1;
			++dst_col;	      }
		    else
		    {
			*dst++ = '\t';
			*dst_size += 1;
			dst_col = (dst_col + dst_tab_len) / dst_tab_len * dst_tab_len;
		    }
		}
		break;
		
	    default:
		if (src_col - dst_col + 1 > dst_len - *dst_size)
		    return FALSE;
		while (dst_col < src_col)
		{
		    *dst++ = ' ';
		    ++dst_col;
		    *dst_size += 1;
		}
		
		if (*src == '\n' || *src == '\r')
		{
		    src_col = 0;
		    dst_col = 0;
		}
		else
		{
		    src_col += 1;
		    dst_col += 1;
		}
		
		while (*src != '\0')
		{
		    src_next = g_utf8_next_char(src);
		    size = src_next - src;
		    if (size + 1 > dst_len - *dst_size)
			return FALSE;
		    memcpy(dst, src, size);
		    src += size;
		    dst += size;
		    *dst_size += size;
		}
		break;
	}
    }
    
    if (src_col - dst_col + 1 > dst_len - *dst_size)
	return FALSE;
    while (dst_col < src_col)
    {
	*dst++ = ' ';
	++dst_col;
	*dst_size += 1;
    }

    *dst = '\0';
    
    return TRUE;
}
#endif

static gboolean buffer_save_to_file(Buffer *buffer, const gchar *filename,
    	GError **error)
{
    GtkTextIter start;
    GnomeVFSHandle *handle;
    
    if (gnome_vfs_error(error,
	    gnome_vfs_create(&handle, filename, GNOME_VFS_OPEN_WRITE, FALSE, 0666),
	    filename))
	return FALSE;

    gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(buffer), &start);
    while (!gtk_text_iter_is_end(&start))
    {
	GtkTextIter end = start;
    	GnomeVFSFileSize bytes_written;
	gchar *buf, *converted_buf;
	gsize converted_bytes_read, converted_bytes_written;
		
	gtk_text_iter_forward_line(&end);
		
	buf = gtk_text_iter_get_visible_text(&start, &end);
	    
	converted_buf = g_convert(buf, strlen(buf),
	    	buffer->codeset, "utf8", &converted_bytes_read,
	    	&converted_bytes_written, error);
        if (converted_buf == NULL) 
            return FALSE;
	    
    	if (gnome_vfs_error(error,
	    	gnome_vfs_write(handle, converted_buf,
	    	    converted_bytes_written, &bytes_written),
	    	filename))
	    break;

	start = end;
    }
    
    if (gnome_vfs_error(error,
	    gnome_vfs_close(handle),
	    filename))
	return FALSE;
    
    return TRUE;
}

gboolean buffer_save(Buffer *buffer, GError **error)
{
    const gchar *filename = buffer_get_filename(buffer);
    if (filename != NULL)
    {
	GtkTextIter start;
    	GnomeVFSURI *uri;
    	gchar *basename, *backup_basename;
    	GnomeVFSURI *backup_uri;
    	gchar *backup_filename;
    	GnomeVFSHandle *handle;
	GnomeVFSFileInfo backup_file_info, file_info;
    
    	uri = gnome_vfs_uri_new(filename);
    	basename = gnome_vfs_uri_extract_short_name(uri);
    	if (basename[0] != '.')
            backup_basename = g_strconcat(".", basename, "~", NULL);
        else
            backup_basename = g_strconcat(basename, "~", NULL);
        g_free(basename);
        backup_uri = gnome_vfs_uri_resolve_relative(uri, backup_basename);
        gnome_vfs_uri_unref(uri);
        g_free(backup_basename);
        backup_filename = gnome_vfs_uri_to_string(backup_uri,
            	GNOME_VFS_URI_HIDE_NONE);
    	gnome_vfs_uri_unref(backup_uri);
    	// We do not check for error here in case file doesn't exist
	gnome_vfs_move(filename, backup_filename, TRUE);
	if (gnome_vfs_error(NULL,
	    	gnome_vfs_get_file_info(backup_filename, &backup_file_info,
	    	    GNOME_VFS_FILE_INFO_FOLLOW_LINKS),
	    	filename))
	{
	    backup_file_info.permissions = 0666;
	}
	g_free(backup_filename);
    
    	if (!buffer_save_to_file(buffer, filename, error))
    	    return FALSE;
    	
	buffer_set_modified(buffer, FALSE);
	    
	if (gnome_vfs_error(error,
	    	gnome_vfs_get_file_info(filename, &file_info,
	    	    GNOME_VFS_FILE_INFO_FOLLOW_LINKS),
	    	filename))
	    buffer->last_mtime = 0;
	else buffer->last_mtime = file_info.mtime;
	
	file_info.permissions = backup_file_info.permissions;
	
	if (gnome_vfs_error(error,
	    	gnome_vfs_set_file_info(filename, &file_info,
	    	    GNOME_VFS_SET_FILE_INFO_PERMISSIONS),
	    	filename))
	{
	    status_bar_set(_("WARNING: unable to preserve file permissions for %s"),
	    	    filename);
	    if (error)
	    {
	    	g_error_free(*error);
	    	*error = NULL;
	    }
	}

    	// Correctly get MIME type
    	buffer_preferences_changed_cb(buffer, NULL);
    }
    else g_warning("buffer_save: filename == NULL");

    return *error == NULL;
}

gchar *buffer_get_autosave_filename(Buffer *buffer)
{
    const gchar *filename;
    GnomeVFSURI *uri;
    gchar *basename, *autosave_basename;
    GnomeVFSURI *autosave_uri;
    gchar *autosave_filename;
    GnomeVFSFileInfo autosave_file_info, autosave_info;
    
    filename = buffer_get_filename(buffer);
    
    uri = gnome_vfs_uri_new(filename);
    basename = gnome_vfs_uri_extract_short_name(uri);
    if (basename[0] != '.')
        autosave_basename = g_strconcat(".", basename, ".autosave", NULL);
    else
        autosave_basename = g_strconcat(basename, ".autosave", NULL);
    g_free(basename);
    autosave_uri = gnome_vfs_uri_resolve_relative(uri, autosave_basename);
    gnome_vfs_uri_unref(uri);
    g_free(autosave_basename);
    autosave_filename = gnome_vfs_uri_to_string(autosave_uri,
            GNOME_VFS_URI_HIDE_NONE);
    gnome_vfs_uri_unref(autosave_uri);
    	
    return autosave_filename;
}

gboolean buffer_autosave(Buffer *buffer, GError **error)
{
    const gchar *filename = buffer_get_filename(buffer);
    if (filename != NULL)
    {
    	gchar *autosave_filename = buffer_get_autosave_filename(buffer);
    
    	if (!buffer_save_to_file(buffer, autosave_filename, error))
    	    return FALSE;
    	    
	g_free(autosave_filename);
    }
    else g_warning("buffer_autosave: filename == NULL");

    return *error == NULL;
}

static gboolean buffer_load_from_file(Buffer *buffer, const gchar *filename,
    	GError **error)
{
    GnomeVFSHandle *handle;
    GtkTextIter start;
    gchar buf[4096];
    gsize have_bytes = 0;

    if (gnome_vfs_error(error,
	    gnome_vfs_open(&handle, filename, GNOME_VFS_OPEN_READ),
	    filename))
	return FALSE;
    
    gtk_source_buffer_begin_not_undoable_action(GTK_SOURCE_BUFFER(buffer));
    gtk_text_buffer_set_text(GTK_TEXT_BUFFER(buffer), "", 0);
    while (TRUE)
    {
	GnomeVFSFileSize bytes_read;
	GnomeVFSResult result = gnome_vfs_read(handle, &buf[have_bytes],
	    sizeof(buf) - have_bytes, &bytes_read);
	gchar *converted_buf;
	gsize converted_bytes_read, converted_bytes_written;
	    
	if (result == GNOME_VFS_ERROR_EOF) break;
	    
	converted_buf = g_convert(buf, bytes_read, "utf8",
	    	buffer->codeset, &converted_bytes_read,
	    	&converted_bytes_written, error);
    	if (converted_buf == NULL) 
            return FALSE;
	    
	if (gnome_vfs_error(error, result, filename))
	    return FALSE;
	    
	gtk_text_buffer_insert_at_cursor(GTK_TEXT_BUFFER(buffer),
    	    converted_buf, converted_bytes_written);
    	    
    	g_free(converted_buf);
    	    
    	have_bytes = bytes_read - converted_bytes_read;
    	g_memmove(buf, &buf[converted_bytes_read], have_bytes); 
    }
    gtk_source_buffer_end_not_undoable_action(GTK_SOURCE_BUFFER(buffer));
	    
    if (gnome_vfs_error(error,
	    gnome_vfs_close(handle),
	    filename))
	return FALSE;
	
    gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(buffer), &start);
    gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), &start);
    
    return *error == NULL;
}

gboolean buffer_load(Buffer *buffer, GError **error)
{
    const gchar *filename = buffer->filename;
    if (filename != NULL)
    {
    	GnomeVFSFileInfo file_info;
    	
    	if (!buffer_load_from_file(buffer, filename, error))
    	    return FALSE;
	
	buffer_set_modified(buffer, FALSE);
		
	if (gnome_vfs_error(error,
	    	gnome_vfs_get_file_info(filename, &file_info,
	    	    GNOME_VFS_FILE_INFO_FOLLOW_LINKS),
	    	filename))
	    buffer->last_mtime = 0;
    	else buffer->last_mtime = file_info.mtime;
    }
    else g_warning("buffer_load: filename == NULL");
    
    return *error == NULL;
}

gboolean buffer_autoload(Buffer *buffer, GError **error)
{
    gboolean result = FALSE;
    const gchar *filename = buffer_get_filename(buffer);
    gchar *autosave_filename = buffer_get_autosave_filename(buffer);
    if (filename != NULL || autosave_filename != NULL)
    {
    	GnomeVFSFileInfo file_info;
    	
    	if (buffer_load_from_file(buffer, autosave_filename, error))
    	{
	    buffer_set_modified(buffer, TRUE);
    		
	    if (gnome_vfs_error(NULL,
	    	    gnome_vfs_get_file_info(filename, &file_info,
	    	    	GNOME_VFS_FILE_INFO_FOLLOW_LINKS),
	    	    filename))
	    	buffer->last_mtime = 0;
    	    else buffer->last_mtime = file_info.mtime;
    	    
    	    result = TRUE;
    	}
    }
    else g_warning("buffer_autoload: filename == NULL || autosave_filename == NULL");
    	    
    g_free(autosave_filename);
    
    return result;
}

gboolean buffer_have_newer_autosave(Buffer *buffer)
{
    gboolean result = FALSE;
    const gchar *filename = buffer->filename;
    gchar *autosave_filename = buffer_get_autosave_filename(buffer);
    if (filename != NULL || autosave_filename != NULL)
    {
    	gboolean have_file_info;
    	GnomeVFSFileInfo file_info;
    	gboolean have_autosave_file_info;
    	GnomeVFSFileInfo autosave_file_info;
    		
	have_file_info = !gnome_vfs_error(NULL,
	    	gnome_vfs_get_file_info(filename, &file_info,
	    	    GNOME_VFS_FILE_INFO_FOLLOW_LINKS),
	    	filename);
	
	have_autosave_file_info = !gnome_vfs_error(NULL,
	    	gnome_vfs_get_file_info(autosave_filename, &autosave_file_info,
	    	    GNOME_VFS_FILE_INFO_FOLLOW_LINKS),
	    	autosave_filename);
	    	
	result = have_autosave_file_info
	    	&& (!have_file_info
	    	    || autosave_file_info.mtime > file_info.mtime);
    }
    else g_warning("buffer_have_newer_autosave: filename == NULL || autosave_filename == NULL");
    
    g_free(autosave_filename);
    
    return result;
}

gboolean buffer_replace_selection(Buffer *buffer, const gchar *str, gint len)
{
    Mark *insert = gtk_text_buffer_get_insert(GTK_TEXT_BUFFER(buffer));
    Mark *selection_bound = gtk_text_buffer_get_selection_bound(GTK_TEXT_BUFFER(buffer));
    Iter start, end;
    
    if (gtk_text_buffer_get_selection_bounds(GTK_TEXT_BUFFER(buffer), &start, &end))
 	gtk_text_buffer_delete(GTK_TEXT_BUFFER(buffer), &start, &end);

    gtk_text_buffer_move_mark(GTK_TEXT_BUFFER(buffer), selection_bound, &start);
    gtk_text_buffer_insert(GTK_TEXT_BUFFER(buffer), &end, str, len);
    gtk_text_buffer_move_mark(GTK_TEXT_BUFFER(buffer), insert, &end);
    
    return TRUE;
}

gboolean buffer_get_visual_selection_bounds(Buffer *buffer, Iter *start, Iter *end)
{
    gboolean have_selection = gtk_text_buffer_get_selection_bounds(GTK_TEXT_BUFFER(buffer), start, end);
    if (have_selection)
    {
	if (gtk_text_iter_ends_line(start))
	{
	    while (!gtk_text_iter_starts_line(start))
	    {
		if (!gtk_text_iter_forward_char(start))
		    break;
	    }
	}
	
	if (gtk_text_iter_starts_line(end))
	{
	    do
	    {
		if (!gtk_text_iter_backward_char(end))
		    break;
	    }
	    while (!gtk_text_iter_ends_line(end));
	}
    
	have_selection = !gtk_text_iter_equal(start, end);
    }
    return have_selection;
}

static Iter buffer_get_cursor_iter(Buffer *buffer)
{
    GtkTextMark *insert = gtk_text_buffer_get_insert(GTK_TEXT_BUFFER(buffer));
    Iter iter;
    
    gtk_text_buffer_get_iter_at_mark(GTK_TEXT_BUFFER(buffer), &iter, insert);
    
    return iter;
}

gint buffer_get_cursor_line_num(Buffer *buffer)
{
    Iter iter = buffer_get_cursor_iter(buffer);

    return gtk_text_iter_get_line(&iter);
}

gint buffer_get_cursor_column_num(Buffer *buffer)
{
    Iter iter = buffer_get_cursor_iter(buffer);

    return buffer_get_column(buffer, &iter);
}

void buffer_goto_line_number(Buffer *buffer, gint line_number)
{
    Iter iter;

    gtk_text_buffer_get_iter_at_line(GTK_TEXT_BUFFER(buffer), &iter, line_number);

    gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), &iter);

    if (buffer == get_cur_buffer())
        view_scroll_to_insert(get_cur_view());
}

void buffer_goto_line_number_next_resize(Buffer *buffer, gint line_number)
{
    Iter iter;

    gtk_text_buffer_get_iter_at_line(GTK_TEXT_BUFFER(buffer), &iter, line_number);

    gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), &iter);

    if (buffer == get_cur_buffer())
        view_scroll_next_resize(get_cur_view());
}

gboolean buffer_is_word_char(const Buffer *buffer, gunichar ch)
{
    return (ch >= '0' && ch <= '9')
	    || (ch >= 'a' && ch <= 'z')
	    || (ch >= 'A' && ch <= 'Z')
	    || ch == '_';
} 

gboolean buffer_is_word(const Buffer *buffer, const gchar *str)
{
    while (*str)
    {
	if (!buffer_is_word_char(buffer, g_utf8_get_char(str)))
	    return FALSE;
	str = g_utf8_next_char(str);
    }
    
    return TRUE;
} 

gboolean buffer_revert(Buffer *buffer, GError **error)
{
    gboolean result = FALSE;
    
    if (buffer != NULL)
	result = buffer_load(buffer, error);
    else g_warning("buffer_revert: buffer == NULL");
    
    return result;
}

static void buffer_desc_changed_cb(Buffer *buffer, const gchar *desc,
    	gpointer user_data)
{
    buffer_set_title(buffer);
}

static void buffer_modified_changed_cb(Buffer *buffer, gpointer user_data)
{
    buffer_set_title(buffer);
}

static void buffer_gconf_value_changed_cb(GConfClient* client,
        const gchar* key, GConfValue* value, Buffer *buffer)
{
    buffer_preferences_changed_cb(buffer, NULL);
}

static void update_line_column_nums(Buffer *buffer)
{
    status_bar_set_line_num(buffer_get_cursor_line_num(buffer));
    status_bar_set_column_num(buffer_get_cursor_column_num(buffer));
}

static void buffer_changed_cb(Buffer *buffer, gpointer user_data)
{
    if (buffer == get_cur_buffer())
    {
        view_scroll_to_insert(get_cur_view());
        update_line_column_nums(buffer);
    }

    gtk_text_buffer_set_modified(GTK_TEXT_BUFFER(buffer), TRUE);
}

static void buffer_mark_set_cb(Buffer *buffer, Iter *iter,
	GtkTextMark *mark, gpointer user_data)
{
    GtkTextMark *insert = gtk_text_buffer_get_insert(GTK_TEXT_BUFFER(buffer));
    
    if (mark == insert)
    {
    	if (buffer == get_cur_buffer())
            view_scroll_to_insert(get_cur_view());

	update_line_column_nums(buffer);
    }
}

static void buffer_init(Buffer *buffer)
{
    GConfClient *client = gconf_client_get_default();
    int i;
    const gchar *local_codeset;
    g_get_charset(&local_codeset);
    
    buffer->filename = NULL;
    buffer->last_mtime = 0;
    for (i=0; i<2; ++i)
	buffer->views[i] = NULL;
    buffer->is_temp = FALSE;
    buffer->last_focus_time = 0;
    buffer->codeset = g_strdup("");

    buffer_set_modified(buffer, FALSE);

    file_list_add_buffer(FILE_LIST(file_list), buffer);
    
    g_signal_connect(G_OBJECT(buffer), "desc-changed",
    	    GTK_SIGNAL_FUNC(buffer_desc_changed_cb), NULL);
    g_signal_connect(G_OBJECT(buffer), "modified-changed",
    	    GTK_SIGNAL_FUNC(buffer_modified_changed_cb), NULL);
    g_signal_connect(G_OBJECT(buffer), "changed",
    	    GTK_SIGNAL_FUNC(buffer_changed_cb), NULL);
    g_signal_connect(G_OBJECT(buffer), "mark-set",
    	    GTK_SIGNAL_FUNC(buffer_mark_set_cb), NULL);
    g_signal_connect(G_OBJECT(client), "value_changed",
    	    GTK_SIGNAL_FUNC(buffer_gconf_value_changed_cb), buffer);
}

void buffer_finalize(GObject *_buffer)
{
    Buffer *buffer = (Buffer *)_buffer;
    int i;
    
    file_list_remove_buffer(FILE_LIST(file_list), buffer);
    
    for (i=0; i<2; ++i)
    {
	if (buffer->views[i] != NULL)
	    g_object_ref(G_OBJECT(buffer->views[i]));
    }
    
    g_free(buffer->codeset);
    g_free(buffer->filename);
}

static void buffer_class_init(BufferClass *class)
{
    buffer_signals[FILENAME_CHANGED_SIGNAL] =
	    g_signal_new("filename-changed",
			  G_TYPE_FROM_CLASS(class),
			  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			  G_STRUCT_OFFSET(BufferClass, filename_changed),
			  NULL, NULL,
			  g_cclosure_marshal_VOID__STRING, G_TYPE_NONE, 1,
			  G_TYPE_STRING);
    buffer_signals[DESC_CHANGED_SIGNAL] =
	    g_signal_new("desc-changed",
			  G_TYPE_FROM_CLASS(class),
			  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			  G_STRUCT_OFFSET(BufferClass, desc_changed),
			  NULL, NULL,
			  g_cclosure_marshal_VOID__STRING, G_TYPE_NONE, 1,
			  G_TYPE_STRING);
    buffer_signals[FOCUS_IN_SIGNAL] =
	    g_signal_new("focus-in",
			  G_TYPE_FROM_CLASS(class),
			  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			  G_STRUCT_OFFSET(BufferClass, focus_in),
			  NULL, NULL,
			  g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1,
			  G_TYPE_POINTER);
    buffer_signals[FOCUS_OUT_SIGNAL] =
	    g_signal_new("focus-out",
			  G_TYPE_FROM_CLASS(class),
			  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			  G_STRUCT_OFFSET(BufferClass, focus_out),
			  NULL, NULL,
			  g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1,
			  G_TYPE_POINTER);
    buffer_signals[DESTROY_SIGNAL] =
	    g_signal_new("destroy",
			  G_TYPE_FROM_CLASS(class),
			  G_SIGNAL_RUN_FIRST | G_SIGNAL_ACTION,
			  G_STRUCT_OFFSET(BufferClass, destroy),
			  NULL, NULL,
			  g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

    G_OBJECT_CLASS(class)->finalize = buffer_finalize;
}

GType buffer_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
	static const GTypeInfo info =
	{
	    sizeof (BufferClass),
	    NULL, /* buffer_base_init */
	    NULL, /* base_finalize */
	    (GClassInitFunc) buffer_class_init,
	    NULL, /* class_finalize */
	    NULL, /* class_data */
	    sizeof (Buffer),
	    0,	  /* n_preallocs */
	    (GInstanceInitFunc) buffer_init,
	};

	type = g_type_register_static(GTK_TYPE_SOURCE_BUFFER,
		"Buffer", &info, 0);
    }

    return type;
}

Buffer *buffer_new(void)
{
    Buffer *buffer = g_object_new(TYPE_BUFFER, NULL);

    views_add_buffer(buffer);

    buffer_preferences_changed_cb(buffer, NULL);

    return buffer;
}

Buffer *buffer_new_with_filename(const gchar *filename)
{
    Buffer *buffer = g_object_new(TYPE_BUFFER, NULL);

    buffer_set_filename(buffer, filename);

    views_add_buffer(buffer);

    buffer_preferences_changed_cb(buffer, NULL);

    return buffer;
}

void buffer_free(Buffer *buffer)
{
    g_signal_emit(G_OBJECT(buffer), buffer_signals[DESTROY_SIGNAL], 0);

    file_list_remove_buffer(FILE_LIST(file_list), buffer);
    
    views_remove_buffer(buffer);
}

static gboolean view_key_press_event_cb(GtkWidget *widget,
		GdkEventKey *event, gpointer user_data)
{
    View *view = VIEW(widget);
    Buffer *buffer = view_get_buffer(view);
    
    if (event->keyval == GDK_Tab || event->keyval == GDK_ISO_Left_Tab)
    {
	Iter start, end;
	if (buffer_get_selection_bounds(buffer, &start, &end)
		&& gtk_text_iter_get_line(&start) != gtk_text_iter_get_line(&end))
	{
	    Iter cur;
	    GtkTextMark *start_mark, *end_mark;
	    
	    if (gtk_text_iter_ends_line(&start)
		    && !gtk_text_iter_starts_line(&start))
	    {
		do 
		    gtk_text_iter_forward_char(&start);
		while (!gtk_text_iter_starts_line(&start));
	    }
	    else
	    {
		while (!gtk_text_iter_starts_line(&start))
		    gtk_text_iter_backward_char(&start);
	    }
	    
	    if (gtk_text_iter_starts_line(&end)
		    && !gtk_text_iter_ends_line(&end))
	    {
		do 
		    gtk_text_iter_backward_char(&end);
		while (!gtk_text_iter_ends_line(&end));
	    }
	    else
	    {
		while (!gtk_text_iter_ends_line(&end))
		    gtk_text_iter_forward_char(&end);
	    }
	    
	    cur = start;
	    
	    start_mark = gtk_text_buffer_create_mark(GTK_TEXT_BUFFER(buffer), "tmp-start", &start, TRUE);
	    end_mark = gtk_text_buffer_create_mark(GTK_TEXT_BUFFER(buffer), "tmp-end", &end, FALSE);
	    
	    gtk_text_buffer_begin_user_action(GTK_TEXT_BUFFER(buffer));
	    for (; gtk_text_iter_compare(&cur, &end) < 0; gtk_text_iter_forward_line(&cur))
	    {
		iter_goto_visible_line_start(&cur);
	    
		if (event->keyval == GDK_Tab)
		    buffer_indent(buffer, &cur);
		else
		    buffer_exdent(buffer, &cur);
		
		gtk_text_buffer_get_iter_at_mark(GTK_TEXT_BUFFER(buffer), &end, end_mark); 
	    }
	    gtk_text_buffer_end_user_action(GTK_TEXT_BUFFER(buffer));
    	    gtk_text_buffer_get_iter_at_mark(GTK_TEXT_BUFFER(buffer), &start,
    	    	    start_mark); 
	    
	    gtk_text_buffer_delete_mark(GTK_TEXT_BUFFER(buffer), end_mark);
	    gtk_text_buffer_delete_mark(GTK_TEXT_BUFFER(buffer), start_mark);
	    
	    view_select(view, &start, &end);
	    
	    return TRUE;
	}
	else if (event->keyval == GDK_Tab)
	{
	    buffer_indent_at_cursor(buffer);
	    
	    return TRUE;
	}
	else if (event->keyval == GDK_ISO_Left_Tab)
	{
	    buffer_exdent_at_cursor(buffer);
	    
	    return TRUE;
	}
    }
    else if (event->keyval == GDK_BackSpace)
    {
    	Iter start;
	if (!gtk_text_buffer_get_selection_bounds(GTK_TEXT_BUFFER(buffer),
	    	&start, NULL))
	    return buffer_backspace(buffer, &start);
    }
    else if (event->keyval == GDK_Delete)
    {
    	Iter start, end;
	if (!gtk_text_buffer_get_selection_bounds(GTK_TEXT_BUFFER(buffer), &start, &end)
    	    	&& !gtk_text_iter_ends_line(&end))
	{
	    return buffer_delete(buffer, &end);
	}
    }
    if ((event->keyval == GDK_Left)
	    && (event->state & (GDK_SHIFT_MASK | GDK_CONTROL_MASK)) == 0)
    {
	Iter start, end;
	if (buffer_get_visual_selection_bounds(buffer, &start, &end))
	{
	    gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), &start);
	    return TRUE;
	}
#if 0	
	else if (!gtk_text_iter_starts_line(&start)
	    	&& buffer_in_leading_space(buffer, &start))
	{
	    iter_backward_to_line_start(&start);
	    gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), &start);
	    return TRUE;
	}
#endif
    }
    
    if ((event->keyval == GDK_Home)
	    && (event->state & (GDK_SHIFT_MASK | GDK_CONTROL_MASK)) == 0)
    {
	Iter start, end;
	if (buffer_get_visual_selection_bounds(buffer, &start, &end))
	{
	    gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), &start);
	    return gtk_text_iter_get_line(&start) != gtk_text_iter_get_line(&end);
	}
	else if (buffer_in_leading_space(buffer, &start)
	        && !gtk_text_iter_starts_line(&start))
	{
	    iter_goto_line_start(&start);
	    gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), &start);
	    return TRUE;
	}
    }
    
    if ((event->keyval == GDK_Up)
	    && (event->state & (GDK_SHIFT_MASK | GDK_CONTROL_MASK)) == 0)
    {
	Iter start, end;
	if (buffer_get_visual_selection_bounds(buffer, &start, &end))
	{
	    gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), &start);
	    return gtk_text_iter_get_line(&start) != gtk_text_iter_get_line(&end);
	}
    }
    
    if ((event->keyval == GDK_Right)
	    && (event->state & (GDK_SHIFT_MASK | GDK_CONTROL_MASK)) == 0)
    {
	Iter start, end;
	if (buffer_get_visual_selection_bounds(buffer, &start, &end))
	{
	    gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), &end);
	    return TRUE;
	}
#if 0
	else if (buffer_in_leading_space(buffer, &start))
	{
	    Iter maybe_new_start = start;
	    iter_goto_visible_line_start(&maybe_new_start);
	    if (!gtk_text_iter_equal(&maybe_new_start, &start))
	    {
	    	gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer),
	    	    	&maybe_new_start);
	    	return TRUE;
	    }
	}
#endif
    }
    
    if ((event->keyval == GDK_End)
	    && (event->state & (GDK_SHIFT_MASK | GDK_CONTROL_MASK)) == 0)
    {
	Iter start, end;
	if (buffer_get_visual_selection_bounds(buffer, &start, &end))
	{
	    gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), &end);
	    return gtk_text_iter_get_line(&start) != gtk_text_iter_get_line(&end);
	}
	else if (buffer_in_trailing_space(buffer, &end)
	        && !gtk_text_iter_ends_line(&end))
	{
	    iter_goto_line_end(&end);
	    gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), &end);
	    return TRUE;
	}
    }
     
    if ((event->keyval == GDK_Down)
	    && (event->state & (GDK_SHIFT_MASK | GDK_CONTROL_MASK)) == 0)
    {
	Iter start, end;
	if (buffer_get_visual_selection_bounds(buffer, &start, &end))
	{
	    gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), &end);
	    return gtk_text_iter_get_line(&start) != gtk_text_iter_get_line(&end);
	}
    }
     
    return FALSE;
}

static gboolean view_focus_in_event_cb(GtkWidget *view,
	GdkEventFocus *event, gpointer user_data)
{
    Buffer *buffer = view_get_buffer(VIEW(view));
    
    buffer->last_focus_time = time(NULL);
    
    gchar *new_dirname = buffer_get_dirname(buffer);
    if (new_dirname != NULL)
    {
    	g_free(last_dirname);
    	last_dirname = new_dirname;
    }
    
    buffer_set_title(buffer);

    update_line_column_nums(buffer);
    status_bar_show_numbers();
    
    g_signal_emit(G_OBJECT(buffer), buffer_signals[FOCUS_IN_SIGNAL], 0, view);
    
    buffer_check_file_changed(buffer);
    
    return FALSE;
}

static gboolean view_focus_out_event_cb(GtkWidget *view,
	GdkEventFocus *event, gpointer user_data)
{
    Buffer *buffer = view_get_buffer(VIEW(view));

    g_signal_emit(G_OBJECT(buffer), buffer_signals[FOCUS_OUT_SIGNAL], 0, view);
	
    return FALSE;
}

static View *buffer_create_view(Buffer *buffer)
{
    View *view = view_new(buffer);

    gtk_source_view_set_auto_indent(GTK_SOURCE_VIEW(view), TRUE);
    gtk_source_view_set_show_margin(GTK_SOURCE_VIEW(view), TRUE);
    //gtk_source_view_set_show_line_markers(GTK_SOURCE_VIEW(buffer->view), TRUE);
    gtk_source_view_set_highlight_current_line(GTK_SOURCE_VIEW(view), TRUE);
    
    g_signal_connect(G_OBJECT(view), "key-press-event",
	    GTK_SIGNAL_FUNC(view_key_press_event_cb), NULL);
    g_signal_connect(G_OBJECT(view), "focus-in-event",
	    GTK_SIGNAL_FUNC(view_focus_in_event_cb), NULL);
    g_signal_connect(G_OBJECT(view), "focus-out-event",
	    GTK_SIGNAL_FUNC(view_focus_out_event_cb), NULL);
	    
    g_object_ref(G_OBJECT(view));

    return view;
}

View *buffer_get_view(Buffer *buffer, int i)
{
    g_assert(i >=0 && i < 2);
    
    if (buffer->views[i] == NULL)
    {
	buffer->views[i] = buffer_create_view(buffer);
	g_assert(buffer->views[i] != NULL);
    }
    return buffer->views[i];
}

View *buffer_get_primary_view(Buffer *buffer)
{
    return buffer_get_view(buffer, 0);
}

View *buffer_get_secondary_view(Buffer *buffer)
{
    return buffer_get_view(buffer, 1);
}

gboolean buffer_get_selection_bounds(Buffer *buffer, Iter *start, Iter *end)
{
    return gtk_text_buffer_get_selection_bounds(GTK_TEXT_BUFFER(buffer),
	    start, end);
}

gboolean buffer_have_selection(Buffer *buffer)
{
    return gtk_text_buffer_get_selection_bounds(GTK_TEXT_BUFFER(buffer),
	    NULL, NULL);
}

gboolean buffer_get_modified(Buffer *buffer)
{
    return !buffer->is_temp && gtk_text_buffer_get_modified(GTK_TEXT_BUFFER(buffer));
}

void buffer_set_modified(Buffer *buffer, gboolean modified)
{
    gtk_text_buffer_set_modified(GTK_TEXT_BUFFER(buffer), modified);
}

gboolean buffer_has_file_changed(Buffer *buffer)
{
    if (buffer->filename != NULL)
    {
	GnomeVFSFileInfo file_info;

	if (gnome_vfs_error(NULL,
	    	gnome_vfs_get_file_info(buffer->filename, &file_info,
	    	    GNOME_VFS_FILE_INFO_FOLLOW_LINKS),
	    	buffer->filename))
	    return FALSE;
    	else return buffer->last_mtime < file_info.mtime;
    }
    else return FALSE;
}

void buffer_freshen(Buffer *buffer)
{
     time(&buffer->last_mtime);
}

void buffer_check_file_changed(Buffer *buffer)
{
    if (buffer != NULL)
    {
	gboolean has_file_changed = buffer_has_file_changed(buffer);
	if (has_file_changed)
	{
	    gboolean is_buffer_modified = buffer_get_modified(buffer);
	    if (!is_exiting || is_buffer_modified)
	    {
	        GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(app),
		        0, is_buffer_modified? GTK_MESSAGE_WARNING: GTK_MESSAGE_QUESTION,
		        GTK_BUTTONS_NONE,
		        _("File %s has changed; reload?%s"),
		        buffer_get_filename(buffer),
		        is_buffer_modified? _("  YOUR CHANGES WILL BE LOST"): "");
        
	        gtk_dialog_add_buttons(GTK_DIALOG(dialog),
		        GTK_STOCK_YES, GTK_RESPONSE_YES,
		        GTK_STOCK_NO, GTK_RESPONSE_NO,
		        NULL);
        
	        if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES)
	        {
		    GError *error = NULL;
		    Mark *insert;
		    Iter iter;
		    gint offset;
		    
		    insert = gtk_text_buffer_get_insert(GTK_TEXT_BUFFER(buffer));
		    gtk_text_buffer_get_iter_at_mark(GTK_TEXT_BUFFER(buffer),
		    	    &iter, insert);
		    offset = gtk_text_iter_get_offset(&iter);
		    
		    buffer_load(buffer, &error);
		    
		    if (error != NULL)
		        consume_error(error);
		    else
		    {
		        Iter end;
		        gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(buffer), &iter, &end);
		        gtk_text_iter_set_offset(&iter, offset);
		        if (gtk_text_iter_compare(&iter, &end) > 0)
		    	    iter = end;
		        gtk_text_buffer_place_cursor(GTK_TEXT_BUFFER(buffer), &iter);
		    }
	        }
	        else buffer_freshen(buffer);
        
	        gtk_widget_destroy(dialog);
	    }
	}
    }
    else g_warning("buffer_check_file_changed: buffer == NULL");
}

Buffer *get_cur_buffer(void)
{
    View *cur_view = get_cur_view();
    
    if (cur_view != NULL)
    	return view_get_buffer(cur_view);
    else return NULL;
}

void buffer_select_line(Buffer *buffer, const Iter *iter)
{
    if (buffer != NULL && iter != NULL)
    {
	Iter start = *iter, end = *iter;
	
	iter_goto_line_start(&start);
	iter_goto_line_end(&end);
	
	buffer_select(buffer, &start, &end);
    }
    else g_warning("buffer_select_line: buffer != NULL || iter != NULL");
}

void buffer_clear(Buffer *buffer)
{
    Iter start, end;
    
    gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(buffer), &start, &end);
    
    gtk_text_buffer_delete(GTK_TEXT_BUFFER(buffer), &start, &end);
}

void buffer_set_is_temp(Buffer *buffer, gboolean is_temp)
{
    buffer->is_temp = is_temp;
}

gboolean buffer_get_is_temp(Buffer *buffer)
{
    return buffer->is_temp;
}

time_t buffer_get_last_focus_time(Buffer *buffer)
{
    return buffer->last_focus_time;
}

gchar *buffer_get_dirname(Buffer *buffer)
{
    gchar *dirname_with_slash = NULL;
    
    if (buffer != NULL)
    {
	const gchar *filename = buffer_get_filename(buffer);
	if (filename != NULL)
	{
	    gchar *dirname = g_path_get_dirname(filename);
	    dirname_with_slash = g_strconcat(dirname, "/", NULL);
	    g_free(dirname);
	}
    }
    
    return dirname_with_slash;
}

Mark *buffer_get_insert_mark(Buffer *buffer)
{
    return MARK(gtk_text_buffer_get_insert(GTK_TEXT_BUFFER(buffer)));
}

Iter buffer_get_iter_at_mark(Buffer *buffer, Mark *mark)
{
    Iter iter;
    
    gtk_text_buffer_get_iter_at_mark(GTK_TEXT_BUFFER(buffer), &iter, mark);
    
    return iter;
}

Iter buffer_get_insert_iter(Buffer *buffer)
{
    Mark *insert = buffer_get_insert_mark(buffer);
    
    return buffer_get_iter_at_mark(buffer, insert);
}

gboolean buffer_insert_on_first_line(Buffer *buffer)
{
    Iter insert = buffer_get_insert_iter(buffer);
    
    return !gtk_text_iter_backward_line(&insert);
}

gboolean buffer_insert_on_last_line(Buffer *buffer)
{
    Iter insert = buffer_get_insert_iter(buffer);
    
    return !gtk_text_iter_forward_line(&insert);
}

gboolean buffer_insert_at_line_start(Buffer *buffer)
{
    Iter insert = buffer_get_insert_iter(buffer);
    
    return gtk_text_iter_starts_line(&insert);
}

gboolean buffer_insert_at_line_end(Buffer *buffer)
{
    Iter insert = buffer_get_insert_iter(buffer);
    
    return gtk_text_iter_ends_line(&insert);
}

gboolean buffer_is_empty(Buffer *buffer)
{
    return gtk_text_buffer_get_char_count(GTK_TEXT_BUFFER(buffer)) == 0;
}

gboolean buffer_can_undo(Buffer *buffer)
{
    return gtk_source_buffer_can_undo(GTK_SOURCE_BUFFER(buffer));
}

gboolean buffer_can_redo(Buffer *buffer)
{
    return gtk_source_buffer_can_redo(GTK_SOURCE_BUFFER(buffer));
}

gchar *buffer_get_mime_type(Buffer *buffer)
{
    const gchar *filename = buffer_get_filename(buffer);
    if (filename != NULL)
    {
    	GnomeVFSFileInfo file_info;
    	GError *error = NULL;
	
		if (gnome_vfs_error(&error,
	    		gnome_vfs_get_file_info(filename, &file_info,
	    		GNOME_VFS_FILE_INFO_GET_MIME_TYPE
	    		    | GNOME_VFS_FILE_INFO_FOLLOW_LINKS),
	    		filename))
		return NULL;
			    
		return g_strdup(gnome_vfs_file_info_get_mime_type(&file_info));
	}
	else return NULL;
}

void buffer_set_codeset(Buffer *buffer, const gchar *codeset)
{
    g_free(buffer->codeset);
    buffer->codeset = g_strdup(codeset);
}

const gchar *buffer_get_codeset(Buffer *buffer)
{
    return buffer->codeset;
}

const gchar *buffer_get_last_dirname()
{
    return last_dirname;
}
