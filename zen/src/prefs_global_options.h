/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef PREFS_GLOBAL_OPTIONS_H
#define PREFS_GLOBAL_OPTIONS_H
#include <glib.h>
G_BEGIN_DECLS

#include "prefs_options.h"

#define TYPE_PREFS_GLOBAL_OPTIONS \
    	(prefs_global_options_get_type ())
#define PREFS_GLOBAL_OPTIONS(obj) \
    	(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
    	    	TYPE_PREFS_GLOBAL_OPTIONS, \
    	    	PrefsGlobalOptions))
#define PREFS_GLOBAL_OPTIONS_CLASS(klass) \
    	(G_TYPE_CHECK_CLASS_CAST ((klass), \
    	    	TYPE_PREFS_GLOBAL_OPTIONS, \
    	    	PrefsGlobalOptionsClass))
#define IS_PREFS_GLOBAL_OPTIONS(obj) \
    	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
    	    	TYPE_PREFS_GLOBAL_OPTIONS))
#define IS_PREFS_GLOBAL_OPTIONS_CLASS(klass) \
    	(G_TYPE_CHECK_CLASS_TYPE ((klass), \
    	    	TYPE_PREFS_GLOBAL_OPTIONS))
#define PREFS_GLOBAL_OPTIONS_GET_CLASS(obj) \
    	(G_TYPE_INSTANCE_GET_CLASS ((obj), \
    	    	TYPE_PREFS_GLOBAL_OPTIONS, \
    	    	PrefsGlobalOptionsClass))

typedef struct _PrefsGlobalOptions        PrefsGlobalOptions;
typedef struct _PrefsGlobalOptionsClass   PrefsGlobalOptionsClass;

struct _PrefsGlobalOptionsClass
{
    PrefsOptionsClass parent_class;
};

struct _PrefsGlobalOptions
{
    PrefsOptions parent;
    
    GtkWidget *old_file_chooser_checkbox;
    
    GtkWidget *background_color;
    GtkWidget *foreground_color;
};

GType prefs_global_options_get_type(void);

GtkWidget *prefs_global_options_new(void);

G_END_DECLS
#endif
