/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "zen.h"
#include "find.h"
#include "view.h"
#include "buffer.h"
#include "file_list.h"
#include "prefs.h"
#include "status_bar.h"
#include "mark.h"
#include "views.h"
#include "prefs_dialog.h"
#include "cmd.h"
#include "menu.h"
#include "make.h"

#include <gnome.h>
#include <glib/gprintf.h>
#include <libgnomeui/gnome-app.h>
#include <gtk/gtktextbuffer.h>
#include <gtksourceview/gtksourceview.h>
#include <gtksourceview/gtksourcelanguagesmanager.h>
#include <gtksourceview/gtksourceiter.h>
#include <gtksourceview/gtksourcetag.h>
#include <gconf/gconf-client.h>
#include <locale.h>
#include <sys/signal.h>

#undef DEBUG
#undef VIM_COLORS
#define CUSTOM_COLORS

GtkWidget *app;
GtkWidget *paned;
GtkWidget *file_list;

void consume_error(GError *error)
{
    if (error != NULL)
    {
	GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(app),
    	    	GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR,
    	    	GTK_BUTTONS_CLOSE, _("%s (error code %d)"),
    	    	error->message, error->code);
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	status_bar_set("%s (error code %d)", error->message, error->code);

	g_error_free(error);
    }
    else g_warning("consume_error: error == NULL");
}

gboolean gnome_vfs_error(GError **error, GnomeVFSResult result, const char *uri)
{
    if (result && error && !*error)
    {
    	if (uri)
            *error = g_error_new(g_quark_from_string("ZEN_GNOMEVFS_ERROR"),
            	    result,
            	    "%s (URI %s)",
            	    gnome_vfs_result_to_string(result),
            	    uri);
        else
            *error = g_error_new(g_quark_from_string("ZEN_GNOMEVFS_ERROR"),
            	    result,
            	    "%s",
            	    gnome_vfs_result_to_string(result));
    }
    
    return result != 0;
}

#define MAIN_CONF_PATH(key) CONF_PATH("Geometry") "/" key

#define CONF_FILE_LIST_WIDTH	MAIN_CONF_PATH("FileListWidth")
#define CONF_WIDTH		MAIN_CONF_PATH("Width")
#define CONF_HEIGHT		MAIN_CONF_PATH("Height")

static void load_app_conf(GtkWindow *app)
{
    GConfClient *client = gconf_client_get_default();
    gint width, height;

    width = gconf_client_get_int(client, CONF_WIDTH, NULL);
    if (width == 0) width = 400;
    height = gconf_client_get_int(client, CONF_HEIGHT, NULL);
    if (height == 0) height = 300;
    
    gtk_window_set_default_size(app, width, height);
}

void save_app_conf(GtkWindow *app)
{
    GConfClient *client = gconf_client_get_default();
    gint width, height;

    gtk_window_get_size(app, &width, &height);
    gconf_client_set_int(client, CONF_WIDTH, width, NULL);
    gconf_client_set_int(client, CONF_HEIGHT, height, NULL);
}

static void load_paned_conf(GtkPaned *paned)
{
    GConfClient *client = gconf_client_get_default();
    gint pos;

    pos = gconf_client_get_int(client, CONF_FILE_LIST_WIDTH, NULL);
    if (pos == 0) pos = 100;
    
    gtk_paned_set_position(paned, pos);
}

void save_paned_conf(GtkPaned *paned)
{
    GConfClient *client = gconf_client_get_default();

    gconf_client_set_int(client, CONF_FILE_LIST_WIDTH,
	    gtk_paned_get_position(paned), NULL);
}

gboolean check_for_changed_file(gpointer user_data)
{
    View *view = get_cur_view();
    
    if (view != NULL && GTK_WIDGET_HAS_FOCUS(GTK_WIDGET(get_cur_view())))
	buffer_check_file_changed(view_get_buffer(view));
    
    return TRUE;
}

void gconf_client_error_handler_cb(GConfClient *client, GError *error)
{
    if (error != NULL)
	consume_error(error);
}

extern Buffer *open_file(const gchar *filename, gboolean request_create);

void we_are_dying(int signum)
{  
    FileListIter iter;
    GError *error = NULL;

    signal(signum, SIG_IGN);
    
    g_warning(_("Caught signal %d; saving backup copies of files and exiting"),
    	    signum);
    
    for (file_list_iter_start(FILE_LIST(file_list), &iter);
    	    !file_list_iter_done(&iter);
    	    file_list_iter_next(&iter))
    {
	Buffer *buffer = file_list_iter_get_buffer(&iter);

    	if (buffer_get_modified(buffer))
    	    buffer_autosave(buffer, &error);
    }
    
    signal(signum, SIG_DFL);
    kill(getpid(), signum);
}

int main(int argc, char *argv[])
{
    struct poptOption options[] = {
      {
	NULL,
	'\0',
	0,
	NULL,
	0,
	NULL,
	NULL
      }
    };
    poptContext pctx;
    const char *const *args;
    GtkWidget *vbox;

    bindtextdomain (GETTEXT_PACKAGE, ZENLOCALEDIR);
    bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
    textdomain (GETTEXT_PACKAGE);
    
    setlocale(LC_ALL, "");
  
    gnome_init_with_popt_table(PACKAGE_NAME, VERSION_NAME, argc, argv, options, 0, &pctx);

    args = poptGetArgs(pctx);

    gdk_rgb_init();

    app = gnome_app_new(PACKAGE_NAME, FRIENDLY_NAME);
    load_app_conf(GTK_WINDOW(app));

    gconf_client_set_global_default_error_handler(gconf_client_error_handler_cb);

    gtk_window_set_policy(GTK_WINDOW(app), FALSE, TRUE, FALSE);
    gtk_window_set_wmclass(GTK_WINDOW(app), PACKAGE_NAME, FRIENDLY_NAME);

    gtk_signal_connect(GTK_OBJECT(app), "delete_event",
	    GTK_SIGNAL_FUNC(cmd_quit), NULL);

    find_replace = find_replace_new();

    paned = gtk_hpaned_new();
    {
	GtkWidget *frame;
    	
    	file_list = file_list_new();
    	
	frame = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(frame),
	    	GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(frame),
	    	GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_container_add(GTK_CONTAINER(frame), file_list);
	gtk_paned_add1(GTK_PANED(paned), frame);
    }
    gtk_widget_show_all(paned);
    gtk_paned_add2(GTK_PANED(paned), GTK_WIDGET(views_new()));

    vbox = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), menu_new(GTK_WINDOW(app)), FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(paned), TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), status_bar_new(GTK_WINDOW(app)), FALSE, FALSE, 0);

    gnome_app_set_contents(GNOME_APP(app), vbox);

    load_paned_conf(GTK_PANED(paned));
    
    prefs_init(TRUE);

    status_bar_hide_numbers();

    if (args != NULL)
    {
	int i;
	Buffer *last_buffer = NULL;
	Buffer *buffer = NULL;

	for (i=0; args[i]; ++i)
	{
	    Buffer *new_buffer = open_file(args[i], TRUE);
	    if (new_buffer != NULL)
	    {
	        last_buffer = buffer;
	    	buffer = new_buffer;
    	    } 
	}

	if (buffer != NULL)
	    view_focus(views_set_one_view(buffer));
	if (last_buffer != NULL && i == 2)
	    views_set_bottom_view(last_buffer);
    }

    poptFreeContext(pctx);

    gtk_timeout_add(1000, check_for_changed_file, NULL);

    signal(SIGINT, we_are_dying);
    signal(SIGQUIT, we_are_dying);
    signal(SIGTERM, we_are_dying);

    gtk_widget_show(app);
    gtk_main();

    prefs_free();

    g_object_unref(G_OBJECT(find_replace));

    gnome_config_sync();

    return 0;
}
