/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef PREFS_FILE_TYPE_OPTIONS_H
#define PREFS_FILE_TYPE_OPTIONS_H
#include <glib.h>
G_BEGIN_DECLS

#include "prefs_options.h"

#define TYPE_PREFS_FILE_TYPE_OPTIONS \
    	(prefs_file_type_options_get_type ())
#define PREFS_FILE_TYPE_OPTIONS(obj) \
    	(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
    	    	TYPE_PREFS_FILE_TYPE_OPTIONS, \
    	    	PrefsFileTypeOptions))
#define PREFS_FILE_TYPE_OPTIONS_CLASS(klass) \
    	(G_TYPE_CHECK_CLASS_CAST ((klass), \
    	    	TYPE_PREFS_FILE_TYPE_OPTIONS, \
    	    	PrefsFileTypeOptionsClass))
#define IS_PREFS_FILE_TYPE_OPTIONS(obj) \
    	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
    	    	TYPE_PREFS_FILE_TYPE_OPTIONS))
#define IS_PREFS_FILE_TYPE_OPTIONS_CLASS(klass) \
    	(G_TYPE_CHECK_CLASS_TYPE ((klass), \
    	    	TYPE_PREFS_FILE_TYPE_OPTIONS))
#define PREFS_FILE_TYPE_OPTIONS_GET_CLASS(obj) \
    	(G_TYPE_INSTANCE_GET_CLASS ((obj), \
    	    	TYPE_PREFS_FILE_TYPE_OPTIONS, \
    	    	PrefsFileTypeOptionsClass))

typedef struct _PrefsFileTypeOptions        PrefsFileTypeOptions;
typedef struct _PrefsFileTypeOptionsClass   PrefsFileTypeOptionsClass;

struct _PrefsFileTypeOptionsClass
{
    PrefsOptionsClass parent_class;
};

struct _PrefsFileTypeOptions
{
    PrefsOptions parent;
    
    gchar *file_type;
    
    GtkWidget *font_sel;
    GtkWidget *use_tabs_checkbutton;
    GtkWidget *tab_entry;
    GtkWidget *indent_entry;
};

GType prefs_file_type_options_get_type(void);

GtkWidget *prefs_file_type_options_new(const gchar *file_type);

G_END_DECLS
#endif
