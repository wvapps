/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef ITER_H
#define ITER_H

#include <gtk/gtktextiter.h>

struct _Buffer;

typedef GtkTextIter Iter;

gboolean iter_starts_word(Iter *iter);
gboolean iter_ends_word(Iter *iter);

void iter_swap(Iter *one, Iter *two);

void iter_forward_to_line_end(Iter *iter);
void iter_backward_to_line_start(Iter *iter);

void iter_goto_line_start(Iter *iter);
void iter_goto_line_end(Iter *iter);

void iter_goto_visible_line_start(Iter *iter);
void iter_goto_visible_line_end(Iter *iter);

void iter_goto_space_group_start(Iter *iter);
void iter_goto_space_group_end(Iter *iter);

struct _Buffer *iter_get_buffer(Iter *iter);

#endif

