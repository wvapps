/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "prefs_dialog.h"

#include "prefs.h"
#include "zen.h"
#include "prefs_global_options.h"
#include "prefs_file_type_options.h"
#include "buffer.h"

#include <gtksourceview/gtksourcelanguagesmanager.h>
#include <gdk-pixbuf/gdk-pixdata.h>

extern const GdkPixdata zen_logo;

GType prefs_dialog_get_type (void)
{
    static GType type = 0;
    
    if (!type)
    {
    	static const GTypeInfo info =
    	{
    	    sizeof (PrefsDialogClass),
    	    NULL, /* base_init */
    	    NULL, /* base_finalize */
    	    NULL, /* (GClassInitFunc) */
    	    NULL, /* class_finalize */
    	    NULL, /* class_data */
    	    sizeof (PrefsDialog),
    	    0,	  /* n_preallocs */
    	    NULL  /* (GInstanceInitFunc) */
    	};
        
    	type = g_type_register_static (GTK_TYPE_DIALOG,
	    	"PrefsDialog", &info, 0);
    }
    
    return type;
}

static void prefs_dialog_add_category(GtkTreeStore *categories,
    	const gchar *name, GtkWidget *options, GtkSourceLanguage *source_language, 
    	GtkTreeIter *_iter, GtkTreeIter *parent)
{
    GtkTreeIter *iter, tmp_iter;
    
    if (_iter != NULL)
    	iter = _iter;
    else iter = &tmp_iter;

    gtk_tree_store_append(GTK_TREE_STORE(categories), iter, parent);
    gtk_tree_store_set(GTK_TREE_STORE(categories), iter,
    	    0, name, 1, options, 2, source_language, -1);
    	    
    g_object_ref(G_OBJECT(options));
}

static void prefs_dialog_add_file_types_categories(GtkTreeStore *categories,
    	GtkTreeIter *parent)
{
    const GSList *list = gtk_source_languages_manager_get_available_languages(source_languages_manager);
    while (list != NULL)
    {
    	GtkTreeIter iter;
	GtkSourceLanguage *source_language = (GtkSourceLanguage *)list->data;
	const GSList *mime_type_list =
	    	gtk_source_language_get_mime_types(source_language);
	const gchar *name = gtk_source_language_get_name(source_language);

    	prefs_dialog_add_category(categories,
    	    	name, prefs_file_type_options_new(name), source_language,
    	    	&iter, parent);
    	    	
	while (mime_type_list != NULL)
	{
	    //g_message(" %s", (gchar *)mime_type_list->data);
	    mime_type_list = g_slist_next(mime_type_list);
	}

	list = g_slist_next(list);
    }
}

static void prefs_dialog_add_categories(GtkTreeStore *categories)
{
    GtkTreeIter file_types_iter;
    GtkWidget *vbox;
    GtkWidget *label;
    GError *error = NULL;
    GdkPixbuf *pixbuf = gdk_pixbuf_from_pixdata(&zen_logo, FALSE, &error);
    
    prefs_dialog_add_category(categories, "Global",
    	    prefs_global_options_new(), NULL, NULL, NULL);
    
    vbox = gtk_vbox_new(FALSE, 8);
    gtk_box_pack_start(GTK_BOX(vbox),
    	gtk_image_new_from_pixbuf(pixbuf), FALSE, FALSE, 4);

    label = gtk_label_new(_("Select an options category on the left!"));
    gtk_label_set_line_wrap(GTK_LABEL(label), TRUE);
    gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 4);
    
    prefs_dialog_add_category(categories, "File Types",
    	    vbox, NULL, &file_types_iter, NULL);
    prefs_dialog_add_file_types_categories(categories, &file_types_iter);
}

void prefs_dialog_selection_changed_foreach_cb(GtkTreeModel *model,
        GtkTreePath *path, GtkTreeIter *iter, gpointer _dialog)
{
    PrefsDialog *dialog = (PrefsDialog *)_dialog;
    GtkWidget *options;
    
    gtk_tree_model_get(GTK_TREE_MODEL(dialog->categories), iter,
    	    1, &options, -1);

    if (gtk_bin_get_child(GTK_BIN(dialog->options_pane)) != GTK_WIDGET(options))
    {
    	if (gtk_bin_get_child(GTK_BIN(dialog->options_pane)) != NULL)
            gtk_container_remove(GTK_CONTAINER(dialog->options_pane),
            	    gtk_bin_get_child(GTK_BIN(dialog->options_pane)));
        gtk_widget_show_all(options);
        gtk_container_add(GTK_CONTAINER(dialog->options_pane),
            	GTK_WIDGET(options));
    }
}

static void prefs_dialog_selection_changed_cb(GtkTreeSelection *selection,
        gpointer _dialog)
{
    gtk_tree_selection_selected_foreach(selection,
    	    prefs_dialog_selection_changed_foreach_cb, _dialog);
}

typedef struct
{
    PrefsDialog *dialog;
    GtkSourceLanguage *source_language;
} SelectFileTypeData;

static gboolean prefs_dialog_select_file_type_cb(GtkTreeModel *model,
        GtkTreePath *path, GtkTreeIter *iter, gpointer _data)
{
    SelectFileTypeData *data = (SelectFileTypeData *)_data;
    GtkSourceLanguage *this_source_language;
    
    gtk_tree_model_get(data->dialog->categories, iter,
    	    2, &this_source_language, -1);
    
    if (this_source_language == data->source_language)
    {
        gtk_tree_view_scroll_to_cell(GTK_TREE_VIEW(data->dialog->view),
            	path, NULL, TRUE, 0.5, 1.0);
        gtk_tree_view_set_cursor(GTK_TREE_VIEW(data->dialog->view),
            	path, NULL, FALSE);
            	
        return TRUE;
    }
    else return FALSE;
}

static void prefs_dialog_select_file_type(PrefsDialog *dialog)
{
    Buffer *buffer = get_cur_buffer();
    if (buffer == NULL)
    	return;
    	
    gchar *mime_type = buffer_get_mime_type(buffer);
    if (mime_type == NULL)
    	return;

    SelectFileTypeData data;
    data.dialog = dialog;
    data.source_language = prefs_mime_type_get_source_language(mime_type);

    gtk_tree_model_foreach(dialog->categories,
    	    prefs_dialog_select_file_type_cb, &data);
    	    
    g_free(mime_type);  	
}

static GtkWidget *prefs_dialog_build_categories(PrefsDialog *dialog)
{
    GtkWidget *sw;
    GtkCellRenderer *renderer;
    GtkTreeViewColumn *column;
    GtkTreeSelection *selection;

    renderer = gtk_cell_renderer_text_new();

    column = gtk_tree_view_column_new();
    gtk_tree_view_column_pack_start(column, renderer, TRUE);
    gtk_tree_view_column_add_attribute(column, renderer, "text", 0);

    dialog->categories = GTK_TREE_MODEL(gtk_tree_store_new(3, G_TYPE_STRING, GTK_TYPE_WIDGET, GTK_TYPE_SOURCE_LANGUAGE));
    prefs_dialog_add_categories(GTK_TREE_STORE(dialog->categories));

    dialog->view = gtk_tree_view_new_with_model(dialog->categories);
    gtk_tree_view_append_column(GTK_TREE_VIEW(dialog->view), column);
    gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(dialog->view), FALSE);
 
    selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(dialog->view));
    gtk_tree_selection_set_mode(selection, GTK_SELECTION_BROWSE);
    g_signal_connect(G_OBJECT(selection), "changed",
    	    GTK_SIGNAL_FUNC(prefs_dialog_selection_changed_cb), dialog);

    gtk_tree_view_expand_all(GTK_TREE_VIEW(dialog->view));
    
    g_object_unref(dialog->categories); /* destroy model automatically with view */

    sw = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(sw), GTK_SHADOW_IN);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
	    GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
    gtk_container_add(GTK_CONTAINER(sw), dialog->view);

    gtk_widget_show_all(sw);
    
    return sw;
}

static GtkWidget *prefs_dialog_build_options(PrefsDialog *dialog)
{
    dialog->options_pane = GTK_FRAME(gtk_frame_new(NULL));
    
    gtk_frame_set_shadow_type(dialog->options_pane, GTK_SHADOW_NONE);
    
    gtk_widget_show(GTK_WIDGET(dialog->options_pane));
    
    return GTK_WIDGET(dialog->options_pane);
}

static GtkWidget *prefs_dialog_build_content(PrefsDialog *dialog)
{
    GtkWidget *hbox = gtk_hbox_new(FALSE, 4);
        
    gtk_box_pack_start(GTK_BOX(hbox), prefs_dialog_build_categories(dialog),
    	    FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), prefs_dialog_build_options(dialog),
    	    TRUE, TRUE, 0);

    prefs_dialog_select_file_type(dialog);

    gtk_widget_show_all(hbox);
    
    return hbox;
}

static void prefs_dialog_response_cb(PrefsDialog *dialog, gint response)
{
    switch (response)
    {
    	case GTK_RESPONSE_CLOSE:
	    gtk_widget_destroy(GTK_WIDGET(dialog));
	case GTK_RESPONSE_DELETE_EVENT:
	    break;
    }
}

GtkWidget *prefs_dialog_new(void)
{
    PrefsDialog *dialog = g_object_new(TYPE_PREFS_DIALOG, NULL);
    
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox),
    	    prefs_dialog_build_content(dialog), TRUE, TRUE, 0);

    gtk_dialog_add_button(GTK_DIALOG(dialog), "Close", GTK_RESPONSE_CLOSE);

    g_signal_connect(GTK_OBJECT(dialog), "response",
	    G_CALLBACK(prefs_dialog_response_cb), NULL);

    gtk_window_set_title(GTK_WINDOW(dialog), "Preferences");
    gtk_window_set_default_size(GTK_WINDOW(dialog), 300, 200);

    return GTK_WIDGET(dialog);
}
