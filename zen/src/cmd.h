/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CMD_H
#define CMD_H
#include <glib.h>
G_BEGIN_DECLS

void cmd_about_dlg(void);
void cmd_close(void);
void cmd_close_all(void);
void cmd_copy(void);
void cmd_cut(void);
void cmd_find_dlg(void);
void cmd_find_next(void);
void cmd_find_next_in_files(void);
void cmd_find_previous(void);
void cmd_find_previous_in_files(void);
void cmd_goto_line_dlg(void);
void cmd_join_lines(void);
void cmd_delete_lines(void);
void cmd_man_dlg(void);
void cmd_make_dlg(void);
void cmd_find_in_files_dlg(void);
void cmd_maximize_view(void);
void cmd_new(void);
void cmd_next_error(void);
void cmd_find_next_in_files(void);
void cmd_open_dlg(void);
void cmd_paste(void);
void cmd_preferences_dlg(void);
void cmd_previous_error(void);
gboolean cmd_quit(void);
void cmd_redo(void);
void cmd_replace_dlg(void);
void cmd_replace_next(void);
void cmd_revert(void);
void cmd_save(void);
void cmd_save_all(void);
void cmd_save_as_dlg(void);
void cmd_select_all(void);
void cmd_split_line(void);
void cmd_split_view(void);
void cmd_undo(void);
void cmd_save_all_and_quit(void);

extern gboolean is_exiting;

G_END_DECLS
#endif
