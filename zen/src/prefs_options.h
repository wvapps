/*
 * Authors : Peter Zion <pzion@nit.ca>
 *
 * Copyright 2004, Net Integration Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef PREFS_OPTIONS_H
#define PREFS_OPTIONS_H
#include <glib.h>
G_BEGIN_DECLS

#include <gtk/gtk.h>

#define TYPE_PREFS_OPTIONS \
    	(prefs_options_get_type ())
#define PREFS_OPTIONS(obj) \
    	(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
    	    	TYPE_PREFS_OPTIONS, PrefsOptions))
#define PREFS_OPTIONS_CLASS(klass) \
    	(G_TYPE_CHECK_CLASS_CAST ((klass), \
    	    	TYPE_PREFS_OPTIONS, \
    	    	PrefsOptionsClass))
#define IS_PREFS_OPTIONS(obj) \
    	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_PREFS_OPTIONS))
#define IS_PREFS_OPTIONS_CLASS(klass) \
    	(G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_PREFS_OPTIONS))
#define PREFS_OPTIONS_GET_CLASS(obj) \
    	(G_TYPE_INSTANCE_GET_CLASS ((obj), \
    	    	TYPE_PREFS_OPTIONS, \
    	    	PrefsOptionsClass))

typedef struct _PrefsOptions        PrefsOptions;
typedef struct _PrefsOptionsClass   PrefsOptionsClass;

struct _PrefsOptionsClass
{
    GtkTableClass parent_class;
    
    void (*changed)(PrefsOptions *options);
    void (*apply)(PrefsOptions *self);
};

struct _PrefsOptions
{
    GtkTable parent;
};

GType prefs_options_get_type(void);

void prefs_options_apply(PrefsOptions *options);

/* these should be "protected" */
void prefs_options_changed(PrefsOptions *self);
void prefs_options_add_row(PrefsOptions *self,
    	const gchar *desc, GtkWidget *widget, int *row);

G_END_DECLS
#endif
