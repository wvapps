
// Compose the reply.
function doReplyToList(event)
{
    dump("Replying to list\n");

    if (!currentHeaderData 
            || !currentHeaderData["list-post"] 
            || !currentHeaderData["list-post"].headerValue)
        return;

    var loadedFolder = GetLoadedMsgFolder();
    var messageArray = GetSelectedMessages();
    ComposeMessage(msgComposeType.ReplyToList, 
        (event && event.shiftKey) ? 
                msgComposeFormat.OppositeOfDefault : msgComposeFormat.Default,
        loadedFolder, messageArray);
}

// Controller for the Reply To List command.  Supports enabling and disabling
// the command, as well as passing along the request to actually do the reply.
var ReplyToListController = {
    supportsCommand : function (cmd) { return (cmd == "cmd_replyToList"); },
    isCommandEnabled : function (cmd) { 

        if (cmd == "cmd_replyToList" 
                && !!currentHeaderData 
                && !!currentHeaderData["list-post"] 
                && !!currentHeaderData["list-post"].headerValue) {
            return true;
        } else {
            return false;
        }
    },

    doCommand : function (cmd) { 
        goUpdateCommand(cmd);
        // If the user invoked a key short cut then it is possible that we got
        // here for a command which is really disabled.  Kick out if the
        // command should be disabled.
        if (!this.isCommandEnabled(cmd)) return;

        return doReplyToList();
    },
    
    onEvent : function (cmd) { }
};

function loadController()
{
    top.controllers.appendController(ReplyToListController);
}

function onReplyToListStartHeaders() {
}

function onReplyToListEndHeaders() {
    goUpdateCommand("cmd_replyToList");
}

// Hack: When we display the headers for a new message, we check if
// there's a List-Post header and update the Reply To List button status.
// This only works when the preview pane is visible.  Ideally we'd be able to
// request the List-Post header for an arbitrary message, and update whenever
// the selection changes on the message list regardless of the status of the
// preview pane.
function initReplyToListMsgHdrListener() {
    // Add ourself to the list of message display listeners so we get notified
    // when we are about to display a message.
    var listener = {};
    listener.onStartHeaders = onReplyToListStartHeaders;
    listener.onEndHeaders = onReplyToListEndHeaders;
    gMessageListeners.push(listener);
}

// Make sure the controller is active, and that we get notified when the
// message header display changes.
loadController();
addEventListener('messagepane-loaded', initReplyToListMsgHdrListener, true);

